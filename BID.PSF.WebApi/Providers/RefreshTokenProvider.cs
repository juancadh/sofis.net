﻿using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Helpers;
using BID.PSF.Model.DBModel;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Practices.Unity;
using System;
using System.Threading.Tasks;

namespace BID.PSF.WebApi.Providers
{
    public class RefreshTokenProvider : IAuthenticationTokenProvider
    {
        public void Create(AuthenticationTokenCreateContext context)
        {
            throw new NotImplementedException();
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var container = UnityConfig.GetConfiguredContainer();
            var oAuthLogic = container.Resolve<IOAuthLogic>();

            var clientid = context.Ticket.Properties.Dictionary[AuthorizationServerProvider.ClientId];

            if (string.IsNullOrEmpty(clientid))
            {
                return;
            }

            var refreshTokenId = Guid.NewGuid().ToString("n");

            var refreshTokenLifeTime = context.OwinContext.Get<string>(AuthorizationServerProvider.ClientRefreshTokenLifeTime);

            var token = new RefreshToken
            {
                Id = OAuthHelper.GetHash(refreshTokenId),
                ClientId = clientid,
                Subject = "Refresh Token",
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
            };

            context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

            token.ProtectedTicket = context.SerializeTicket();

            var result = oAuthLogic.AddRefreshToken(token);

            if (result)
                context.SetToken(refreshTokenId);
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            throw new NotImplementedException();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var container = UnityConfig.GetConfiguredContainer();
            var oAuthLogic = container.Resolve<IOAuthLogic>();

            var allowedOrigin = context.OwinContext.Get<string>(AuthorizationServerProvider.ClientAllowedOrigin) ?? "*";

            if (context.OwinContext.Response.Headers["Access-Control-Allow-Origin"] == null)
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var hashedTokenId = OAuthHelper.GetHash(context.Token);

            var refreshToken = oAuthLogic.FindRefreshToken(hashedTokenId);

            if (refreshToken != null)
            {
                context.DeserializeTicket(refreshToken.ProtectedTicket);
                oAuthLogic.RemoveRefreshToken(hashedTokenId);
            }
        }

    }
}