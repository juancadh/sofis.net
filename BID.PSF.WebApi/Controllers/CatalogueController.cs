﻿using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Enums;
using BID.PSF.Core.Resources;
using BID.PSF.Model.ApiModel;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BID.PSF.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/catalogues")]
    public class CatalogueController : ApiController
    {
        private readonly ICatalogueLogic _catalogueLogic;
        private readonly IDataSetsLogic _datasetsLogic;

        public CatalogueController(ICatalogueLogic catalogueLogic, IDataSetsLogic datasetsLogic)
        {
            _catalogueLogic = catalogueLogic;
            _datasetsLogic = datasetsLogic;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult GetCatalogues(int catalogueId)
        {

            var text = Strings.ColName_Data_Year;
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.GetCatalogues(userId);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("structure")]
        public IHttpActionResult GetTableStructure(int catalogueId, int category, int? dataSetId = null)
        {
            var result = new ApiResult();
            var currentUserId = User.Identity.GetUserId();

            var columns = _catalogueLogic.GetTableStructure(currentUserId, catalogueId, category,  dataSetId);

            if (!columns.Any())
            {
                result.StatusCode = HttpStatusCode.NoContent;
            }
            else
            {
                result.Data = columns;
                result.StatusCode = HttpStatusCode.OK;
            }

            return Content(result.StatusCode, result);
        }

        [HttpPost]
        [Route("select/{id}")]
        public async Task<IHttpActionResult> AssignCatalogueToUser(int id)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = await _catalogueLogic.AssignCatalogueToUser(id, userId);

            return Content(response.StatusCode, response);
        }

        [HttpPost]
        [Route("values")]
        public IHttpActionResult PostValues(CatalogueColumnValues values)
        {
            var response = _catalogueLogic.AddColumnValues(values);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("values")]
        public IHttpActionResult GetValues(Enums.ColumnCategory category,int catalogueId, int? dataSetId = null)
        {
            var response = _catalogueLogic.GetValues(User.Identity.GetUserId(), catalogueId, category, dataSetId);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("subcategories")]
        public IHttpActionResult GetSubcategories()
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.GetSubCategories();

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("currency-units")]
        public IHttpActionResult GetCurrencyUnits()
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.GetCurrencyUnits();

            return Content(response.StatusCode, response);
        }

        [HttpPost]
        [Route("dynamic-column")]
        public IHttpActionResult PostDynamicColumn(DynamicColumnDTO dynamicColumn, [FromUri] int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.CreateDynamicColumn(_datasetsLogic.GetUserDataSet(userId, catalogueId).Id, dynamicColumn);

            return Content(response.StatusCode, response);
        }

        [HttpDelete]
        [Route("dynamic-column/{id}")]
        public IHttpActionResult DeleteDynamicColumn(int id, int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.DeleteDynamicColumn(userId, catalogueId, id);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("dataset/years")]
        public IHttpActionResult GetDataSetYears(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.GetDataSetYears(userId, catalogueId);
            return Content(response.StatusCode, response);
        }

        [HttpPost]
        [Route("dataset/years")]
        public IHttpActionResult PostDataSetYears(DataSetYears years, [FromUri] int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.UpdateDataSetYears(userId, catalogueId, years);
            return Content(response.StatusCode, response);
        }

        [HttpPost]
        [Route("setHistoricYear")]
        public IHttpActionResult SetHistoricYear(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.SetHistoricYear(userId, catalogueId);

            return Content(response.StatusCode, response);
        }
    }
}