﻿using System;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Http;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Enums;
using BID.PSF.Core.Helpers.Interfaces;
using BID.PSF.Model.ApiModel;

namespace BID.PSF.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        private readonly IOAuthLogic _oauthLogic;
        private readonly ILogger _logger;

        public UsersController(IOAuthLogic oauthLogic, ILogger logger)
        {
            _oauthLogic = oauthLogic;
            _logger = logger;
        }

        [Route("")]
        [HttpGet]
        public IHttpActionResult GetUserByEmail(string email)
        {
            var response = _oauthLogic.FindUserByEmail(email);
            var result = new ApiResult { Data = response };

            return Content(HttpStatusCode.OK, result);

        }

        [Route("")]
        [HttpPut]
        public IHttpActionResult UpdateUser(UserDTO user)
        {
            var response = _oauthLogic.UpdateUser(user);
            return Content(HttpStatusCode.OK, response);
        }

        [Route("reset-password")]
        [HttpPost]
        public IHttpActionResult ResetPasssword(ResetPasswordDTO resetPassword)
        {
            var response = _oauthLogic.ResetPassword(resetPassword);
            return Content(response, response);
        }

        [Route("forgot-password")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult ForgotPasssword(ForgotPasswordDTO forgotPassword)
        {
            var response = _oauthLogic.ForgotPassword(forgotPassword);
            return Content(response, response);
        }

        [Route("authentication-type")]
        [HttpGet]
        [AllowAnonymous]
        public Enums.AuthenticationType AuthenticationType()
        {
            string authenticationType = ConfigurationManager.AppSettings["AuthenticationType"];
            return (Enums.AuthenticationType)Enum.Parse(typeof(Enums.AuthenticationType), authenticationType);
            
        }
    }
}
