﻿using System.Net;
using System.Web.Http;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Helpers.Interfaces;
using BID.PSF.Model.ApiModel;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace BID.PSF.WebApi.Controllers
{
    // ReSharper disable once InconsistentNaming
    [Authorize]
    [RoutePrefix("api/asd")]
    public class ASDController : ApiController
    {
        private readonly IASDLogic _asdLogic;
        private readonly ILogger _logger;

        public ASDController(IASDLogic asdLogic, ILogger logger)
        {
            _asdLogic = asdLogic;
            _logger = logger;
        }

        // POST api/asd/standard-approach
        [HttpPost]
        [Route("standard-approach")]
        public async Task<IHttpActionResult> GetStandardApproach(StandardApproachDTO data, [FromUri] int catalogueId)
        {
            _logger.LogMessage($"User:{User.Identity.Name} {Request.Method}({Request.RequestUri})");

            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = await _asdLogic.StandardApproach(userId, catalogueId);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("standard-aproximation-data")]
        public IHttpActionResult GetStandardAproximationData(int catalogueId)
        {
            _logger.LogMessage($"User:{User.Identity.Name} {Request.Method}({Request.RequestUri})");

            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _asdLogic.GetStandardAproximationData(userId, catalogueId);

            return Content(response.StatusCode, response);
        }

        // POST api/asd/standard-approach/sensitivity-analysis
        [HttpPost]
        [Route("standard-approach/sensitivity-analysis")]
        public async Task<IHttpActionResult> GetSensitivityAnalysis(StandardApproachDTO data, [FromUri] int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = await _asdLogic.SensitivityAnalysis(userId, catalogueId, data);

            return Content(response.StatusCode, response);
        }

        // POST api/asd/dynamics-of-debt
        [HttpPost]
        [Route("dynamics-of-debt")]
        public IHttpActionResult DynamicsOfDebt(DynamicsOfDebtDTO data, [FromUri] int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _asdLogic.DynamicsOfDebt(userId, catalogueId, data);

            return Content(response.StatusCode, response);
        }

        // GET api/asd/dynamics-of-debt/dynamics-years
        [HttpGet]
        [Route("dynamics-of-debt/dynamics-years")]
        public IHttpActionResult GetDynamicYears(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _asdLogic.GetDynamicYears(userId, catalogueId);

            return Content(response.StatusCode, response);
        }

        // POST api/asd/fan-chart
        [HttpPost]
        [Route("fan-chart")]
        public IHttpActionResult FanChart(FanChartDTO data, [FromUri] int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _asdLogic.FanChart(userId, catalogueId, data);
            return Content(response.StatusCode, response);
        }

        // POST api/asd/fan-chart/probabilistic-assumptions
        [HttpGet]
        [Route("fan-chart/probabilistic-assumptions")]
        public IHttpActionResult GetProbabilisticAssumptions(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _asdLogic.GetProbabilisticAssumptions(userId, catalogueId);
            return Content(response.StatusCode, response);
        }

        // POST api/asd/fan-chart/years
        [HttpGet]
        [Route("fan-chart/years")]
        public IHttpActionResult GetFutureYears(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _asdLogic.GetFanChartYears(userId, catalogueId);
            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("fan-chart/getInitialConditions")]
        public IHttpActionResult GetInitialConditions(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);


            var response = _asdLogic.GetInitialConditions(userId, catalogueId);
            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("debt-dynamics-data")]
        public IHttpActionResult GetDebtDynamicsData(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);


            var response = _asdLogic.GetDebtDynamicsData(userId, catalogueId);
            return Content(response.StatusCode, response);
        }

        [HttpPost]
        [Route("set-fanchartconfig")]
        public IHttpActionResult SetFanChartConfig(string matrix)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);


            var response = _asdLogic.SetFanChartConfig(userId, matrix);
            return Content(response.StatusCode, response);
        }
    }
}
