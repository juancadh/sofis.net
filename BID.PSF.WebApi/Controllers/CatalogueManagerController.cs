﻿using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Enums;
using BID.PSF.Model.ApiModel;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace BID.PSF.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/cataloguemanager")]
    public class CatalogueManagerController : ApiController
    {
        private readonly ICatalogueLogic _catalogueLogic;
        private readonly IDataSetsLogic _datasetsLogic;

        public CatalogueManagerController(ICatalogueLogic catalogueLogic, IDataSetsLogic datasetsLogic)
        {
            _catalogueLogic = catalogueLogic;
            _datasetsLogic = datasetsLogic;
        }

        #region Column Methods
        [HttpPost]
        [Route("createColumn")]
        public IHttpActionResult CreateColumn(CatalogueColumnDTO column)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);
                        var response = _catalogueLogic.CreateCatalogueColumn(column);
            return Content(response.StatusCode, response);
        }

        [HttpPut]
        [Route("updateColumn")]
        public IHttpActionResult UpdateColumn(CatalogueColumnDTO column)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);
            var response = _catalogueLogic.UpdateCatalogueColumn(column);
            return Content(response.StatusCode, response);
        }

        [HttpDelete]
        [Route("deleteColumn")]
        public IHttpActionResult DeleteColumn(int columnId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _catalogueLogic.DeleteCatalogueColumn(columnId);
            return Content(response.StatusCode, response);
        }
        #endregion

        #region Private Methods
        private bool AutorizhedCatalogue(string userId, CreateCatalogueDTO catalogue)
        {
            var user = _catalogueLogic.GetCurrentUserByCatalogue(userId);
            if (user.Roles.Contains(Enums.Roles.Administrator))
            {
                return true;
            }
            else if ((catalogue.Id == 0 || _catalogueLogic.UserIsInDataset(user.Id, catalogue.Id)) && catalogue.IsSubcatalogue)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

    }
}