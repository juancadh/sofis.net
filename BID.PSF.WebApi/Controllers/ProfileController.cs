﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Helpers.Interfaces;
using BID.PSF.Model.ApiModel;
using Microsoft.AspNet.Identity;

namespace BID.PSF.WebApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/profile")]
    public class ProfileController : ApiController
    {
        private readonly IProfileLogic _profileLogic;
        private readonly ILogger _logger;

        public ProfileController(IProfileLogic profileLogic, ILogger logger)
        {
            _profileLogic = profileLogic;
            _logger = logger;
        }

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetCurrentUser(string culture)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = await _profileLogic.GetCurrentUser(userId, culture);

            return Content(response.StatusCode, response);
        }

        [Route("")]
        [HttpPut]
        public async Task<IHttpActionResult> UpdateProfile(UserDTO profile)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            profile.Id = userId;
            var response = await _profileLogic.UpdateUserAsync(profile);
            return Content(HttpStatusCode.OK, response);
        }

        [Route("setlang")]
        [HttpPut]
        public async Task<IHttpActionResult> SetUserLanguage(string lang)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = await _profileLogic.SetUserLanguageAsync(userId, lang);
            return Content(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("lists")]
        public IHttpActionResult GetLists()
        {
            var response = _profileLogic.GetLists();
            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("countries/{id}")]
        public IHttpActionResult GetCountryById(int id)
        {
            var response = _profileLogic.GetCountryById(id);
            return Content(response.StatusCode, response);
        }
    }
}
