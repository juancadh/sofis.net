﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Helpers.Interfaces;
using BID.PSF.DAL.Migrations;
using Microsoft.AspNet.Identity;

namespace BID.PSF.WebApi.Controllers
{
    // ReSharper disable once InconsistentNaming
    [Authorize]
    [RoutePrefix("api/mfmp")]
    public class MFMPController : ApiController
    {
        private readonly IMFMPLogic _mfmpLogic;

        public MFMPController(IMFMPLogic mfmpLogic)
        {
            _mfmpLogic = mfmpLogic;
        }
        [HttpGet]
        [Route("inertial-approximation/{years}")]
        public IHttpActionResult GetInertialApproximation(int years, int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.InertialApproximation(userId, catalogueId, years);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("firstprojectionyear")]
        public IHttpActionResult GetFirstProjectionYear(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.GetFirstProjectionYear(userId, catalogueId);

            return Content(response.StatusCode, response);
        }

        

        [HttpGet]
        [Route("last-five-years-approximation/{years}")]
        public IHttpActionResult LastFiveYearsApproximation(int years, int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.InertialApproximation(userId, catalogueId, years, true);

            return Content(response.StatusCode, response);
        }

        [HttpPost]
        [Route("fixed-parametric-approximation/{years}")]
        public IHttpActionResult FixedParametricApproximation(int years, Dictionary<int, decimal> fixedVariation, int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.InertialApproximation(userId, catalogueId, years);

            return Content(response.StatusCode, response);
        }

        [HttpPost]
        [Route("variable-parametric-approximation/{years}")]
        public IHttpActionResult VariableParametricApproximation(int years, Dictionary<int, Dictionary<int, decimal>> yearVariation, int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.InertialApproximation(userId, catalogueId, years);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("fixed-approximation-structure")]
        public IHttpActionResult FixedApproximationStructure(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.GetFixedApproximationStructure(userId, catalogueId);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("variable-approximation-structure")]
        public IHttpActionResult VariableApproximationStructure(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.GetVariableApproximationStructure(userId, catalogueId);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("rules")]
        public IHttpActionResult RulesStructure(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.GetRulesStructure(userId, catalogueId);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("charts")]
        public IHttpActionResult RulesCharts(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.GetRulesCharts(userId, catalogueId);

            return Content(response.StatusCode, response);
        }

        [HttpPost]
        [Route("limits")]
        public IHttpActionResult SaveLimits(Model.DBModel.UserRule userRule, int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.SaveLimits(userId, catalogueId, userRule);

            return Content(response.StatusCode, response);
        }

        [HttpGet]
        [Route("limits")]
        public IHttpActionResult GetProjectionYear(int catalogueId)
        {
            var userId = User.Identity.GetUserId();
            if (userId == null)
                return Content(HttpStatusCode.InternalServerError, string.Empty);

            var response = _mfmpLogic.GetProjectionYear(userId, catalogueId);

            return Content(response.StatusCode, response);
        }
    }
}