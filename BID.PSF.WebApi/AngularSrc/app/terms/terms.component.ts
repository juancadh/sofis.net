﻿import { Component, OnInit } from '@angular/core';
import { SessionService } from '../api-helper/session.service';

@Component({
    selector: 'app-terms',
    templateUrl: './terms.component.html',
    styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {
    constructor(private SessionService: SessionService) { }

    username: string;

    ngOnInit(): void {
       this.username = this.SessionService.getCurrentUser();
    }
}
