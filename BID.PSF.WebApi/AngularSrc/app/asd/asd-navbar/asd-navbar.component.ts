﻿// ReSharper disable InconsistentNaming
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'asd-navbar',
    templateUrl: './asd-navbar.component.html',
    styleUrls: ['./asd-navbar.component.css']
})

export class ASDNavbarComponent {
    @Input() title;
}