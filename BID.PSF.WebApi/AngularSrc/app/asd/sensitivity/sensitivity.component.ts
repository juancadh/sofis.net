import { SessionService } from './../../api-helper/session.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sensitivity',
  templateUrl: './sensitivity.component.html',
  styleUrls: ['./sensitivity.component.css']
})
export class SensitivityComponent implements OnInit {

  @Input() proba: number;
  @Input() firstYear: number;
  @Input() umbrales: number[];
  @Input() futureYears: number[];
  @Input() series: number[];

  @Output() x: EventEmitter<number> = new EventEmitter<number>();

  umbral: number = this.getDefaultUmbral() || 0;
  seriesReady: boolean = false;
  /**
   * Defines the table values for each future year
   * @type {number: number[]}
  */

  constructor(private translateService: TranslateService, private SessionService: SessionService) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes['series']) {
      this.series = this.setSeries(this.series);
    }
  }

  ngOnInit() {
    setTimeout(() => {
      this.series = this.setSeries(this.series);
      this.changeUmbral();
    }, 100);
  }

  private getDefaultUmbral(): number {
    let config = JSON.parse(this.SessionService.getFanchartConfig());
    return parseInt(config.OtherX);
  }

  changeUmbral() {
    let config = JSON.parse(this.SessionService.getFanchartConfig());
    config.OtherX = this.umbral;
    this.SessionService.setFanchartConfig(JSON.stringify(config));
    this.x.emit(this.umbral);
  }

  private setSeries(data): any[] {
    this.seriesReady = false;
    var obj: number[] = [];
    if (this.futureYears && data) {
      this.futureYears.forEach((n, i) => {
        obj[n] = data[i];
      });

      this.seriesReady = true;
      return obj;
    }
  }
}

