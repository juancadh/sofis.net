﻿import { PercentagePipe } from './../api-helper/pipes/percentage.pipe';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HotTableModule } from 'angular-handsontable';
import { SharedModule } from './../Components/Shared/shared.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';


import { ASDNavbarComponent } from './asd-navbar/asd-navbar.component';

import { StandardApproachComponent } from './standard-approach/standard-approach.component';
import { DynamicsOfDebtComponent } from './dynamics-of-debt/dynamics-of-debt.component';
import { FanChartAnalysisComponent } from './fan-chart-analysis/fan-chart-analysis.component';

import { TopNavbarASDComponent } from './top-navbar-asd/top-navbar-asd.component';
import { WhatsThisComponent } from './whats-this/whats-this.component';

import { ImageCropperModule } from 'ngx-image-cropper';
import { SensitivityComponent } from './sensitivity/sensitivity.component';


@NgModule({
    imports: [BrowserModule, FormsModule, RouterModule,NgxSmartModalModule, ImageCropperModule, HotTableModule, SharedModule],
    declarations: [StandardApproachComponent, DynamicsOfDebtComponent, FanChartAnalysisComponent, TopNavbarASDComponent, ASDNavbarComponent, WhatsThisComponent, SensitivityComponent],
    providers: [PercentagePipe]
})

export class ASDModule { }