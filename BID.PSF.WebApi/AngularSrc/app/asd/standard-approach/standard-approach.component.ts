﻿import { Alert } from './../../api-helper/Interfaces/Alert';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { HotRegisterer } from 'angular-handsontable';
import { ApiEndpoints } from '../../api-helper/api-endpoints';
import { ApiService } from '../../api-helper/api.service';

import { Utils as ut } from '../../data-management/utils/utils';
import { SofisStandardAproximationInput } from '../../api-helper/models/sofis-standard-aproximation-input';
import { SofisApiService } from '../../api-helper/sofis-api/sofis-api.service';

@Component({
    selector: 'app-standard-approach',
    templateUrl: './standard-approach.component.html',
    styleUrls: ['./standard-approach.component.css']
})
export class StandardApproachComponent implements OnInit {

    public title: string;
    public title_required: string;
    public title_superavit: string;
    public formule_help = '';
    public noResultMessage: string[];
    public gErrorMessage: boolean = false;
    public noData = false;
    public assumptions = false;
    public sensitivity = false;
    public isLoading = false;
    public error = false;
    public ASDAlert: Alert;
    /* Supuestos */
    public debtOverIncome: number;
    public realInterestRate: number;
    public inflationRate: number;
    public growthRate: number;
    public estimatedSurplus: number;

    public rateInflation: number;
    /* Análisis de Sensibilidad */
    public showSurplusMatrix = true;
    public showAdjustmentMatrix = false;

    public growthValues: number[];
    public interestValues: number[];
    public surplusMatrix: number[][];
    public adjustmentMatrix: number[][];

    public incomeDeviationValues = [1, 2, 3];
    public rateDeviationValues = [1, 2, 3];

    public incomeDeviation: number;
    public rateDeviation: number;
    public standardIncomeDeviation: number;
    public standardRateDeviation: number;

    public inputData: any;

    showBasePoints = false;
    showDeviation = true;

    standardValues = {};
    downloading = false;
    topNav = false;

    constructor(private readonly apiService: ApiService, private sofisApiService: SofisApiService, private translateService: TranslateService) {
        this.rateInflation = 0;

    }

    ngOnInit(): void {
        this.isLoading = true;
        this.getInitialStandarAproximationData();
        this.translateService.get(['ASD.STANDARD_APROXIMATION', "ASD.STANDARD_APROX_SUPERAVIT_AJUST", "ASD.STANDARD_APROX_REQUIRED_AJUST"]).subscribe(r => {
            this.title = r["ASD.STANDARD_APROXIMATION"];
            this.title_required = r["ASD.STANDARD_APROX_REQUIRED_AJUST"];
            this.title_superavit = r["ASD.STANDARD_APROX_SUPERAVIT_AJUST"];
        });
        this.translateService.get('TEXTS.ASD_STANDARD_APROX_HELP_2', { src: '/AngularSrc/assets/images/formula_asd_standard.png', w: '40%', h: 'auto' }).subscribe(t => this.formule_help = t);
        if (Array.from(document.getElementsByClassName('active'))[1])
            Array.from(document.getElementsByClassName('active'))[1].classList.remove('active');
        document.getElementById('standard').classList.add('active');


        this.incomeDeviation = this.incomeDeviationValues[2];
        this.rateDeviation = this.rateDeviationValues[2];
        this.standardIncomeDeviation = 0;
        this.standardRateDeviation = 0;
    }


    loadingHandler(event) {
        this.topNav = !event;
        this.isLoading = event;
        this.noData = false;
        this.error = false;
        this.ngOnInit();
    }


    getSensitivityAnalysis(firstCall): void {
        this.inputData.fdesv_g = this.standardIncomeDeviation == null ? 0 : this.standardIncomeDeviation;;
        this.inputData.fdesv_r = this.standardRateDeviation == null ? 0 : this.standardRateDeviation;

        if (!firstCall) {
            this.inputData.r = +this.realInterestRate;
            this.inputData.g = +this.growthRate;
            this.inputData.d = +this.debtOverIncome;
        }

        this.sofisApiService.getStandardAproximationResults(this.inputData).subscribe((result: any) => {
            this.sensitivity = true;

            this.growthValues = result.g_header
            this.interestValues = result.r_header;

            this.surplusMatrix = result.R
            this.adjustmentMatrix = result.R_ajust

            this.isLoading = false;
        }, (error) => {
            console.error(error);
            this.error = true;
            this.isLoading = false;
        });
    }

    changeTable(): void {
        if (this.showAdjustmentMatrix) {
            this.showAdjustmentMatrix = false;
            this.showSurplusMatrix = true;
        } else {
            this.showAdjustmentMatrix = true;
            this.showSurplusMatrix = false;
        }
    }

    exportTable(tableId) {
        this.downloading = true;
        var tab_text = "<meta charset='utf-8'>";

        if (tableId == 'sensibilidad-todas') {
            let sensibilidad_superavit_text = this.getTableAsHtmlString('sensibilidad-superavit');
            let sensibilidad_ajuste_text = this.getTableAsHtmlString('sensibilidad-ajuste');

            tab_text += sensibilidad_superavit_text + sensibilidad_ajuste_text;
        }
        else
            tab_text += this.getTableAsHtmlString(tableId);

        // Internet Explorer
        if (navigator.userAgent.indexOf("Trident") !== -1) {
            let blob = new Blob([tab_text], { type: 'text/html' });
            window.navigator.msSaveBlob(blob, tableId + ".xls");
        }
        else {
            var a = document.createElement('a');
            a.setAttribute("href", 'data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
            a.setAttribute("download", tableId + ".xls");
            a.setAttribute("type", "hidden");
            document.body.appendChild(a);
            a.click();
            a.remove();
        }

        this.downloading = false;
    }

    getTableAsHtmlString(tableId): string {

        let table = document.getElementById(tableId) as HTMLTableElement;
        let title: string;

        switch (tableId) {
            case 'sensibilidad-superavit':
                title = this.title_superavit;
                break;
            case 'sensibilidad-ajuste':
                title = this.title_required;
                break;
        }

        var tab_text = "<h1>" + title + "</h1><table border='2px'><tr>";
        var style;
        for (var j = 0; j < table.rows.length; j++) {
            style = table.rows[j].className.split(" ");
            if (style.length < 2)
                tab_text = tab_text + table.rows[j].innerHTML + "</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<a[^>]*>|<\/a>/g, "");
        tab_text = tab_text.replace(/<img[^>]*>/gi, "");
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, "");


        return tab_text;
    }

    updateInput($event) {
        $event.preventDefault();
        let value = parseFloat($event.target.value);

        if (isNaN(value))
            value = 0;

        $event.target.value = value.toFixed(2);
    }

    getInitialStandarAproximationData() {
        this.apiService.get(ApiEndpoints.asd_getStandardAproximationData, null).then((result) => {
            this.inputData = this.getStandardAproximationInputData(result.data);
            this.sofisApiService.getStandardAproximationInputs(this.inputData).subscribe((result: any) => {
                this.assumptions = true;
                this.inputData.g = result.inputs.g; //Este valor viene de Python
                this.growthRate = +this.inputData.g.toFixed(2); //Este valor viene de Python

                this.inputData.r = result.inputs.r;
                this.realInterestRate = +this.inputData.r.toFixed(2); //

                this.inputData.gstd = result.inputs.gstd;
                this.inputData.rstd = result.inputs.rstd;
                this.getSensitivityAnalysis(true);
                this.isLoading = false;
            }, (error) => {
                //python
                this.gErrorMessage = true;
                this.isLoading = false;
                this.error = true;
                this.assumptions = false;
            });
        }, (error) => {
            //sofis
            if (error.result === 'MissingData') {
                this.noData = true;
                this.noResultMessage = error.message.split(',').map(s => s.trim());
                this.isLoading = false;
                this.assumptions = false;
            }
            else if (error.result === "NotData") {
                this.noData = true;
                this.noResultMessage = [];
                this.isLoading = false;
                this.assumptions = false;
            }
            else {
                this.gErrorMessage = true;
                this.isLoading = false;
                this.error = true;
                this.assumptions = false;
            }
            console.error(error);
        });
    }

    getStandardAproximationInputData(result): SofisStandardAproximationInput {
        if (result.Catalog == 1) {
            var ret: SofisStandardAproximationInput = {
                catalog: 'M',
                tidt: result.Debt.LocalDebtInterestRate,
                ild: result.IncomeExpense.AvailableIncome,
                dpt: result.IncomeExpense.PublicDebtT,
                T: result.Periods,
                fdesv_g: this.standardIncomeDeviation,
                fdesv_r: this.standardRateDeviation,
                ting: result.Ting,
                teg: result.Teg,
                g: null,
                r: null,
                gstd: null,
                rstd: null,
                ditf: null,
                dtiv: null,
                d: 0,
                pint: 0
            }

            this.debtOverIncome = +ret.d.toFixed(2);

            return ret;
        } else {
            var ret: SofisStandardAproximationInput = {
                catalog: 'M',
                tidt: result.Debt.LocalInterestRate,
                ild: result.IncomeExpense.AvailableIncome,
                dpt: result.IncomeExpense.PublicDebtT,
                T: result.Periods,
                fdesv_g: this.standardIncomeDeviation,
                fdesv_r: this.standardRateDeviation,
                ting: result.Ting,
                teg: result.Teg,
                g: null,
                r: null,
                gstd: null,
                rstd: null,
                ditf: null,
                dtiv: null,
                d: 0,
                pint: 0
            }

            this.debtOverIncome = +ret.d.toFixed(2);

            return ret;
        }
    }

    isSimpleCellVisible(i, selectedValue) {
        return i >= (3 - (+selectedValue)) && i <= (3 + (+selectedValue))
    }

    isCellVisible(i, selectedValueI, j, selectedValueJ) {
        return this.isSimpleCellVisible(i, selectedValueI) && this.isSimpleCellVisible(j, selectedValueJ);
    }
}
