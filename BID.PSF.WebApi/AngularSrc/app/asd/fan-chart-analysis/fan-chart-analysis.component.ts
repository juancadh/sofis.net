﻿import { SessionService } from './../../api-helper/session.service';
import { MatrixConfig } from './../../api-helper/models/matrixconfig';
import { Alert } from './../../api-helper/Interfaces/Alert';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { ApiEndpoints } from '../../api-helper/api-endpoints';
import { ApiService } from '../../api-helper/api.service';
import { saveAs } from 'file-saver';
import * as $ from 'jquery';
import * as html2canvas from 'html2canvas';
import { SofisApiService } from '../../api-helper/sofis-api/sofis-api.service';
import { SofisTotalDebt, VarMatrix, VarVector } from '../../api-helper/models/sofis-total-debt';
import { SofisTotalDebtResult } from '../../api-helper/models/sofis-total-debt-result';

declare var CanvasJS: any;
const SOFIS_TOTAL_DEBT_CONFIG = {
    DATAPOINTS: 1000, // CONFIGURATION TO SPECIFY THE AMOUNT OF SIMULATIONS WILL HAVE TO DO PYTHON, FOR SOFIS IS A FIX VALUE
    T: 5, // NUMBER OF PERIODS THAT WILL PREDICTION, FOR SOFIS IS A FIX VALUE
}

@Component({
    selector: 'app-fan-chart-analysis-approximation',
    templateUrl: './fan-chart-analysis.component.html',
    styleUrls: ['./fan-chart-analysis.component.css'],
})
export class FanChartAnalysisComponent implements OnInit {
    title: string;
    constructor(private apiService: ApiService, private sofisApiService: SofisApiService, private translateService: TranslateService, private sessionService: SessionService) {
        translateService.get('ASD.PROB_ANALYSIS').subscribe(r => this.title = r);
    }

    /*ng2 charts*/
    _firstCopy = false;

    public _labelMFL: Array<any> = [];
    public ASDAlert: Alert;
    public chartLegends: boolean = false;

    varModel = "var";
    externalModel = "external";
    externalWithCorrelatedErrorsModel = "externalWithCorrelatedErrors";
    weightedModel = "weighted";

    // labels
    public _lineChartLabels: Array<any>;
    _lineChartColors: Array<any> = [{
        backgroundColor: 'red',
        borderColor: 'red',
        pointBackgroundColor: 'red',
        pointBorderColor: 'red',
        pointHoverBackgroundColor: 'red',
        pointHoverBorderColor: 'red'
    }];

    public _ChartType = 'line';
    simulationTitle: string;

    public chartClicked(e: any): void {
    }

    public chartHovered(e: any): void {
    }

    deleteChart(chartId) {
        let chart = document.getElementById(chartId);
        chart.style.display = 'none';
    }

    /*ng2 charts*/
    topNav = false;

    // SUPUESTOS PROBABILÍSTICOS
    crecimientoIngresoStd: number;
    crecimientoIngresoSerie: number;

    tasaLocalStd: number;
    tasaLocalSerie: number;

    tasaCambioStd: number;
    tasaCambioSerie: string;

    superavitStd: number;
    superavitSerie: number;

    inflacionStd: number;
    inflacionSerie: number;

    tasaExtranjeraStd: number;
    tasaExtranjeraSerie: number;

    alphas = [1, 2, 3];
    alpha: number = 0.1;
    otherX: number; //initial umbral
    otherD0: number;
    beta = 0.1;

    probabilidad = 0;
    umbrales = [];

    futureYears: number[];
    historicYears: number[];

    firstYear: number;

    varCoVarMatrix = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
    ];

    varInterceptLevels = [0, 0, 0, 0, 0, 0];

    varCoeficientsMatrix = [
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0],
    ];

    // METODOLOGÍA
    methodology: string;
    showShocks = false;
    isLoading = false;
    error = false;
    noData: boolean;
    public noResultMessage: string[];
    public gErrorMessage: boolean = false;
    probabilidades = [];

    /********************* FAN CHART  ****************************/
    public historicFCLine: number[] = [0, 0, 0, 0, 0];
    public simulationReady = false;
    public chartReady = false;
    public series = [];
    public yaxismarginrate = 0.8;
    public lineChartLegend = true;
    public lineChartType = 'line';
    public lineChartData: Array<any>;
    public lineChartLabels: Array<any>;
    public lineChartOptions: any = {
        maintainAspectRatio: false,
        scales: {
            xAxes: [
                {
                    offset: true,
                    gridLines: {
                        drawBorder: false,
                        display: false
                    }
                }],
            yAxes: [
                {
                    id: 'y-axes-0',
                    gridLines: {
                        drawBorder: false,
                        color: 'rgb(181, 181, 181)',
                        borderDash: [8, 4],
                        display: true
                    },
                    ticks: {
                        padding: 20,
                        callback: (label, index, labels) => {
                            var n = label.toString(), p = n.indexOf('.');
                            return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, ($0, i) => {
                                let result = p < 0 || i < p ? ($0 + ',') : $0
                                return result;
                            });
                        }
                    },
                    scaleLabel: {
                        display: false,
                        labelString: 'mil = 1000'
                    }
                }]
        },
        annotation: {
            annotations: [
            ],
        },
        elements: {
            point:
            {
                radius: 1,
                hitRadius: 3,
                hoverRadius: 5,
                hoverBorderWidth: 2
            }
        },
        legend: {
            display: false
        },
        responsive: true,
    };


    public lineChartColors: Array<any> = [
        {
            backgroundColor: 'rgba(204,235,254,1)',
            borderColor: 'rgba(6,156,247,0.2)',
            fill: "+1"
        },
        {
            backgroundColor: 'rgba(129,205,250,1)',
            borderColor: 'rgba(6,156,247,0.2)',
            fill: "+1"
        },
        {
            backgroundColor: 'rgba(104,180,225,1)',
            borderColor: 'rgba(6,156,247,0.2)',
            fill: "+1"
        },
        {
            backgroundColor: 'rgba(80,150,200,1)',
            borderColor: 'rgba(6,156,247,0.2)',
            fill: 4
        },
        {
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: 'rgba(6,156,247, 1)',
            fill: false
        },
        {
            backgroundColor: 'rgba(80,150,200,1)',
            borderColor: 'rgba(6,156,247,0.2)',
            fill: 4
        },
        {
            backgroundColor: 'rgba(104,180,225,1)',
            borderColor: 'rgba(6,156,247,0.2)',
            fill: "-1"
        },
        {
            backgroundColor: 'rgba(129,205,250,1)',
            borderColor: 'rgba(6,156,247,0.2)',
            fill: "-1"
        },
        {
            backgroundColor: 'rgba(204,235,254,1)',
            borderColor: 'rgba(6,156,247,0.2)',
            fill: "-1"
        },
    ];

    ngOnInit(): void {

        let config = JSON.parse(this.sessionService.getFanchartConfig());
        if (config == null)
            this.setMatrixConfig();

        if (Array.from(document.getElementsByClassName('active'))[1])
            Array.from(document.getElementsByClassName('active'))[1].classList.remove('active');

        document.getElementById('fanChart').classList.add('active');

        this.noData = false;
        this.methodology = this.varModel;
        this.alpha = this.alphas[0];

        if (this.topNav) {
            this.otherX = config != null && !isNaN(Number(config.otherX)) ? Number(config.otherX) : 0;
            this.umbrales = this.buildUmbrals(this.otherX);

            this.getSimulationTitle(this.methodology);
            this.getProbabilisticAssumptions();
            this.getFutureYears();
            this.loadMatrixConfig();
            this.simulate();
        }

    }

    getSimulationTitle(methodology: string) {
        switch (methodology) {
            case this.varModel:
                this.translateService.get('GRAPHS.PROB_SIMULATION_MODEL_VAR_APROX').subscribe(t => this.simulationTitle = t);
                break;
            case this.weightedModel:
                this.translateService.get('GRAPHS.ASD_WEIGHTED_PROJECTION').subscribe(t => this.simulationTitle = t);
                break;
            case this.externalModel:
                this.translateService.get('GRAPHS.ASD_EXT_PROJECTIONS').subscribe(t => this.simulationTitle = t);
                break;
            case this.externalWithCorrelatedErrorsModel:
                this.translateService.get('GRAPHS.ASD_EXT_PROJECTIONS_WITH_ERRORS').subscribe(t => this.simulationTitle = t);
                break;
            default:
                this.simulationTitle = null;
                break
        }
    }

    loadingHandler(event) {
        this.topNav = !event;
        this.noData = false;
        this.gErrorMessage = false;
        this.isLoading = event;
        this.error = false;
        this.ngOnInit();
    }

    customTrackBy(index: number, obj: any): any {
        return index;
    }


    getProbabilisticAssumptions() {
        this.isLoading = true;

        this.apiService.get(ApiEndpoints.fanChart_getProbabilisticAssumptions, null).then(
            result => {
                this.crecimientoIngresoStd = result.data.CrecimientoIngresoStd;
                this.crecimientoIngresoSerie = result.data.CrecimientoIngresoSerie;

                this.tasaLocalStd = result.data.TasaLocalStd;
                this.tasaLocalSerie = result.data.TasaLocalSerie;

                this.tasaCambioStd = result.data.TasaCambioStd;
                this.tasaCambioSerie = result.data.TasaCambioSerie;

                this.superavitStd = result.data.SuperavitStd;
                this.superavitSerie = result.data.SuperavitSerie;

                this.inflacionStd = result.data.InflacionStd;
                this.inflacionSerie = result.data.InflacionSerie;

                this.tasaExtranjeraStd = result.data.TasaExtranjeraStd;
                this.tasaExtranjeraSerie = result.data.TasaExtranjeraSerie;
                this.isLoading = false;
            },
            error => {
                this.isLoading = false;
                this.error = true;
            });
    }


    getFutureYears() {
        this.apiService.get(ApiEndpoints.fanChart_getFutureYears, null).then(
            result => {
                this.futureYears = result.data.FutureYears.slice(0, 5);
                this.showShocks = true;

                this.firstYear = this.futureYears[0] - 2;
                this.historicYears = this.futureYears.map((y) => (y - 5));
                this._lineChartLabels = [...this.historicYears, ...this.futureYears];
            },
            error => {
                this.isLoading = false;
                this.error = true;
            });
    }

    getAnalysis() {
        this.isLoading = true;
        this.chartReady = false;
        this.simulationReady = false;

        this._labelMFL = [];

        this.buildSofisTotalDebt().then((postData) => {
            this.sofisApiService.getTotalDebt(postData).subscribe((result: SofisTotalDebtResult) => {
                this.series = [result.totaldebt.ffitsup, result.totaldebt.ffit, result.totaldebt.ffitinf];
                this.probabilidad = result.totaldebt.PROBA;
                this.probabilidades = result.totaldebt.sensitivity_analysis;

                // Series superiores
                this._labelMFL.push(...result.totaldebt.series_perentiles.map(i => {
                    return {
                        data: [null, null, null, null, null, ...i.sup]
                    };
                }));

                // Linea media
                this._labelMFL.push({ data: [...this.historicFCLine, ...result.totaldebt.ffit], borderWidth: 2 });

                // Series inferiores
                this._labelMFL.push(...result.totaldebt.series_perentiles.map(i => {
                    return {
                        data: [null, null, null, null, null, ...i.inf]
                    };
                }).reverse());

                this.simulationReady = true;
                this.chartReady = true;
                this.isLoading = false;
            },
                (error) => {
                    console.error("Python->", error);
                    this.chartReady = true;
                    this.simulationReady = true;
                    this.isLoading = false;
                }
            );
        }, (error) => {
            console.error(error);
            this.chartReady = true;
            this.simulationReady = true;
            this.isLoading = false;
        });
    }

    UpdateSimulation(methodology) {
        this.simulationReady = false;
        this.getSimulationTitle(methodology);
        this._labelMFL.splice(0, this._labelMFL.length);
        this._labelMFL.splice(0, this.series.length);
        this.methodology = methodology;
        this.simulate();
    }

    cleanCells() {
        this.varCoVarMatrix = [
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
        ];

        this.varInterceptLevels = [0, 0, 0, 0, 0, 0];

        this.varCoeficientsMatrix = [
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0],
        ];

        this.alpha = 0;
        this.beta = 0;
    }

    private buildUmbrals(initial: number): number[] {
        let result: number[] = [];
        result[0] = initial;

        for (var i = 1; i < 4; i++)
            result[i] = parseFloat((result[i - 1] + 5).toFixed(2));

        return result;
    }

    changeUmbral(newUmbral) {
        this.otherX = newUmbral;
        this.umbrales = this.buildUmbrals(Number(this.otherX));

        /*UPDATING PROBABILITIES*/
        this.buildSofisTotalDebt().then((postData) => {
            this.sofisApiService.getTotalDebt(postData).subscribe((result: SofisTotalDebtResult) => {
                this.probabilidad = result.totaldebt.PROBA;
                this.probabilidades = result.totaldebt.sensitivity_analysis;
            });
        });
    }

    randomize() {
        let _lineChartData: Array<any> = new Array(this.lineChartData.length);
        for (let i = 0; i < this.lineChartData.length; i++) {
            _lineChartData[i] = {
                data: new Array(this.lineChartData[i].data.length),
                label: this.lineChartData[i].label
            };

            for (let j = 0; j < 5; j++) {
                _lineChartData[i].data[j] = this.lineChartData[i].data[j];
            }

            if (i == 0) {
                for (let j = 5; j < this.lineChartData[i].data.length; j++) {
                    _lineChartData[i].data[j] = this.lineChartData[i].data[j];
                }
            } else {
                for (let j = 5; j < this.lineChartData[i].data.length; j++) {
                    _lineChartData[i].data[j] = Math.floor((Math.random() * 10) + 1);
                }
            }
        }
        this.lineChartData = _lineChartData;
    }

    toggleAssumptions() {
        $('#panel').animate({ width: 'toggle' });
    }

    exportChart(chartId) {
        let chart = document.getElementById(chartId);

        html2canvas(chart).then(canvas => {
            canvas.toBlob(blob => {
              saveAs(blob, `${chartId}.png`);
            });
          });
    }

    simulate() {
        // Set new MatrixValuesHere
        this.setMatrixConfig();
        this.getAnalysis();
    }

    loadMatrixConfig() {
        const fnch = JSON.parse(this.sessionService.getFanchartConfig());
        let matrix: MatrixConfig = fnch || null;

        if (fnch && Object.keys(fnch).length > 0) {
            this.varInterceptLevels = matrix.VarinterceptLevels;
            this.varCoVarMatrix = matrix.VarCoVarMatrix;
            this.varCoeficientsMatrix = matrix.VarCoeficientsMatrix;
            this.alpha = matrix.Alpha;
            this.otherX = matrix.OtherX;
            this.otherD0 = matrix.OtherD0;
            this.beta = matrix.Beta;
        }

        return matrix;
    }

    setMatrixConfig() {
        const matrix: MatrixConfig = {
            VarinterceptLevels: this.varInterceptLevels,
            VarCoVarMatrix: this.varCoVarMatrix,
            VarCoeficientsMatrix: this.varCoeficientsMatrix,
            Alpha: this.alpha,
            Beta: this.beta,
            OtherD0: this.otherD0,
            OtherX: this.otherX
        };
        var query = JSON.stringify(matrix);
        this.apiService.post(`${ApiEndpoints.asd_set_matrix_values}?matrix=${query}`, null, null).then((r) => {
            this.sessionService.setFanchartConfig(r.data);
            this.loadMatrixConfig();
        });
    }


    private buildSofisTotalDebt(): Promise<SofisTotalDebt> {
        this.loadMatrixConfig();
        var omegaMatrix: VarMatrix = {
            rtml: this.varCoVarMatrix[0] as [number, number, number, number, number, number],
            rtme: this.varCoVarMatrix[1] as [number, number, number, number, number, number],
            et: this.varCoVarMatrix[2] as [number, number, number, number, number, number],
            gildt: this.varCoVarMatrix[3] as [number, number, number, number, number, number],
            gidet: this.varCoVarMatrix[4] as [number, number, number, number, number, number],
            ggpt: this.varCoVarMatrix[5] as [number, number, number, number, number, number]
        };

        var mu1: VarMatrix = {
            rtml: this.varCoeficientsMatrix[0] as [number, number, number, number, number, number],
            rtme: this.varCoeficientsMatrix[1] as [number, number, number, number, number, number],
            et: this.varCoeficientsMatrix[2] as [number, number, number, number, number, number],
            gildt: this.varCoeficientsMatrix[3] as [number, number, number, number, number, number],
            gidet: this.varCoeficientsMatrix[4] as [number, number, number, number, number, number],
            ggpt: this.varCoeficientsMatrix[5] as [number, number, number, number, number, number],
        };

        var mu0: VarVector = {
            rtml: [this.varInterceptLevels[0]],
            rtme: [this.varInterceptLevels[1]],
            et: [this.varInterceptLevels[2]],
            gildt: [this.varInterceptLevels[3]],
            gidet: [this.varInterceptLevels[4]],
            ggpt: [this.varInterceptLevels[5]]
        };

        return new Promise<SofisTotalDebt>((resolve) => {
            this.apiService.get(ApiEndpoints.fanChart_getInitialConditions, null).then(
                (result) => {
                    this.otherD0 = this.otherD0 ? this.otherD0 : result.data.DebtInitialValue;
                    this.historicFCLine = result.data.FFit;

                    var ret: SofisTotalDebt = {
                        model: this.methodology.toLowerCase(),
                        datapoints: SOFIS_TOTAL_DEBT_CONFIG.DATAPOINTS,
                        T: SOFIS_TOTAL_DEBT_CONFIG.T,
                        D0: this.otherD0,
                        OMT: omegaMatrix,
                        mu_0: mu0,
                        mu_1: mu1,

                        rtml: result.data.Rtml.slice(0, result.data.HistoricColumns),
                        rtme: result.data.Rtme.slice(0, result.data.HistoricColumns),
                        et: result.data.Et.slice(0, result.data.HistoricColumns),
                        gildt: result.data.Gildt.slice(0, result.data.HistoricColumns),
                        gidet: result.data.Gidet.slice(0, result.data.HistoricColumns),
                        ggpt: result.data.Ggpt.slice(0, result.data.HistoricColumns),

                        external_series: {
                            rtml: result.data.Rtml.slice(result.data.HistoricColumns),
                            rtme: result.data.Rtme.slice(result.data.HistoricColumns),
                            et: result.data.Et.slice(result.data.HistoricColumns),
                            gildt: result.data.Gildt.slice(result.data.HistoricColumns),
                            gidet: result.data.Gidet.slice(result.data.HistoricColumns),
                            ggpt: result.data.Ggpt.slice(result.data.HistoricColumns)
                        },

                        X: Number(this.otherX) || 0,
                        alpha: this.alpha,
                        beta: this.beta,
                        IDE0: result.data.IDE0,
                        ILD0: result.data.ILD0,
                        GP0: result.data.Gp0,
                        umbrals_for_sensitivity_analysis: this.umbrales || [0, 0, 0, 0, 0]
                    };
                    resolve(ret);
                },
                (error) => {
                    if (error.result === 'MissingData') {
                        this.noData = true;
                        this.noResultMessage = error.message.split(',').map(s => s.trim());
                    }
                    else if (error.result === "NotData") {
                        this.noData = true;
                        this.noResultMessage = [];
                    }
                    else {
                        this.noData = true;
                        this.gErrorMessage = true;
                    }
                });
        });
    }
}