﻿import { EventEmitter } from '@angular/core';
import { Component, OnInit, Output, Input } from '@angular/core';
import { TopNavLogicService } from '../../api-helper/top-nav-logic.service';
import { Alert } from '../../api-helper/Interfaces/Alert';

@Component({
    selector: 'top-navbar-asd',
    templateUrl: './top-navbar-asd.component.html',
    styleUrls: ['./top-navbar-asd.component.css']
})
export class TopNavbarASDComponent implements OnInit {
    @Input() ExternalAlert: Alert = { show: false, msg: '', type: '' };
    @Output() isLoading = new EventEmitter<boolean>();

    constructor(public topNavLogic: TopNavLogicService) {
        this.topNavLogic.externalAlert = this.ExternalAlert;
        this.isLoading.emit(true);
        this.topNavLogic.isLoadingChange.subscribe(value => this.isLoading.emit(value));
        this.topNavLogic.externalAlertChange.subscribe(value => this.ExternalAlert = value);
    }

    async ngOnInit() {
        await this.topNavLogic.init();
    }
}
