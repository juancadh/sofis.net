﻿import { Headers } from '@angular/http';
import { Component, OnInit, OnChanges, SimpleChanges, ViewChild, Input } from '@angular/core';
import { HotRegisterer } from 'angular-handsontable';
import { ApiEndpoints } from '../../api-helper/api-endpoints';
import { ApiService } from '../../api-helper/api.service';
import { ColumnCategory } from '../../api-helper/models/enums';
import { ShockDTO } from '../../api-helper/models/enums';
import { TranslateService } from '@ngx-translate/core';

import * as html2canvas from 'html2canvas';
import { SofisDebtDynamicsInput, SofisDebtDynamicsShocks } from '../../api-helper/models/sofis-debt-dynamics-input';
import { SofisApiService } from '../../api-helper/sofis-api/sofis-api.service';

import { PercentagePipe } from './../../api-helper/pipes/percentage.pipe';

import { Alert } from './../../api-helper/Interfaces/Alert';
import * as $ from 'jquery';
import 'jqueryui';
import * as XLSX from 'xlsx';
import { ExportType } from './../../api-helper/models/enums';
import { saveAs } from 'file-saver';



@Component({
    selector: 'app-dynamics-of-debt',
    templateUrl: './dynamics-of-debt.component.html',
    styleUrls: ['./dynamics-of-debt.component.css']
})
export class DynamicsOfDebtComponent implements OnInit {
    title: string;
    historics = 3; // Max historics setting
    formule_help = '';

    constructor(private apiService: ApiService, private percentagePipe: PercentagePipe, private sofisApiService: SofisApiService, private translate: TranslateService) {
        this.setDinamycTranslations();
        this.chartType = { caption: this.chartTypes[0].caption, value: 'line' };
    }

    maxShockShowValue = 4;
    shock: string;
    shocks = [];
    pythonReady = false;
    noData = false;
    topNav = false;
    public noResultMessage: string[];
    public gErrorMessage: boolean = false;
    tables = false;
    charts = false;
    isLoading = false;
    bigLoader = true;
    error = false;
    dataReady = false;
    years = [];
    chartsReady = false;
    noShockSelected = false;
    downloading = false;
    public existsDynamicYearError: boolean = false;
    public shockErrMsg: string = '';
    private initialData = {};
    public simulationResults;
    public ASDAlert: Alert;


    selectedShocks: ShockDTO[] = [];

    //chart settings 
    chartTypes = [];
    chartType: any;
    chartLegends: true;
    chartLabels = [];
    currentChartLabels: Array<any>;
    chartOptions: any = {
        maintainAspectRatio: false,
        scales: {
            xAxes: [
                {
                    offset: true,
                    gridLines: {
                        drawBorder: false,
                        display: false
                    }
                }],
            yAxes: [
                {
                    gridLines: {
                        drawBorder: false,
                        color: 'rgb(181, 181, 181)',
                        borderDash: [8, 4],
                        display: true
                    },
                    ticks: {
                        padding: 20,
                        callback: (label, index, labels) => {
                            var n = label.toString(), p = n.indexOf('.');
                            return n.replace(/\d(?=(?:\d{3})+(?:\.|$))/g, ($0, i) => {
                                let result = p < 0 || i < p ? ($0 + ',') : $0
                                return result;
                            });
                        }
                    },
                    scaleLabel: {
                        display: false,
                        labelString: 'mil = 1000'
                    }
                }]
        },
        annotation: {
            annotations: [
            ],
        },
        elements: {
            line: {
                tension: 0
            },
            point: {
                radius: 0
            }
        },
        responsive: true,
    };

    lineChartColors: Array<any> = [
        {
            backgroundColor: 'transparent',
            borderColor: '#0092FC'

        },
        {
            backgroundColor: 'transparent',
            borderColor: '#FF455C'
        },
        {
            backgroundColor: 'transparent',
            borderColor: '#37BA9D'
        },
        {
            backgroundColor: 'transparent',
            borderColor: '#004E70'
        },
        {
            backgroundColor: 'transparent',
            borderColor: '#F8D800'
        },
        {
            backgroundColor: 'transparent',
            borderColor: '#00ff00'
        }
    ];
    barChartColors: Array<any> = [
        {
            backgroundColor: '#0092FC'
        },
        {
            backgroundColor: '#FF455C'
        },
        {
            backgroundColor: '#37BA9D'
        },
        {
            borderColor: '#004E70'
        },
        {
            backgroundColor: '#F8D800'
        },
        {
            backgroundColor: '#00ff00'
        }
    ];
    currentChartColors = this.lineChartColors;

    // Balance
    balanceData: Array<any>;
    currentBalanceData = [];

    // Deuda
    debtData: Array<any>;
    currentDebtData = [];

    // DEV STD
    devStdTasa: number;
    devStdIngreso: number;
    devStdExtranjera: number;
    devStdLocal: number;

    // VALORES
    valorTasa: number;
    valorIngreso: number;
    valorExtranjera: number;
    valorLocal: number;

    // Shocks
    tasaCambio = false;
    ingreso = false;
    extranjera = false;
    local = false;

    public shock_ingreso_total = {
        checked: false,
        Ti: null,
        Tf: null,
        shock: 0
    };

    public shock_tipo_cambio = {
        checked: false,
        Ti: null,
        Tf: null,
        shock: 0
    };

    public shock_tasa_int_moneda_local = {
        checked: false,
        Ti: null,
        Tf: null,
        shock: 0
    };

    public shock_tasa_int_moneda_extranjera = {
        checked: false,
        Ti: null,
        Tf: null,
        shock: 0
    };

    ngOnChanges(changes: SimpleChanges) {
        // changes.prop contains the old and the new value...
    }

    ngOnInit(): void {
        this.years = [];
        if (Array.from(document.getElementsByClassName('active'))[1])
            Array.from(document.getElementsByClassName('active'))[1].classList.remove('active');
        document.getElementById('dynamicsOfDebt').classList.add('active');
        this.translate.get('TEXTS.ASD_DYNAMIC_DEBT_HELP_2', { src: '/AngularSrc/assets/images/formula_asd_dynamic.png', w: '60%', h: '60px' }).subscribe(t => this.formule_help = t);


        if (this.topNav) {
            this.getInitialData();
            this.setChartType('line');
            this.tables = true;
            this.charts = true;
        }
    }

    countShockValueLength(value: number) {
        return value.toString().replace(/[-,\.]/g, '').length;
    }

    loadingHandler(event) {
        this.years = [];
        this.topNav = !event;
        this.noData = false;
        this.isLoading = event;
        this.bigLoader = event;
        this.error = false;
        this.ngOnInit();
    }

    setDinamycTranslations() {
        //Titulo
        this.translate.get('ASD.DYNAMIC_OF_DEBT').subscribe(t => this.title = t);
        //Traduzco graphs
        this.translate.get(['GRAPHS.TYPE_LINE', 'GRAPHS.TYPE_BAR']).subscribe((t: any) => {
            this.chartTypes.push({ caption: t['GRAPHS.TYPE_LINE'], value: 'line' });
            this.chartTypes.push({ caption: t['GRAPHS.TYPE_BAR'], value: 'bar' });
        });
        //Traduzco shoks
        this.translate.get(['ASD.SHOKS_TABLE_CHANGE_TYPE', 'ASD.SHOKS_TABLE_TOTAL_INCOME', 'ASD.SHOKS_TABLE_FOREIGN_GROW_RATE', 'ASD.SHOKS_TABLE_LOCAL_GROW_RATE'])
            .subscribe((t: any) => {
                this.shocks[0] = t['ASD.SHOKS_TABLE_CHANGE_TYPE'];
                this.shocks[1] = t['ASD.SHOKS_TABLE_TOTAL_INCOME'];
                this.shocks[2] = t['ASD.SHOKS_TABLE_FOREIGN_GROW_RATE'];
                this.shocks[3] = t['ASD.SHOKS_TABLE_LOCAL_GROW_RATE'];
            });
    }

    filterTables() {
        let chkTasa = document.getElementById("chkTasa") as HTMLInputElement;
        let chkIngreso = document.getElementById("chkTasa") as HTMLInputElement;
        let chkExtranjera = document.getElementById("chkTasa") as HTMLInputElement;
        let chkLocal = document.getElementById("chkTasa") as HTMLInputElement;

        if (chkTasa && chkTasa.checked) this.tasaCambio = true;
        if (chkIngreso && chkIngreso.checked) this.ingreso = true;
        if (chkExtranjera && chkExtranjera.checked) this.extranjera = true;
        if (chkLocal && chkLocal.checked) this.local = true;
    }

    getInitialData() {

        this.apiService.get(ApiEndpoints.asd_debtdynamicsdata, null).then(
            result => {
                this.initialData = result.data;
                this.years = [];
                for (var i = result.data.Tini; i <= result.data.Tproy; i++)
                    this.years.push(i);
                this.filterTables();
                this.getdebtDynamicsResult(result.data, {});
            },
            error => {
                this.noData = true;
                if (error.result === 'MissingData') {
                    this.noResultMessage = error.message.split(',').map(s => s.trim());
                    console.error(this.noResultMessage);
                }
                else if (error.result === "NotData") {
                    this.noData = true;
                    this.noResultMessage = [];
                    this.isLoading = false;
                }
                else {

                    this.gErrorMessage = true;
                }
                this.isLoading = false;
                this.bigLoader = false;
                console.error(error);
            });
    }

    private getdebtDynamicsResult(data: any, shocks: any) {
        this.pythonReady = false;
        this.sofisApiService.getDebtDynamicsResults(this.getDebtDynamicsInputData(data, shocks)).subscribe((result) => {
            this.simulationResults = result;
            this.buildCharts(this.simulationResults);
            this.bigLoader = false;
            this.isLoading = false;
            this.chartsReady = true;
            this.dataReady = true;
            this.chartType.value = this.chartTypes[0].value;
            this.currentChartColors = this.lineChartColors;
            this.pythonReady = true;
            setTimeout(() => {
                $("#sortable").sortable({
                    handle: $(".drag")
                });
                $(".half-box").disableSelection();
            }, 0);
        });
    }

    private buildChartSerie(label, data) {
        var series: any = {};
        series.data = this.getPercentGraphValuesFromPts(data);
        series.label = label;

        return series;
    }

    getPercentGraphValuesFromPts(values: any) {
        values.forEach((e, i) => {
            values[i] = (e * 100).toFixed(1);
        });
        return values;
    }

    private buildAnnotationProjections(xmin: number) {
        this.chartOptions.annotation.annotations.splice(0, this.chartOptions.annotation.annotations.length); // reseting value
        this.chartOptions.annotation.annotations.push(
            {
                type: 'box',
                xScaleID: 'x-axis-0',
                yScaleID: 'x-axis-0',
                xMin: xmin,
                backgroundColor: 'rgba(216, 240, 255, 0.5)',
                borderWidth: 1
            }
        );
    }
    private buildCharts(simulationResults) {

        this.chartLabels = this.years;
        if (this.chartLabels && this.chartLabels.length > 0)
            this.buildAnnotationProjections(this.chartLabels[this.historics]);

        this.currentBalanceData = [];
        this.currentDebtData = [];

        this.translate.get('ASD.BASE_SCENARY').subscribe(async t => {
            await this.currentBalanceData.push(this.buildChartSerie(t, simulationResults.all_shocks.base_balance_primario_ingreso_total));
            await this.currentDebtData.push(this.buildChartSerie(t, simulationResults.all_shocks.base_deuda_total_ingreso_total));
        })

        if (this.shock_tipo_cambio.checked && simulationResults.tipo_cambio) {
            this.translate.get('ASD.SHOKS_TABLE_CHANGE_TYPE').subscribe(async t => {
                await this.currentBalanceData.push(this.buildChartSerie(t, simulationResults.tipo_cambio.balance_primario_ingreso_total));
                await this.currentDebtData.push(this.buildChartSerie(t, simulationResults.tipo_cambio.deuda_total_ingreso_total));
            });
        }

        if (this.shock_ingreso_total.checked && simulationResults.ingreso_total) {
            this.translate.get('ASD.SHOKS_TABLE_TOTAL_INCOME').subscribe(async t => {
                await this.currentBalanceData.push(this.buildChartSerie(t, simulationResults.ingreso_total.balance_primario_ingreso_total));
                await this.currentDebtData.push(this.buildChartSerie(t, simulationResults.ingreso_total.deuda_total_ingreso_total));
            });
        }

        if (this.shock_tasa_int_moneda_extranjera.checked && simulationResults.tasa_int_moneda_extranjera) {
            this.translate.get('ASD.SHOKS_TABLE_FOREIGN_GROW_RATE').subscribe(async t => {
                await this.currentBalanceData.push(this.buildChartSerie(t, simulationResults.tasa_int_moneda_extranjera.balance_primario_ingreso_total));
                await this.currentDebtData.push(this.buildChartSerie(t, simulationResults.simulationResults.tasa_int_moneda_extranjera.deuda_total_ingreso_total));
            }
            );
        }

        if (this.shock_tasa_int_moneda_local.checked && simulationResults.tasa_int_moneda_local) {
            this.translate.get('ASD.SHOKS_TABLE_LOCAL_GROW_RATE').subscribe(t => {
                this.currentBalanceData.push(this.buildChartSerie(t, simulationResults.tasa_int_moneda_local.balance_primario_ingreso_total));
                this.currentDebtData.push(this.buildChartSerie(t, simulationResults.tasa_int_moneda_local.deuda_total_ingreso_total));
            });
        }

        if (this.shock_ingreso_total.checked || this.shock_tasa_int_moneda_extranjera.checked || this.shock_tasa_int_moneda_local.checked || this.shock_tipo_cambio.checked) {
            this.translate.get('ASD.ALL_SHOKS').subscribe(t => {
                this.currentBalanceData.push(this.buildChartSerie(t, simulationResults.all_shocks.balance_primario_ingreso_total));
                this.currentDebtData.push(this.buildChartSerie(t, simulationResults.all_shocks.deuda_total_ingreso_total));
            });
        }
    }

    private getDebtDynamicsInputData(data, shocks): SofisDebtDynamicsInput {
        var ret: SofisDebtDynamicsInput = {
            shocks: shocks,
            dml: data.Dml,
            dme: data.Dme,
            rml: data.Rml,
            rme: data.Rme,
            tcambio: data.Tcambio,
            ingt: data.Ingt,
            teg: data.Teg,
            pint: data.Pint,
            Tini: data.Tini,
            Tproy: data.Tproy
        }

        return ret;
    }

    getDynamicsOfDebt() {
        this.tables = false;
        this.charts = false;
        this.chartsReady = false;
        this.selectedShocks = [];

        let shockRows = document.getElementsByClassName('shockRow');

        for (let i = 0; i < shockRows.length; i++) {
            let shockRow = shockRows[i];
            let checked = (shockRow.querySelector('input[type="checkbox"]') as HTMLInputElement).checked;

            let initialYear = shockRow.querySelector('.initialYear') as HTMLInputElement;
            let finalYear = shockRow.querySelector('.finalYear') as HTMLInputElement;

            // Quito errores si los había.
            initialYear.classList.remove('dynamic-year-error');
            finalYear.classList.remove('dynamic-year-error');

            if (checked) {
                let shockName = shockRow.querySelector('.shock') as HTMLElement;
                let standardDeviation = shockRow.querySelector('.standardDeviation') as HTMLInputElement;
                let shock = new ShockDTO(shockName.innerHTML, parseInt(initialYear.value), parseInt(finalYear.value), parseFloat(standardDeviation.value));
                this.selectedShocks.push(shock);
            }
        }

        // Si no hay shock seleccionado, avisar.
        if (this.selectedShocks.length == 0) {
            this.noShockSelected = true;
            return;
        }

        this.noShockSelected = false;
        let errCount = 0;
        // Si a algún input de los shocks seleccionados le falta ingresar año, avisar.
        for (let i = 0; i < this.selectedShocks.length; i++) {
            let shock = this.selectedShocks[i];

            if (this.invalidInitialYear(shock.InitialYear, shock.FinalYear)) {
                let shockInput = this.findInputElementByShockName(shock.Shock, true);
                shockInput.classList.add('dynamic-year-error');
                errCount++;
            }
            else {
                let shockInput = this.findInputElementByShockName(shock.Shock, true);
                shockInput.classList.remove('dynamic-year-error');
                this.existsDynamicYearError = false;
            }

            if (this.invalidFinalYear(shock.InitialYear, shock.FinalYear)) {
                let shockInput = this.findInputElementByShockName(shock.Shock, false);
                shockInput.classList.add('dynamic-year-error');
                errCount++;
            }
            else {
                let shockInput = this.findInputElementByShockName(shock.Shock, false);
                shockInput.classList.remove('dynamic-year-error');
                this.existsDynamicYearError = false;
            }
        }
        if (errCount > 0) {
            this.existsDynamicYearError = true;
            this.translate.get(['ERRORS.GENERIC', 'ERRORS.INVALID_YEAR']).subscribe((t: any) => {
                this.shockErrMsg = t['ERRORS.GENERIC'] + ': ' + t['ERRORS.INVALID_YEAR'];
            });
        }

        if (this.existsDynamicYearError)
            return;

        this.isLoading = true;

        var shocks: any = {};
        if (this.shock_ingreso_total.checked) {
            shocks.ingreso_total = this.shock_ingreso_total;
        }

        if (this.shock_tasa_int_moneda_extranjera.checked) {
            shocks.tasa_int_moneda_extranjera = this.shock_tasa_int_moneda_extranjera;
        }

        if (this.shock_tasa_int_moneda_local.checked) {
            shocks.tasa_int_moneda_local = this.shock_tasa_int_moneda_local;
        }

        if (this.shock_tipo_cambio.checked) {
            shocks.tipo_cambio = this.shock_tipo_cambio;
        }

        this.getdebtDynamicsResult(this.initialData, shocks);

        this.tables = true;
        this.charts = true;
        this.isLoading = false;
        this.chartsReady = true;
        return;
    }


    setChartType(chart) {
        this.chartsReady = false;
        this.charts = false;
        switch (chart) {
            case this.chartTypes[0].value:
                this.currentChartColors = this.lineChartColors;
                break;
            case this.chartTypes[1].value:
            case this.chartTypes[2].value:
                this.currentChartColors = this.barChartColors;
                break;
        }
    }

    changeChartType(event) {
        this.chartsReady = false;
        this.charts = false;
        const chart = event.target.value;
        switch (chart) {
            case this.chartTypes[0].value:
                this.currentChartColors = this.lineChartColors;
                break;
            case this.chartTypes[1].value:
            case this.chartTypes[2].value:
                this.currentChartColors = this.barChartColors;
                break;
        }

        this.chartsReady = true;
        this.charts = true;
    }

    exportChart(chartId) {
        let chart = document.getElementById(chartId);

        html2canvas(chart).then(canvas => {
            canvas.toBlob(blob => {
              saveAs(blob, `${chartId}.png`);
            });
          });
    }

    deleteChart(chartId) {
        let chart = document.getElementById(chartId);
        chart.style.display = 'none';
    }

    invalidInitialYear(initialYear: number, finalYear: number): boolean {
        if (isNaN(initialYear))
            return true;

        let minYear = this.years[0] + 2;
        let maxYear = this.years[this.years.length - 1];

        if (initialYear >= minYear && initialYear <= maxYear)
            if (isNaN(finalYear))
                return false;
            else if (initialYear > finalYear)
                return true;
            else
                return false;
        else
            return true;
    }

    invalidFinalYear(initialYear: number, finalYear: number): boolean {
        if (isNaN(finalYear))
            return true;

        let minYear = this.years[0] + 2;
        let maxYear = this.years[this.years.length - 1];

        if (finalYear >= minYear && finalYear <= maxYear)
            if (isNaN(initialYear))
                return false;
            else if (finalYear < initialYear)
                return true;
            else
                return false;
        else
            return true;
    }

    findInputElementByShockName(shockName: string, initial: boolean): HTMLElement {
        let input;

        let shockRows = document.getElementsByClassName('shockRow');
        let i = 0;
        let found = false;

        while (i < shockRows.length && !found) {
            let shockRow = shockRows[i];
            let checked = (shockRow.querySelector('input[type="checkbox"]') as HTMLInputElement).checked;
            let shock = shockRow.querySelector('.shock').innerHTML;

            if (checked && shockName === shock) {
                if (initial)
                    input = shockRow.querySelector('.initialYear') as HTMLElement;
                else
                    input = shockRow.querySelector('.finalYear') as HTMLElement;

                found = true;
            }

            i++;
        }

        return input;
    }

    downloadingXls = false;
    exportTable(tables: any[]) {
        this.downloadingXls = true;

        let shocksNames = {
            base: "",
            all: "",
            changetype: "",
            totalincome: "",
            foreigninterest: "",
            localinterest: ""
        };

        let shocksTitle = "";
        let table_name_balance_primario_ingreso_total = "test1";
        let table_name_deuda_ingreso_total = "test2";


        this.translate
            .get(["ASD.BASE_SCENARY", "ASD.ALL_SHOKS", "ASD.DYNAMIC_OF_DEBT_RESULTS_SHOCK_STAGE", "ASD.SHOKS_TABLE_CHANGE_TYPE", "ASD.SHOKS_TABLE_TOTAL_INCOME", "ASD.SHOKS_TABLE_LOCAL_GROW_RATE", "ASD.SHOKS_TABLE_FOREIGN_GROW_RATE", "ASD.DYNAMIC_OF_DEBT_SIMULATION_2ND_TITLE", "ASD.PRIMARY_BALANCE_SHOKS_TABLE_TITLE", "ASD.DEBT_SHOKS_TABLE_TITLE"])
            .subscribe(t => {
                shocksTitle = t['ASD.DYNAMIC_OF_DEBT_SIMULATION_2ND_TITLE'];
                table_name_balance_primario_ingreso_total = t['ASD.PRIMARY_BALANCE_SHOKS_TABLE_TITLE'];
                table_name_deuda_ingreso_total = t['ASD.DEBT_SHOKS_TABLE_TITLE'];
                shocksNames.changetype = `${t['ASD.DYNAMIC_OF_DEBT_RESULTS_SHOCK_STAGE']} ${t['ASD.SHOKS_TABLE_CHANGE_TYPE']}`;
                shocksNames.totalincome = `${t['ASD.DYNAMIC_OF_DEBT_RESULTS_SHOCK_STAGE']} ${t['ASD.SHOKS_TABLE_TOTAL_INCOME']}`;
                shocksNames.localinterest = t['ASD.SHOKS_TABLE_LOCAL_GROW_RATE'];
                shocksNames.foreigninterest = t['ASD.SHOKS_TABLE_FOREIGN_GROW_RATE'];
                shocksNames.base = t['ASD.BASE_SCENARY'];
                shocksNames.all = t['ASD.ALL_SHOKS'];
            });

        const fileName = `${this.title.replace(' ', '_')}[${Date.now.toString()}].xlsx`;
        const headers = [shocksTitle, ...this.years.map((x: number) => {
            let pFrom = this.chartLabels[this.historics];
            if (x >= pFrom)
                return x.toString() + "p"; // P represents future values
            else
                return x.toString();
        })];
        const settings: any = {
            cellStyles: true,
            cellDates: true,
            header: headers,
            origin: 'A2'
        };
        if (this.simulationResults && this.simulationResults.all_shocks) {
            let rowCount = 4; // starting in A3
            let rows = [];

            if (tables.includes(ExportType.ASD_Dynamic_PrimaryBalcance)) {

                // Add rows
                // Set title
                rows.push([" ".repeat(100) + table_name_balance_primario_ingreso_total]);

                // Base First
                if (this.simulationResults.all_shocks.base_balance_primario_ingreso_total) {
                    rows.push([shocksNames.base, ...this.simulationResults.all_shocks.base_balance_primario_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);
                    rowCount++;
                }


                // Shocks (if exists)
                if (this.simulationResults.ingreso_total && this.simulationResults.ingreso_total.balance_primario_ingreso_total) {
                    rows.push([shocksNames.totalincome, ...this.simulationResults.ingreso_total.balance_primario_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);
                    rowCount++;
                }

                if (this.simulationResults.tipo_cambio && this.simulationResults.tipo_cambio.balance_primario_ingreso_total) {
                    rows.push([shocksNames.changetype, ...this.simulationResults.tipo_cambio.balance_primario_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);
                    rowCount++;
                }

                if (this.simulationResults.tasa_int_moneda_extrangera && this.simulationResults.tasa_int_moneda_extrangera.balance_primario_ingreso_total) {
                    rows.push([shocksNames.foreigninterest, ...this.simulationResults.tasa_int_moneda_extrangera.balance_primario_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);
                    rowCount++;
                }

                if (this.simulationResults.tasa_int_moneda_local && this.simulationResults.tasa_int_moneda_local.balance_primario_ingreso_total) {
                    rows.push([shocksNames.localinterest, ...this.simulationResults.tasa_int_moneda_local.balance_primario_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);
                    rowCount++;
                }

                // All 
                if (this.simulationResults.all_shocks.balance_primario_ingreso_total) {
                    rows.push([shocksNames.all, ...this.simulationResults.all_shocks.balance_primario_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);
                    rowCount++;
                }

            }

            if (tables.includes(ExportType.ASD_Dynamic_Debt)) {

                // Add rows
                // Set title
                rows.push([" ".repeat(115) + table_name_deuda_ingreso_total]);

                // Base First
                if (this.simulationResults.all_shocks.base_deuda_total_ingreso_total)
                    rows.push([shocksNames.base, ...this.simulationResults.all_shocks.base_deuda_total_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);

                // Shocks (if exists)
                if (this.simulationResults.ingreso_total && this.simulationResults.ingreso_total.deuda_total_ingreso_total)
                    rows.push([shocksNames.totalincome, ...this.simulationResults.ingreso_total.deuda_total_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);

                if (this.simulationResults.tipo_cambio && this.simulationResults.tipo_cambio.deuda_total_ingreso_total)
                    rows.push([shocksNames.changetype, ...this.simulationResults.tipo_cambio.deuda_total_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);

                if (this.simulationResults.tasa_int_moneda_extrangera && this.simulationResults.tasa_int_moneda_extrangera.deuda_total_ingreso_total)
                    rows.push([shocksNames.foreigninterest, ...this.simulationResults.tasa_int_moneda_extrangera.deuda_total_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);

                if (this.simulationResults.tasa_int_moneda_local && this.simulationResults.tasa_int_moneda_local.deuda_total_ingreso_total)
                    rows.push([shocksNames.localinterest, ...this.simulationResults.tasa_int_moneda_local.deuda_total_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);

                // All 
                if (this.simulationResults.all_shocks.deuda_total_ingreso_total)
                    rows.push([shocksNames.all, ...this.simulationResults.all_shocks.deuda_total_ingreso_total.map((i: number, idx) => `${i.toString()}%`)]);
            }

            const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.MapXLSRows(rows, headers), settings);
            const wb: XLSX.WorkBook = XLSX.utils.book_new();

            XLSX.utils.book_append_sheet(wb, ws, this.title);

            ws['!cols'] = [{ wch: 40 }];
            var rowSettings = new Array(16); //max rows with data
            const rowHeight = 25;

            for (let i = 0; i < rowSettings.length; i++) {
                let obj = {};

                if (i == 2 || i == rowCount - 1)
                    obj['hpt'] = rowHeight;

                rowSettings[i] = obj;
            }

            ws['!rows'] = rowSettings;

            var firstTitle = XLSX.utils.decode_range("A3:I3");
            var SecondTitle = XLSX.utils.decode_range(`A${rowCount.toString()}:I${rowCount.toString()}`);

            ws['!merges'] = [firstTitle, SecondTitle];
            XLSX.writeFile(wb, fileName);
        }
        this.downloadingXls = false;
    }

    MapXLSRows(rows: any[], headers: any[]) {
        return rows.map((r: any) => {
            let obj: any = {};
            headers.forEach((key: any, idx) => {
                obj[key] = r[idx];
            });
            return obj;
        });
    }
}
