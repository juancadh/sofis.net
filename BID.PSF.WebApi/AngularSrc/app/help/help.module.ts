import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from './../Components/Shared/shared.module';
import { HelpComponent } from './help.component';
import { RouterModule } from '@angular/router';
@NgModule({
    imports: [BrowserModule, SharedModule, RouterModule],
    declarations: [HelpComponent]
})

export class HelpModule { }