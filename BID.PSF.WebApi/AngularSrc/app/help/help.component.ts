﻿import { LanguageService } from './../api-helper/language.service';
import { Component, OnInit} from '@angular/core';
import { SessionService } from '../api-helper/session.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
    selector: 'app-help',
    templateUrl: './help.component.html',
    styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit{
    help_text_link='';

    constructor(private SessionService: SessionService, private languageService: LanguageService, private translateService: TranslateService) {
        var lang = languageService.getLocalLang();
    }

    username: string;
    ngOnInit(): void {
        this.username = this.SessionService.getCurrentUser();
        this.translateService.get('TEXTS.HELP_PARRAGRAPH_3', {url: '#'} ).subscribe(t=> this.help_text_link = t);
    }
}
