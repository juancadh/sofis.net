﻿// ReSharper disable InconsistentNaming
import { Component } from '@angular/core';

@Component({
    selector: 'data-navbar',
    templateUrl: './data-navbar.component.html',
    styleUrls: ['./data-navbar.component.css']
})

export class DataNavbarComponent {
}