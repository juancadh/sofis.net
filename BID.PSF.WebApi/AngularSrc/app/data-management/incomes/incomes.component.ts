﻿import { DataManagmentService } from './../data-managment-service/data-managment-service.service';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HotRegisterer } from 'angular-handsontable';
import { CataloguesService } from '../catalogues/catalogues.service';
import { CatalogueManagerService } from './../../api-helper/CatalogueManager/catalogue-manager.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ColumnCategory } from '../../api-helper/models/enums';
import { NotifierService } from 'angular-notifier';


@Component({
    selector: 'app-incomes',
    templateUrl: './incomes.component.html',
    styleUrls: ['./incomes.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class IncomesComponent implements OnInit {
    columnForm: FormGroup;
    get f() { return this.columnForm.controls; }
    private readonly notifier: NotifierService;

    constructor(
        private hotRegisterer: HotRegisterer,
        private cataloguesService: CataloguesService,
        private catalogemanagerservice: CatalogueManagerService,
        private modal: NgxSmartModalService,
        private fb: FormBuilder,
        private location: Location,
        notifierService: NotifierService,
        private translate: TranslateService,
        public dms: DataManagmentService) {
        this.notifier = notifierService;
    }

    async ngOnInit() {
        this.columnForm = this.fb.group({
            ColumnName: this.fb.control('', Validators.required)
        });

        await this.dms.InitCategory(ColumnCategory.Income);
        
        setTimeout(()=> {
            this.dms.renderHandsontableHeaders();
        }, 0);
    }
    
    loadingHandler(event) {
        this.dms.currentData = [];
        this.dms.topNav = !event;
        this.location.replaceState('/ingresos');
        this.ngOnInit();
    }

    showAlertHandler(event) {
        if (event) {
            this.dms.showAlert.show = false;
        }
    }

    openModal() {
        if (this.dms.submitEdit)
            this.columnForm.controls['ColumnName'].setValue(this.dms.baseColumnName);
        else
            this.columnForm.reset();

        this.modal.getModal('NewColModal').open();
    }
}
