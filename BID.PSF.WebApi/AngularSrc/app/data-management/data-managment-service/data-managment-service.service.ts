import { HotRegisterer } from 'angular-handsontable';
import { Utils } from './../utils/utils';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { CatalogueManagerService } from './../../api-helper/CatalogueManager/catalogue-manager.service';
import { CatalogueManagerHelper } from './../utils/cataloguemanager_helper';
import { FormGroup } from '@angular/forms';
import { GridYearData } from './../../api-helper/models/grid-year-data';
import { CurrencyUnit, CatalogueColumnCreate, CatalogueColumnValues, CatalogueColumn, DynamicColumn, SubCategory } from './../../api-helper/models/catalogue-column-values';
import { ColumnCategory, DebtColumns, ExpenseColumns } from './../../api-helper/models/enums';
import { CataloguesService } from './../catalogues/catalogues.service';
import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class DataManagmentService {
  instance: string;
  currentCategory: ColumnCategory;
  catalogue: number;
  topNav = false;

  settings = {
    fixedColumnsLeft: 1,
    colHeaders: true,
    columnHeaderHeight: 50,
    colWidths: 150,
    wordWrap: true,
    rowHeaders: false,
    className: "htCenter ht-m",
    fillHandle: {
      autoInsertRow: false
    },
    renderAllRows: false,
    preventOverflow: 'horizontal',
    autoRowSize: false,
    viewportColumnRenderingOffset: 100,
    viewportRowRenderingOffset: 100,
    cells: (row, col, prop) => {
      var cellProperties = {
        renderer: this.cellRenderer
      };
      return cellProperties;
    }
  };

  submitStatus: boolean;
  submitEdit: boolean;
  tableReady: boolean;
  apiError: boolean;
  showAlert: any;

  subcategories: Array<any>;
  currencyUnits: Array<any>;
  currencyUnit: CurrencyUnit;
  toYear: number;
  fromYear: number;
  projectionYears: number[];
  fromProjectionYear: number;
  toProjectionYear: number;
  currentData: GridYearData[];
  newColumnData: CatalogueColumnCreate;
  parentColumn: CatalogueColumnCreate;
  parentColumnId: number;
  allData: CatalogueColumnValues;
  baseColumn: number;
  columns: CatalogueColumn[];
  years: number[];
  dynamicColumns: DynamicColumn[];

  dynamicColumn: number;
  subcategory: SubCategory;
  dataSource: string;
  baseColumnName: any;
  baseColumnLevel: number;
  baseColumnCanDelete: boolean;
  baseColumnCanAdd: boolean;
  baseColumnIndex: any;
  columnsCantAddSubcategory: string[];

  constructor(
    @Inject(DOCUMENT) private document: HTMLDocument,
    private cataloguesService: CataloguesService,
    private catalogemanagerservice: CatalogueManagerService,
    private hotRegisterer: HotRegisterer,
    public modal: NgxSmartModalService,
  ) { }

  InitCategory = async (currentCategory: ColumnCategory) => {
    this.tableReady = false;
    this.showAlert = { type: '', show: false, msg: "" }
    this.currentData = [];
    this.columns = [];
    this.currentCategory = currentCategory;
    this.instance = "hotInstance";
    this.submitStatus = true;
    this.submitEdit = false;
    this.apiError = false;
    this.subcategories = [];
    this.currencyUnits = [];
    this.columnsCantAddSubcategory =
      [
        ExpenseColumns[ExpenseColumns.TotalExpense],
        ExpenseColumns[ExpenseColumns.TaggedExpense],
        ExpenseColumns[ExpenseColumns.UntaggedExpense]
      ];

    this.catalogue = parseInt(localStorage.getItem("catalogue"));

    await this.getDataSetYears();
    await this.cataloguesService.loadColumnDefinitions(this.currentCategory, this.loadColumnsSuccess.bind(this), this.loadColumnsErrors);
    await this.cataloguesService.loadCategoryValues(this.currentCategory, this.populateDataSuccess.bind(this), this.populateDataError);

    await this.cataloguesService.loadSubcategories(
      (res) => {
        this.subcategories = res.data;
      },
      (err) => { console.error(err) });

    await this.cataloguesService.loadCurrencyUnits(
      (res) => {
        this.currencyUnits = res.data;
        this.currencyUnit = res.data[0];
      },
      (err) => { console.error(err) });

    if (this.currentCategory !== ColumnCategory.Debt && this.currentCategory !== ColumnCategory.Macroeconomy) {
      this.document.onclick = (e) => {
        var target = e.target as HTMLElement;
        var dropdown = this.document.getElementById('dropdown-subcategory');
        var container = this.document.getElementById('subcategory-container');
        if (dropdown && container && !target.classList.contains("subcategory-button") && !dropdown.contains(target) && !container.contains(target)) {
          dropdown.style.display = 'none';
          container.style.display = 'none';
        }
      };
    }
    this.tableReady = true;
  }

  refresh() {
    this.tableReady = false;
    this.submitEdit = false;
    this.showAlert = { type: '', show: false, msg: "" };
    this.InitCategory(this.currentCategory);
  }

  onFromChange($event) {
    if (this.fromYear > this.toYear)
      this.toYear = this.fromYear;

    this.projectionYears = this.getProjectionYears();

    this.currentData = this.cataloguesService.processCurrentData(this.fromYear, this.toProjectionYear, this.allData);
    this.updateDataSetYears();
  }

  onToChange($event) {
    if (this.toYear < this.fromYear)
      this.fromYear = this.toYear;

    this.projectionYears = this.getProjectionYears();

    this.currentData = this.cataloguesService.processCurrentData(this.fromYear, this.toProjectionYear, this.allData);
    this.updateDataSetYears();
  }

  onToProjectionChange($event) {
    this.currentData = this.cataloguesService.processCurrentData(this.fromYear, this.toProjectionYear, this.allData);
    this.updateDataSetYears();
  }

  submitNewColumnData(columnForm: FormGroup, edit: boolean, init) {
    this.submitStatus = false;
    if (columnForm.valid) {
      this.newColumnData = {
        ColumnName: columnForm.controls.ColumnName.value,
        InternalName: CatalogueManagerHelper.setInternalName(columnForm.controls.ColumnName.value),
        Index: this.parentColumn.Index + 1,
        Category: this.currentCategory,
        ParentId: this.parentColumnId,
        CatalogueId: parseInt(localStorage.getItem('catalogue')),
        IsDefaultColumn: false
      };
      if (!edit) {
        this.catalogemanagerservice.createColumn(this.newColumnData)
          .then(r => { this.showAlert.show = true; this.showAlert.msg = 'OK'; this.showAlert.type = 'success'; columnForm.reset(); init() }) //SHARED.SUCCESS_SAVED
          .catch(e => { this.showAlert.show = true; this.submitStatus = true; this.showAlert.msg = e.message; this.showAlert.type = 'error' });
      } else {
        this.submitEdit = false; // reseting submit to New by defautl
        const data = { ...this.newColumnData, Id: this.baseColumn }
        this.catalogemanagerservice.updateColumn(data)
          .then(r => { this.showAlert.show = true; this.showAlert.msg = 'OK'; this.showAlert.type = 'success'; columnForm.reset(); init() }) //SHARED.SUCCESS_SAVED
          .catch(e => { this.showAlert.show = true; this.submitStatus = true; this.showAlert.msg = e.message; this.showAlert.type = 'error' });
      }
    }

    this.modal.getModal('NewColModal').close();
  }

  getColumnLevel(column: CatalogueColumn, columns: CatalogueColumn[]) {
    let level = 0;
    const readOnlyColumns = columns.filter(c => c.readOnly);

    if (column.readOnly && !column.parentId) {
      level = 1;
    } else if (column.parentId == columns[1].data) {
      level = 2;
    } else if (level === 0) {
      if (readOnlyColumns.filter((r, i) => r.data == column.parentId && column.index > r.index).length > 0)
        level = 3;
    }

    return level;
  }

  populateDataSuccess(result) {
    this.allData = result.data;
    this.currentData = this.cataloguesService.processCurrentData(this.fromYear, this.toProjectionYear, this.allData);
  }


  populateDataError(result) {
    this.apiError = true;
    console.error(result);
  }

  afterChange($event) {
    if ($event[1] === 'loadData') {
      return;
    };

    const hot = this.hotRegisterer.getInstance(this.instance);
    this.cataloguesService.listenToCells($event, this.currentData, this.allData, hot);

    this.currentData.filter(d => d.year > this.toYear).forEach(v => {
      v.projected = true;
    });

    const cbSuccess = () => { };
    const cbError = () => { };

    this.cataloguesService.gridAfterChangeEvent($event,
      this.currentData,
      this.currentCategory,
      this.allData.dataSetId,
      cbSuccess.bind(this),
      cbError);
  }

  deleteColumn(init) {
    if (this.currentCategory !== ColumnCategory.Debt) {
      this.catalogemanagerservice.deleteColumn(this.baseColumn)
        .then(r => { this.showAlert.show = true; this.showAlert.msg = 'OK'; this.showAlert.type = 'success'; init() }) //SHARED.SUCCESS_SAVED
        .catch(e => { this.showAlert.show = true; this.showAlert.msg = e.message; this.showAlert.type = 'error' });
      var dropdown = this.document.getElementById('dropdown-subcategory');
      dropdown.style.display = 'none';
    }
  }


  addDynamicColumn(init) {
    if (this.currentCategory !== ColumnCategory.Debt) {
      this.cataloguesService.postDynamicColumn(this.baseColumn, this.dynamicColumn, this.subcategory.id, this.currencyUnit.id, this.dataSource,
        () => {
          this.cataloguesService.loadColumnDefinitions(this.currentCategory, this.loadColumnsSuccess.bind(this), this.loadColumnsErrors);
          init();
        },
        (err) => {
          console.error(err);
        });

      const dropdown = this.document.getElementById("dropdown-subcategory");
      const container = this.document.getElementById("subcategory-container");
      dropdown.style.display = 'none';
      container.style.display = 'none';
    }
  }

  private getProjectionYears(): number[] {
    const result: number[] = new Array();

    for (let i = this.toYear + 1; i <= this.toYear + 5; i++)
      result.push(i);

    this.fromProjectionYear = result[0];

    if (this.toProjectionYear < result[0]) {
      this.toProjectionYear = result[0];
      this.updateDataSetYears();
    }
    else if (this.toProjectionYear > result[4]) {
      this.toProjectionYear = result[4];
      this.updateDataSetYears();
    }

    return result;
  }

  private async getDataSetYears() {
    const result = await this.cataloguesService.getDataSetYears();
    this.fromYear = result.data.HistoricFrom;
    this.toYear = result.data.HistoricTo;
    this.fromProjectionYear = result.data.ProjectionFrom;
    this.toProjectionYear = result.data.ProjectionTo;
    this.years = this.yearOptions();
    this.projectionYears = this.getProjectionYears();
  }

  private updateDataSetYears() {
    var newYears = {
      HistoricFrom: this.fromYear,
      HistoricTo: this.toYear,
      ProjectionFrom: this.fromProjectionYear,
      ProjectionTo: this.toProjectionYear
    }

    this.cataloguesService.postDataSetYears(newYears);
  }

  private loadColumnsSuccess(result) {
    this.columns = result.data.map(r => {
      r.title = r.readOnly && r.index !== 0 ?
        `<div class='custom-col-h shadow title-${(r.level).toString()}'>${r.title}
              <div class='img'></div>
          </div>`
        : `<div class='custom-col-h title-${(r.level).toString()}'>${r.title}<div></div></div>`;
      return r;
    });
    this.dynamicColumns = result.data.filter(c => c.dynamic === true);
    this.columns.forEach((r, i) => {
      this.columns[i].title = Utils.fixColHeader(r.title);
    });
  }

  private loadColumnsErrors(error) {
    this.apiError = true;
  }

  private yearOptions(): number[] {
    const currentYear = new Date().getUTCFullYear();
    const result: number[] = new Array();

    for (let i = 1990; i <= currentYear; i++)
      result.push(i);

    return result;
  }

  private getColTitleWithOutHtml(html: string): String {
    return html.replace(/<[^>]*>/g, '');
  }

  private offset(el) {
    var rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || this.document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || this.document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  }

  private cellRenderer = (instance, td, row, col, prop, value, cellProperties) => {

    if (value != undefined) {
      if (isNaN(parseInt(value)) || col === 0)
        td.innerText = value;
      else
        td.innerText = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }

    if (this.currentData != undefined && this.currentData[row] != undefined) {
      if (this.columns.filter(c => c.data === prop && c.readOnly === true).length > 0) {
        td.className += " text-bold";
      } else {
        td.className += " cell-hover"
      }

      if (this.columns.filter(c => c.data === prop && c.internalName == DebtColumns[DebtColumns.LocalRate]).length > 0) {
        td.className += " percentage";
      }

      if (this.currentData[row]['year'] > this.toYear)
        td.className += " future";
    }
  }

  public renderHandsontableHeaders() {
    const headers = this.document.querySelectorAll('.ht_clone_top th');
    for (let j = 1; j < headers.length; j++) {
      const th = headers[j];
      const nextTh = headers[j + 1] || null;
      const column = th.children[0].children[0];
      const subcategoryButton = this.document.createElement('img');
      subcategoryButton.classList.add('subcategory-button');
      const dropdown = this.document.getElementById("dropdown-subcategory");
      const container = this.document.getElementById("subcategory-container");
      const catalogueColumn = this.columns.filter(c => this.getColTitleWithOutHtml(c.title) == this.getColTitleWithOutHtml(column.innerHTML))[0];
      th.children[0].insertBefore(subcategoryButton, th.children[0].firstChild);

      const internalName = catalogueColumn && catalogueColumn.internalName;
      const level = catalogueColumn && catalogueColumn.level <= 2;
      if (!this.columnsCantAddSubcategory.includes(internalName) && level)
        subcategoryButton.src = '/AngularSrc/assets/images/menu.png';
      else
        subcategoryButton.src = '/AngularSrc/assets/images/edit.svg';

      // La Columna siguiente a la columna padre, se le pone la sombra
      if (catalogueColumn && catalogueColumn.readOnly && nextTh != null)
        nextTh.children[0].classList.add('shadow-header-col');

      if (this.currentCategory !== ColumnCategory.Debt && this.currentCategory !== ColumnCategory.Macroeconomy) {
        //listeners
        subcategoryButton.addEventListener('mouseover', () => {
          subcategoryButton.style.cursor = "pointer"
          subcategoryButton.style.opacity = "1";
        });

        subcategoryButton.addEventListener('mouseout', () => {
          subcategoryButton.style.opacity = "0";
        });

        subcategoryButton.addEventListener('click', () => {
          const thOffset = this.offset(th);
          subcategoryButton.style.opacity = "1";

          if (dropdown.style.display === 'block')
            dropdown.style.display = 'none';
          else if (container.style.display === 'block')
            container.style.display = 'none';
          else {
            dropdown.style.top = (parseInt(thOffset.top) - 190).toString() + "px";

            if (nextTh != null) {
              dropdown.style.left = (parseInt(thOffset.left) + 160).toString() + "px";
            } else {
              dropdown.style.left = ((parseInt(thOffset.left) - 60)).toString() + "px";
            }

            dropdown.style.display = 'block';
            this.baseColumnIndex = catalogueColumn.index;
            this.baseColumnLevel = catalogueColumn.level;
            this.baseColumnName = this.getColTitleWithOutHtml(catalogueColumn.title);
            this.baseColumn = catalogueColumn.data;
            this.parentColumnId = catalogueColumn.data;
            this.baseColumnCanDelete = catalogueColumn.readOnly || catalogueColumn.isDefaultColumn;
            this.baseColumnCanAdd = !this.columnsCantAddSubcategory.includes(catalogueColumn.internalName);
            this.parentColumn = {
              Index: catalogueColumn.index,
              ColumnName: catalogueColumn.title,
              InternalName: '',
              Category: this.currentCategory,
              IsDefaultColumn: catalogueColumn.dynamic,
              CatalogueId: parseInt(localStorage.getItem('catalogue')),
              ParentId: catalogueColumn.parentId,
            }
          }
        });
      }

    }
  }
}