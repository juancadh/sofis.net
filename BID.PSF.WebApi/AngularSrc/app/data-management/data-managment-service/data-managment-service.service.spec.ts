import { TestBed, inject } from '@angular/core/testing';

import { DataManagmentService } from './data-managment-service.service';

describe('DataManagmentServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataManagmentService]
    });
  });

  it('should be created', inject([DataManagmentService], (service: DataManagmentService) => {
    expect(service).toBeTruthy();
  }));
});
