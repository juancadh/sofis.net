﻿import { Injectable } from '@angular/core';

import { ApiService } from '../../api-helper/api.service';
import { ApiEndpoints } from '../../api-helper/api-endpoints';
import { ColumnCategory } from '../../api-helper/models/enums';
import { CatalogueColumnValues, CatalogueColumnValueData } from '../../api-helper/models/catalogue-column-values';
import { GridYearData } from '../../api-helper/models/grid-year-data';

import { CatalogueColumn } from '../../api-helper/models/catalogue-column-values';
import { Observable } from 'rxjs';

@Injectable()
export class CataloguesService {
    constructor(private readonly apiService: ApiService) { }

    columns: CatalogueColumn[];

    async loadColumnDefinitions(category: ColumnCategory, onSuccess: (any) => void, onError: (any) => void, datasetId?: number) {
        try {
            const result = await this.apiService.get(ApiEndpoints.catalogues_getTableStructure + "?category=" + category + (datasetId ? "&datasetId = " + datasetId : ''), null);
            this.columns = result.data;
            onSuccess(result);
        } catch (err) {
            console.error(err);
            onError(err);
        }
    }

    async loadCategoryValues(category: ColumnCategory, onSuccess: (any) => void, onError: (any) => void, datasetId?: number) {
        try {
            const result = await this.apiService.get(ApiEndpoints.catalogues_getValues + "?category=" + category + (datasetId ? "&datasetId = " + datasetId : ''), null);
            onSuccess(result);

        } catch (err) {
            console.error(err);
            onError(err);
        }
    }

    async loadSubcategories(onSuccess: (any) => void, onError: (any) => void) {
        try{
            const result = await this.apiService.get(ApiEndpoints.catalogues_getSubcategories, null);
            onSuccess(result);
        }catch(err){
            onError(err);
        }
    }

    async loadCurrencyUnits(onSuccess: (any) => void, onError: (any) => void) {
        try{
            const result = await this.apiService.get(ApiEndpoints.catalogues_getCurrencyUnits, null);
            onSuccess(result);
        }catch(err){
            onError(err);
        }
    }

    postCategoryData(category: ColumnCategory, datasetId: number, yearDatas: GridYearData[], onSuccess: (any) => void, onError: (any) => void) {
        var data = this.buildPosteableData(category, datasetId, yearDatas);

        this.apiService.post(ApiEndpoints.catalogues_postValues, null, data).then(
            result => {
                onSuccess(result);
            },
            err => {
                // Log errors if any
                console.error(err);
                onError(err);
            });
    }

    postDynamicColumn(baseColumn: number, dynamicColumn: number, subcategory: number, currencyUnit: number, dataSource: string, onSuccess: (any) => void, onError: (any) => void) {
        const data = {
            parentBaseColumnId: baseColumn,
            parentDynamicColumnId: dynamicColumn,
            subcategoryId: subcategory,
            currencyUnitId: currencyUnit,
            dataSource: dataSource,
        };

        this.apiService.post(ApiEndpoints.catalogues_postDynamicColumn, null, data).then(
            result => {
                onSuccess(result);
            },
            err => {
                console.error(err);
                onError(err);
            });
    }

    deleteDynamicColumn(selectedColumn: number, onSuccess: (any) => void, onError: (any) => void) {
        this.apiService.delete(ApiEndpoints.catalogues_deleteDynamicColumn + "/" + selectedColumn, null).then(
            result => {
                onSuccess(result);
            },
            err => {
                console.error(err);
                onError(err);
            });
    }

    processCurrentData(fromYear: number, toYear: number, allData: CatalogueColumnValues): GridYearData[] {

        let result: GridYearData[] = new Array();
        let yearData: GridYearData;

        for (var i = fromYear; i <= toYear; i++) {
            if (allData.values != null) {
                let yearDatas = allData.values.filter(x => x.year === i);

                if (yearDatas != null && yearDatas.length > 0) {
                    yearData = result.find(x => x.year === i);
                    if (yearData == null) {
                        yearData = new GridYearData();
                        yearData.year = i;
                        result.push(yearData);
                    }

                    for (var j = 0; j < yearDatas.length; j++)
                        yearData[yearDatas[j].columnId] = yearDatas[j].value;

                    continue;
                }
            }

            yearData = new GridYearData();
            yearData.year = i;

            result.push(yearData);
        }

        return result;
    }

    gridAfterChangeEvent($event, currentData: GridYearData[], category: ColumnCategory, datasetId: number, onSuccess: (any) => void, onError: (any) => void) {
        if ($event[1] === 'loadData') {
            return;
        }

        if ($event[0] == null)
            return;

        let yearDatasToPost: GridYearData[] = new Array();

        for (var i = 0; i < $event[0].length; i++) {
            const data = $event[0][i][1];
            let column = this.columns.filter(c => c.data === data)[0];
            let dynamic = column !== undefined ? column.dynamic : false;
            let d: GridYearData = {
                year: currentData[$event[0][i][0]].year,
                dynamic: dynamic,
                projected: currentData[$event[0][i][0]].projected
            } as GridYearData;

            d[$event[0][i][1]] = $event[0][i][3];

            yearDatasToPost.push(d);
        }

        this.postCategoryData(category, datasetId, yearDatasToPost, onSuccess, onError);
    }

    listenToCells($event, currentData: GridYearData[], allData: CatalogueColumnValues, hotInstance: any) {
        // If no data has changed then return
        if ($event[0] == null)
            return;

        let updatedRows = new Array();
        let itemsToPush = new Array();

        // For each updated row
        for (var i = 0; i < $event[0].length; i++) {
            // If row already updated then continue
            if (updatedRows.find(u => u === $event[0][i][1]) != null)
                continue;

            // Find all row cells
            let yearData = currentData[$event[0][i][0]];
            let calculatedCells = allData.values.filter(c => c.year === yearData.year && c.formula != null && c.formula != undefined).reverse();
            updatedRows.push(yearData.year);

            // Find calculated cells
            calculatedCells.forEach(cell => {
                let cols = cell.formula.replace('=', '').split(',');
                let val = 0;
                cols.forEach(col => {
                    var aux = yearData[col];
                    if (aux != null)
                        val += (new Number(aux)).valueOf();
                });

                var oldVal = cell.value;
                cell.value = val;
                yearData[cell.columnId] = val;

                // Update event in order to save autocalculated rows
                itemsToPush.push([$event[0][i][0], cell.columnId.toString(), oldVal, val]);
            });

        }

        itemsToPush.forEach((item) => {
            $event[0].push(item);
        });

        hotInstance.render();
    }

    getDataSetYears(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.apiService.get(ApiEndpoints.catalogues_datasetyears, null).then(result => {
                resolve(result);
            }, err => {
                reject(err);
            });
        });
    }

    postDataSetYears(years): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.apiService.post(ApiEndpoints.catalogues_datasetyears, null, years).then(result => {
                resolve(result);
            }, err => {
                reject(err);
            });
        });
    }

    private buildPosteableData(category: number, datasetId: number, yearDatas: GridYearData[]): CatalogueColumnValues {
        let dataToPost: CatalogueColumnValues = new CatalogueColumnValues();
        dataToPost.values = new Array<CatalogueColumnValueData>();
        dataToPost.category = category;
        dataToPost.dataSetId = datasetId;

        for (var i = 0; i < yearDatas.length; i++) {
            let gridYearData = yearDatas[i];
            for (let prop in gridYearData) {
                if (gridYearData.hasOwnProperty(prop) && prop !== 'year' && prop !== 'dynamic' && prop !== 'projected') {

                    let catalogueColumnValue: CatalogueColumnValueData = {
                        columnId: Number.parseInt(prop),
                        year: gridYearData.year,
                        value: gridYearData[prop],
                        dynamic: gridYearData.dynamic,
                        projected: gridYearData.projected
                    } as CatalogueColumnValueData;

                    dataToPost.values.push(catalogueColumnValue);
                }
            };
        }

        return dataToPost;
    }

    setHistoricToYear(onSuccess: (any) => void, onError: (any) => void) {
        this.apiService.post(ApiEndpoints.catalogues_setHistoricYear, null, null).then(
            result => {
                onSuccess(result);
            },
            err => {
                onError(err);
            });
    }
}