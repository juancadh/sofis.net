﻿import { CataloguesService } from './../catalogues/catalogues.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { TranslateService } from '@ngx-translate/core';
import { NotifierService } from 'angular-notifier';
import { TopNavLogicService } from '../../api-helper/top-nav-logic.service';


@Component({
    selector: 'top-navbar',
    templateUrl: './top-navbar.component.html',
    styleUrls: ['./top-navbar.component.css']
})

export class TopNavbarComponent implements OnInit {

    showForeignAlert = { type: '', show: false, msg: "" }

    @Output() isLoading = new EventEmitter();
    private readonly notifier: NotifierService;

    constructor(
        private readonly ar: ActivatedRoute,
        public modal: NgxSmartModalService,
        private t: TranslateService,
        notifierService: NotifierService,
        public topNavLogic: TopNavLogicService,
        public cataloguesService: CataloguesService

    ) {
        this.notifier = notifierService;
        this.isLoading.emit(true);
        this.topNavLogic.isLoadingChange.subscribe(value=> this.isLoading.emit(value));
    }

    async ngOnInit() {
        await this.topNavLogic.init();
    }

    async updateHistorcYear() {
        await this.cataloguesService.setHistoricToYear(
            (res) => {
                if (res.data)
                    this.t.get("SHARED.PROJECTION_ALERT").subscribe(t => this.notifier.notify('warning', t));
            },
            (err) => {
                this.t.get("SHARED.PROJECTION_ALERT_ERROR").subscribe(t => this.notifier.notify('error', t));
            });
    }

    loadForeignAlert() {
        this.ar.params.subscribe(r => {
            if (r.alert && r.alert === '1') {
                this.showForeignAlert.show = true;

                switch (r.type) {
                    case '1':
                        this.showForeignAlert.type = 'success'
                        break;
                    case '2':
                        this.showForeignAlert.type = 'error'
                        break;
                    default:
                        this.showForeignAlert.type = 'success'
                        break;
                }

                switch (r.msg) {
                    case '1':
                        this.t.get('SHARED.SUCCESS_NEW_CATALOGUE').subscribe(t => this.showForeignAlert.msg = t);
                        break;
                    case '2':
                        this.t.get('SHARED.SUCCESS_CLONE_CATALOGUE').subscribe(t => this.showForeignAlert.msg = t);
                        break;
                    case '3':
                        this.t.get('ERRORS.OPERATIONS').subscribe(t => this.showForeignAlert.msg = t);
                        break;

                    default:
                        this.t.get('SHARED.SUCCESS_NEW_CATALOGUE').subscribe(t => this.showForeignAlert.msg = t);
                        break;
                }
            }
        });
    }

    showForeignAlertHandler(event) {
        if (event)
            this.showForeignAlert.show = false;
    }
}