﻿import { DataManagmentService } from './../data-managment-service/data-managment-service.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ColumnCategory } from '../../api-helper/models/enums';

@Component({
    selector: 'app-debts',
    templateUrl: './debts.component.html',
    styleUrls: ['./debts.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class DebtsComponent implements OnInit {
    columnForm: FormGroup;
   
    constructor(
        public dms: DataManagmentService,
        private fb: FormBuilder
        ) {
        this.columnForm = this.fb.group({
            ColumnName: ['', Validators.required]
        });       
    }

    async ngOnInit() {
        await this.dms.InitCategory(ColumnCategory.Debt);
        setTimeout(()=> {
            this.dms.renderHandsontableHeaders();
        }, 0);
    }

    loadingHandler(event) {
        this.dms.currentData = [];
        this.dms.topNav = !event;
        this.ngOnInit();
    }

    openModal() {
        if (this.dms.submitEdit){
            this.columnForm.controls['ColumnName'].setValue(this.dms.baseColumnName);
        }
        else{
            this.columnForm.reset();
        }

        this.dms.modal.getModal('NewColModal').open();
    }
}
