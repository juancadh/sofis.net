﻿import { NgxSmartModalService } from 'ngx-smart-modal';
import { NotifierService } from 'angular-notifier';
import { DataManagmentService } from './../data-managment-service/data-managment-service.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core'; //
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ColumnCategory } from '../../api-helper/models/enums';

@Component({
    selector: 'app-expenses',
    templateUrl: './expenses.component.html',
    styleUrls: ['./expenses.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class ExpensesComponent implements OnInit {
    columnForm: FormGroup;
    private readonly notifier: NotifierService;

    constructor(
        private fb: FormBuilder,
        public dms: DataManagmentService,
        notifierService: NotifierService,
        private modal: NgxSmartModalService,
    ) {
        this.columnForm = this.fb.group({
            ColumnName: ['', Validators.required]
        });

        this.notifier = notifierService;
    }

    async ngOnInit() {
        await this.dms.InitCategory(ColumnCategory.Expense);
        setTimeout(()=> {
            this.dms.renderHandsontableHeaders();
        }, 0);
    }

    async loadingHandler(event = false) {
        this.dms.currentData = [];
        this.dms.topNav = !event;
        this.dms.tableReady = event;
        await this.ngOnInit();
    }

    openModal() {
        if (this.dms.submitEdit)
            this.columnForm.controls['ColumnName'].setValue(this.dms.baseColumnName);
        else
            this.columnForm.reset();

        this.dms.modal.getModal('NewColModal').open();
    }
}
