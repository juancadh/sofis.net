﻿import { Component, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-dynamic-column',
    templateUrl: './dynamic-column.component.html',
    styleUrls: ['./dynamic-column.component.css']
})
export class DynamicColumnComponent {
    constructor(public activeModal: NgbActiveModal) { }
}
