import { DataManagmentService } from './../data-managment-service/data-managment-service.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HotRegisterer } from 'angular-handsontable';
import { CataloguesService } from '../catalogues/catalogues.service';
import { ColumnCategory } from '../../api-helper/models/enums';
import { NotifierService } from 'angular-notifier';


@Component({
    selector: 'app-macroeconomics',
    templateUrl: './macroeconomics.component.html',
    styleUrls: ['./macroeconomics.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class MacroeconomicsComponent implements OnInit {
    private readonly notifier: NotifierService;

    constructor(
        private readonly hotRegisterer: HotRegisterer,
        private readonly cataloguesService: CataloguesService,
        private readonly translate: TranslateService,
        notifierService: NotifierService,
        public dms: DataManagmentService) {
        this.notifier = notifierService;
    }

    async ngOnInit() {
        await this.dms.InitCategory(ColumnCategory.Macroeconomy);
        setTimeout(()=> {
            this.dms.renderHandsontableHeaders();
        }, 0);
    }

    loadingHandler(event) {
        this.dms.currentData = [];
        this.dms.topNav = !event;
        this.ngOnInit();
    }
}
