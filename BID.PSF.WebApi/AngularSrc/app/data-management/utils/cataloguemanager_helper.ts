import { CatalogueColumn, CatalogueColumnCreate } from './../../api-helper/models/catalogue-column-values';
import { Column } from './../../api-helper/Interfaces/Catalogue';

export class CatalogueManagerHelper {

    constructor() {

    }

    public static setNewColumnData = (data: CatalogueColumnCreate, parentId: number): CatalogueColumnCreate => {
        const col: CatalogueColumnCreate = {
            Index: data.Index,
            ColumnName: data.ColumnName,
            InternalName: data.InternalName,
            Category : data.Category,
            IsDefaultColumn: false,
            CatalogueId: parseInt(localStorage.getItem('catalogue')),
            ParentId: parentId,

        };
        return col;
    }

    public static createColumn = (data: CatalogueColumnCreate, cb: Function) => {
        if ( data ) {
            cb(data);
        }
    }

    public static setInternalName( str: any ): any {
        str = str.toLowerCase().split(' ');
        for (let i = 0; i < str.length; i++) {
          str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
        }
        return str.join(' ').replace(/\s/g, '');
    }
}
