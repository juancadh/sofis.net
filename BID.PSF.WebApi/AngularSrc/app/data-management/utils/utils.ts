export class Utils {

    public static fixColHeader(colHeader) {
        const regularEXP = /(\([A-Z =+]+\))/;
        const e = regularEXP.exec(colHeader);
        if ( e == null) { return colHeader; }
        const index = colHeader.indexOf(e[0]);
        return colHeader.slice(0, index).trim() + '<br/>' + colHeader.slice(index).trim();
    }

    public static asignNumericValue(value: any): any {
        if(isNaN(value))
            return value.replace('%', '');
        
        return value;
    }

}
