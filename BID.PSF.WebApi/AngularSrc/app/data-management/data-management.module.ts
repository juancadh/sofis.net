﻿import { DataManagmentService } from './data-managment-service/data-managment-service.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotTableModule } from 'angular-handsontable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContextMenuModule } from 'ngx-contextmenu';
import { SharedModule } from './../Components/Shared/shared.module';
import { CataloguesService } from './catalogues/catalogues.service';
import { MacroeconomicsComponent } from './macroeconomics/macroeconomics.component';
import { ExpensesComponent } from './expenses/expenses.component';
import { DebtsComponent } from './debts/debts.component';
import { IncomesComponent } from './incomes/incomes.component';

import { ImageCropperModule } from 'ngx-image-cropper';

import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { DataNavbarComponent } from './data-navbar/data-navbar.component';
import { DataManagmentTooltipComponent } from './data-managment-tooltip/data-managment-tooltip.component';



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ContextMenuModule.forRoot({
      autoFocus: true,
    }),
    ReactiveFormsModule,
    RouterModule,
    ImageCropperModule,
    HotTableModule,
    NgxSmartModalModule.forRoot(),
    NgbModule,
    SharedModule
  ],
  declarations: [
    MacroeconomicsComponent,
    ExpensesComponent,
    DebtsComponent,
    IncomesComponent,
    TopNavbarComponent,
    DataNavbarComponent,
    DataManagmentTooltipComponent
  ],
  providers: [CataloguesService, DataManagmentService],
})
export class DataManagementModule { }