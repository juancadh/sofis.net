import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataManagmentTooltipComponent } from './data-managment-tooltip.component';

describe('DataManagmentTooltipComponent', () => {
  let component: DataManagmentTooltipComponent;
  let fixture: ComponentFixture<DataManagmentTooltipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataManagmentTooltipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataManagmentTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
