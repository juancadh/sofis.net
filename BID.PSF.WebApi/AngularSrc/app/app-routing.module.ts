﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationRoutingModule } from './authentication/authentication-routing.module';

import { LoginComponent } from './authentication/login/login.component';
import { ResetPasswordComponent } from './profile/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './profile/forgot-password/forgot-password.component';

import { MacroeconomicsComponent } from './data-management/macroeconomics/macroeconomics.component';
import { ExpensesComponent } from './data-management/expenses/expenses.component';
import { DebtsComponent } from './data-management/debts/debts.component';
import { IncomesComponent } from './data-management/incomes/incomes.component';

import { InertialApproximationComponent } from './mfmp/inertial-approximation/inertial-approximation.component';
import { LastFiveYearsApproximationComponent } from './mfmp/last-five-years/last-five-years.component';
import { FixedApproximationComponent } from './mfmp/fixed-approximation/fixed-approximation.component';
import { VariableApproximationComponent } from './mfmp/variable-approximation/variable-approximation.component';
import { RulesComponent } from './mfmp/rules/rules.component';

import { StandardApproachComponent } from './asd/standard-approach/standard-approach.component';

import { DynamicsOfDebtComponent } from './asd/dynamics-of-debt/dynamics-of-debt.component';
import { FanChartAnalysisComponent } from './asd/fan-chart-analysis/fan-chart-analysis.component';

import { ProfileComponent } from './profile/profile.component';
import { HelpComponent } from './help/help.component';
import { TermsComponent } from './terms/terms.component';

import { OktaCallbackComponent } from '@okta/okta-angular';

const routes: Routes = [
    { path: 'login', component: LoginComponent },

    { path: 'macroeconomicas', component: MacroeconomicsComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'gastos', component: ExpensesComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'deudas', component: DebtsComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'ingresos', component: IncomesComponent, canActivate: [AuthenticationRoutingModule] },

    { path: 'aproximacion-inercial', component: InertialApproximationComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'aproximacion-5-años', component: LastFiveYearsApproximationComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'aproximacion-parametrica-fija', component: FixedApproximationComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'aproximacion-parametrica-variable', component: VariableApproximationComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'reglas', component: RulesComponent, canActivate: [AuthenticationRoutingModule] },

    { path: 'aproximacion-estandar', component: StandardApproachComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'dinamica-deuda', component: DynamicsOfDebtComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'fan-chart', component: FanChartAnalysisComponent, canActivate: [AuthenticationRoutingModule] },

    { path: 'perfil', component: ProfileComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'reset-password', component: ResetPasswordComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'forgot-password', component: ForgotPasswordComponent },

    { path: 'ayuda', component: HelpComponent, canActivate: [AuthenticationRoutingModule] },
    { path: 'terminos', component: TermsComponent, canActivate: [AuthenticationRoutingModule] },

    { path: 'implicit/callback', component: OktaCallbackComponent },

    { path: '', component: LoginComponent },
    // otherwise redirect to home - Dejar esto al final
    //{ path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
