﻿export class GridYearData {
    public object: any[];
    public year: number;
    public dynamic: boolean;
    public projected: boolean;
}