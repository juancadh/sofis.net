﻿export class LanguageData {
    id: number;
    longName: string;
    shortName: string;
    encodedName: string;
}