﻿export class MunicipalityData {
    public id: number;
    public name: string;
    public countryId: number;
}