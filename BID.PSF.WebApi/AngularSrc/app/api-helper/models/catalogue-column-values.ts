﻿import { ColumnCategory } from './enums';

export class CatalogueColumnValues {
    category: ColumnCategory;
    dataSetId: number;
    values: CatalogueColumnValueData[];
}

export class CatalogueColumnValueData {
    columnId: number;
    value: number;
    year: number;
    formula: string;
    dynamic: boolean;
    projected: boolean;
}

export class CatalogueColumn {
    data: number;
    title: string;
    dynamic: boolean;
    readOnly: boolean;
    columnId: number;
    parentId: number;
    index: number;
    level: number;
    isDefaultColumn: boolean;
    internalName: string;
}

export class CatalogueColumnCreate {
    Index: number;
    ColumnName: string;
    InternalName: string;
    Category: number;
    IsDefaultColumn: boolean = false;
    CatalogueId: number;
    ParentId: number;
}

export class DynamicColumn {
    data: number;
    title: string;
}

export class SubCategory {
    id: number;
    name: string;
}

export class CurrencyUnit {
    id: number;
    name: string;
}