export class SofisStandardAproximationInput {
    catalog: string;
    ditf: number[];
    dtiv: number[];
    tidt: number[];
    //ting: number[];
    ild: number[];
    //teg: number[];
    dpt: number[];
    T: number;
    fdesv_g: number;
    fdesv_r: number;

    g: number;
    r: number;
    gstd: number;
    rstd: number;

    d: number;
    ting: number;
    teg: number;
    pint: number;
}