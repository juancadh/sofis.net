﻿export enum Aproximation {
    Inertial = 0,
    FiveYears = 1,
    Fixed = 2,
    Variable = 3,
}

export enum Catalogues {
    Mexico = 1,
    Standard = 2
}

export enum ColumnCategory {
    Macroeconomy = 0,
    Income = 1,
    Expense = 2,
    Debt = 3,
    DebtService = 4,
    PrimaryBalance = 5,
    FiscalBalance = 6
}

export class ExportType {
    public static ASD_Dynamic_PrimaryBalcance = "PRIMARY_BALANCE";
    public static ASD_Dynamic_Debt = "DEBT";
}

export class EntryData {
    Account: string;
    ColumnId: number;
    Parameter: number;
    Parameters: Array<number>;
    Autocalculated: boolean;
    Level: string;
}

export class ShockDTO {
    Shock: string;
    InitialYear: number;
    FinalYear: number;
    StandardDeviation: number;

    constructor(shock: string, initialYear: number, finalYear: number, standardDeviation: number) {
        this.Shock = shock;
        this.InitialYear = initialYear;
        this.FinalYear = finalYear;
        this.StandardDeviation = standardDeviation;
    }
}

export enum MacroeconomicsColumns {
    ConsumerIndexPrice, LocalChangeRate
}

export enum IncomeColumns {
    TotalIncome, AvailableIncome, NotAvailableIncome, AvailableTransfer, NotAvailableTransfer, AvailableTax, AvailableContribution, Incentives, DerivatedIncomes, FederalTransfers
}

export enum ExpenseColumns {
    TotalExpense,
    Investment,
    PublicInvestment,
    Roster,
    GoodsAndServices,
    Transfer,
    Other,
    PDLabeled,
    PDNLabeled,
    Interests,
    FDEInterests,
    FreeDestinationIncome,
    SpecificDestinationIncome,
    PSLabeled,
    MSLabeled,
    GSLabeled,
    SocialBenefits,
    EXPDefaultExpense,
    UntaggedExpense,
    TaggedExpense,
    FreeDeterminationExpense,
    TASONLabeled,
    PSNLabeled,
    MSNLabeled,
    GSNLabeled,
    BINLabeled,
    BILabeled,
    INVNolaveled,
    INVolaveled,
    FDEinvestment,
    EXPinvestment,
    UnlabeledPublicInvestment,
    LabeledPublicInvestment
}

export enum DebtColumns {
    TotalDebt,
    DebtService,
    TotalAmortizations,
    TotalInterests,
    HistoricVariableRateDomesticCurrency,
    HistoricFixedRateDomesticCurrency,
    NewVariableRateDomesticCurrency,
    NewFixedRateDomesticCurrency,
    HistoricVariableRateForeignCurrency,
    HistoricFixedRateForeignCurrency,
    NewVariableRateForeignCurrency,
    NewFixedRateForeignCurrency,
    ForeignRate,
    LocalRate,
    LocalCurrencyAmortizationFixeRate,
    LocalCurrencyAmortizationVariableRate,
    ForeignCurrencyAmortizationFixeRate,
    ForeignCurrencyAmortizationVariableRate,
    AmortizationDebtFixedRate,
    AmortizationDebtVariableRate
}

export enum BalanceColumns {
    PrimaryBalance,
    FiscalBalance
}