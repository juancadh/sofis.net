﻿// ReSharper disable InconsistentNaming
export class IncomeData {
    public year: number;
    public totalIncome: number;
    public freeDestinationIncome: number;
    public notAvailableIncome: number;
    public freeDestinationTaxes: number;
    public tenureAndUseOfVehicles: number;
    public others: number;
    public notFreeDestinationTaxes: number;
    public payroll: number;
    public furniturePropertyDisposalVehicleAcquisition: number;
    public lodging: number;
    public lotteriesRafflesAndSweepstakes: number;
    public totalRightsAndProducts: number;
    public exploitation: number;
    public contributions: number;
    public contributionImprovements: number;
    public federalTaxRevenueSharesAndIncentives: number;
    public generalFund: number;
    public inspectionFundForFederalEntities: number;
    public specialTaxOnProductionAndServices: number;
    public taxOnTenureOrUseOfVehicles: number;
    public municipalDevelopmentFund: number;
    public newCarTax: number;
    public rfp: number;
    public otherParticipableIncentives: number;
    public feief: number;
    public notFreeTransfersDestination: number;
    public cfHealthServices: number;
    public cfStrengtheningMunicipalities: number;
    public cfStrengtheningFederalEntities: number;
    public cfMunicipalSocialInfraestructure: number;
    public cfPublicSecurityStatesFederalDistrict: number;
    public cfAdultTechnologicalEducation: number;
    public cfStateSocialInfraestructure: number;
    public multipleContributionFund: number;
    public transfersHigherAndHigherSecondaryEducation: number;
    public popularInsurance: number;
    public partnershipProgramField: number;
    public regionalDevelopmentProgram: number;
    public agreedProgramsConagua: number;
    public secretaryPublicEducation: number;
    public healthSecretary: number;
    public secretaryFinancePublicCredit: number;
    public secretaryCommunicationsTransportation: number;
    public publicSecurity: number;
    public metropolitanFunds: number;
    public remainderTransfersAgreementsFederalGovernment: number;
    public trusteeInfraestructureStates: number;
    public encarb: number;
    public nationalSportsCommissionSportsInfraestructure: number;
    public collegeScientificTechnologicalStudiesStateCoahuila: number;
    public programControlFederalizedCase: number;
    public secretaryEnvironmentNaturalResources: number;
    public secretarySocialDevelopment: number;
}