﻿// ReSharper disable InconsistentNaming
export class DebtData {
    public year: number;
    public totalDebt: number;
    public historicalDebtVariableRate: number;
    public historicalDebtFixedRate: number;
    public totalAmortization: number;
    public amortizationHistoricalDebtVariableRate: number;
    public amortizationHistoricalDebtFixedRate: number;
    public totalPaymentForInterests: number;
    public interestRateHistoricalDebtVariableRate: number;
    public interestDebtHistoricalRateFixedRate: number;
}