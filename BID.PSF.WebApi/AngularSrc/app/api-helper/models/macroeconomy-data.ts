﻿export class MacroeconomyData {
    public year: number;
    public gdp: number ;
    public nacionalCpi: number ;
    public usdExchangeRate: number ;
    public gdpEntity: number ;
    public internalCpi: number ;
    public internalInterestRate: number ;
    public domesticInterestRate: number ;
}