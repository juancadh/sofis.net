export class SofisTotalDebt {
    model: string;
    external_series: ExternalSeries;
    datapoints: number;
    T: number;
    D0: number;
    OMT: VarMatrix;
    mu_0: VarVector;
    mu_1: VarMatrix;
    rtml: number[];
    rtme: number[];
    et: number[];
    gildt: number[];
    gidet: number[];
    ggpt: number[];
    X: number;
    alpha: number;
    beta: number; // Must be between 0 and 1
    IDE0: number;
    ILD0: number;
    GP0: number;
    umbrals_for_sensitivity_analysis: number[];
}

export class ExternalSeries {
    rtml: number[];
    rtme: number[];
    et: number[];
    gildt: number[];
    gidet: number[];
    ggpt: number[];
}

export class VarMatrix {
    rtml: [number, number, number, number, number, number];
    rtme: [number, number, number, number, number, number];
    et: [number, number, number, number, number, number];
    gildt: [number, number, number, number, number, number];
    gidet: [number, number, number, number, number, number];
    ggpt: [number, number, number, number, number, number];
}

export class VarVector {
    rtml: [number];
    rtme: [number];
    et: [number];
    gildt: [number];
    gidet: [number];
    ggpt: [number];
}