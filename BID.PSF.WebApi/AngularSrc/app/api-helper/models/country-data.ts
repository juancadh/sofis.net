﻿import { MunicipalityData } from './municipality-data';

export class CountryData {
    public id: number;
    public name: string;
    public municipalities: MunicipalityData[];
}