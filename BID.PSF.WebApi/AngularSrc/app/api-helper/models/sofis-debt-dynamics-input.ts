export class SofisDebtDynamicsInput {
    shocks: SofisDebtDynamicsShocks;
    dml: number[];
    dme: number[];
    rml: number[];
    rme: number[];
    tcambio: number[];
    ingt: number[];
    teg: number[];
    pint: number[];
    Tini: number;
    Tproy: number;
}

export class SofisDebtDynamicsShocks {
    ingreso_total: DebtShock;
    tipo_cambio: DebtShock;
    tasa_int_moneda_local: DebtShock;
    tasa_int_moneda_extranjera: DebtShock;
}

export class DebtShock {
    Ti: number;
    Tf: number;
    shock: number;
}

