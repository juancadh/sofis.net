﻿export class CatalogueData {
    public id: number;
    public name: string;
    public selected: boolean;
    public Icon: string = null;
}