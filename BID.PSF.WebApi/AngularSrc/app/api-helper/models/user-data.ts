﻿export class UserData {
    public name: string;
    public surname: string;
    public email: string;
    public country: number;
    public language: string;
    public organization: string;
    public position: string;
    public role: string;
    public recievesUpdates: boolean;
    public subtopics: Array<string>;
}