export interface PythonTotalDebt {
    data: PythonChartDataPoints[];
}

export interface PythonChartDataPoints {
    x: number,
    y: number[]
}
