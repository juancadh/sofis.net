export class SofisTotalDebtResult {
    totaldebt: TotalDebt;
}

export class TotalDebt {
    PROBA: number;
    ffit: number[];
    ffitinf: number[];
    ffitsup: number[];
    l1: number[];
    series_perentiles: SeriesPerentiles[];
    sensitivity_analysis: number[];
}

export class SeriesPerentiles {
    inf: number[];
    p_inf: number;
    p_sup: number;
    sup: number[];
}
