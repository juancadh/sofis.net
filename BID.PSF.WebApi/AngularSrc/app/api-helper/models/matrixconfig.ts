export interface MatrixConfig {
    VarinterceptLevels: any[],
    VarCoVarMatrix: any[],
    VarCoeficientsMatrix: any[],
    Alpha: number,
    Beta: number,
    OtherD0: number,
    OtherX: number
}