export interface ChartJSData {
    label?: string,
    labels?: string[],
    data: number[],
    borderWidth?: number,
    backgroundColor?: string,
    borderColor?: string
}