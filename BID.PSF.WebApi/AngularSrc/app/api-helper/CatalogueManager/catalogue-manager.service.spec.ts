import { TestBed, inject } from '@angular/core/testing';

import { CatalogueManagerService } from './catalogue-manager.service';

describe('CatalogueManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CatalogueManagerService]
    });
  });

  it('should be created', inject([CatalogueManagerService], (service: CatalogueManagerService) => {
    expect(service).toBeTruthy();
  }));
});
