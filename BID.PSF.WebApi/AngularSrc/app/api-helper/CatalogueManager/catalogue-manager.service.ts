import { SessionService } from './../session.service';
import { Injectable } from '@angular/core';
import { ApiService } from './../api.service';
import { ApiEndpoints } from './../api-endpoints';

@Injectable()
export class CatalogueManagerService {

  constructor(private _apiService: ApiService, private _sessionService: SessionService) { }

  // Catalogue
  public async create(data: any) {
    //const params = `?catalogueId=${this._sessionService.getCatalogue()}`;
    const task = await this._apiService.post(ApiEndpoints.catalogue_manager_create, null, data, { ['Content-Type']: 'application/json', Accept: 'application/json' });
    return task;
  }

  public async update(data: any) {
    //const params = `?catalogueId=${this._sessionService.getCatalogue()}`;
    const task = await this._apiService.put(ApiEndpoints.catalogue_manager_update, null, data);
    return task;
  }

  public async clone(id: number, name: string) {
    const task = await this._apiService.post(ApiEndpoints.catalogue_manager_clone + '?catalogueId=' + id + '&name=' + name, null, null);
    return task;
  }

  public async assign(id: number) {
    const task = await this._apiService.put(ApiEndpoints.catalogue_manager_assign, null, null);
    return task;
  }

  public async delete(id: number) {
    const task = await this._apiService.delete(ApiEndpoints.catalogue_manager_delete, null);
    return task;
  }

  // Columns

  public async createColumn(data: any) {
    const task = await this._apiService.post(ApiEndpoints.catalogue_manager_create_column, null, data);
    return task;
  }

  public async updateColumn(data: any) {
    const task = await this._apiService.put(ApiEndpoints.catalogue_manager_update_column, null, data);
    return task;
  }

  public async deleteColumn(columnId) {
    const param = `?columnId=${columnId}`;
    const task = await this._apiService.delete(ApiEndpoints.catalogue_manager_delete_column + param, null);
    return task;
  }
}
