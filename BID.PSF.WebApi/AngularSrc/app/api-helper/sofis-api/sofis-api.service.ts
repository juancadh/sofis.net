import { Injectable } from '@angular/core';
import { SofisTotalDebt } from '../models/sofis-total-debt';
import { SofisStandardAproximationInput } from '../models/sofis-standard-aproximation-input';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { ApiEndpoints } from '../api-endpoints';
import { SofisDebtDynamicsInput } from '../models/sofis-debt-dynamics-input';
import { MfmpDynamicResults } from '../models/sofis-mfmp';
import { SessionService } from '../session.service';
@Injectable()
export class SofisApiService {

    constructor(private http: HttpClient, private _sessionService: SessionService) {
    }

    //ASD

    public getTotalDebt(data: SofisTotalDebt) {
        var url = `${ApiEndpoints.modelApiUrl}/${ApiEndpoints.asd_totaldebt}?code=${ApiEndpoints.access_token}`;
        return this.commonHttpPost(url, data, null);
    }

    public getStandardAproximationInputs(data: SofisStandardAproximationInput) {
        var url = `${ApiEndpoints.modelApiUrl}/${ApiEndpoints.asd_standardaproximation_inputs}?code=${ApiEndpoints.access_token}`;
        return this.commonHttpPost(url, data, null);
    }

    public getStandardAproximationResults(data: SofisStandardAproximationInput) {
        var url = `${ApiEndpoints.modelApiUrl}/${ApiEndpoints.asd_standardaproximation_results}?code=${ApiEndpoints.access_token}`;
        return this.commonHttpPost(url, data, null);
    }

    public getDebtDynamicsResults(data: SofisDebtDynamicsInput) {
        var url = `${ApiEndpoints.modelApiUrl}/${ApiEndpoints.asd_dynamicdebt}?code=${ApiEndpoints.access_token}`;
        return this.commonHttpPost(url, data, null);
    }

    // MFMP
    public getMFMPDynamicsResults(data: MfmpDynamicResults) {
        const url = `${ApiEndpoints.modelApiUrl}/${ApiEndpoints.mfmp}?code=${ApiEndpoints.access_token}`;
        return this.commonHttpPostPromise(url, data, null);
    }

    // Common
    private commonHttpPost(url: string, data: any, headers: HttpHeaders) {
        return this.http.post(url, data, { headers: headers });
    }
    // Common promise
    private commonHttpPostPromise(url: string, data: any, headers: HttpHeaders) {
        return this.http.post(url, data, { headers: headers }).toPromise();
    }
}
