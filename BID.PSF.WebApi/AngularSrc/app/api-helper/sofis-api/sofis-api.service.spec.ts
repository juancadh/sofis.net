import { TestBed, inject } from '@angular/core/testing';

import { SofisApiService } from './sofis-api.service';

describe('SofisApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SofisApiService]
    });
  });

  it('should be created', inject([SofisApiService], (service: SofisApiService) => {
    expect(service).toBeTruthy();
  }));
});
