﻿import { default as app_settings } from './../../assets/Config/AppSettings.json';

export class ApiEndpoints {

    public static baseUrl = window.location.origin + '/api/';
    //Python
    //public static modelApiUrl = 'http://nachokun.pythonanywhere.com/';
    public static modelApiUrl = app_settings.HOST_API;


    public static profile_getProfile = 'profile';
    public static profile_updateProfile = 'profile';
    public static profile_getLists = 'profile/lists';
    public static profile_setLang = 'profile/setlang';

    public static profile_resetPassword = 'users/reset-password';
    public static profile_forgotPassword = 'users/forgot-password';

    public static app_getFirstProjectionYear = 'mfmp/firstprojectionyear';

    public static mfmp_getAproximacionInercial = 'mfmp/inertial-approximation';
    public static mfmp_getLastFiveYearsApproximation = 'mfmp/last-five-years-approximation';
    public static mfmp_getFixedApproximation = 'mfmp/fixed-parametric-approximation';
    public static mfmp_getVariableApproximation = 'mfmp/variable-parametric-approximation';

    public static mfmp_getFixedApproximationStructure = 'mfmp/fixed-approximation-structure';
    public static mfmp_getVariableApproximationStructure = 'mfmp/variable-approximation-structure';
    public static mfmp_getRulesStructure = 'mfmp/rules';
    public static mfmp_getRulesCharts = 'mfmp/charts';
    public static mfmp_postLimits = 'mfmp/limits';

    public static asd_getStandardApproach = 'asd/standard-approach';
    public static asd_getSensitivityAnalysis = 'asd/standard-approach/sensitivity-analysis';

    public static asd_getDynamicsOfDebt = 'asd/dynamics-of-debt';
    public static asd_getDynamicYears = 'asd/dynamics-of-debt/dynamics-years';
    public static asd_getStandardAproximationData = 'asd/standard-aproximation-data';
    public static asd_debtdynamicsdata = 'asd/debt-dynamics-data/';
    public static asd_set_matrix_values = 'asd/set-fanchartconfig';

    public static fanChart_getFanChart = 'asd/fan-chart';
    public static fanChart_getProbabilisticAssumptions = 'asd/fan-chart/probabilistic-assumptions';
    public static fanChart_getFutureYears = 'asd/fan-chart/years';
    public static fanChart_getProbability = 'asd/fan-chart/probability';
    public static fanChart_getThreshold = 'asd/fan-chart/threshold';
    public static fanChart_getInitialConditions = 'asd/fan-chart/getInitialConditions';

    public static catalogues_getCatalogues = 'catalogues';
    public static catalogues_getTableStructure = 'catalogues/structure';
    public static catalogues_getSubcategories = 'catalogues/subcategories';
    public static catalogues_getCurrencyUnits = 'catalogues/currency-units';
    public static catalogues_selectCatalogue = 'catalogues/select';
    public static catalogues_setHistoricYear = 'catalogues/setHistoricYear';

    public static catalogues_getValues = 'catalogues/values';
    public static catalogues_postValues = 'catalogues/values';
    public static catalogues_postDynamicColumn = 'catalogues/dynamic-column';
    public static catalogues_deleteDynamicColumn = 'catalogues/dynamic-column';

    public static catalogues_datasetyears = 'catalogues/dataset/years';

    // Catalogue Manager

    public static catalogue_manager_create = 'cataloguemanager/create';
    public static catalogue_manager_create_column = 'cataloguemanager/createColumn';
    public static catalogue_manager_update_column = 'cataloguemanager/updateColumn';
    public static catalogue_manager_delete_column = 'cataloguemanager/deleteColumn';
    public static catalogue_manager_update = 'cataloguemanager/update';
    public static catalogue_manager_clone = 'cataloguemanager/clone';
    public static catalogue_manager_assign = 'cataloguemanager/assign';
    public static catalogue_manager_delete = 'cataloguemanager/delete';

    // Model Endpoints (Python)
    public static access_token = app_settings.TOKEN;
    public static asd_standardaproximation_inputs = app_settings.API_ENDPOINTS.BASE + app_settings.API_ENDPOINTS.ASD.STANDARD_APROXIMATION.INPUTS;
    public static asd_standardaproximation_results = app_settings.API_ENDPOINTS.BASE + app_settings.API_ENDPOINTS.ASD.STANDARD_APROXIMATION.RESULTS;
    public static asd_totaldebt = app_settings.API_ENDPOINTS.BASE + app_settings.API_ENDPOINTS.ASD.TOTAL_DEBT;
    public static asd_dynamicdebt = app_settings.API_ENDPOINTS.BASE + app_settings.API_ENDPOINTS.ASD.DYNAMIC_DEBT;
    public static mfmp = app_settings.API_ENDPOINTS.BASE + app_settings.API_ENDPOINTS.MFMP;
}