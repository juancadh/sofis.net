import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from './api.service';
import { ApiEndpoints } from './api-endpoints';

@Injectable()
export class LanguageService {

  constructor(private translate: TranslateService, private apiService: ApiService) {
    this.loadDBUserLanguage();
  }

  async loadDBUserLanguage() {
    const browserLang = this.translate.getBrowserLang();
    this.translate.addLangs(['en', 'es', 'fr', 'pt']);
    this.translate.setDefaultLang(browserLang);
    return await this.apiService.get(`${ApiEndpoints.profile_getProfile}?culture=${browserLang}`, null).then(r => {
      const langencodedName = r.data.language.encodedName;
      if (browserLang && langencodedName) {
        this.translate.use(langencodedName.match(/en|es|fr|pt/) ? langencodedName : browserLang.match(/en|es|fr|pt/) ? browserLang : 'es');
        this.setLocalLang(langencodedName.match(/en|es|fr|pt/) ? langencodedName : browserLang.match(/en|es|fr|pt/) ? browserLang : 'es');
      }

      return r;
    }).catch(e => e);
  }

  loadLSUserLanguage() {
    const localLang = this.getLocalLang();
    this.translate.use(localLang);
  }

  setLocalLang(lang: string) {
    localStorage.setItem('userLang', lang);
  }

  getLocalLang() {
    return localStorage.getItem('userLang');
  }

}
