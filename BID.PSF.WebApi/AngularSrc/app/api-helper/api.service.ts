﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';

import { ApiResult } from './api-result'
import { ApiEndpoints } from './api-endpoints'

import { SessionService } from './session.service';

import { OktaAuthService } from '@okta/okta-angular';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService {
    // Resolve HTTP using the constructor
    constructor(private http: Http, private oktaAuth: OktaAuthService, private _sessionService: SessionService) { }

    private addCatalogueId(relativeURL: string): string {
        var condition = relativeURL.indexOf('?');
        if (condition > -1)
            return `&catalogueId=${this._sessionService.getCatalogue()}`;
        else
            return `?catalogueId=${this._sessionService.getCatalogue()}`;
    }

    async get(relativeUrl: string, aditionalUrlData: string[], adicionalHeaders: any = {}): Promise<ApiResult> {
        relativeUrl += this.addCatalogueId(relativeUrl);
        const accessToken = await this.oktaAuth.getAccessToken();
        let headers = new Headers({
            Authorization: 'Bearer ' + accessToken,
            ...adicionalHeaders
        });

        let finalUrl = this.buildUrl(relativeUrl, aditionalUrlData);

        // Add authorization header with token
        let requestOptions = this.getRequestOptions(headers);

        return this.http.get(finalUrl, requestOptions)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'))
            .toPromise();
    }

    async post(relativeUrl: string, aditionalUrlData: string[], body: any, adicionalHeaders: any = {}): Promise<ApiResult> {
        relativeUrl += this.addCatalogueId(relativeUrl);
        const accessToken = await this.oktaAuth.getAccessToken();
        let headers = new Headers({
            Authorization: 'Bearer ' + accessToken,
            ...adicionalHeaders
        });

        let finalUrl = this.buildUrl(relativeUrl, aditionalUrlData);

        // Add authorization header with token
        let requestOptions = this.getRequestOptions(headers);

        return this.http.post(finalUrl, body, requestOptions)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'))
            .toPromise();
    }

    async put(relativeUrl: string, aditionalUrlData: string[], body: any, adicionalHeaders: any = {}): Promise<ApiResult> {
        relativeUrl += this.addCatalogueId(relativeUrl);
        const accessToken = await this.oktaAuth.getAccessToken();
        let headers = new Headers({
            Authorization: 'Bearer ' + accessToken,
            ...adicionalHeaders
        });

        let finalUrl = this.buildUrl(relativeUrl, aditionalUrlData);

        // Add authorization header with token
        let requestOptions = this.getRequestOptions(headers);

        return this.http.put(finalUrl, body, requestOptions)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'))
            .toPromise();
    }

    async delete(relativeUrl: string, aditionalUrlData: string[]): Promise<ApiResult> {
        const accessToken = await this.oktaAuth.getAccessToken();
        let headers = new Headers({
            Authorization: 'Bearer ' + accessToken
        });

        let finalUrl = this.buildUrl(relativeUrl, aditionalUrlData);

        // Add authorization header with token
        let requestOptions = this.getRequestOptions(headers);

        return this.http.delete(finalUrl, requestOptions)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json() || 'Server error'))
            .toPromise();
    }

    // TODO: Implement post, delete, update, etc

    getRequestOptions(aditionalHeaders: Headers): RequestOptions {
        // TODO: Create base header with token
        let headers = new Headers();

        if (aditionalHeaders != null && aditionalHeaders != undefined)
            for (var key of aditionalHeaders.keys()) {
                if (!headers.has(key))
                    headers.append(key, aditionalHeaders.get(key));
            }

        let args = new RequestOptions({ headers: headers });

        return args;
    }

    buildUrl(url: string, aditionalData: string[]): string {
        let retUrl = ApiEndpoints.baseUrl;

        if (!retUrl.endsWith('/') && !url.startsWith('/'))
            retUrl = retUrl.concat('/');
        retUrl = retUrl.concat(url);

        if (aditionalData == undefined || aditionalData == null)
            return retUrl;

        let index = 0;
        for (let data in aditionalData) {
            if (!retUrl.endsWith('/') && !data.startsWith('/')) {
                retUrl = retUrl.concat('/');
                retUrl = retUrl.concat(data);
            }

            if (!retUrl.endsWith('/') && !data.startsWith('/') && data.endsWith('&')) {
                if (index == 0) {
                    retUrl = retUrl.concat('?');
                    retUrl = retUrl.concat(data);
                } else {
                    retUrl = retUrl.concat(data);
                }
            }

            index++;
        }

        return retUrl;
    }
}