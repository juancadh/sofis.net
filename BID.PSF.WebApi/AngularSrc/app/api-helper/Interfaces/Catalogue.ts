export interface Column {
   Index: number,
   ColumnName: string,
   InternalName: string,
   Category: number, // Macroeconomy = 0, Income, Expense, Debt
   CatalogueId: number,
   ParentId: number,
}
