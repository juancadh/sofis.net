export interface Alert {
    type: string,
    show: boolean,
    msg: string
}