import { NgxSmartModalService } from 'ngx-smart-modal';
import { LanguageService } from './language.service';
import { SessionService } from './session.service';
import { Injectable } from '@angular/core';
import { CatalogueData } from './models/catalogue-data';
import { FormGroup } from '@angular/forms';
import { ApiService } from './api.service';
import { ApiEndpoints } from './api-endpoints';
import { Subject } from 'rxjs';
import { Alert } from './Interfaces/Alert';

@Injectable()
export class TopNavLogicService {


  cropping = false;
  imageChangedEvent: any = '';
  croppedImage: any = '';

  username: string;
  catalogueList: CatalogueData[];
  catalogue: CatalogueData;
  actualCatalogueSelected: CatalogueData = { name: '', id: null, selected: true, Icon: null };
  language: any;
  langReady = false;
  catalogueAction = { value: '', key: null };
  myForm: FormGroup;
  showAlert = { type: '', show: false, msg: "" }
  isLoading = true;
  isLoadingChange: Subject<boolean> = new Subject<boolean>();
  externalAlert: Alert = { show: false, msg: '', type: '' };
  externalAlertChange: Subject<Alert> = new Subject<Alert>();


  constructor(private readonly apiService: ApiService,
    private SessionService: SessionService,
    private languageService: LanguageService,
    public modal: NgxSmartModalService,
  ) {
    this.isLoadingChange.subscribe(value => {
      this.isLoading = value;
    });

    this.externalAlertChange.subscribe(value => this.externalAlert = value);
  }

  async init() {
    this.setIsLoading(true);
    await this.getSelectedLang()
      .then(() => {
        this.getCatalogues();
        this.username = this.SessionService.getCurrentUser();
        this.setIsLoading(false);
      }).catch(e => {
        this.setIsLoading(false);
        console.error(e);
      });
  }

  setIsLoading(status) {
    this.isLoadingChange.next(status);
  }

  setShowExternalAlert(show = false) {
    this.externalAlert.show = show;
    this.externalAlertChange.next(this.externalAlert);
  }

  crop() {
    this.cropping = false;
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(image: string) {
    this.croppedImage = image;
  }

  imageLoaded() {
    // show cropper
  }
  loadImageFailed() {
    // show message
  }

  handlerCrop(event) {
    this.cropping = event;
  }

  async langHandler(lang) {
    this.langReady = false;
    const aditionalsHeaders = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    await this.apiService.put(ApiEndpoints.profile_setLang + `?lang=${lang}`, null, null, aditionalsHeaders).then(
      result => {
        this.language = result.data;
        this.langReady = true;
        this.init();
      },
      err => {
        console.log(err);
        this.langReady = true;
        this.init();
      });
  }

  async getSelectedLang() {
    await this.languageService.loadDBUserLanguage().then(result => {
      this.langReady = true;
      this.language = result.data.language;
      this.SessionService.setCatalogue(result.data.CurrentCatalogueId);
    }).catch(err => {
      console.log(err);
      this.langReady = true;
    });
  }

  alertsHandler(event) {
    this.modal.getModal('dataOperationsModal').close();
    if (event.type) {
      this.showAlert = {
        type: event.type,
        show: true,
        msg: event.detail
      }
    }
  }

  showAlertHandler(event, externalAlert = false) {

    if (event && !externalAlert) {
        this.showAlert.show = false;
    }

    if (event && externalAlert) {
      this.setShowExternalAlert(false);
    }
}

  openAction(action) {
    this.catalogueAction = action;
    this.modal.getModal('dataOperationsModal').open();
  }

  getCatalogues() {
    this.apiService.get(ApiEndpoints.catalogues_getCatalogues, null).then(
      result => {
        this.catalogueList = result.data;
        this.catalogue = this.catalogueList[0];
        this.setSelectedCatalogue(result.data);
      },
      err => {
        console.log(err);
      });
  }

  changeCatalogue(catalogue) {
    this.modal.getModal('catalogueModal').close();
    this.SessionService.setCatalogue(catalogue.id);
    this.apiService.post(ApiEndpoints.catalogues_selectCatalogue + '/' + catalogue.id, null, null)
      .then(
        () => {
          this.init();
        }).catch((e) => {
          console.log(e);
          this.init();
        });
  }

  setSelectedCatalogue = (catalogueList: CatalogueData[]) => {
    catalogueList.forEach(i => {
      if (i.selected === true) {
        this.actualCatalogueSelected.name = i.name;
        this.actualCatalogueSelected.id = i.id;
        this.actualCatalogueSelected.selected = i.selected;
        this.actualCatalogueSelected.Icon = i.Icon;
      }
    });
  }

}
