﻿export class ApiResult {
    constructor(
        public data: any,
        public result: string,
        public statusCode: number,
        public message: string
    ){}
}