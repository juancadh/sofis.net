import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percentage'
})
export class PercentagePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    // BasePoints to percentage values
    return args ? (value * 100).toFixed(2) + '%': (value * 100).toFixed(2);
  }

}
