import { LimitcategorylevelPipe } from './limitcategorylevel.pipe';

describe('LimitcategorylevelPipe', () => {
  it('create an instance', () => {
    const pipe = new LimitcategorylevelPipe();
    expect(pipe).toBeTruthy();
  });

  it('limitcategorylevel_Should', () => {
    const pipe = new LimitcategorylevelPipe();

    var dataValues = [
      { Id: 1, Category: 1, InternalName: 1, Account: 'A', Level: 0 },
      { Id: 2, Category: 1, InternalName: 2, Account: 'B', Level: 1 },
      { Id: 3, Category: 2, InternalName: 1, Account: 'C', Level: 0 },
      { Id: 4, Category: 2, InternalName: 2, Account: 'D', Level: 1 },
      { Id: 5, Category: 3, InternalName: 1, Account: 'E', Level: 0 },
      { Id: 6, Category: 3, InternalName: 2, Account: 'F', Level: 1 },
      { Id: 7, Category: 3, InternalName: 3, Account: 'F', Level: 2 }
    ];

    var expectValues = [
      { Id: 1, Category: 1, InternalName: 1, Account: 'A', Level: 0 },
      { Id: 2, Category: 1, InternalName: 2, Account: 'B', Level: 1 },
      { Id: 3, Category: 2, InternalName: 1, Account: 'C', Level: 0 },
      { Id: 4, Category: 2, InternalName: 2, Account: 'D', Level: 1 },
      { Id: 5, Category: 3, InternalName: 1, Account: 'E', Level: 0 },
    ];

    var resp = pipe.transform(dataValues, 3, 0);

    expect(resp).toContain(expectValues, dataValues);

  });

});
