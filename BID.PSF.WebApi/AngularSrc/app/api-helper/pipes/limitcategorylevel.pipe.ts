import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitcategorylevel'
})
export class LimitcategorylevelPipe implements PipeTransform {

  transform(value: any[], category?: number, levelLimit?: number): any {
    return value.filter(x => x.Category != category || (x.Category === category && x.Level <= levelLimit));
  }

}
