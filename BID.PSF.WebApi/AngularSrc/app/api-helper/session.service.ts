﻿export class SessionService {

    public setCurrentUser(currentUser) {
        sessionStorage.setItem('currentUser', JSON.stringify(currentUser));
    }

    public getCurrentUser() {
        const currentUser = localStorage.getItem('okta-token-storage');
        return currentUser != null ? JSON.parse(currentUser).idToken.claims.email : null;
    }

    public setCatalogue(id) {
        localStorage.setItem('catalogue', id)
    }

    public getCatalogue() {
        return localStorage.getItem('catalogue')
    }

    public setCatalogueType(name) {
        localStorage.setItem('catalogueType', name)
    }

    public getCatalogueType() {
        return localStorage.getItem('catalogueType')
    }

    public setFanchartConfig(fch) {
        return localStorage.setItem('fanchart-config', fch);
    }

    public getFanchartConfig() {
        return localStorage.getItem('fanchart-config');
    }
}