﻿import { NgModule, Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { OktaAuthService } from '@okta/okta-angular';

@NgModule({})

@Injectable()
export class AuthenticationRoutingModule implements CanActivate {
    isAuthenticated: boolean;

    constructor(
        private router: Router,
        private oktaAuth: OktaAuthService
    ) {
        this.oktaAuth.$authenticationState.subscribe(
            (isAuthenticated: boolean) => this.isAuthenticated = isAuthenticated
        );
    }

    async ngOnInit() {
        // Get the authentication state for immediate use
        this.isAuthenticated = await this.oktaAuth.isAuthenticated();
    }

    async canActivate() {
        this.isAuthenticated = await this.oktaAuth.isAuthenticated();
        if (this.isAuthenticated) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}