﻿// ReSharper disable InconsistentNaming
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/toPromise';

import { SessionService } from '../../api-helper/session.service';

import { OktaAuthService } from '@okta/okta-angular';

@Injectable()
export class AuthenticationService {
    private loginUrl = 'http://localhost:27758/oauth/token';
    //private loginUrl = 'http://psf-dev.azurewebsites.net/oauth/token';
    //private loginUrl = 'http://sofis-uat.azurewebsites.net/oauth/token';

    constructor(private http: Http, private router: Router, private SessionService: SessionService, private oktaAuth: OktaAuthService) { }

    login(user) {
        const authUser = `grant_type=password&username=${encodeURIComponent(user.email)}&password=${user.password}&client_id=DefaultClient`;

        return this.http.post(this.loginUrl, authUser).map((response: Response) => {
            var data = response.json();
            this.SessionService.setCurrentUser({
                'token': data.access_token,
                'userName': data.userName
            });
            return "";
        });
    }

    isLoggedIn() {
        return this.SessionService.getCurrentUser() != null;
    }

    logout() {
        this.oktaAuth.logout('/');
    }

    //logout() {
    //    this.SessionService.setCurrentUser(null);
    //    this.router.navigate(['/login']);
    //}
}