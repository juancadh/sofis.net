﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LoginComponent } from './login/login.component';
import { SharedModule } from './../Components/Shared/shared.module';

@NgModule({
    imports: [FormsModule, BrowserModule, AuthenticationRoutingModule, SharedModule],
    declarations: [LoginComponent],
})
export class AuthenticationModule { }