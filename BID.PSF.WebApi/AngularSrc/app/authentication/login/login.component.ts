﻿import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

import { LoginModel } from '../shared/login.model';
import { ApiEndpoints } from '../../api-helper/api-endpoints';

import { AuthenticationService } from '../shared/authentication.service';
import { ApiService } from '../../api-helper/api.service';
import { SessionService } from '../../api-helper/session.service';

import { OktaAuthService } from '@okta/okta-angular';
import {TranslateService} from '@ngx-translate/core';


@Component({
    moduleId: module.id.toString(),
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    isAuthenticated: boolean;

    constructor(
        private router: Router,
        private AuthenticationService: AuthenticationService,
        private ApiService: ApiService,
        private SessionService: SessionService,
        private oktaAuth: OktaAuthService,
        private translate: TranslateService
    ) {
        this.oktaAuth.$authenticationState.subscribe(
            (isAuthenticated: boolean) => this.isAuthenticated = isAuthenticated
        );
        translate.addLangs(['en', 'es','fr','pt']);
        translate.setDefaultLang('es');
        const browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|es|fr|pt/) ? browserLang : 'es');
    }

    async ngOnInit() {
        // Get the authentication state for immediate use
        this.isAuthenticated = await this.oktaAuth.isAuthenticated();
        if (this.isAuthenticated) {
            this.router.navigate(['/aproximacion-inercial']);
        }
    }

    async loginUser() {
        this.oktaAuth.loginRedirect('/aproximacion-inercial');
    }

    submitted = false;
    loading = false;
    user = {
        'email': null,
        'password': null
    };

    authMsg = null;
    redirect() {
        this.router.navigate(['/forgot-password']);
    }
}