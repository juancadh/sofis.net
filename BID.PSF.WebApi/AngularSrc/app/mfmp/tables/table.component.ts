﻿import { LimitcategorylevelPipe } from './../../api-helper/pipes/limitcategorylevel.pipe';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'mfmp-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.css']
})

export class TableComponent {
    @Input() settings = {};
    @Input() columns = [];
    @Input() colHeaders = [];
    @Input() data: object[];
    @Input() instance: string;
}