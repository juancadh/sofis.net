﻿import { Aproximation } from './../../api-helper/models/enums';
import { MfmpService } from './../mfmp-service/mfmp-service.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-last-five-years',
    templateUrl: './last-five-years.component.html',
    styleUrls: ['./last-five-years.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class LastFiveYearsApproximationComponent implements OnInit {
    title: string;

    constructor(
        private translateService: TranslateService,
        public readonly mfmp: MfmpService) {
    }

    async ngOnInit() {
        this.translateService.get('MFMP.FIVE_YEARS_APROX').subscribe(r => this.title = r);
        const years = parseInt(sessionStorage.getItem("yearsToSimulate.five"));

        if (this.mfmp.topNav) {
            await this.mfmp.initAproximation(Aproximation.FiveYears, years, 'five');
        }
    }

    loadingHandler(event) {
        this.mfmp.resetFlags(event);
        this.ngOnInit();
    }
}