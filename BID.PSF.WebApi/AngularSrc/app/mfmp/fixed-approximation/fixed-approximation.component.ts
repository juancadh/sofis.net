﻿import { Aproximation } from './../../api-helper/models/enums';
import { MfmpService } from './../mfmp-service/mfmp-service.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-fixed-approximation',
    templateUrl: './fixed-approximation.component.html',
    styleUrls: ['./fixed-approximation.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class FixedApproximationComponent implements OnInit {
    title: string;

    constructor(
        public mfmp: MfmpService,
        private translateService: TranslateService) {
    }

    async ngOnInit() {
        this.translateService.get('MFMP.FIXED_APROX').subscribe(r => this.title = r);
        const years = parseInt(sessionStorage.getItem("yearsToSimulate.fixed"));

        if (this.mfmp.topNav) {
            await this.mfmp.initAproximation(Aproximation.Fixed, years, 'fixed');
        }
    }

    loadingHandler(event) {
        this.mfmp.resetFlags(event);
        this.ngOnInit();
    }
}