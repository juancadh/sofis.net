import { SofisApiService } from './../../api-helper/sofis-api/sofis-api.service';
import { MfmpDynamicResults } from './../../api-helper/models/sofis-mfmp';
import * as enums from './../../api-helper/models/enums';

export class Pylogic {

    private body: MfmpDynamicResults = {
        rates: null,
        years_to_project: 5,
        serie: []
    };

    private catalogueType: number;

    constructor(public _catalogueType = 1) {
        this.catalogueType = _catalogueType;
    }

    public getMfMpAproxData(service: SofisApiService, dataValues: any[], dataStructure: any[], historics: number, rates: any = null): Promise<any> {
        let serie = [];
        let allValues = [];
        let fullRow = [];
        let maxHistoricYear = dataStructure[dataStructure.length - 1].data - 5;
        let minHistoricYear = maxHistoricYear - historics + 1;
        const years = this.getAllAproxYears(minHistoricYear, maxHistoricYear + 5);
        const promises: Promise<void>[] = [];

        dataValues.forEach((res, index) => {
            for (let i = minHistoricYear; i <= maxHistoricYear; i++) {
                serie.push(res[`${i}`] !== null ? res[`${i}`] : 0);
            }

            if (rates && !res.Autocalculated) {
                if (rates[index] && rates[index].length >= 5) {
                    //variable
                    this.body.rates = rates[index] && rates[index].map((i: number) => isNaN(i) ? 0 : i);
                } else {
                    //fixed
                    const val = rates[index] && rates[index].length > 0 ? rates[index][0] : 0;
                    this.body.rates = [val, val, val, val, val];
                }
            }
            else {
                this.body.rates = null;
            }

            this.body.serie = serie;
            if (!res.Autocalculated) {
                const promise = service.getMFMPDynamicsResults(this.body).then(
                    (calcValues: any) => {
                        if (calcValues) {
                            const apiValues = calcValues;

                            years.forEach((item, index) => {
                                allValues[item] = apiValues[index];
                            });

                            const sheet = {
                                ...allValues,
                                Id: res.Id,
                                Account: res.Account,
                                Autocalculated: res.Autocalculated,
                                BaseParent: res.BaseParent,
                                Category: res.Category,
                                Chart: res.Chart,
                                Table: res.Table,
                                Order: index,
                                Level: res.Level,
                                ParentId: res.ParentId,
                                InternalName: res.InternalName
                            };
                            fullRow.push(sheet);
                        }
                    },
                    (error: any) => {
                        console.log('ERROR!: ', error);
                        if (error.status === 500) {
                            let values = [];
                            years.forEach((item) => {
                                if (item > maxHistoricYear)
                                    values[item] = res[maxHistoricYear] || 0;
                                else
                                    values[item] = res[item] || 0;
                            });
                            // Aqui se llena fullRow con lo que halla venido de .net pero -sin proyecciones-
                            fullRow.push({
                                ...values,
                                Account: res.Account,
                                Autocalculated: res.Autocalculated,
                                BaseParent: res.BaseParent,
                                Category: res.Category,
                                Chart: res.Chart,
                                Table: res.Table,
                                Id: res.Id,
                                ParentId: res.ParentId,
                                Order: index,
                                InternalName: res.InternalName,
                                Level: res.Level
                            });
                        }
                    }
                );

                promises.push(promise);
            } else {
                let values = [];
                years.forEach((item) => {
                    if (item > maxHistoricYear)
                        values[item] = 0;
                    else
                        values[item] = res[item] != undefined ? res[item] : 0;
                });
                fullRow.push({
                    ...values,
                    Account: res.Account,
                    Autocalculated: res.Autocalculated,
                    BaseParent: res.BaseParent,
                    Category: res.Category,
                    Chart: res.Chart,
                    Table: res.Table,
                    ParentId: res.ParentId,
                    Id: res.Id,
                    Order: index,
                    InternalName: res.InternalName,
                    Level: res.Level
                });
            }

            serie.splice(0, serie.length);

        });
        return Promise.all(promises).then(() => {
            return new Promise(resolve => resolve(this.getTreeUpwardSumatory(fullRow, years.filter(y => y > maxHistoricYear))));
        });
    }

    public orderRows(list: any) {
        let newList = list.slice().sort(function (a, b) {
            if (a.Order < b.Order) {
                return -1;
            }
            if (a.Order > b.Order) {
                return 1;
            }
            return 0;
        });
        return newList;
    }

    private getAllAproxYears(minYear, maxYear) {
        let getAllAproxYears = [];
        for (var j = minYear; j <= maxYear; j++) {
            getAllAproxYears.push(j);
        }
        return getAllAproxYears;
    }

    private getTreeUpwardSumatory(dataValues: any[], projectionY: number[]): any {
        let sheetNodes = dataValues.filter(n => !n.Autocalculated);
        sheetNodes.forEach((data) => {
            let currentChild = data;
            var treeIndex = dataValues.findIndex(i => i.Id == data.ParentId);
            while (treeIndex >= 0) {
                projectionY.forEach(year => {
                    dataValues[treeIndex][year] += currentChild[year];
                });
                treeIndex = dataValues.findIndex(i => i.Id == dataValues[treeIndex].ParentId && i.Category < enums.ColumnCategory.PrimaryBalance);
            }
        });

        // Calculo y agrego las columnas de valance, Filtro datos de deuda, de esta forma solo se mostrara la Deuda Total (Buisness Requeriment).
        return this.addBalanceCategories(dataValues, projectionY).filter(x => x.Category != enums.ColumnCategory.Debt || (x.Category === enums.ColumnCategory.Debt && x.InternalName === enums.DebtColumns.TotalDebt));
    }

    private addBalanceCategories(dataValues: any[], projectionY: number[]): any[] {
        let balanceNodes = dataValues.filter(n => n.Category > enums.ColumnCategory.DebtService);

        //Total income result
        var ti = dataValues.find(x => x.Category == enums.ColumnCategory.Income && x.InternalName == enums.IncomeColumns.TotalIncome);

        //Total Expense result
        var te = dataValues.find(x => x.Category == enums.ColumnCategory.Expense && x.InternalName == enums.ExpenseColumns.TotalExpense);

        //Labeled result
        var _ge = dataValues.find(x => enums.ColumnCategory.Expense == x.Category && x.InternalName == enums.ExpenseColumns.PDNLabeled);

        //Not labeled result
        var _gne = dataValues.find(x => enums.ColumnCategory.Expense == x.Category && x.InternalName == enums.ExpenseColumns.PDLabeled);

        // Interests result
        var _gp = dataValues.find(x => enums.ColumnCategory.Expense == x.Category && x.InternalName == enums.ExpenseColumns.Interests);

        // FDEInterests result
        var _gld = dataValues.find(x => enums.ColumnCategory.Expense == x.Category && x.InternalName == enums.ExpenseColumns.FDEInterests);

        balanceNodes.forEach(data => {
            var treeIndex = dataValues.findIndex(i => i.Category == data.Category);

            // Balance primario Category 5
            // Formula -> Balance Primario = Ingreso Total - Gasto Total + Intereses
            // Servicio de la deuda = Cat Mex(Id -> 1) => comprende las subcategorías tanto en gastos etiquetados como no etiquetados.
            // Servicio de la deuda = Cat STD(Id -> 2) => comprende las subcategorías tanto en gastos predeterminados como de libre destinacion.
            if (data.Category == enums.ColumnCategory.PrimaryBalance) {


                projectionY.forEach(y => {

                    var totalIncome = ti && Object.keys(ti).includes(y.toString()) ? ti[y] : 0;
                    var totalExpense = te && Object.keys(te).includes(y.toString()) ? te[y] : 0;

                    /*DebtService Operations*/
                    var interests = 0;
                    if (this.catalogueType === enums.Catalogues.Mexico) {
                        var ge = _ge && Object.keys(_ge).includes(y.toString()) ? _ge[y] : 0;
                        var gne = _gne && Object.keys(_gne).includes(y.toString()) ? _gne[y] : 0;
                        interests = ge + gne;
                    } else {
                        var gp = _gp && Object.keys(_gp).includes(y.toString()) ? _gp[y] : 0;
                        var gld = _gld && Object.keys(_gld).includes(y.toString()) ? _gld[y] : 0;
                        interests = gp + gld;
                    }

                    /*DebtService Operations*/
                    dataValues[treeIndex][y] = totalIncome - totalExpense + interests;
                });
            }

            // Balance fiscal Category 6
            //Formula -> Balance Fiscal = Ingreso Total - Gasto Total

            if (data.Category == enums.ColumnCategory.FiscalBalance) {
                projectionY.forEach(y => {
                    var totalIncome = ti && Object.keys(ti).includes(y.toString()) ? ti[y] : 0;
                    var totalExpense = te && Object.keys(te).includes(y.toString()) ? te[y] : 0;
                    dataValues[treeIndex][y] = totalIncome - totalExpense;
                });
            }
        });

        return dataValues;

    }
}