﻿import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import { HotRegisterer } from 'angular-handsontable';
import { ApiEndpoints } from '../../api-helper/api-endpoints';
import { ApiService } from '../../api-helper/api.service';

import * as html2canvas from 'html2canvas';
import { saveAs } from 'file-saver';

import * as $ from 'jquery';
import 'jqueryui';

@Component({
    selector: 'app-rules',
    templateUrl: './rules.component.html',
    styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit {
    title: string
    instance: string = "hotInstance";
    noDataMessage: string = "addas";

    constructor(private hotRegisterer: HotRegisterer, private apiService: ApiService, private translateService: TranslateService) {
    }

    public ShowedRules = [
        { selected: true, id: 1 },
        { selected: true, id: 2 },
        { selected: true, id: 3 },
        { selected: true, id: 4 },
        { selected: true, id: 5 },
        { selected: true, id: 6 },
        { selected: true, id: 7 },
        { selected: true, id: 8 },
        { selected: true, id: 9 },
    ];

    public rulesReady = false;
    public dataReady = false;
    public chartsReady = false;
    public noData = false;
    public isLoading = false;
    public error = false;
    public chartWidth = 370;
    public settings = {
        colHeaders: true,
        columnHeaderHeight: 50,
        rowHeaders: false,
        className: "htCenter",
        //manualColumnResize: true,
        fillHandle: {
            autoInsertRow: false
        },
        wordWrap: false,
        preventOverflow: 'horizontal',
        viewportColumnRenderingOffset: 100,
    }

    public columns = [];
    public colHeaders = [];
    public data: object[];

    public chartType: string = 'line';
    public chartLegends: boolean = false;
    public chartOptions: any = {
        scales: {
            xAxes: [
                {
                    offset: true,
                    gridLines: {
                        drawBorder: false,
                        display: false
                    }
                }
            ],
            yAxes: [
                {
                    gridLines: {
                        drawBorder: false,
                        color: 'rgb(181, 181, 181)',
                        borderDash: [8, 4],
                        display: true
                    },
                    ticks: {
                        //min: 0,
                        //max: 100,
                        callback: (value) => {
                            return value + "%";
                        }
                    },
                }
            ]
        },
        annotation: {
            annotations: [
            ],
        },
        elements: {
            line: {
                tension: 0
            },
            point: {
                radius: 2,
                hitRadius: 2,
                hoverRadius: 4,
                hoverBorderWidth: 2
            }
        },
        responsive: true,
        maintainAspectRatio: false
    };
    public chartColors: Array<any> = [
        {
            backgroundColor: 'transparent',
            borderColor: 'black'
        },
        {
            backgroundColor: 'transparent',
            borderColor: '#37BA9D'

        }
    ];

    public allData: Array<any>;

    public firstproyectedyear;

    public ruleOneData: Array<any>;
    public ruleTwoData: Array<any>;
    public ruleThreeData: Array<any>;
    public ruleFourData: Array<any>;
    public ruleFiveData: Array<any>;
    public ruleSixData: Array<any>;
    public ruleSevenData: Array<any>;
    public ruleEightData: Array<any>;
    public ruleNineData: Array<any>;

    limit1: string;
    limit2: string;
    limit3: string;
    limit4: string;
    limit5: string;
    limit6: string;
    limit7: string;
    limit8: string;
    limit9: string;

    ngOnInit(): void {
        this.translateService.get('SHARED.RULES').subscribe(r => this.title = r);
        this.translateService.get('RULES.NOT_DATA_MESSAGE').subscribe(t => this.noDataMessage = t);
        this.loadTable();
        this.loadRules();
        this.setAnnotationsToYearsProyected();
    }

    loadTable(): void {
        this.isLoading = true;
        this.apiService.get(ApiEndpoints.mfmp_getRulesStructure, null).then(
            result => {
                this.columns = result.data.Structure;
                this.limit1 = result.data.Data.Data[0].Limit;
                this.limit2 = result.data.Data.Data[1].Limit;
                this.limit3 = result.data.Data.Data[2].Limit;
                this.limit4 = result.data.Data.Data[3].Limit;
                this.limit5 = result.data.Data.Data[4].Limit;
                this.limit6 = result.data.Data.Data[5].Limit;
                this.limit7 = result.data.Data.Data[6].Limit;
                this.limit8 = result.data.Data.Data[7].Limit;
                this.limit9 = result.data.Data.Data[8].Limit;

                this.rulesReady = true;

                setTimeout(() => {
                    const limits = document.getElementsByClassName('rule-input');

                    for (let i = 0; i < limits.length; i++) {
                        let number = parseFloat((limits[i] as HTMLInputElement).value);
                        if (isNaN(number)) number = 0;
                        (limits[i] as HTMLInputElement).value = number.toFixed(2) + "%";
                        limits[i].addEventListener('keyup', (e: KeyboardEvent) => {
                            if (e.keyCode === 13) {
                                (e.target as HTMLElement).blur();
                            }
                        }, false);
                    }

                    var ruleTDs = document.getElementsByClassName('rule-input-td');

                    for (let i = 0; i < ruleTDs.length; i++) {
                        ruleTDs[i].id = result.data.Data.Data[i].Rule;
                    }
                }, 1000);
            },
            err => {
                this.error = true;
                this.isLoading = false;
                console.log(err);
            });
    }

    loadRules(): void {
        this.apiService.get(ApiEndpoints.mfmp_getRulesCharts, null).then(
            result => {
                if (result.data.Data.length === 0) {
                    this.noData = true;
                    this.isLoading = false;
                    return;
                }
                this.allData = result.data.Data;

                setTimeout(() => {
                    this.loadCharts();
                    this.dataReady = true;
                    this.chartsReady = true;
                    this.isLoading = false;
                }, 0);
            },
            err => {
                this.error = true;
                this.isLoading = false;
                console.log(err);
            });
    }

    loadCharts() {
        this.rule1();
        this.rule2();
        this.rule3();
        this.rule4();
        this.rule5();
        this.rule6();
        this.rule7();
        this.rule8();
        this.rule9();

        setTimeout(() => {
            $("#sortable").sortable({
                handle: $(".drag")
            });
            $(".half-box").disableSelection();
        }, 0);
    }

    ruleOneLabels: Array<any>;
    ruleTwoLabels: Array<any>;
    ruleThreeLabels: Array<any>;
    ruleFourLabels: Array<any>;
    ruleFiveLabels: Array<any>;
    ruleSixLabels: Array<any>;
    ruleSevenLabels: Array<any>;
    ruleEightLabels: Array<any>;
    ruleNineLabels: Array<any>;

    rule1(): void {
        this.ruleOneData = [];
        this.ruleOneLabels = [];

        let ruleData = this.allData[0];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleOneData[0] = {};
        this.ruleOneData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleOneData[0].label = t);

        this.ruleOneData[1] = {};
        this.ruleOneData[1].data = [];
        this.translateService.get('GRAPHS.LIMITS').subscribe(t => this.ruleOneData[1].label = t);

        for (let i = 0; i < years.length; i++) {
            this.ruleOneData[0].data[i] = ruleData[years[i]];
            this.ruleOneData[1].data[i] = parseInt(this.limit1);
            this.ruleOneLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk1") as HTMLInputElement;
            var input = document.getElementById("limit1") as HTMLInputElement;

            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            chk.setAttribute("title", this.noDataMessage);
        }
    }

    rule2(): void {
        this.ruleTwoData = [];
        this.ruleTwoLabels = [];

        let ruleData = this.allData[1];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleTwoData[0] = {};
        this.ruleTwoData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleTwoData[0].label = t);

        this.ruleTwoData[1] = {};
        this.ruleTwoData[1].data = [];
        this.translateService.get('GRAPHS.LIMITS').subscribe(t => this.ruleTwoData[1].label = t);

        for (let i = 0; i < years.length; i++) {
            this.ruleTwoData[0].data[i] = ruleData[years[i]];
            this.ruleTwoData[1].data[i] = parseInt(this.limit2);
            this.ruleTwoLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk2") as HTMLInputElement;
            var input = document.getElementById("limit2") as HTMLInputElement;

            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            chk.setAttribute("title", this.noDataMessage);
        }
    }

    rule3(): void {
        this.ruleThreeData = [];
        this.ruleThreeLabels = [];

        let ruleData = this.allData[2];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleThreeData[0] = {};
        this.ruleThreeData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleThreeData[0].label = t);


        this.ruleThreeData[1] = {};
        this.ruleThreeData[1].data = [];
        this.translateService.get('GRAPH.LIMITS').subscribe(t => this.ruleThreeData[1].label = t);

        this.ruleThreeData[1].label = "Limits";

        for (let i = 0; i < years.length; i++) {
            this.ruleThreeData[0].data[i] = ruleData[years[i]];
            this.ruleThreeData[1].data[i] = parseInt(this.limit3);
            this.ruleThreeLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk3") as HTMLInputElement;
            var input = document.getElementById("limit3") as HTMLInputElement;

            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            chk.setAttribute("title", this.noDataMessage);
        }
    }

    rule4(): void {
        this.ruleFourData = [];
        this.ruleFourLabels = [];

        let ruleData = this.allData[3];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleFourData[0] = {};
        this.ruleFourData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleFourData[0].label = t);

        this.ruleFourData[1] = {};
        this.ruleFourData[1].data = [];
        this.translateService.get('GRAPHS.LIMITS').subscribe(t => this.ruleFourData[1].label = t);

        for (let i = 0; i < years.length; i++) {
            this.ruleFourData[0].data[i] = ruleData[years[i]];
            this.ruleFourData[1].data[i] = parseInt(this.limit4);
            this.ruleFourLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk4") as HTMLInputElement;
            var input = document.getElementById("limit4") as HTMLInputElement;
            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            chk.setAttribute("title", this.noDataMessage);
        }
    }

    rule5(): void {
        this.ruleFiveData = [];
        this.ruleFiveLabels = [];

        let ruleData = this.allData[4];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleFiveData[0] = {};
        this.ruleFiveData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleFiveData[0].label = t);

        this.ruleFiveData[1] = {};
        this.ruleFiveData[1].data = [];
        this.translateService.get('GRAPHS.LIMITS').subscribe(t => this.ruleFiveData[1].label = t);

        for (let i = 0; i < years.length; i++) {
            this.ruleFiveData[0].data[i] = ruleData[years[i]];
            this.ruleFiveData[1].data[i] = parseInt(this.limit5);
            this.ruleFiveLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk5") as HTMLInputElement;
            var input = document.getElementById("limit5") as HTMLInputElement;

            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            chk.setAttribute("title", this.noDataMessage);
        }
    }

    rule6(): void {
        this.ruleSixData = [];
        this.ruleSixLabels = [];

        let ruleData = this.allData[5];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleSixData[0] = {};
        this.ruleSixData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleSixData[0].label = t);

        this.ruleSixData[1] = {};
        this.ruleSixData[1].data = [];
        this.translateService.get('GRAPHS.LIMITS').subscribe(t => this.ruleSixData[1].label = t);

        for (let i = 0; i < years.length; i++) {
            this.ruleSixData[0].data[i] = ruleData[years[i]];
            this.ruleSixData[1].data[i] = parseInt(this.limit6);
            this.ruleSixLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk6") as HTMLInputElement;
            var input = document.getElementById("limit6") as HTMLInputElement;

            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            chk.setAttribute("title", this.noDataMessage);
        }
    }

    rule7(): void {
        this.ruleSevenData = [];
        this.ruleSevenLabels = [];

        let ruleData = this.allData[6];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleSevenData[0] = {};
        this.ruleSevenData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleSevenData[0].label = t);

        this.ruleSevenData[1] = {};
        this.ruleSevenData[1].data = [];
        this.translateService.get('GRAPHS.LIMITS').subscribe(t => this.ruleSevenData[1].label = t);

        for (let i = 0; i < years.length; i++) {
            this.ruleSevenData[0].data[i] = ruleData[years[i]];
            this.ruleSevenData[1].data[i] = parseInt(this.limit7);
            this.ruleSevenLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk7") as HTMLInputElement;
            var input = document.getElementById("limit7") as HTMLInputElement;

            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            chk.setAttribute("title", this.noDataMessage);
        }
    }

    rule8(): void {
        this.ruleEightData = [];
        this.ruleEightLabels = [];

        let ruleData = this.allData[7];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleEightData[0] = {};
        this.ruleEightData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleEightData[0].label = t);

        this.ruleEightData[1] = {};
        this.ruleEightData[1].data = [];
        this.translateService.get('GRAPHS.LIMITS').subscribe(t => this.ruleEightData[1].label = t);

        for (let i = 0; i < years.length; i++) {
            this.ruleEightData[0].data[i] = ruleData[years[i]];
            this.ruleEightData[1].data[i] = parseInt(this.limit8);
            this.ruleEightLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk8") as HTMLInputElement;
            var input = document.getElementById("limit8") as HTMLInputElement;

            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            chk.setAttribute("title", this.noDataMessage);
        }
    }

    rule9(): void {
        this.ruleNineData = [];
        this.ruleNineLabels = [];

        let ruleData = this.allData[8];
        let years = [];
        let i = 0;
        for (let key in ruleData) {
            if (ruleData.hasOwnProperty(key)) {
                years[i] = key;
                i++;
            }
        }

        this.ruleNineData[0] = {};
        this.ruleNineData[0].data = [];
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.ruleNineData[0].label = t);

        this.ruleNineData[1] = {};
        this.ruleNineData[1].data = [];
        this.translateService.get('GRAPHS.LIMITS').subscribe(t => this.ruleNineData[1].label = t);

        for (let i = 0; i < years.length; i++) {
            this.ruleNineData[0].data[i] = ruleData[years[i]];
            this.ruleNineData[1].data[i] = parseInt(this.limit9);
            this.ruleNineLabels[i] = years[i];
        }

        if (years.length === 0) {
            var chk = document.getElementById("chk9") as HTMLInputElement;
            var input = document.getElementById("limit9") as HTMLInputElement;

            chk.checked = false;
            chk.disabled = true;
            input.readOnly = true;
            input.style.color = '#999';
            input.setAttribute("title", this.noDataMessage);
        }
    }

    setAnnotationsToYearsProyected() {

        this.apiService.get(ApiEndpoints.app_getFirstProjectionYear, null).then(
            result => {
                this.chartOptions.annotation.annotations.splice(0, this.chartOptions.annotation.annotations.length); // reseting value
                this.chartOptions.annotation.annotations.push(
                    {
                        type: 'box',
                        xScaleID: 'x-axis-0',
                        yScaleID: 'x-axis-0',
                        xMin: parseInt(result.data).toString(),
                        backgroundColor: 'rgba(216, 240, 255, 0.5)',
                        borderWidth: 1
                    }
                )
            });

    }

    showChart(chartContainerId, limitInputId): void {
        this.setAnnotationsToYearsProyected();

        let rstatus = this.getRuleShowStatus(chartContainerId);
        let input = document.getElementById(limitInputId);

        if (!rstatus) {
            this.setRuleShowStatus(chartContainerId, true);
            (input as HTMLInputElement).readOnly = false;
            (input as HTMLInputElement).style.color = 'black';
        } else {
            this.setRuleShowStatus(chartContainerId, false);
            (input as HTMLInputElement).readOnly = true;
            (input as HTMLInputElement).style.color = '#999';

            setTimeout(() => {
                this.loadCharts();
            }, 0);
        }
    }

    exportChart(chartId) {
        let chart = document.getElementById(chartId);

        html2canvas(chart).then(canvas => {
            canvas.toBlob(blob => {
              saveAs(blob, `${chartId}.png`);
            });
          });
    }

    getRuleShowStatus(id: number): boolean {
        return this.ShowedRules.filter(r => r.id == id)[0].selected || false;
    }

    setRuleShowStatus(id, status) {
        let i = this.ShowedRules.findIndex(r => r.id == id);
        this.ShowedRules[i].selected = status;
    }

    updateInput($event) {
        $event.preventDefault();

        let value = parseFloat($event.target.value);
        let ruleId = $event.target.parentNode.id;

        if (isNaN(value))
            value = 0;

        var data = {
            ruleId: ruleId,
            limits: value
        };

        // Guardo límites
        this.apiService.post(ApiEndpoints.mfmp_postLimits, null, data).then(
            result => {
            },
            err => {
                this.error = true;
                console.log(err);
            });

        //Actualizo límite que va a tomar la gráfica
        this.setLimit(ruleId, value);

        $event.target.value = `${value.toFixed(2)}%`;
    }

    setLimit(ruleId, value) {
        switch (ruleId) {
            case "1":
                this.limit1 = `${value}%`;
                this.rule1();
                break;
            case "2":
                this.limit2 = `${value}%`;
                this.rule2();
                break;
            case "3":
                this.limit3 = `${value}%`;
                this.rule3();
                break;
            case "4":
                this.limit4 = `${value}%`;
                this.rule4();
                break;
            case "5":
                this.limit5 = `${value}%`;
                this.rule5();
                break;
            case "6":
                this.limit6 = `${value}%`;
                this.rule6();
                break;
            case "7":
                this.limit7 = `${value}%`;
                this.rule7();
                break;
            case "8":
                this.limit8 = `${value}%`;
                this.rule8();
                break;
            case "9":
                this.limit9 = `${value}%`;
                this.rule9();
                break;
        }
    }
}