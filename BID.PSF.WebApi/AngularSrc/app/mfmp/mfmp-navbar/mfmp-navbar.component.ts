﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
@Component({
    selector: 'mfmp-navbar',
    templateUrl: './mfmp-navbar.component.html',
    styleUrls: ['./mfmp-navbar.component.css']
})

export class MFMPNavbarComponent {
    @Input() title;
    @Input() yearsToSimulate;
    @Input() simulableYears;
    @Input() download;

    @Output() onSimulate: EventEmitter<number> = new EventEmitter<number>();
    @Output() onTables: EventEmitter<any> = new EventEmitter();
    @Output() onCharts: EventEmitter<any> = new EventEmitter();

    constructor(private translate: TranslateService){}
    simulate(): void {
        this.onSimulate.emit(this.yearsToSimulate);
    }

    showTables(): void {
        this.onTables.emit();
    }

    showCharts(): void {
        this.onCharts.emit();
    }
}