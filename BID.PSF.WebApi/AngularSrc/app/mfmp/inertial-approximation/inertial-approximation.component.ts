﻿import { MfmpService } from './../mfmp-service/mfmp-service.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ApiEndpoints } from '../../api-helper/api-endpoints';
import { ApiService } from '../../api-helper/api.service';
import { Aproximation } from '../../api-helper/models/enums';

@Component({
    selector: 'app-inertial-approximation',
    templateUrl: './inertial-approximation.component.html',
    styleUrls: ['./inertial-approximation.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class InertialApproximationComponent implements OnInit {
    htmlHelp_inertial_2_2 = '';
    title = '';

    constructor(
        public mfmp: MfmpService,
        private translateService: TranslateService) {
    }

    async ngOnInit() {
        this.translateService.get('TEXTS.MFMP_INERTIAL_HELP_2_2').subscribe(t => this.htmlHelp_inertial_2_2 = t);
        this.translateService.get('MFMP.INERTIAL_APROX').subscribe(t => this.title = t);
        let years = parseInt(sessionStorage.getItem("yearsToSimulate.inertial"));

        if (this.mfmp.topNav) {
            await this.mfmp.initAproximation(Aproximation.Inertial, years, 'inertial');
        }
    }

    loadingHandler(event) {
        this.mfmp.resetFlags(event);
        this.ngOnInit();
    }
}