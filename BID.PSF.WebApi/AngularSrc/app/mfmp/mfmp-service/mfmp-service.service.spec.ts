import { TestBed, inject } from '@angular/core/testing';

import { MfmpService } from './mfmp-service.service';

describe('MfmpServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MfmpService]
    });
  });

  it('should be created', inject([MfmpService], (service: MfmpService) => {
    expect(service).toBeTruthy();
  }));
});
