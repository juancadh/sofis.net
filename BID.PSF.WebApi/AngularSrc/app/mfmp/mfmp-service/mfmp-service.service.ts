import { TranslateService } from '@ngx-translate/core';
import { ApiEndpoints } from './../../api-helper/api-endpoints';
import { ColumnCategory, Aproximation, EntryData } from './../../api-helper/models/enums';
import { Pylogic } from './../py-logic/py-logic';
import { SofisApiService } from './../../api-helper/sofis-api/sofis-api.service';
import { ApiService } from './../../api-helper/api.service';
import { Injectable, Inject } from '@angular/core';
import { IncomeColumns } from '../../api-helper/models/enums';
import { DebtColumns } from '../../api-helper/models/enums';
import { ExpenseColumns } from '../../api-helper/models/enums';

import * as XLSX from 'xlsx';
import * as moment from 'moment';
import * as html2canvas from 'html2canvas';

import { DOCUMENT } from '@angular/common';
import * as $ from 'jquery';
import 'jqueryui';

import { saveAs } from 'file-saver';

const _historics = 3;

@Injectable()
export class MfmpService {
  currentAproximation: Aproximation = Aproximation.Inertial;

  chartType: string = 'line';
  chartLegends: boolean = false;
  projectionLabel: string;
  catalogue: number;
  chartOptions: any = {
    maintainAspectRatio: false,
    scales: {
      xAxes: [
        {
          offset: true,
          gridLines: {
            drawBorder: false,
            display: false
          }
        }],
      yAxes: [
        {
          gridLines: {
            drawBorder: false,
            color: 'rgb(181, 181, 181)',
            borderDash: [8, 4],
            display: true
          },
          ticks: {
            padding: 20,
            callback: (label, index, labels) => {
              return label.toLocaleString("en-US", {maximumFractionDigits:2});
            }
          },
          scaleLabel: {
            display: false,
            labelString: 'mil = 1000'
          }
        }]
    },
    annotation: {
      annotations: [
      ],
    },
    elements: {
      line: {
        tension: 0
      },
      point: {
        radius: 0
      }
    },
    responsive: true,
  };

  chartColors: Array<any> = [
    {
      backgroundColor: 'transparent',
      borderColor: '#0092FC'
    },
    {
      backgroundColor: 'transparent',
      borderColor: '#FF455C'

    },
    {
      backgroundColor: 'transparent',
      borderColor: '#37BA9D'
    },
    {
      backgroundColor: 'transparent',
      borderColor: '#004E70'
    },
    {
      backgroundColor: 'transparent',
      borderColor: '#F1C40F'
    },
    {
      backgroundColor: 'transparent',
      borderColor: '#F39C12'
    },
    {
      backgroundColor: 'transparent',
      borderColor: '#BDC3C7'
    },
    {
      backgroundColor: 'transparent',
      borderColor: '#7D3C98'
    }
  ];

  settings = {
    fixedColumnsLeft: 1,
    colHeaders: true,
    columnHeaderHeight: 50,
    rowHeaders: false,
    className: "htCenter",
    manualColumnResize: [350],
    fillHandle: {
      autoInsertRow: false
    },
    wordWrap: false,
    viewportColumnRenderingOffset: 100,
    cells: (row, col, prop) => {
      var cellProperties = {
        renderer: this.cellRenderer
      };

      return cellProperties;
    }
  }

  isLoading: boolean;
  pythonReady: boolean;
  xlsbutton: boolean;
  dataReady: boolean;
  downloading: boolean;
  tables: boolean;
  charts: boolean;
  assumptions: boolean;
  disabledButton: boolean;
  analysis: boolean;
  topNav: boolean = false;

  title: string;
  fromCellDotted: string;
  hotInstance: string;

  simulableYears: Array<number> = [0, 1, 2, 3, 4, 5];
  allColumns: Array<any>;
  firstHistoric: number;

  allData_inercial: Array<any>;
  allData_fija: Array<any>;
  allData_5: Array<any>;
  allData_variable: Array<any>;

  catalogueType = 1;
  years: Array<any>;
  chartLabels: Array<any>;
  incomesData: Array<any>;
  expensesData: Array<any>;
  balanceData: Array<any>;
  debtData: Array<any>;
  debtServiceData: Array<any>;
  yearsToSimulate: number;
  basicColumns: number;
  firstProjection: number;
  currentColumns: Array<any>;
  currentChartLabels: Array<any>;
  currentExpensesData: Array<any>;
  currentIncomesData: Array<any>;
  currentPercentExpensesData: Array<any>;
  currentPercentIncomesData: Array<any>;
  currentBalanceData: Array<any>;
  currentPercentBalanceData: Array<any>;
  currentDebtData: Array<any>;
  currentPercentDebtData: Array<any>;

  incomesLegends: Array<any>;
  incomesPercentLegends: Array<any>;
  expensesLegends: Array<any>;
  debtsLegends: Array<any>;
  debtServiceLegends: Array<any>;
  balanceLegends: Array<any>;

  parameters: Array<any>;
  futureYears: Array<any>;
  entryData: Array<EntryData>;
  columnIds: Array<any>;
  entryColumns: Array<any>;
  futureYearsTmp: Array<any>;

  error: boolean;
  noData: boolean;
  noResultMessage: Array<any>;

  sendParameters: boolean;


  constructor(@Inject(DOCUMENT) private document: HTMLDocument, private apiService: ApiService, private _sofisApiService: SofisApiService, private translateService: TranslateService) {
  }

  initAproximation = async (aproximation: Aproximation, yearsToSimulate: number, viewId: string) => {

    this.chartOptions.annotation.annotations.slice();
    this.currentAproximation = aproximation;
    this.isLoading = false;
    this.pythonReady = false;
    this.xlsbutton = false;
    this.dataReady = false;
    this.downloading = false;
    this.sendParameters = false;
    this.tables = true;
    this.charts = false;
    this.assumptions = false;
    this.disabledButton = false;
    this.analysis = false;
    this.title = null;
    this.fromCellDotted = null;
    this.allColumns = [];
    this.allData_inercial = [];
    this.allData_fija = [];
    this.allData_5 = [];
    this.allData_variable = [];
    this.years = [];
    this.chartLabels = [];
    this.incomesData = [];
    this.expensesData = [];
    this.balanceData = [];
    this.debtData = [];
    this.debtServiceData = [];
    this.yearsToSimulate = 0;
    this.basicColumns = 0;
    this.firstProjection = 0;
    this.currentColumns = [];
    this.currentChartLabels = [];
    this.currentExpensesData = [];
    this.currentIncomesData = [];
    this.currentPercentExpensesData = [];
    this.currentPercentIncomesData = [];
    this.currentBalanceData = [];
    this.currentPercentBalanceData = [];
    this.currentDebtData = [];
    this.currentPercentDebtData = [];
    this.incomesLegends = [];
    this.incomesPercentLegends = [];
    this.expensesLegends = [];
    this.debtsLegends = [];
    this.debtServiceLegends = [];
    this.balanceLegends = [];
    this.parameters = [];
    this.entryData = [];
    this.columnIds = [];
    this.futureYears = [];
    this.entryColumns = [];
    this.futureYearsTmp = [];
    this.error = false;
    this.noData = false;
    this.noResultMessage = [];

    this.translateService.get('GRAPHS.PRIMARY_BALANCE').subscribe(t => this.fromCellDotted = t);

    if (Array.from(this.document.getElementsByClassName('active'))[1]) {
      Array.from(this.document.getElementsByClassName('active'))[1].classList.remove('active');
    }

    this.document.getElementById(viewId).classList.add('active');

    if (yearsToSimulate) {
      this.yearsToSimulate = yearsToSimulate;
    }
    else {
      this.yearsToSimulate = this.currentAproximation === Aproximation.FiveYears ? this.simulableYears[5] : this.simulableYears[3];
    }

    if (this.currentAproximation === Aproximation.Inertial || this.currentAproximation === Aproximation.FiveYears) {
      await this.loadApproximation(true);
    } else {
      await this.loadTableStructure();
    }

    const apiResult = await this.apiService.get(ApiEndpoints.app_getFirstProjectionYear, null);
    this.firstProjection = apiResult.data;
  }

  loadApproximation = async (showLoader, first = false) => {
    let data = null;

    if (showLoader)
      this.isLoading = true;

    if (this.currentAproximation === Aproximation.Fixed || this.currentAproximation === Aproximation.Variable) {
      this.assumptions = false;

      if (first) {
        data = this.buildParametricData();
      }
    }

    const result = await this.getData(data);

    if (result) {
      this.isLoading = true;
      this.catalogueType = result.data.CatalogueType;
      this.allColumns = result.data.Structure;
      this.firstHistoric = parseInt(result.data.Structure[1].data);
      const pylogic = new Pylogic(this.catalogueType);
      const resp = await this.getPythonData(result, pylogic);
      if (resp) {
        let orderedRows = pylogic.orderRows(resp);
        this.setAproxData(orderedRows.filter(d => d.Table === true)); //setting Data by Aproximation
        this.pythonReady = true;
        this.xlsbutton = true;

        this.years = [];
        this.chartLabels = [];

        for (let i = 1; i < result.data.Structure.length; i++) {
          this.years[this.years.length] = result.data.Structure[i].data;
        }

        // Ingresos
        this.incomesData = this.buildCategoryData(this.getCurrentAproxData().filter(d => d['Category'] === ColumnCategory.Income));

        // Gastos
        this.expensesData = this.buildCategoryData(this.getCurrentAproxData().filter(d => d['Category'] === ColumnCategory.Expense));

        // Balance Primario
        this.balanceData = this.buildCategoryData(this.getCurrentAproxData().filter(d => d['Category'] === ColumnCategory.PrimaryBalance || d['Category'] === ColumnCategory.FiscalBalance), false);

        // Deuda Total
        this.debtData = this.buildCategoryData(this.getCurrentAproxData().filter(d => d['Category'] === ColumnCategory.Debt));

        // Servicio de la Deuda
        this.debtServiceData = this.buildCategoryData(this.getCurrentAproxData().filter(d => d['Category'] === ColumnCategory.DebtService));

        for (let i = 0; i < result.data.Structure.length - 1; i++) {
          this.chartLabels[this.chartLabels.length] = result.data.Structure[i + 1].title;
        }

        this.simulate(this.yearsToSimulate);
      }

      this.isLoading = false;
    }
  }

  simulate(years) {
    if (this.currentAproximation == Aproximation.Variable) {
      this.futureYearsTmp = this.futureYears.slice(0, 5);
    }

    // Solución para redibujar los gráficos
    this.dataReady = false;
    this.yearsToSimulate = years;

    if (isNaN(years)) years = 0;

    this.setYearToSimulateByAproximation(years);

    // Actualizar valores de las tablas
    if (this.allColumns) {
      this.currentColumns = this.allColumns.slice(null, _historics + 1 + parseInt(years));
    }

    // Actualizar valores de los gráficos
    if (this.chartLabels && this.incomesData && this.expensesData) {
      this.currentChartLabels = this.chartLabels.slice(null, _historics + parseInt(years));
      // Ingresos
      let incomesData = this.incomesData.filter(d => d.InternalName === IncomeColumns.TotalIncome
        || d.InternalName === IncomeColumns.FederalTransfers
        || d.InternalName === IncomeColumns.NotAvailableIncome
        || d.InternalName === IncomeColumns.AvailableIncome);
      this.currentIncomesData = [];
      for (let i = 0; i < incomesData.length; i++) {
        this.currentIncomesData[i] = {};
        this.currentIncomesData[i].data = incomesData[i].data.slice(null, _historics + parseInt(years));
        this.currentIncomesData[i].label = incomesData[i].label;
        this.currentIncomesData[i].InternalName = incomesData[i].InternalName;

        this.incomesLegends[i] = incomesData[i].label;
      }

      let availableincomes = this.currentIncomesData.find(function (element) {
        return element.InternalName === IncomeColumns.AvailableIncome;
      });

      incomesData = this.currentIncomesData.filter(d => d.InternalName === IncomeColumns.TotalIncome
        || d.InternalName === IncomeColumns.FederalTransfers
        || d.InternalName === IncomeColumns.NotAvailableIncome);
      for (let i = 0; i < incomesData.length; i++) {
        this.incomesPercentLegends[i] = incomesData[i].label;
      }

      this.currentPercentIncomesData = this.getPercentCurrentData(incomesData, availableincomes.data);

      // Gastos
      let totalexpenses = this.expensesData.find(function (element) {
        return element.InternalName === ExpenseColumns.TotalExpense;
      });

      let operatingExpenses = this.expensesData.filter(d => d.InternalName === ExpenseColumns.PSLabeled
        || d.InternalName === ExpenseColumns.MSLabeled
        || d.InternalName === ExpenseColumns.GSLabeled
        || d.InternalName === ExpenseColumns.Roster
        || d.InternalName === ExpenseColumns.GoodsAndServices);

      let transfers = this.expensesData.filter(d => d.InternalName === ExpenseColumns.TASONLabeled
        || d.InternalName === ExpenseColumns.PublicInvestment
        || d.InternalName === ExpenseColumns.SocialBenefits);

      this.currentExpensesData = [];
      this.currentExpensesData[0] = {};
      this.currentExpensesData[0].data = totalexpenses.data.slice(null, this.basicColumns + parseInt(years));
      this.currentExpensesData[0].label = totalexpenses.label;
      this.expensesLegends[0] = totalexpenses.label;

      if (operatingExpenses.length > 0) {
        var data = operatingExpenses[0].data;
        for (let i = 1; i < operatingExpenses.length - 1; i++) {
          data.concat(operatingExpenses[i].data);
        }
        this.currentExpensesData[1] = {};
        this.currentExpensesData[1].data = data.slice(null, _historics + parseInt(years));
        this.translateService.get('GRAPHS.OPERATING_EXPENSES').subscribe(t => this.currentExpensesData[1].label = t);
        this.translateService.get('GRAPHS.OPERATING_EXPENSES').subscribe(t => this.expensesLegends[1] = t);
      }

      if (transfers.length > 0) {
        var data = transfers[0].data;
        for (let i = 1; i < transfers.length - 1; i++) {
          data.concat(transfers[i].data);
        }
        this.currentExpensesData[2] = {};
        this.currentExpensesData[2].data = data.slice(null, _historics + parseInt(years));
        this.translateService.get('GRAPHS.TRANSFERS').subscribe(t => this.currentExpensesData[2].label = t);
        this.translateService.get('GRAPHS.TRANSFERS').subscribe(t => this.expensesLegends[2] = t);
      }

      //Gastos en porcentaje
      this.currentPercentExpensesData = this.getPercentCurrentData(this.currentExpensesData, availableincomes.data);

      // Balance Primario
      let balanceData = this.balanceData;
      this.currentBalanceData = [];
      for (let i = 0; i < this.balanceData.length; i++) {
        this.currentBalanceData[i] = {};
        this.currentBalanceData[i].data = balanceData[i].data.slice(null, _historics + parseInt(years));
        this.currentBalanceData[i].label = balanceData[i].label;

        this.balanceLegends[i] = balanceData[i].label;
      }

      //Balance en porcentaje
      this.currentPercentBalanceData = this.getPercentCurrentData(this.currentBalanceData, availableincomes.data);

      // Deuda Total
      let debtsData = this.debtData.filter(d => d.InternalName === DebtColumns.TotalDebt);
      this.currentDebtData = [];
      for (let i = 0; i < debtsData.length; i++) {
        this.currentDebtData[i] = {};
        this.currentDebtData[i].data = debtsData[i].data.slice(null, _historics + parseInt(years));
        this.currentDebtData[i].label = debtsData[i].label;

        this.debtsLegends[i] = debtsData[i].label;
      }

      //Deuda en porcentaje
      this.currentPercentDebtData = this.getPercentCurrentData(this.currentDebtData, availableincomes.data);
    }

    setTimeout(() => {
      this.dataReady = true;
      this.isLoading = false;

      setTimeout(() => {
        $("#sortable").sortable(
          {
            revert: true,
            handle: $(".drag")
          }
        );
        $(".half-box").disableSelection();
      }, 0);
    }, 0);
  }

  loadTableStructure = async () => {
    this.isLoading = true;

    try {

      if (this.currentAproximation === Aproximation.Inertial || this.currentAproximation === Aproximation.FiveYears) {
        this.isLoading = false;
        this.error = true;
        return;
      }

      const result = await this.apiService.get(this.currentAproximation === Aproximation.Fixed ? ApiEndpoints.mfmp_getFixedApproximationStructure : ApiEndpoints.mfmp_getVariableApproximationStructure, null);

      if (result) {
        this.entryColumns = result.data.Structure;
        this.futureYears = this.entryColumns.slice(1).map(y => y.data);
        this.catalogue = result.data.Data.Catalogue;
        this.entryData = result.data.Data.Data.filter(d => d.Category != ColumnCategory.Debt && d.Category != ColumnCategory.Macroeconomy);
        this.catalogueType = result.data.Data.CatalogueType;
        this.getColumnIds();
        await this.loadApproximation(false, true);
        this.isLoading = false;
      }
    } catch (err) {
      this.error = true;
      this.isLoading = false;
      console.log(err);
    }

  }

  cellRenderer = (instance, td, row, col, prop, value, cellProperties) => {
    const currentData = this.getCurrentAproxData();
    if (currentData != undefined && currentData[row] != undefined) {
      this.renderFutureHeaders();
      if (col === 0) {
        switch (currentData[row]['Level']) {
          case 0:
            td.className = 'text-bold';
            break;
          case 1:
            td.className = "children-level1";
            break;
          case 2:
            td.className = "children-level2";
            break;
          case 3:
            td.className = "children-level3";
            break;
        }
        if (currentData[row]['Level'] == 1 && currentData[row]['Autocalculated'] == false)
          td.className = "children-account-cell";
      } else if (col > 0 && currentData[row]['Autocalculated'] == true)
        td.className = ' text-bold';
      if (col > _historics)
        td.className += " future";

      if (col === _historics + 1)
        td.className += " firstfuture";

      if (currentData[row]['Category'] === ColumnCategory.PrimaryBalance) {
        td.classList.add('dotted-separator');
      }
    }

    if (value != undefined) {
      if (isNaN(value))
        td.innerText = value;
      else
        td.innerText = value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
    }
  }

  exportTable(title: string) {
    const currentData = this.getCurrentAproxData();
    this.downloading = true;
    const sheetNameLimit = 28;
    if (currentData && !isNaN(this.firstHistoric)) {
      const fileName = `${title.replace(' ', '_')}[${moment(Date.now()).format("DD-MM-YYYY")}].xlsx`;
      const rows = this.filterAndMapXLSRows(currentData, this.firstHistoric);
      const settings: any = {
        cellStyles: true,
        cellDates: true,
        header: ['Account'],
        origin: 'A2'
      };

      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(rows, settings);
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      let sheetName = title.length >= sheetNameLimit ? `${title.substring(0, sheetNameLimit - 3).replace(' ', '-')}...` : `${title.replace(' ', '-')}`;
      XLSX.utils.book_append_sheet(wb, ws, sheetName.toLowerCase());
      XLSX.utils.sheet_set_array_formula(ws, 'A2:A2', 'CONCATENATE("")'); // custom B1 (variables title)
      ws['!cols'] = [{ wch: 40 }];
      XLSX.writeFile(wb, fileName);
    }
    this.downloading = false;
  }

  filterAndMapXLSRows(rows: any[], firstHistoric) {
    return rows.map((r: any) => {
      let obj: any = {};
      obj.Account = r.Account;
      const keys = Object.keys(r);
      keys.forEach((key: any, index: number) => {
        if (!isNaN(key) && parseInt(key) >= firstHistoric) {
          if (parseInt(key) >= this.firstProjection)
            obj[`${key}p`] = r[key];
          else
            obj[key] = r[key];
        }
      });
      return obj;
    });
  }

  showTables() {
    this.tables = true;
    this.charts = false;

    this.document.getElementById("link-tables").classList.add("active");
    this.document.getElementById("link-charts").classList.remove("active");
  }

  showCharts(chartOptions) {

    // Setting dinamic chart options
    this.apiService.get(ApiEndpoints.app_getFirstProjectionYear, null).then(
      result => {
        chartOptions.annotation.annotations.splice(0, chartOptions.annotation.annotations.length); // reseting value
        chartOptions.annotation.annotations.push(
          {
            type: 'box',
            xScaleID: 'x-axis-0',
            yScaleID: 'x-axis-0',
            xMin: parseInt(result.data).toString(),
            backgroundColor: 'rgba(216, 240, 255, 0.5)',
            borderWidth: 1
          }
        )
      });

    this.tables = false;
    this.charts = true;

    setTimeout(() => {
      $("#sortable").sortable({
        handle: $(".drag")
      });
      $(".half-box").disableSelection();
    }, 0);

    this.document.getElementById("link-tables").classList.remove("active");
    this.document.getElementById("link-charts").classList.add("active");
  }

  deleteChart(chartId) {
    let chart = this.document.getElementById(chartId);
    chart.style.display = 'none';
  }

  exportChart(chartId) {
    let chart = this.document.getElementById(chartId);

    html2canvas(chart).then(canvas => {
      canvas.toBlob(blob => {
        saveAs(blob, `${chartId}.png`);
      });
    });
  }

  resetFlags = (isLoading = false) => {
    this.topNav = !isLoading;
    this.noData = false;
    this.tables = true;
    this.charts = false;
    this.error = false;
    this.isLoading = isLoading;
    this.pythonReady = false;
  }

  startAnalysis() {
    this.charts = false;
    let inputClass = this.currentAproximation === Aproximation.Fixed ? 'fixed-input' : 'variable-input';
    let tableClass = this.currentAproximation === Aproximation.Fixed ? 'table-fixed' : 'table-variable';
    let elementsPerSubArray = this.currentAproximation === Aproximation.Fixed ? 1 : 5;

    // Para cada input actualizar el arreglo de parametros
    const inputs = this.document.getElementsByClassName(inputClass);
    var val;
    this.parameters = [];
    for (let i = 0; i < inputs.length; i++) {
      val = (inputs[i] as HTMLInputElement).value;
      this.parameters[i] = parseFloat(val);
    }
    this.parameters = this.listToMatrix(this.parameters, elementsPerSubArray);
    this.sendParameters = true;
    this.loadApproximation(true, true);
    var wrapper = (this.document.getElementsByClassName(tableClass)[0] as HTMLElement);
    if (wrapper) {
      wrapper.setAttribute('margin-top', '245px');
    }

    this.showTables();
  }

  listToMatrix(list, elementsPerSubArray) {
    var matrix = [], i, k;
    for (i = 0, k = -1; i < list.length; i++) {
      if (i % elementsPerSubArray === 0) {
        k++;
        matrix[k] = [];
      }
      matrix[k].push(list[i]);
      if (this.currentAproximation === Aproximation.Fixed) {
        this.entryData[i].Parameter = list[i];
      }
    }
    return matrix;
  }

  updateInput($event) {
    let value = parseFloat($event.target.value);

    if (isNaN((value)))
      value = 0;

    let number = value.toFixed(2);

    $event.target.value = number + "%";
  }

  enterAssumptions() {
    this.disabledButton = true;
    this.assumptions = true;
    this.analysis = false;
    this.dataReady = false;

    setTimeout(() => {
      let inputClass = this.currentAproximation === Aproximation.Fixed ? 'fixed-input' : 'variable-input';

      const inputs = this.document.getElementsByClassName(inputClass);
      if (this.currentAproximation === Aproximation.Fixed) {

        for (let i = 0; i < inputs.length; i++) {
          inputs[i].id = this.columnIds[i];
          let number = parseFloat(this.parameters[i]);
          if (isNaN(number)) {
            number = 0;
          }
          (inputs[i] as HTMLInputElement).value = number.toFixed(2) + "%";
          inputs[i].addEventListener('keyup', (e: KeyboardEvent) => {
            if (e.keyCode === 13) {
              (e.target as HTMLElement).blur();
            }
          }, false);
        }
      } else {
        var matrix = this.listToMatrix(inputs, 5);

        if (matrix && this.parameters) {
          var number = 0;

          for (let i = 0; i < matrix.length; i++) {
            for (let j = 0; j < 5; j++) {
              matrix[i][j].id = this.columnIds[i] + "-" + (j + 1);
              if (this.parameters.length !== 0 && this.parameters[i][j]) {
                number = parseFloat(this.parameters[i][j]);
                if (isNaN(number)) {
                  number = 0;
                }
              } else {
                number = 0;
              }

              (matrix[i][j] as HTMLInputElement).value = number.toFixed(2) + "%";
              matrix[i][j].addEventListener('keyup', (e: KeyboardEvent) => {
                if (e.keyCode === 13) {
                  (e.target as HTMLElement).blur();
                }
              }, false);
            }
          }
        }
      }

    }, 1000);
  }

  hideAssumptions() {
    this.disabledButton = false;
    this.assumptions = false;
    this.dataReady = true;
    this.analysis = false;
    let tableClass = this.currentAproximation === Aproximation.Fixed ? 'table-fixed' : 'table-variable';
    const wrapper = (this.document.getElementsByClassName(tableClass)[0] as HTMLElement);

    if (wrapper) {
      wrapper.setAttribute('margin-top', '340px');
    }
  }

  private async getData(data = null): Promise<any> {
    let url = `${this.getAproxUrl()}/${this.simulableYears[this.simulableYears.length - 1]}`;
    try {
      if (this.currentAproximation === Aproximation.Inertial || this.currentAproximation === Aproximation.FiveYears) {
        return await this.apiService.get(url, null);
      }
      return await this.apiService.post(url, null, data);
    } catch (err) {
      this.error = true;
      if (err.result === "MissingData") {
        this.noData = true;
        this.noResultMessage = err.message.split(',').map(s => s.trim());
        this.isLoading = false;
      }
      else if (err.result === "NotData") {
        this.noData = true;
        this.noResultMessage = [];
        this.isLoading = false;
      }
      else {
        this.error = true;
        this.isLoading = false;
        console.log(err);
      }
    }
  }

  private async getPythonData(aproxData, pylogic: Pylogic): Promise<any> {
    try {
      if (this.currentAproximation === Aproximation.Inertial || this.currentAproximation === Aproximation.FiveYears) {
        return await pylogic.getMfMpAproxData(this._sofisApiService, aproxData.data.Data, aproxData.data.Structure, aproxData.data.BasicColumns);
      }
      return await pylogic.getMfMpAproxData(this._sofisApiService, aproxData.data.Data, aproxData.data.Structure, _historics, this.sendParameters ? this.parameters : null)
    } catch (err) {
      this.error = true;
      this.isLoading = false;
      console.log(err);
    }
  }

  private buildParametricData() {
    let data = null;

    if (this.currentAproximation === Aproximation.Fixed) {
      data = {};
      this.entryData.forEach((item) => {
        var columnId = item.ColumnId.toString();
        if (this.document.getElementById(columnId) && (this.document.getElementById(columnId) as HTMLInputElement).value)
          data[columnId] = parseFloat((this.document.getElementById(columnId) as HTMLInputElement).value);
        else
          data[columnId] = 0;
      });
    }

    if (this.currentAproximation === Aproximation.Variable) {
      data = {};
      this.entryData.forEach((item) => {
        var columnId = item.ColumnId.toString();
        var years = this.futureYears;
        data[columnId] = {};

        for (let i = 0; i < years.length; i++) {
          let year = years[i];
          let yearColumnId = columnId + "-" + (i + 1);

          if (this.document.getElementById(yearColumnId) && (this.document.getElementById(yearColumnId) as HTMLInputElement).value)
            data[columnId][year] = parseFloat((this.document.getElementById(yearColumnId) as HTMLInputElement).value);
          else
            data[columnId][year] = 0;
        }
      });
    }

    return data;
  }

  private getColTitleWithOutHtml(html: string): string {
    return html.replace(/<[^>]*>/g, '');
  }

  private renderFutureHeaders() {
    const headers = this.document.getElementsByClassName('relative')
    if (headers.length > 0) {
      for (let x = 0; x < headers.length; x++) {
        let number = parseInt(this.getColTitleWithOutHtml(headers[x].firstElementChild.innerHTML));
        if (number && number >= this.firstProjection) {
          headers[x].classList.add('future-header');
          if (number === this.firstProjection) {
            headers[x].parentElement.classList.add('future-header-border');
          }
        }
      }
    }
  }

  private getPercentCurrentData(data, availableincomes) {
    let result = [];
    for (let i = 0; i < data.length; i++) {
      result[i] = {};
      result[i].data = [];
      result[i].label = data[i].label;

      for (let j = 0; j < data[i].data.length; j++) {
        result[i].data[j] = data[i].data[j] * 100 / availableincomes[j];
      }
    }
    return result;
  }

  private getAproxUrl() {
    switch (this.currentAproximation) {
      case Aproximation.Inertial:
        return ApiEndpoints.mfmp_getAproximacionInercial;

      case Aproximation.Fixed:
        return ApiEndpoints.mfmp_getFixedApproximation;

      case Aproximation.Variable:
        return ApiEndpoints.mfmp_getVariableApproximation;

      case Aproximation.FiveYears:
        return ApiEndpoints.mfmp_getLastFiveYearsApproximation;

      default:
        return ApiEndpoints.mfmp_getAproximacionInercial;
    }
  }

  private setAproxData = (newData) => {
    switch (this.currentAproximation) {
      case Aproximation.Inertial:
        this.allData_inercial = newData;
        break;

      case Aproximation.Fixed:
        this.allData_fija = newData;
        break;

      case Aproximation.Variable:
        this.allData_variable = newData;
        break;

      case Aproximation.FiveYears:
        this.allData_5 = newData;
        break;

      default:
        this.allData_inercial = newData;
        break;
    }
  }

  private getCurrentAproxData = () => {
    switch (this.currentAproximation) {
      case Aproximation.Inertial:
        return this.allData_inercial;

      case Aproximation.Fixed:
        return this.allData_fija;

      case Aproximation.Variable:
        return this.allData_variable;

      case Aproximation.FiveYears:
        return this.allData_5;

      default:
        return this.allData_inercial;
    }
  }

  private setYearToSimulateByAproximation = (years) => {
    switch (this.currentAproximation) {
      case Aproximation.Inertial:
        sessionStorage.setItem("yearsToSimulate.inertial", years);
        break;
      case Aproximation.Fixed:
        sessionStorage.setItem("yearsToSimulate.fixed", years);
        break;
      case Aproximation.Variable:
        sessionStorage.setItem("yearsToSimulate.variable", years);
        break;
      case Aproximation.FiveYears:
        sessionStorage.setItem("yearsToSimulate.five", years);
        break;
      default:
        sessionStorage.setItem("yearsToSimulate.inertial", years);
        break;
    }
  }

  private getColumnIds() {
    if (this.entryData) {
      this.entryData.forEach(i => {
        this.columnIds.push(i.ColumnId);
        if (this.currentAproximation === Aproximation.Fixed) this.setFixedParameters(i);
        if (this.currentAproximation === Aproximation.Variable) this.setVariableParameters(i);
      });
    }
  }

  private setFixedParameters(item) {
    if (item.Parameter) {
      this.parameters.push(item.Parameter);
    } else {
      this.parameters.push(0);
    }
  }

  private setVariableParameters(item) {
    if (item.Parameters && item.Parameters.length > 0) {
      item.Parameters.forEach(e => {
        this.parameters.push(e);
      });
    } else {
      const voidParameters = [0, 0, 0, 0, 0];
      voidParameters.forEach(i => {
        this.parameters.push(i);
      });
    }
  }

  private buildCategoryData(categoryData: Array<any>, hasInternalName = true) {
    const result = [];

    if (categoryData && categoryData.length) {
      for (let i = 0; i < categoryData.length; i++) {
        result[i] = {};
        result[i].data = [];
        result[i].label = categoryData[i]['Account'];
        
        if(hasInternalName){
          result[i].InternalName = categoryData[i]['InternalName'];
        }

        for (let j = 0; j < this.years.length; j++) {
          result[i].data[j] = categoryData[i][this.years[j]];
        }
      }
    }

    return result;
  }
}