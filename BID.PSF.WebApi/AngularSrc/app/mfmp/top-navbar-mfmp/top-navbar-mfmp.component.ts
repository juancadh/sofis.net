﻿import { TopNavLogicService } from './../../api-helper/top-nav-logic.service';
import { EventEmitter } from '@angular/core';
import { Component, OnInit, Output } from '@angular/core';

@Component({
    selector: 'top-navbar-mfmp',
    templateUrl: './top-navbar-mfmp.component.html',
    styleUrls: ['./top-navbar-mfmp.component.css']
})

export class TopNavbarMFMPComponent implements OnInit {
    @Output() isLoading = new EventEmitter<boolean>();

    constructor(public topNavLogic: TopNavLogicService) {
        this.isLoading.emit(true);
        this.topNavLogic.isLoadingChange.subscribe(value=> this.isLoading.emit(value));
    }

    async ngOnInit() {
        await this.topNavLogic.init();
    }
}