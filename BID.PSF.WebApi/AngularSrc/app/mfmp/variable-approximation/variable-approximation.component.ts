﻿import { Aproximation } from './../../api-helper/models/enums';
import { MfmpService } from './../mfmp-service/mfmp-service.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ApiEndpoints } from '../../api-helper/api-endpoints';
import { ApiService } from '../../api-helper/api.service';

@Component({
    selector: 'app-variable-approximation',
    templateUrl: './variable-approximation.component.html',
    styleUrls: ['./variable-approximation.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class VariableApproximationComponent implements OnInit {
    title: string;

    constructor(
        public mfmp: MfmpService,
        private translateService: TranslateService) {
    }

    async ngOnInit() {
        event.preventDefault();
        this.translateService.get('MFMP.PARAMETRIC_APROX').subscribe(r => this.title = r);
        const years = parseInt(sessionStorage.getItem("yearsToSimulate.variable"));

        if (this.mfmp.topNav) {
            await this.mfmp.initAproximation(Aproximation.Variable, years, 'variable');
        }
    }

    loadingHandler(event) {
        this.mfmp.resetFlags(event);
        this.ngOnInit();
    }
}