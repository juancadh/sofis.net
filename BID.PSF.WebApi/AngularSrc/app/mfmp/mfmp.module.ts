﻿import { MfmpService } from './mfmp-service/mfmp-service.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotTableModule } from 'angular-handsontable';
import { ChartsModule } from 'ng2-charts';

import { SharedModule } from './../Components/Shared/shared.module';
import { NgxSmartModalModule } from 'ngx-smart-modal';

import { InertialApproximationComponent } from './inertial-approximation/inertial-approximation.component';
import { LastFiveYearsApproximationComponent } from './last-five-years/last-five-years.component';
import { FixedApproximationComponent } from './fixed-approximation/fixed-approximation.component';
import { VariableApproximationComponent } from './variable-approximation/variable-approximation.component';
import { RulesComponent } from './rules/rules.component';

import { TopNavbarMFMPComponent } from './top-navbar-mfmp/top-navbar-mfmp.component';
import { MFMPNavbarComponent } from './mfmp-navbar/mfmp-navbar.component';
import { TableComponent } from './tables/table.component';
import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
    imports: [BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule,
        HotTableModule,
        ChartsModule,
        NgxSmartModalModule,
        ImageCropperModule,
        SharedModule],
    declarations: [InertialApproximationComponent,
        LastFiveYearsApproximationComponent,
        FixedApproximationComponent,
        VariableApproximationComponent,
        TopNavbarMFMPComponent,
        MFMPNavbarComponent,
        TableComponent,
        RulesComponent],
    providers: [MfmpService]
})

export class MFMPModule { }