import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCatalogueDataComponent } from './new-catalogue-data.component';

describe('NewCatalogueDataComponent', () => {
  let component: NewCatalogueDataComponent;
  let fixture: ComponentFixture<NewCatalogueDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCatalogueDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCatalogueDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
