import { Location } from '@angular/common';
import { ApiEndpoints } from './../../../api-helper/api-endpoints';
import { ApiService } from './../../../api-helper/api.service';
import { CatalogueData } from './../../../api-helper/models/catalogue-data';
import { CatalogueManagerService } from './../../../api-helper/CatalogueManager/catalogue-manager.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from '../../../api-helper/session.service';


@Component({
  selector: 'app-new-catalogue-data',
  templateUrl: './new-catalogue-data.component.html',
  styleUrls: ['./new-catalogue-data.component.css']
})

export class NewCatalogueDataComponent implements OnInit {

  @Input() action;
  @Input() croppedimg;

  isLoading = false; // for submit
  submited = false;
  catalogueDataForm: FormGroup;
  titleAction: string;
  inputLabelAction: string;
  btnAction: string;
  routerAlert = {
    alert: 1, //1 true 0 false
    type: 1, // 1 success 2 error
    msg: 1, // 1 create 2 clone 3 error
  };
  @Output() success = new EventEmitter();
  @Output() displaycrop = new EventEmitter();
  @Output() fileImg = new EventEmitter();
  @Output() selectNew = new EventEmitter();

  inputs: {
    name: String
  };

  private otherDest;

  constructor(private t: TranslateService,
    private _catalogueService: CatalogueManagerService,
    private fb: FormBuilder,
    private router: Router,
    private ApiService: ApiService,
    private SessionService: SessionService,
    private location: Location
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.action && changes.action.previousValue !== changes.action.currentValue && changes.action.currentValue) {
      this.init(changes.action.currentValue);
    }
  }

  ngOnInit() {
    this.otherDest = this.router.url != '/ingresos';
    this.init(this.action);
  }

  init = (action: any) => {
    this.emmitCropper(false);
    this.catalogueDataForm = this.fb.group({
      name: ['', Validators.required]
    });

    switch (action.value) {
      case 'edit':
        this.t.get('CATALOGUE_OPERATIONS.FORM_EDIT_TITLE').subscribe(t => this.titleAction = t);
        this.t.get('CATALOGUE_OPERATIONS.FORM_EDIT_BUTTON').subscribe(t => this.btnAction = t);
        this.t.get('CATALOGUE_OPERATIONS.FORM_EDIT_NAME').subscribe(t => this.inputLabelAction = t);
        this.inputs.name = action.key.name;
        /*         this.catalogueDataForm = this.fb.group({
                  name: [this.inputs.name, Validators.required],
                }); */
        break;
      case 'clone':
        this.t.get('CATALOGUE_OPERATIONS.FORM_CLONE_TITLE').subscribe(t => this.titleAction = t);
        this.t.get('CATALOGUE_OPERATIONS.FORM_CLONE_BUTTON').subscribe(t => this.btnAction = t);
        this.t.get('CATALOGUE_OPERATIONS.FORM_CLONE_NAME').subscribe(t => this.inputLabelAction = t);
        /*           this.inputs.name = action.key.name + 'Copia';
                  this.catalogueDataForm = this.fb.group({
                    name: [this.inputs.name, Validators.required],
                  }); */
        break;
      default:
        this.t.get('CATALOGUE_OPERATIONS.FORM_NEW_TITLE').subscribe(t => this.titleAction = t);
        this.t.get('CATALOGUE_OPERATIONS.FORM_NEW_BUTTON').subscribe(t => this.btnAction = t);
        this.t.get('CATALOGUE_OPERATIONS.FORM_NEW_NAME').subscribe(t => this.inputLabelAction = t);
    }


  }

  emmitFile(file) {
    this.fileImg.emit(file);
    this.emmitCropper(true);
  }

  emmitCropper(state) {
    this.displaycrop.emit(state);
  }

  go_next(catId) {
    setTimeout(() => {
      this.submited = false;
      this.selectNew.emit(catId);

      if (this.otherDest) {
        this.router.navigate(['/ingresos', this.routerAlert]);
      }

    }, 0);
  }

  private selectNewCatalogue(id) {
    this.SessionService.setCatalogue(id);
    this.go_next(id);
  }

  onSubmit(catalogueDataForm: FormGroup) {
    const data = { name: catalogueDataForm.value.name, IsSubcatalogue: true, Icon: null }
    this.submited = true;

    if (catalogueDataForm.valid)
      switch (this.action.value) {
        case 'edit':
          const editData = { ...data, id: this.action.key, Icon: this.croppedimg || null };
          this._catalogueService.update(editData).then(() => {
            this.t.get('SHARED.SUCCESS_EDIT_CATALOGUE').subscribe(t => {
              const s = { type: 'success', detail: t };
              this.success.emit(s);
              this.submited = false;
            });
          }).catch(() => {
            this.t.get('ERRORS.OPERATIONS').subscribe(t => {
              const s = { type: 'success', detail: t };
              this.success.emit(s);
              this.submited = false;
            });
          });
          break;
        case 'clone':
          this.isLoading = true;
          this._catalogueService.clone(this.action.key.id, catalogueDataForm.value.name).then((r) => {
            this.isLoading = false;
            this.submited = false;
            if (this.otherDest) {
              this.routerAlert = {
                alert: 1,
                msg: 2,
                type: 1
              };
              this.selectNewCatalogue(r.data);
            }
            else {
              this.t.get('SHARED.SUCCESS_CLONE_CATALOGUE').subscribe(t => {
                const s = { type: 'success', detail: t };
                this.isLoading = false;
                this.success.emit(s);
                this.selectNewCatalogue(r.data);
              })
            }
          }).catch(() => {
            this.isLoading = false;
            this.submited = false;
            if (this.otherDest) {
              this.routerAlert = {
                alert: 1,
                msg: 3,
                type: 2
              };
            } else {
              this.t.get('ERRORS.OPERATIONS').subscribe(t => {
                const s = { type: 'error', detail: t };
                this.success.emit(s);
              });
            }
          });
          break;
        default:
          this.isLoading = true;
          data.Icon = this.croppedimg || null;
          this._catalogueService.create(data).then((r) => {
            this.isLoading = false;
            this.submited = false;
            if (this.otherDest) {
              this.routerAlert = {
                alert: 1,
                msg: 1,
                type: 1
              };
              this.selectNewCatalogue(r.data);
            } else {
              this.t.get('SHARED.SUCCESS_NEW_CATALOGUE').subscribe(t => {
                this.submited = false;
                const s = { type: 'success', detail: t };
                this.success.emit(s);
                this.selectNewCatalogue(r.data);
              });
            }
          }).catch(e => {
            this.isLoading = false;
            this.submited = false;
            if (this.otherDest) {
              this.routerAlert = {
                alert: 1,
                msg: 3,
                type: 2
              };
            } else {
              this.t.get('ERRORS.OPERATIONS').subscribe(t => {
                const s = { type: 'error', detail: t };
                this.success.emit(s);
              });
            }
          });
      }
  }
}
