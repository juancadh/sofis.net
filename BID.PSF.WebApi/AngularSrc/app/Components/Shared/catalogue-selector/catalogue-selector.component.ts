import { ApiEndpoints } from './../../../api-helper/api-endpoints';
import { ApiService } from './../../../api-helper/api.service';
import { Component, OnInit, Input, Output, OnChanges, SimpleChange, SimpleChanges, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-catalogue-selector',
  templateUrl: './catalogue-selector.component.html',
  styleUrls: ['./catalogue-selector.component.css']
})
export class CatalogueSelectorComponent implements OnInit {

  @Input() catalogueInput;
  @Output() openModal: EventEmitter<boolean> = new EventEmitter<boolean>(); // true to open selector modal
  catName: string;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  modalHandler(action) {
    this.openModal.emit(action);
    this.ngOnInit();
  }
}
