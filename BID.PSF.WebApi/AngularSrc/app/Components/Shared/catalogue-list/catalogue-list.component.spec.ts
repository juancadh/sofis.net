import { CatalogueData } from './../../../api-helper/models/catalogue-data';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogueListComponent } from './catalogue-list.component';

describe('CatalogueListComponent', () => {
  let component: CatalogueListComponent;
  let fixture: ComponentFixture<CatalogueListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CatalogueListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('getImg_IfCatalogueIconIsNull_should_be_stringAndEqualToDefaultUrlImg', () => {
    const fixure = TestBed.createComponent(CatalogueListComponent);
    const Component = fixure.componentInstance;

    const resp = Component.getImg(Component.currentCatalogue);

    expect(typeof resp).toBe('string');
    expect(resp).toEqual(Component.defaultImgUrl);

  });

  it('getImg_IfCatalogueImgIsNotNull_should_be_stringAndEqualToCatalogueIcon', () => {
    const fixure = TestBed.createComponent(CatalogueListComponent);
    const Component = fixure.componentInstance;

    Component.currentCatalogue.Icon = 'test';

    const resp = Component.getImg(Component.currentCatalogue);

    expect(typeof resp).toBe('string');
    expect(resp).toEqual(Component.currentCatalogue.Icon);

  });

  it('actionHandler_whenCalled_should_be_emitAction', () => {
    const fixure = TestBed.createComponent(CatalogueListComponent);
    const Component = fixure.componentInstance;
    spyOn(component.modalAction, 'emit');

    const resp = Component.actionHandler('test', 1);

    fixure.detectChanges();

    expect(resp).toHaveBeenCalledWith({ value: 'test', key: 1 });

  });

  it('catalogueHandler_whenCalled_should_be_emitNewSelectedCatalogue', () => {
    const fixure = TestBed.createComponent(CatalogueListComponent);
    const Component = fixure.componentInstance;
    spyOn(component.actualSelectedEmmiter, 'emit');

    const resp = Component.catalogueHandler(component.currentCatalogue);

    fixure.detectChanges();

    expect(resp).toHaveBeenCalledWith(component.currentCatalogue);

  });
});
