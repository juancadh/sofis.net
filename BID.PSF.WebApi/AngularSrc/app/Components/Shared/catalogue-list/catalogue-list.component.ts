import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { CatalogueData } from './../../../api-helper/models/catalogue-data';

@Component({
  selector: 'app-catalogue-list',
  templateUrl: './catalogue-list.component.html',
  styleUrls: ['./catalogue-list.component.css']
})
export class CatalogueListComponent implements OnInit {

  currentCatalogue: CatalogueData = {
    id: null,
    selected: false,
    name: '',
    Icon: null
  };

  defaultImgUrl: string = "/AngularSrc/assets/images/catalogo-gris@2x.png";
  @Input() catalogueList: CatalogueData[];
  @Input() loadCurrent: CatalogueData;
  @Output() actualSelectedEmmiter = new EventEmitter();
  @Output() modalAction = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.currentCatalogue = this.loadCurrent;
  }

  getImg(catalogue): string {
   if(catalogue.icon && catalogue.icon !== null)
     return catalogue.icon;

   return this.defaultImgUrl;
   
  }

  catalogueHandler(catalogue) {
    this.currentCatalogue = catalogue;
    this.actualSelectedEmmiter.emit(this.currentCatalogue);
  }

  actionHandler(value, key){
    const action = { value: value, key: key }
    this.modalAction.emit(action);
  }
}
