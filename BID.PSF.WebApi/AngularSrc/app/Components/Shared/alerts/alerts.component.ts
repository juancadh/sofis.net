import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { timeInterval } from 'rxjs/operator/timeInterval';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {

  @Input() type; // 'success' , 'error' or 'warning'
  @Input() time;
  @Input() detail;

  @Output() closeEvent = new EventEmitter;

  constructor() {
    if (!this.time) {
      this.time = 4000;
    }
  }

  ngOnInit() {
    setTimeout(() => this.closeEvent.emit(true), this.time);
  }
}
