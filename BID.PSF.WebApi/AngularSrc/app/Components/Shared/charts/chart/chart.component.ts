import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

import * as Chart from 'chart.js'
import * as ChartAnnotation from 'chartjs-plugin-annotation';

interface AxesLimit {
    max?: number;
    min?: number;
}

@Component({
    selector: 'app-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.css']
})

export class ChartComponent implements OnInit {
    canvas: any;
    ctx: any;
    canvasReady = false;
    instanceName = this.create_UUID();
    SofisChart: Chart; //Instance

    @Input() datasets;
    @Input() labels;
    @Input() options;
    @Input() colors: any[];
    @Input() legend;
    @Input() chartType;
    @Input() width = "400";
    @Input() height = "250";
    @Input() YAxisMarginRate = 0.1;
    @Input() borderWidth = 1;
    @Input() lineTension = 0;
    @Input() pointRadius = [2, 2, 2, 2];
    @Input() pointHoverBorderWidth = 3;
    @Input() precision = 2;

    constructor() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if ((changes['datasets'] || changes['labels'] || changes['options'] || changes['colors'] || changes['legend'] || changes['chartType']) && this.instanceName) {
            this.ngOnInit();
        }
    }

    ngOnInit() {
        setTimeout(async () => {
            await this.loadData().then((r) => {
                this.canvasReady = r;
                if (r) {
                    let limits = this.setAxisLimits(this.datasets);
                    this.options.scales.yAxes[0].ticks.suggestedMax = parseInt(limits.max.toFixed(0));
                    this.options.scales.yAxes[0].ticks.suggestedMin = parseInt(limits.min.toFixed(0));
                    this.initGraph();
                }
            });
        }, 500);
    }

    loadData(): Promise<boolean> {
        var result = false;
        this.datasets = this.datasets.map((r, i) => {
            var colorLimit = this.colors && i > this.colors.length - 1 ? this.colors.length - 1 : i;
            return {
                label: r.label,
                data: r.data.map(n => this.round(n)),
                borderWidth: r.borderWidth || this.borderWidth,
                backgroundColor: this.colors && this.colors[colorLimit].backgroundColor,
                borderColor: this.colors && this.colors[colorLimit].borderColor,
                lineTension: this.lineTension,
                pointRadius: this.pointRadius,
                pointHoverBorderWidth: this.pointHoverBorderWidth,
                fill: this.colors[colorLimit].fill || false
            }
        });

        if (this.datasets.length > 0 && this.options && this.colors && this.chartType && this.instanceName) {
            result = true;
        }
        return new Promise((resolve, reject) => resolve(result));
    }

    initGraph() {

        let namedChartAnnotation = ChartAnnotation;
        namedChartAnnotation["id"] = "annotation";
        Chart.pluginService.register(namedChartAnnotation);
        this.canvas = document.getElementById(this.instanceName);

        if (this.canvas != null) {
            if (!this.SofisChart) {
                this.ctx = this.canvas.getContext('2d');
                this.SofisChart = new Chart(this.ctx, {
                    type: this.chartType,
                    data: {
                        labels: this.labels,
                        datasets: this.datasets
                    },
                    options: {
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            callbacks: {
                              label: function(tooltipItems, data){
                                return data.datasets[tooltipItems.datasetIndex].label + ' : ' + tooltipItems.yLabel.toLocaleString("en-US", {maximumFractionDigits:2});
                              }
                            }
                          },
                        ...this.options
                    }
                });
                this.SofisChart.render();
            } else {
                this.SofisChart.config.type = this.chartType;
                this.SofisChart.config.data.datasets = this.datasets;
                this.SofisChart.config.data.labels = this.labels;
                this.SofisChart.update();
            }
        }
    }

    private setAxisLimits(data: any[]): AxesLimit {
        let result: AxesLimit = {
            max: 0,
            min: 0
        };

        if (data) {
            let maxOfEachLine: number[] = new Array(0);
            let minIfEachLine: number[] = new Array(0);
            data.forEach((i) => {
                if (i.data) {
                    let values = i.data.filter(i => !isNaN(i));

                    maxOfEachLine.push(Math.max(...values));

                    minIfEachLine.push(Math.min(...values));
                }
            });

            let max = Math.max(...maxOfEachLine);
            let min = Math.min(...minIfEachLine);

            result.max = max + (max * this.YAxisMarginRate);
            result.min = min - (min * this.YAxisMarginRate);
        }

        return result;
    }

    private round(number) {
        return Number(parseFloat(number).toFixed(this.precision));
    }

    private create_UUID(): string {
        var dt = new Date().getTime();
        var uuid = 'SOFIS-CHART-xxx-yxx-yxx'.replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }

}