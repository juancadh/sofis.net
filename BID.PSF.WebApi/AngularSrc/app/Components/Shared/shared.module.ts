import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotTableModule } from 'angular-handsontable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HttpModule, Http } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { CatalogueSelectorComponent } from './catalogue-selector/catalogue-selector.component';
import { CatalogueListComponent } from './catalogue-list/catalogue-list.component';
import { NewCatalogueDataComponent } from './new-catalogue-data/new-catalogue-data.component';
import { CatalogueManagerService } from '../../api-helper/CatalogueManager/catalogue-manager.service';
import { AlertsComponent } from './alerts/alerts.component';
import { PercentagePipe } from './../../api-helper/pipes/percentage.pipe';
import { ChartComponent } from './charts/chart/chart.component';



// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './content/i18n/', '.json');
}

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [CatalogueSelectorComponent, CatalogueListComponent, NewCatalogueDataComponent, AlertsComponent, PercentagePipe, ChartComponent],
  exports: [CatalogueSelectorComponent, TranslateModule, CatalogueListComponent, NewCatalogueDataComponent, AlertsComponent, PercentagePipe, ChartComponent],
  providers: [CatalogueManagerService]
})
export class SharedModule { }