﻿// ReSharper disable InconsistentNaming
import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { ApiService } from './../../api-helper/api.service';
import { ApiEndpoints } from './../../api-helper/api-endpoints';

import { AuthenticationService } from './../../authentication/shared/authentication.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.css']
})

export class ForgotPasswordComponent {
    constructor(private router: Router, private ApiService: ApiService) { }

    email: string;
    invalidEmail = false;
    showForm = true;
    success = false;

    forgotPassword(): void {
        let data = {
            username: this.email
        }
    }

    redirect() {
        this.router.navigate(['/login']);
    }
}