import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SharedModule } from './../Components/Shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ProfileComponent } from './profile.component';


@NgModule({
  imports: [BrowserModule,FormsModule, SharedModule, RouterModule],
  declarations: [ProfileComponent, ForgotPasswordComponent, ResetPasswordComponent]
})
export class ProfileModule { }
