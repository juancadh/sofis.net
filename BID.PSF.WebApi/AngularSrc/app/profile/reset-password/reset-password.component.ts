﻿// ReSharper disable InconsistentNaming
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from './../../api-helper/api.service';

import { AuthenticationService } from './../../authentication/shared/authentication.service';

import { SessionService } from './../../api-helper/session.service';

import * as $ from 'jquery';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.css']
})

export class ResetPasswordComponent implements OnInit {
    constructor(private router: Router, private ApiService: ApiService, private AuthenticationService: AuthenticationService, private SessionService: SessionService) { }

    firstName: string;
    lastName: string;
    username: string;

    oldPassword: string;
    newPassword: string;
    repeatPassword: string;

    requiredoldpass = false;
    requirednewpass = false;
    passwordsDontMatch = false;

    saving = false;
    success = false;
    error = false;

    ngOnInit(): void {
        this.username = this.SessionService.getCurrentUser();
    }


    resetPassword(): void {
        window.location.href = "https://dev-446099.oktapreview.com/enduser/settings";
    }
}