﻿import { LanguageService } from './../api-helper/language.service';
// ReSharper disable InconsistentNaming
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api-helper/api.service';
import { ApiEndpoints } from '../api-helper/api-endpoints';
import { AuthenticationService } from '../authentication/shared/authentication.service';

import { TranslateService } from '@ngx-translate/core';
import { LanguageData } from '../api-helper/models/language-data';
import { CountryData } from '../api-helper/models/country-data';

import * as $ from 'jquery';

import { OktaAuthService } from '@okta/okta-angular';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    country: CountryData;
    language: LanguageData;
    institution: string;
    institutionType: string;
    title: string;
    receiveUpdates: boolean;
    interests: Array<string> = [];
    products: Array<string> = [];

    countriesList: CountryData[] = [];
    languagesList: LanguageData[] = [];
    institutionTypeList: Array<string> = [];
    titleList: Array<string> = [];
    interestList: Array<string> = [];
    productList: Array<string> = [];

    saving = false;
    success = false;
    error = false;

    requiredfname = false;
    requiredlname = false;

    isLoading = true;
    langReady = false;

    constructor(private router: Router, private ApiService: ApiService, private AuthenticationService: AuthenticationService, private oktaAuth: OktaAuthService, private translate: TranslateService, private readonly languageService: LanguageService) { }

    async ngOnInit() {

        $("#success-alert").hide();
        $("#error-alert").hide();

        await this.getSelectedLang().then(() => {
            this.isLoading = true;
            this.getLists();
        }).catch(err => {
            this.isLoading = false;
            this.error = true;
        });

    }

    async getSelectedLang() {
        await this.languageService.loadDBUserLanguage().then(result => {
            this.langReady = true;
            this.language = result.data.language;
        }).catch(err => {
            console.log(err);
            this.langReady = true;
            this.error = true;
        });
    }

    getProfile(): void {
        this.isLoading = true;
        const browserLang = this.translate.getBrowserLang();
        this.ApiService.get(`${ApiEndpoints.profile_getProfile}?culture=${browserLang}`, null).then(
            result => {
                this.firstName = result.data.firstName;
                this.lastName = result.data.lastName;
                this.email = result.data.email;
                this.country = result.data.country ? this.countriesList.filter(c => c.id === result.data.country.id)[0] : this.countriesList[0];
                this.language = result.data.language ? this.languagesList.filter(l => l.id === result.data.language.id)[0] : this.languagesList[0];
                this.institution = result.data.institution;
                this.institutionType = result.data.institutionType ? this.institutionTypeList.filter(t => t.indexOf(result.data.institutionType) >= 0)[0] : this.institutionTypeList[0];
                this.receiveUpdates = result.data.receiveUpdates;
                this.title = result.data.title;
                this.interests = this.interestList.filter(i => result.data.interests.indexOf(i) >= 0);
                this.products = this.productList.filter(i => result.data.products.indexOf(i) >= 0);
                this.username = this.email;
                this.isLoading = false;
            }).catch(err => {
                this.isLoading = false;
                this.error = true;
                console.log(err);
            });
    }

    getLists(): void {
        this.getProfile();

        this.ApiService.get(ApiEndpoints.profile_getLists, null).then(
            result => {
                this.countriesList = result.data.countries;
                this.languagesList = result.data.languages;
                this.institutionTypeList = result.data.institutionTypes;
                this.titleList = result.data.titles;

                result.data.interests.forEach((r) => {
                    return this.translate.get(r).subscribe(t => {
                        this.interestList.push(t);
                    });
                });

                result.data.products.forEach((r) => {
                    return this.translate.get(r).subscribe(t => {
                        this.productList.push(t);
                    });
                });

                this.isLoading = false;
            }).catch(err => {
                console.log(err);
                this.isLoading = false;
                this.error = true;
            });
    }

    updateInterests(interest): void {
        if (interest.checked) {
            this.interests.push(interest.value);
        } else {
            const interestIndex = this.interests.indexOf(interest.value);
            this.interests.splice(interestIndex, 1);
        }
    }

    updateProducts(product): void {
        if (product.checked) {
            this.products.push(product.value);
        } else {
            const productIndex = this.products.indexOf(product.value);
            this.products.splice(productIndex, 1);
        }
    }

    submit(): void {
        if (this.firstName === "")
            this.requiredfname = true;
        else
            this.requiredfname = false;

        if (this.lastName === "")
            this.requiredlname = true;
        else
            this.requiredlname = false;

        if (this.requiredfname || this.requiredlname)
            return;

        this.saving = true;
        const profile = {
            firstName: this.firstName,
            lastName: this.lastName,
            country: this.country,
            language: this.language,
            institution: this.institution,
            institutionType: this.institutionType,
            title: this.title,
            receiveUpdates: this.receiveUpdates,
            interests: this.interests,
            products: this.products
        };
        this.ApiService.put(ApiEndpoints.profile_updateProfile, null, profile).then(
            result => {
                this.saving = false;
                this.success = true;

                $("#success-alert").fadeTo(2000, 500).slideUp(500, () => {
                    $("#success-alert").slideUp(500);
                });
            },
            err => {
                this.saving = false
                this.error = true;

                $("#error-alert").fadeTo(2000, 500).slideUp(500, () => {
                    $("#error-alert").slideUp(500);
                });

                console.log(err);
            });
    }

    logout() {
        this.oktaAuth.logout();
        this.router.navigate(['/login']);
    }

}
