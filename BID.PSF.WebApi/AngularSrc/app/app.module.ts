import { Interceptor } from './api-helper/interceptor';
import { TopNavLogicService } from './api-helper/top-nav-logic.service';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { ChartsModule } from 'ng2-charts';
import { ProfileModule } from './profile/profile.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { CatalogueManagerService } from './api-helper/CatalogueManager/catalogue-manager.service';

import { LanguageService } from './api-helper/language.service';
import { AppComponent } from './app.component';
import { AuthenticationModule } from './authentication/authentication.module';
import { DataManagementModule } from './data-management/data-management.module';
import { MFMPModule } from './mfmp/mfmp.module';
import { ASDModule } from './asd/asd.module';
import { HelpModule } from './help/help.module';
import { TermsComponent } from './terms/terms.component';
import { ApiService } from './api-helper/api.service';
import { AuthenticationService } from './authentication/shared/authentication.service';
import { SessionService } from './api-helper/session.service';
import { AuthenticationRoutingModule } from './authentication/authentication-routing.module';

import { OktaAuthModule } from '@okta/okta-angular';
import { SofisApiService } from './api-helper/sofis-api/sofis-api.service';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { LimitcategorylevelPipe } from './api-helper/pipes/limitcategorylevel.pipe';

const config = {
    //issuer: 'https://dev-120142.oktapreview.com/oauth2/default',
    //redirectUri: 'http://localhost:27758/implicit/callback',
    //clientId: '0oag9xdt18b8EdL8d0h7',

    //redirectUri: 'http://psf-dev.azurewebsites.net/implicit/callback',
    //issuer: 'https://idbg.okta.com',

    issuer: 'https://idbg.oktapreview.com/oauth2/ausf3cgqokVAiqZjF0h7',
    redirectUri: 'http://sofis-uat.azurewebsites.net/implicit/callback',
    clientId: '0oaei55pnjbvjFJKR0h7'
}


const NotifierOptions: NotifierOptions = {
    position: {
        horizontal: {
            /**
             * Defines the horizontal position on the screen
             * @type {'left' | 'middle' | 'right'}
             */
            position: 'left',
            /**
             * Defines the horizontal distance to the screen edge (in px)
             * @type {number} 
             */
            distance: 0
        },
        vertical: {
            /**
             * Defines the vertical position on the screen
             * @type {'top' | 'bottom'}
             */
            position: 'top',

            /**
             * Defines the vertical distance to the screen edge (in px)
             * @type {number} 
             */
            distance: 0,
            /**
             * Defines the vertical gap, existing between multiple notifications (in px)
             * @type {number} 
             */
            gap: 5
        }
    },
    behaviour: {
        autoHide: 3000,
        onClick: 'hide',
        onMouseover: 'pauseAutoHide',
        showDismissButton: false,
        stacking: 1
    },
};


@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        NgbModule.forRoot(),
        FormsModule, HttpModule,
        AppRoutingModule,
        AuthenticationModule,
        DataManagementModule,
        MFMPModule,
        HelpModule,
        ASDModule,
        ChartsModule,
        ProfileModule,
        AngularFontAwesomeModule,
        OktaAuthModule.initAuth(config),
        HttpClientModule,
        NotifierModule.withConfig(NotifierOptions)
    ],
    declarations: [AppComponent, TermsComponent, LimitcategorylevelPipe],
    providers: [ApiService, AuthenticationService, SessionService, AuthenticationRoutingModule, SofisApiService, LanguageService, CatalogueManagerService, TopNavLogicService,  { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }],
    bootstrap: [AppComponent]
})
export class AppModule { }