﻿using System.Configuration;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Owin;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using Unity.WebApi;
using BID.PSF.Core.Enums;
using System.Threading;
using System.Web;

namespace BID.PSF.WebApi
{
    public partial class Startup
    {
        internal static IDataProtectionProvider DataProtectionProvider { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            var config = new HttpConfiguration
            {
                DependencyResolver = new UnityDependencyResolver(UnityConfig.GetConfiguredContainer())
            };
            if (ConfigurationManager.AppSettings.Get("AuthenticationType") == Enums.AuthenticationType.Okta.ToString())
            {
                DataProtectionProvider = app.GetDataProtectionProvider();
                var container = UnityConfig.GetConfiguredContainer();
                container.RegisterInstance(DataProtectionProvider);

                var authority = ConfigurationManager.AppSettings.Get("okta:Url");

                var configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(
                    authority + "/.well-known/openid-configuration",
                    new OpenIdConnectConfigurationRetriever(),
                    new HttpDocumentRetriever());

                app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        //ValidAudience = "api://default",
                        ValidAudience = ConfigurationManager.AppSettings.Get("TokenValidAudience"),
                        ValidIssuer = authority,
                        IssuerSigningKeyResolver = (token, securityToken, identifier, parameters) =>
                        {
                            var discoveryDocument = Task.Run(() => configurationManager.GetConfigurationAsync()).GetAwaiter().GetResult();
                            return discoveryDocument.SigningKeys;
                        }
                    }
                });
            }
            else
            {
                ConfigureOAuth(app);
                WebApiConfig.Register(config);
            }
            app.UseWebApi(config);

            // Avoid self referencing loops
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            // Configure JWT Bearer middleware
            // with an OpenID Connect Authority

        }
    }
}