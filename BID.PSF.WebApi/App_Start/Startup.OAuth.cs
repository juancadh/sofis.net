﻿using System;
using System.Configuration;
using BID.PSF.Core.Enums;
using BID.PSF.WebApi.Providers;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Practices.Unity;
using Owin;

namespace BID.PSF.WebApi
{
    public partial class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }


        public void ConfigureOAuth(IAppBuilder app)
        {
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            DataProtectionProvider = app.GetDataProtectionProvider();

            var container = UnityConfig.GetConfiguredContainer();
            container.RegisterInstance(DataProtectionProvider);

            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {

                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/oauth/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AuthorizationServerProvider(),
                RefreshTokenProvider = new RefreshTokenProvider()
            };
            if (ConfigurationManager.AppSettings.Get("AuthenticationType") == Enums.AuthenticationType.Identity.ToString())
            {
                oAuthServerOptions.Provider = new AuthorizationServerIdentityProvider();
            }

            // Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);
        }
    }
}