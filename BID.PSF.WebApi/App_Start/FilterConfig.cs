﻿using System.Web;
using System.Web.Mvc;

namespace BID.PSF.WebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ErrorHandler.AiHandleErrorAttribute());
            //filters.Add(new RequireHttpsAttribute());
        }
    }
}
