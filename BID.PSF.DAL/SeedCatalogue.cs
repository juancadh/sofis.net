﻿using System;
using System.Collections.Generic;
using System.Linq;
using BID.PSF.Core.Enums;
using BID.PSF.Core.Resources;
using BID.PSF.Model.DBModel;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BID.PSF.DAL
{
    public static class SeedCatalogue
    {
        public static void Seed(PSFContext context)
        {

            if (!context.SubCategories.Any())
            {
                var subCategory1 = new SubCategory { Name = Strings.SubCategoryName_LDTaxes, InternalName = "" };
                var subCategory2 = new SubCategory { Name = Strings.SubCategoryName_LDTransfers, InternalName = "" };
                var subCategory3 = new SubCategory { Name = Strings.SubCategoryName_LDSocialBenefits, InternalName = "" };
                var subCategory4 = new SubCategory { Name = Strings.SubCategoryName_LDGrants, InternalName = "" };
                var subCategory5 = new SubCategory { Name = Strings.SubCategoryName_LDGoodsandServices, InternalName = "" };
                var subCategory6 = new SubCategory { Name = Strings.SubCategoryName_LDPropertyIncome, InternalName = "" };
                var subCategory7 = new SubCategory { Name = Strings.SubCategoryName_LDOthers, InternalName = "" };

                context.SubCategories.Add(subCategory1);
                context.SubCategories.Add(subCategory2);
                context.SubCategories.Add(subCategory3);
                context.SubCategories.Add(subCategory4);
                context.SubCategories.Add(subCategory5);
                context.SubCategories.Add(subCategory6);
                context.SubCategories.Add(subCategory7);

                context.SaveChanges();
            }

            if (!context.CurrencyUnits.Any())
            {
                var currency1 = new CurrencyUnit { Name = "Moneda Local", ShortName = "ML" };
                var currency2 = new CurrencyUnit { Name = "Moneda Extranjera", ShortName = "ME" };

                context.CurrencyUnits.Add(currency1);
                context.CurrencyUnits.Add(currency2);

                context.SaveChanges();
            }

            if (!context.Languages.Any())
            {
                context.Languages.Add(new Language()
                {
                    LongName = "Inglés",
                    EncodedName = "en",
                    ShortName = "ENG"
                });
                context.Languages.Add(new Language()
                {
                    LongName = "Español",
                    EncodedName = "es",
                    ShortName = "SPA"
                });
                context.Languages.Add(new Language()
                {
                    LongName = "Francés",
                    EncodedName = "fr",
                    ShortName = "FRA"
                });
                context.Languages.Add(new Language()
                {
                    LongName = "Portugués",
                    EncodedName = "pt",
                    ShortName = "POR"
                });
            }

            context.SaveChanges();



        }
    }
}
