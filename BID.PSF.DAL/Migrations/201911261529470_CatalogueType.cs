namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CatalogueType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Catalogues", "Type", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Catalogues", "Type");
        }
    }
}
