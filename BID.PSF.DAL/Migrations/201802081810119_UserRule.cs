namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserRule : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserRules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        RuleId = c.Int(nullable: false),
                        Limits = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rules", t => t.RuleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RuleId);
            
            DropColumn("dbo.Rules", "Limits");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Rules", "Limits", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropForeignKey("dbo.UserRules", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserRules", "RuleId", "dbo.Rules");
            DropIndex("dbo.UserRules", new[] { "RuleId" });
            DropIndex("dbo.UserRules", new[] { "UserId" });
            DropTable("dbo.UserRules");
        }
    }
}
