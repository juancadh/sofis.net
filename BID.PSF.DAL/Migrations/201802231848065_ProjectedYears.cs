namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProjectedYears : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DynamicColumnValues", "Projected", c => c.Boolean(nullable: false));
            AddColumn("dbo.CatalogueColumnValues", "Projected", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CatalogueColumnValues", "Projected");
            DropColumn("dbo.DynamicColumnValues", "Projected");
        }
    }
}
