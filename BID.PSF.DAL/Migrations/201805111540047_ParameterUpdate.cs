namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParameterUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserParameters", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserParameters", new[] { "UserId" });
            AddColumn("dbo.UserParameters", "Email", c => c.String());
            DropColumn("dbo.UserParameters", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserParameters", "UserId", c => c.String(maxLength: 128));
            DropColumn("dbo.UserParameters", "Email");
            CreateIndex("dbo.UserParameters", "UserId");
            AddForeignKey("dbo.UserParameters", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
