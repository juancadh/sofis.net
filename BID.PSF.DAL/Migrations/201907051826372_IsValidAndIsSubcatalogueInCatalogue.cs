namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsValidAndIsSubcatalogueInCatalogue : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CatalogueShocks", "CatalogueId", "dbo.Catalogues");
            DropIndex("dbo.CatalogueShocks", new[] { "CatalogueId" });
            AddColumn("dbo.Catalogues", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Catalogues", "IsSubcatalogue", c => c.Boolean(nullable: false));
            DropTable("dbo.CatalogueShocks");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CatalogueShocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CatalogueId = c.Int(nullable: false),
                        Shock = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.Catalogues", "IsSubcatalogue");
            DropColumn("dbo.Catalogues", "IsActive");
            CreateIndex("dbo.CatalogueShocks", "CatalogueId");
            AddForeignKey("dbo.CatalogueShocks", "CatalogueId", "dbo.Catalogues", "Id", cascadeDelete: true);
        }
    }
}
