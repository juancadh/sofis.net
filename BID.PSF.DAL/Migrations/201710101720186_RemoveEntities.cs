namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveEntities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Debts", "Id", "dbo.YearDatas");
            DropForeignKey("dbo.Expenses", "Id", "dbo.YearDatas");
            DropForeignKey("dbo.Incomes", "Id", "dbo.YearDatas");
            DropForeignKey("dbo.Macroeconomies", "Id", "dbo.YearDatas");
            DropIndex("dbo.Debts", new[] { "Id" });
            DropIndex("dbo.Expenses", new[] { "Id" });
            DropIndex("dbo.Incomes", new[] { "Id" });
            DropIndex("dbo.Macroeconomies", new[] { "Id" });
            DropTable("dbo.Debts");
            DropTable("dbo.Expenses");
            DropTable("dbo.Incomes");
            DropTable("dbo.Macroeconomies");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Macroeconomies",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        GDP = c.Decimal(precision: 18, scale: 2),
                        NacionalCPI = c.Decimal(precision: 18, scale: 2),
                        USDExchangeRate = c.Decimal(precision: 18, scale: 2),
                        GDPEntity = c.Decimal(precision: 18, scale: 2),
                        InternalCPI = c.Decimal(precision: 18, scale: 2),
                        InternalInterestRate = c.Decimal(precision: 18, scale: 2),
                        DomesticInterestRate = c.Decimal(precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Incomes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TotalIncome = c.Decimal(precision: 18, scale: 2),
                        FreeDestinationIncome = c.Decimal(precision: 18, scale: 2),
                        NotAvailableIncome = c.Decimal(precision: 18, scale: 2),
                        FreeDestinationTaxes = c.Decimal(precision: 18, scale: 2),
                        TenureAndUseOfVehicles = c.Decimal(precision: 18, scale: 2),
                        Others = c.Decimal(precision: 18, scale: 2),
                        NotFreeDestinationTaxes = c.Decimal(precision: 18, scale: 2),
                        Payroll = c.Decimal(precision: 18, scale: 2),
                        FurniturePropertyDisposalVehicleAcquisition = c.Decimal(precision: 18, scale: 2),
                        Lodging = c.Decimal(precision: 18, scale: 2),
                        LotteriesRafflesAndSweepstakes = c.Decimal(precision: 18, scale: 2),
                        TotalRightsAndProducts = c.Decimal(precision: 18, scale: 2),
                        Exploitation = c.Decimal(precision: 18, scale: 2),
                        Contributions = c.Decimal(precision: 18, scale: 2),
                        ContributionImprovements = c.Decimal(precision: 18, scale: 2),
                        FederalTaxRevenueSharesAndIncentives = c.Decimal(precision: 18, scale: 2),
                        GeneralFund = c.Decimal(precision: 18, scale: 2),
                        InspectionFundForFederalEntities = c.Decimal(precision: 18, scale: 2),
                        SpecialTaxOnProductionAndServices = c.Decimal(precision: 18, scale: 2),
                        TaxOnTenureOrUseOfVehicles = c.Decimal(precision: 18, scale: 2),
                        MunicipalDevelopmentFund = c.Decimal(precision: 18, scale: 2),
                        NewCarTax = c.Decimal(precision: 18, scale: 2),
                        RFP = c.Decimal(precision: 18, scale: 2),
                        OtherParticipableIncentives = c.Decimal(precision: 18, scale: 2),
                        FEIEF = c.Decimal(precision: 18, scale: 2),
                        NotFreeTransfersDestination = c.Decimal(precision: 18, scale: 2),
                        CFHealthServices = c.Decimal(precision: 18, scale: 2),
                        CFStrengtheningMunicipalities = c.Decimal(precision: 18, scale: 2),
                        CFStrengtheningFederalEntities = c.Decimal(precision: 18, scale: 2),
                        CFMunicipalSocialInfraestructure = c.Decimal(precision: 18, scale: 2),
                        CFPublicSecurityStatesFederalDistrict = c.Decimal(precision: 18, scale: 2),
                        CFAdultTechnologicalEducation = c.Decimal(precision: 18, scale: 2),
                        CFStateSocialInfraestructure = c.Decimal(precision: 18, scale: 2),
                        MultipleContributionFund = c.Decimal(precision: 18, scale: 2),
                        TransfersHigherAndHigherSecondaryEducation = c.Decimal(precision: 18, scale: 2),
                        PopularInsurance = c.Decimal(precision: 18, scale: 2),
                        PartnershipProgramField = c.Decimal(precision: 18, scale: 2),
                        RegionalDevelopmentProgram = c.Decimal(precision: 18, scale: 2),
                        AgreedProgramsConagua = c.Decimal(precision: 18, scale: 2),
                        SecretaryPublicEducation = c.Decimal(precision: 18, scale: 2),
                        HealthSecretary = c.Decimal(precision: 18, scale: 2),
                        SecretaryFinancePublicCredit = c.Decimal(precision: 18, scale: 2),
                        SecretaryCommunicationsTransportation = c.Decimal(precision: 18, scale: 2),
                        PublicSecurity = c.Decimal(precision: 18, scale: 2),
                        MetropolitanFunds = c.Decimal(precision: 18, scale: 2),
                        RemainderTransfersAgreementsFederalGovernment = c.Decimal(precision: 18, scale: 2),
                        TrusteeInfraestructureStates = c.Decimal(precision: 18, scale: 2),
                        ENCARB = c.Decimal(precision: 18, scale: 2),
                        NationalSportsCommissionSportsInfraestructure = c.Decimal(precision: 18, scale: 2),
                        CollegeScientificTechnologicalStudiesStateCoahuila = c.Decimal(precision: 18, scale: 2),
                        ProgramControlFederalizedCase = c.Decimal(precision: 18, scale: 2),
                        SecretaryEnvironmentNaturalResources = c.Decimal(precision: 18, scale: 2),
                        SecretarySocialDevelopment = c.Decimal(precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Expenses",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TotalExpenses = c.Decimal(precision: 18, scale: 2),
                        PersonalServices = c.Decimal(precision: 18, scale: 2),
                        PersonalServicesCE = c.Decimal(precision: 18, scale: 2),
                        GeneralServices = c.Decimal(precision: 18, scale: 2),
                        MaterialsAndSuppliesCE = c.Decimal(precision: 18, scale: 2),
                        GeneralServicesCE = c.Decimal(precision: 18, scale: 2),
                        Investment = c.Decimal(precision: 18, scale: 2),
                        RealStateInvestment = c.Decimal(precision: 18, scale: 2),
                        StatePublicInvestment = c.Decimal(precision: 18, scale: 2),
                        ContributionsAndTrustsInvestment = c.Decimal(precision: 18, scale: 2),
                        Transfers = c.Decimal(precision: 18, scale: 2),
                        SubsidiesTransfers = c.Decimal(precision: 18, scale: 2),
                        TransfersAndReasignedSpending = c.Decimal(precision: 18, scale: 2),
                        PublicSectorTransfersAndAllocations = c.Decimal(precision: 18, scale: 2),
                        PublicSectorRestTransfers = c.Decimal(precision: 18, scale: 2),
                        SocialBenefits = c.Decimal(precision: 18, scale: 2),
                        SocialHelps = c.Decimal(precision: 18, scale: 2),
                        RealStateManagement = c.Decimal(precision: 18, scale: 2),
                        Others = c.Decimal(precision: 18, scale: 2),
                        TrustTranfersAndOther = c.Decimal(precision: 18, scale: 2),
                        MunicipalitiesAssignedResources = c.Decimal(precision: 18, scale: 2),
                        Investments = c.Decimal(precision: 18, scale: 2),
                        Shares = c.Decimal(precision: 18, scale: 2),
                        Conventions = c.Decimal(precision: 18, scale: 2),
                        IncomeReturns = c.Decimal(precision: 18, scale: 2),
                        StockIssueTrusts = c.Decimal(precision: 18, scale: 2),
                        OrganizationAgreements = c.Decimal(precision: 18, scale: 2),
                        ADEFAS = c.Decimal(precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Debts",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TotalDebt = c.Decimal(precision: 18, scale: 2),
                        HistoricalDebtVariableRate = c.Decimal(precision: 18, scale: 2),
                        HistoricalDebtFixedRate = c.Decimal(precision: 18, scale: 2),
                        TotalAmortization = c.Decimal(precision: 18, scale: 2),
                        AmortizationHistoricalDebtVariableRate = c.Decimal(precision: 18, scale: 2),
                        AmortizationHistoricalDebtFixedRate = c.Decimal(precision: 18, scale: 2),
                        TotalPaymentForInterests = c.Decimal(precision: 18, scale: 2),
                        InterestRateHistoricalDebtVariableRate = c.Decimal(precision: 18, scale: 2),
                        InterestDebtHistoricalRateFixedRate = c.Decimal(precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Macroeconomies", "Id");
            CreateIndex("dbo.Incomes", "Id");
            CreateIndex("dbo.Expenses", "Id");
            CreateIndex("dbo.Debts", "Id");
            AddForeignKey("dbo.Macroeconomies", "Id", "dbo.YearDatas", "Id");
            AddForeignKey("dbo.Incomes", "Id", "dbo.YearDatas", "Id");
            AddForeignKey("dbo.Expenses", "Id", "dbo.YearDatas", "Id");
            AddForeignKey("dbo.Debts", "Id", "dbo.YearDatas", "Id");
        }
    }
}
