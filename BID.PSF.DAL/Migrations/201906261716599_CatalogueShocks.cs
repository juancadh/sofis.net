namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CatalogueShocks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CatalogueShocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CatalogueId = c.Int(nullable: false),
                        Shock = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Catalogues", t => t.CatalogueId, cascadeDelete: true)
                .Index(t => t.CatalogueId);
            
        }
        
        public override void Down()
        {

        }
    }
}
