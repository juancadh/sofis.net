namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IsDefaultColumnCatalogueColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CatalogueColumns", "IsDefaultColumn", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CatalogueColumns", "IsDefaultColumn");
        }
    }
}
