namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DynamicColumns : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.DynamicColumns", new[] { "DynamicColumnColumnId" });
            RenameColumn(table: "dbo.DynamicColumns", name: "DynamicColumnColumnId", newName: "ParentDynamicColumnId");
            CreateTable(
                "dbo.SubCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        InternalName = c.String(),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.DynamicColumns", "SubCategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.DynamicColumns", "ParentBaseColumnId", c => c.Int());
            AlterColumn("dbo.DynamicColumns", "ParentDynamicColumnId", c => c.Int());
            CreateIndex("dbo.DynamicColumns", "SubCategoryId");
            CreateIndex("dbo.DynamicColumns", "ParentBaseColumnId");
            CreateIndex("dbo.DynamicColumns", "ParentDynamicColumnId");
            AddForeignKey("dbo.DynamicColumns", "ParentBaseColumnId", "dbo.CatalogueColumns", "Id");
            AddForeignKey("dbo.DynamicColumns", "SubCategoryId", "dbo.SubCategories", "Id", cascadeDelete: true);
            DropColumn("dbo.DynamicColumns", "SubCategory");
            DropColumn("dbo.DynamicColumns", "ParentBaseColumn");
            DropColumn("dbo.DynamicColumns", "Category");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DynamicColumns", "Category", c => c.Int(nullable: false));
            AddColumn("dbo.DynamicColumns", "ParentBaseColumn", c => c.String());
            AddColumn("dbo.DynamicColumns", "SubCategory", c => c.String());
            DropForeignKey("dbo.DynamicColumns", "SubCategoryId", "dbo.SubCategories");
            DropForeignKey("dbo.DynamicColumns", "ParentBaseColumnId", "dbo.CatalogueColumns");
            DropIndex("dbo.DynamicColumns", new[] { "ParentDynamicColumnId" });
            DropIndex("dbo.DynamicColumns", new[] { "ParentBaseColumnId" });
            DropIndex("dbo.DynamicColumns", new[] { "SubCategoryId" });
            AlterColumn("dbo.DynamicColumns", "ParentDynamicColumnId", c => c.Int(nullable: false));
            DropColumn("dbo.DynamicColumns", "ParentBaseColumnId");
            DropColumn("dbo.DynamicColumns", "SubCategoryId");
            DropTable("dbo.SubCategories");
            RenameColumn(table: "dbo.DynamicColumns", name: "ParentDynamicColumnId", newName: "DynamicColumnColumnId");
            CreateIndex("dbo.DynamicColumns", "DynamicColumnColumnId");
        }
    }
}
