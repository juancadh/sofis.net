using System.Linq;
using BID.PSF.Core.Helpers;
using BID.PSF.Model.DBModel;

namespace BID.PSF.DAL.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<PSFContext>
    {
        // Add-Migration -Name "InitialMigration" -ProjectName "BID.PSF.DAL" -StartUpProjectName "BID.PSF.DAL"

        // Update-Database -ProjectName "BID.PSF.DAL" -StartUpProjectName "BID.PSF.DAL"
        
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(PSFContext context)
        {
            if (!context.OAuthClients.Any())
            {
                var client = new OAuthClient
                {
                    Id = "DefaultClient",
                    Secret = OAuthHelper.GetHash("abc@123"),
                    Name = "Client Id",
                    ApplicationType = OAuthClient.ApplicationTypes.JavaScript,
                    Active = true,
                    RefreshTokenLifeTime = 7200,
                    AllowedOrigin = "*"

                };

                context.OAuthClients.Add(client);
                context.SaveChanges();
            }

            // TODO: Remove dummy country and municipality
            if (!context.Countries.Any())
            {
                context.Countries.AddRange(new List<Country>
                {
                    new Country {Name = "Argentina" },
                    new Country {Name = "Bahamas" },
                    new Country {Name = "Barbados" },
                    new Country {Name = "Belize" },
                    new Country {Name = "Bolivia" },
                    new Country {Name = "Brazil" },
                    new Country {Name = "Chile" },
                    new Country {Name = "Colombia" },
                    new Country {Name = "Costa Rica" },
                    new Country {Name = "Dominican Republic" },
                    new Country {Name = "Ecuador" },
                    new Country {Name = "El Salvador" },
                    new Country {Name = "Guatemala" },
                    new Country {Name = "Guyana" },
                    new Country {Name = "Haiti" },
                    new Country {Name = "Honduras" },
                    new Country {Name = "Jamaica" },
                    new Country {Name = "Mexico" },
                    new Country {Name = "Nicaragua" },
                    new Country {Name = "Panama" },
                    new Country {Name = "Paraguay" },
                    new Country {Name = "Peru" },
                    new Country {Name = "Suriname" },
                    new Country {Name = "Trinidad & Tobago " },
                    new Country {Name = "United States" },
                    new Country {Name = "Uruguay" },
                    new Country {Name = "Venezuela" },
                });

                context.SaveChanges();
            }

            if (!context.Languages.Any())
            {
                context.Languages.AddRange(new List<Language>
                {
                    new Language { LongName = "Ingl�s", ShortName = "ENG" },
                    new Language { LongName = "Espa�ol", ShortName = "SPA" },
                    new Language { LongName = "Franc�s", ShortName = "FRA" },
                    new Language { LongName = "Portugu�s", ShortName = "POR" }
                });

                context.SaveChanges();
            }

            if (!context.Rules.Any())
            {
                var r1 = new Rule { Indicator = "Gastos Operacionales / Ingreso Disponible" };
                var r2 = new Rule { Indicator = "Cambio de Gastos Operacionales / Cambio de Ingreso Disponible" };
                var r3 = new Rule { Indicator = "Balance Primario / Ingreso Disponible" };
                var r4 = new Rule { Indicator = "Balance Fiscal / Ingreso Disponible" };
                var r5 = new Rule { Indicator = "(Balance Fiscal - Inversi�n) / Ingreso Disponible" };
                var r6 = new Rule { Indicator = "Servicio de la Deuda / Balance Primario" };
                var r7 = new Rule { Indicator = "Servicio de la Deuda / Ingreso Disponible" };
                var r8 = new Rule { Indicator = "Cambio en Nivel de Endeudamiento / Ingreso Disponible" };
                var r9 = new Rule { Indicator = "Nivel de la Deuda / Ingreso Disponible" };

                context.Rules.Add(r1);
                context.Rules.Add(r2);
                context.Rules.Add(r3);
                context.Rules.Add(r4);
                context.Rules.Add(r5);
                context.Rules.Add(r6);
                context.Rules.Add(r7);
                context.Rules.Add(r8);
                context.Rules.Add(r9);

                context.SaveChanges();
            }

            SeedCatalogue.Seed(context);
        }
    }
}
