namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Profile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Product = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserParameters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        CatalogueColumnId = c.Int(nullable: false),
                        Parameter = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CatalogueColumns", t => t.CatalogueColumnId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CatalogueColumnId);
            
            AddColumn("dbo.AspNetUsers", "Institution", c => c.String());
            AddColumn("dbo.AspNetUsers", "InstitutionType", c => c.Int(nullable: false));
            DropColumn("dbo.AspNetUsers", "Organization");
            DropColumn("dbo.AspNetUsers", "Charge");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Charge", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Organization", c => c.String());
            DropForeignKey("dbo.UserParameters", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserParameters", "CatalogueColumnId", "dbo.CatalogueColumns");
            DropForeignKey("dbo.UserProducts", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserParameters", new[] { "CatalogueColumnId" });
            DropIndex("dbo.UserParameters", new[] { "UserId" });
            DropIndex("dbo.UserProducts", new[] { "UserId" });
            DropColumn("dbo.AspNetUsers", "InstitutionType");
            DropColumn("dbo.AspNetUsers", "Institution");
            DropTable("dbo.UserParameters");
            DropTable("dbo.UserProducts");
        }
    }
}
