namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Catalogues : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.YearDatas", "MunicipalityId", "dbo.Municipalities");
            DropIndex("dbo.YearDatas", new[] { "MunicipalityId" });
            CreateTable(
                "dbo.CatalogueColumns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Index = c.Int(nullable: false),
                        ColumnName = c.String(),
                        InternalColumnName = c.String(),
                        Category = c.Int(nullable: false),
                        CatalogueId = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Catalogues", t => t.CatalogueId, cascadeDelete: true)
                .Index(t => t.CatalogueId);
            
            CreateTable(
                "dbo.Catalogues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DataSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CatalogueId = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Catalogues", t => t.CatalogueId, cascadeDelete: true)
                .Index(t => t.CatalogueId);
            
            CreateTable(
                "dbo.DynamicColumns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubCategory = c.String(),
                        Index = c.Int(nullable: false),
                        CurrencyUnitId = c.Int(nullable: false),
                        DataSource = c.String(),
                        ParentBaseColumn = c.String(),
                        DataSetId = c.Int(nullable: false),
                        DynamicColumnColumnId = c.Int(nullable: false),
                        Category = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CurrencyUnits", t => t.CurrencyUnitId, cascadeDelete: true)
                .ForeignKey("dbo.DataSets", t => t.DataSetId, cascadeDelete: true)
                .ForeignKey("dbo.DynamicColumns", t => t.DynamicColumnColumnId)
                .Index(t => t.CurrencyUnitId)
                .Index(t => t.DataSetId)
                .Index(t => t.DynamicColumnColumnId);
            
            CreateTable(
                "dbo.CurrencyUnits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ShortName = c.String(),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DynamicColumnValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        YearDataId = c.Int(nullable: false),
                        DynamicColumnId = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DynamicColumns", t => t.DynamicColumnId, cascadeDelete: true)
                .ForeignKey("dbo.YearDatas", t => t.YearDataId)
                .Index(t => t.YearDataId)
                .Index(t => t.DynamicColumnId);
            
            CreateTable(
                "dbo.CatalogueColumnValues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        YearDataId = c.Int(nullable: false),
                        CatalogueColumnId = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CatalogueColumns", t => t.CatalogueColumnId, cascadeDelete: true)
                .ForeignKey("dbo.YearDatas", t => t.YearDataId)
                .Index(t => t.YearDataId)
                .Index(t => t.CatalogueColumnId);
            
            CreateTable(
                "dbo.UserDataSets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        DataSetId = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DataSets", t => t.DataSetId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.DataSetId);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LongName = c.String(),
                        ShortName = c.String(),
                        EncodedName = c.String(),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserInterests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Interest = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.YearDatas", "DataSetId", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Organization", c => c.String());
            AddColumn("dbo.AspNetUsers", "Charge", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "Title", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "FiscalManagementNewsletter", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "CountryId", c => c.Int());
            AddColumn("dbo.AspNetUsers", "LanguageId", c => c.Int());
            CreateIndex("dbo.YearDatas", "DataSetId");
            CreateIndex("dbo.AspNetUsers", "CountryId");
            CreateIndex("dbo.AspNetUsers", "LanguageId");
            AddForeignKey("dbo.YearDatas", "DataSetId", "dbo.DataSets", "Id", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUsers", "CountryId", "dbo.Countries", "Id");
            AddForeignKey("dbo.AspNetUsers", "LanguageId", "dbo.Languages", "Id");
            DropColumn("dbo.YearDatas", "MunicipalityId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.YearDatas", "MunicipalityId", c => c.Int(nullable: false));
            DropForeignKey("dbo.UserInterests", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserDataSets", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "LanguageId", "dbo.Languages");
            DropForeignKey("dbo.AspNetUsers", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.UserDataSets", "DataSetId", "dbo.DataSets");
            DropForeignKey("dbo.DynamicColumnValues", "YearDataId", "dbo.YearDatas");
            DropForeignKey("dbo.YearDatas", "DataSetId", "dbo.DataSets");
            DropForeignKey("dbo.CatalogueColumnValues", "YearDataId", "dbo.YearDatas");
            DropForeignKey("dbo.CatalogueColumnValues", "CatalogueColumnId", "dbo.CatalogueColumns");
            DropForeignKey("dbo.DynamicColumnValues", "DynamicColumnId", "dbo.DynamicColumns");
            DropForeignKey("dbo.DynamicColumns", "DynamicColumnColumnId", "dbo.DynamicColumns");
            DropForeignKey("dbo.DynamicColumns", "DataSetId", "dbo.DataSets");
            DropForeignKey("dbo.DynamicColumns", "CurrencyUnitId", "dbo.CurrencyUnits");
            DropForeignKey("dbo.DataSets", "CatalogueId", "dbo.Catalogues");
            DropForeignKey("dbo.CatalogueColumns", "CatalogueId", "dbo.Catalogues");
            DropIndex("dbo.UserInterests", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "LanguageId" });
            DropIndex("dbo.AspNetUsers", new[] { "CountryId" });
            DropIndex("dbo.UserDataSets", new[] { "DataSetId" });
            DropIndex("dbo.UserDataSets", new[] { "UserId" });
            DropIndex("dbo.CatalogueColumnValues", new[] { "CatalogueColumnId" });
            DropIndex("dbo.CatalogueColumnValues", new[] { "YearDataId" });
            DropIndex("dbo.YearDatas", new[] { "DataSetId" });
            DropIndex("dbo.DynamicColumnValues", new[] { "DynamicColumnId" });
            DropIndex("dbo.DynamicColumnValues", new[] { "YearDataId" });
            DropIndex("dbo.DynamicColumns", new[] { "DynamicColumnColumnId" });
            DropIndex("dbo.DynamicColumns", new[] { "DataSetId" });
            DropIndex("dbo.DynamicColumns", new[] { "CurrencyUnitId" });
            DropIndex("dbo.DataSets", new[] { "CatalogueId" });
            DropIndex("dbo.CatalogueColumns", new[] { "CatalogueId" });
            DropColumn("dbo.AspNetUsers", "LanguageId");
            DropColumn("dbo.AspNetUsers", "CountryId");
            DropColumn("dbo.AspNetUsers", "FiscalManagementNewsletter");
            DropColumn("dbo.AspNetUsers", "Title");
            DropColumn("dbo.AspNetUsers", "Charge");
            DropColumn("dbo.AspNetUsers", "Organization");
            DropColumn("dbo.YearDatas", "DataSetId");
            DropTable("dbo.UserInterests");
            DropTable("dbo.Languages");
            DropTable("dbo.UserDataSets");
            DropTable("dbo.CatalogueColumnValues");
            DropTable("dbo.DynamicColumnValues");
            DropTable("dbo.CurrencyUnits");
            DropTable("dbo.DynamicColumns");
            DropTable("dbo.DataSets");
            DropTable("dbo.Catalogues");
            DropTable("dbo.CatalogueColumns");
            CreateIndex("dbo.YearDatas", "MunicipalityId");
            AddForeignKey("dbo.YearDatas", "MunicipalityId", "dbo.Municipalities", "Id", cascadeDelete: true);
        }
    }
}
