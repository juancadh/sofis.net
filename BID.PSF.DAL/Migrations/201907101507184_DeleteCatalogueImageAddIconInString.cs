namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteCatalogueImageAddIconInString : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Catalogues", "Icon", c => c.String());
            DropColumn("dbo.Catalogues", "Image");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Catalogues", "Image", c => c.Binary());
            DropColumn("dbo.Catalogues", "Icon");
        }
    }
}
