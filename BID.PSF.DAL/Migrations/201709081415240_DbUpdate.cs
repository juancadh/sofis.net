namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Debts",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TotalDebt = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HistoricalDebtVariableRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HistoricalDebtFixedRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalAmortization = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AmortizationHistoricalDebtVariableRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AmortizationHistoricalDebtFixedRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPaymentForInterests = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InterestRateHistoricalDebtVariableRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InterestDebtHistoricalRateFixedRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.YearDatas", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.YearDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Expenses",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TotalExpenses = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PersonalServices = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PersonalServicesCE = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GeneralServices = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MaterialsAndSuppliesCE = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GeneralServicesCE = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Investment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RealStateInvestment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StatePublicInvestment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ContributionsAndTrustsInvestment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Transfers = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SubsidiesTransfers = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransfersAndReasignedSpending = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PublicSectorTransfersAndAllocations = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PublicSectorRestTransfers = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SocialBenefits = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SocialHelps = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RealStateManagement = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Others = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TrustTranfersAndOther = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MunicipalitiesAssignedResources = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Investments = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Shares = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Conventions = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IncomeReturns = c.Decimal(nullable: false, precision: 18, scale: 2),
                        StockIssueTrusts = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OrganizationAgreements = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ADEFAS = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.YearDatas", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Incomes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TotalIncome = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FreeDestinationIncome = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NotAvailableIncome = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FreeDestinationTaxes = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenureAndUseOfVehicles = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Others = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NotFreeDestinationTaxes = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Payroll = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FurniturePropertyDisposalVehicleAcquisition = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Lodging = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LotteriesRafflesAndSweepstakes = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalRightsAndProducts = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Exploitation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Contributions = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ContributionImprovements = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FederalTaxRevenueSharesAndIncentives = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GeneralFund = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InspectionFundForFederalEntities = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SpecialTaxOnProductionAndServices = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TaxOnTenureOrUseOfVehicles = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MunicipalDevelopmentFund = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NewCarTax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RFP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OtherParticipableIncentives = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FEIEF = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NotFreeTransfersDestination = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CFHealthServices = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CFStrengtheningMunicipalities = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CFStrengtheningFederalEntities = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CFMunicipalSocialInfraestructure = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CFPublicSecurityStatesFederalDistrict = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CFAdultTechnologicalEducation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CFStateSocialInfraestructure = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MultipleContributionFund = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransfersHigherAndHigherSecondaryEducation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PopularInsurance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PartnershipProgramField = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RegionalDevelopmentProgram = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AgreedProgramsConagua = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SecretaryPublicEducation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HealthSecretary = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SecretaryFinancePublicCredit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SecretaryCommunicationsTransportation = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PublicSecurity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MetropolitanFunds = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RemainderTransfersAgreementsFederalGovernment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TrusteeInfraestructureStates = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ENCARB = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NationalSportsCommissionSportsInfraestructure = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CollegeScientificTechnologicalStudiesStateCoahuila = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ProgramControlFederalizedCase = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SecretaryEnvironmentNaturalResources = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SecretarySocialDevelopment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.YearDatas", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Macroeconomies",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        GDP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NacionalCPI = c.Decimal(nullable: false, precision: 18, scale: 2),
                        USDExchangeRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GDPEntity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InternalCPI = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InternalInterestRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DomesticInterestRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.YearDatas", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Debts", "Id", "dbo.YearDatas");
            DropForeignKey("dbo.Macroeconomies", "Id", "dbo.YearDatas");
            DropForeignKey("dbo.Incomes", "Id", "dbo.YearDatas");
            DropForeignKey("dbo.Expenses", "Id", "dbo.YearDatas");
            DropIndex("dbo.Macroeconomies", new[] { "Id" });
            DropIndex("dbo.Incomes", new[] { "Id" });
            DropIndex("dbo.Expenses", new[] { "Id" });
            DropIndex("dbo.Debts", new[] { "Id" });
            DropTable("dbo.Macroeconomies");
            DropTable("dbo.Incomes");
            DropTable("dbo.Expenses");
            DropTable("dbo.YearDatas");
            DropTable("dbo.Debts");
        }
    }
}
