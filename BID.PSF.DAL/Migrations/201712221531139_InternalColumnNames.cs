namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InternalColumnNames : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CatalogueColumns", "InternalName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CatalogueColumns", "InternalName");
        }
    }
}
