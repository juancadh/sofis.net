// <auto-generated />
namespace BID.PSF.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Catalogues : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Catalogues));
        
        string IMigrationMetadata.Id
        {
            get { return "201710101355290_Catalogues"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
