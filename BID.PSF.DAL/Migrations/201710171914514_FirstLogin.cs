namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstLogin : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsFirstLogin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsFirstLogin");
        }
    }
}
