namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VirtualCurrentCatalogue_User : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "CurrentCatalogueId", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "CurrentCatalogueId");
            AddForeignKey("dbo.AspNetUsers", "CurrentCatalogueId", "dbo.Catalogues", "Id");
            DropColumn("dbo.AspNetUsers", "CurrentCatalogue");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "CurrentCatalogue", c => c.Int(nullable: false));
            DropForeignKey("dbo.AspNetUsers", "CurrentCatalogueId", "dbo.Catalogues");
            DropIndex("dbo.AspNetUsers", new[] { "CurrentCatalogueId" });
            DropColumn("dbo.AspNetUsers", "CurrentCatalogueId");
        }
    }
}
