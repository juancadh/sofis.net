namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datasetFromToYears : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DataSets", "HistoricFrom", c => c.Int());
            AddColumn("dbo.DataSets", "HistoricTo", c => c.Int());
            AddColumn("dbo.DataSets", "ProjectionFrom", c => c.Int());
            AddColumn("dbo.DataSets", "ProjectionTo", c => c.Int());
            DropColumn("dbo.DynamicColumnValues", "Projected");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DynamicColumnValues", "Projected", c => c.Boolean(nullable: false));
            DropColumn("dbo.DataSets", "ProjectionTo");
            DropColumn("dbo.DataSets", "ProjectionFrom");
            DropColumn("dbo.DataSets", "HistoricTo");
            DropColumn("dbo.DataSets", "HistoricFrom");
        }
    }
}
