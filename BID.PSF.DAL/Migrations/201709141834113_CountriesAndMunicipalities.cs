namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CountriesAndMunicipalities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Municipalities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountryId = c.Int(nullable: false),
                        Name = c.String(),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreatedByName = c.String(),
                        CreatedById = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedByName = c.String(),
                        ModifiedById = c.String(),
                        ModifiedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.YearDatas", "MunicipalityId", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "MunicipalityId", c => c.Int());
            CreateIndex("dbo.YearDatas", "MunicipalityId");
            CreateIndex("dbo.AspNetUsers", "MunicipalityId");
            AddForeignKey("dbo.AspNetUsers", "MunicipalityId", "dbo.Municipalities", "Id");
            AddForeignKey("dbo.YearDatas", "MunicipalityId", "dbo.Municipalities", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.YearDatas", "MunicipalityId", "dbo.Municipalities");
            DropForeignKey("dbo.AspNetUsers", "MunicipalityId", "dbo.Municipalities");
            DropForeignKey("dbo.Municipalities", "CountryId", "dbo.Countries");
            DropIndex("dbo.AspNetUsers", new[] { "MunicipalityId" });
            DropIndex("dbo.Municipalities", new[] { "CountryId" });
            DropIndex("dbo.YearDatas", new[] { "MunicipalityId" });
            DropColumn("dbo.AspNetUsers", "MunicipalityId");
            DropColumn("dbo.YearDatas", "MunicipalityId");
            DropTable("dbo.Countries");
            DropTable("dbo.Municipalities");
        }
    }
}
