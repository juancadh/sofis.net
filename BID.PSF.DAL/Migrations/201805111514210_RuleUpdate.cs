namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RuleUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserRules", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserRules", new[] { "UserId" });
            AddColumn("dbo.UserRules", "Email", c => c.String());
            DropColumn("dbo.UserRules", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserRules", "UserId", c => c.String(maxLength: 128));
            DropColumn("dbo.UserRules", "Email");
            CreateIndex("dbo.UserRules", "UserId");
            AddForeignKey("dbo.UserRules", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
