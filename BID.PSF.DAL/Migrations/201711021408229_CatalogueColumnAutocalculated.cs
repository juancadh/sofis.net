namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CatalogueColumnAutocalculated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CatalogueColumns", "ParentId", c => c.Int());
            CreateIndex("dbo.CatalogueColumns", "ParentId");
            AddForeignKey("dbo.CatalogueColumns", "ParentId", "dbo.CatalogueColumns", "Id");
            DropColumn("dbo.CatalogueColumns", "InternalColumnName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CatalogueColumns", "InternalColumnName", c => c.String());
            DropForeignKey("dbo.CatalogueColumns", "ParentId", "dbo.CatalogueColumns");
            DropIndex("dbo.CatalogueColumns", new[] { "ParentId" });
            DropColumn("dbo.CatalogueColumns", "ParentId");
        }
    }
}
