namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CatalogueImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Catalogues", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Catalogues", "Image");
        }
    }
}
