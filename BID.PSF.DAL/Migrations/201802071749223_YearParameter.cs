namespace BID.PSF.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class YearParameter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserParameters", "Year", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserParameters", "Year");
        }
    }
}
