﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.DBModel;

namespace BID.PSF.DAL.Repositories
{
    public class CollectionRepository<T> : ICollectionRepository<T> where T : BaseEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbset;

        public CollectionRepository(PSFContext ctx)
        {
            _context = ctx;
            _dbset = ctx.Set<T>();
        }

        public IEnumerable<T> List(Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> ret = _dbset;

            return filter == null ? ret : ret.Where(filter);
        }

        public int Add(T entity)
        {
            var e = _dbset.Add(entity);
            SaveChanges();
            return e.Id;
        }

        public IEnumerable<T> AddRange(ICollection<T> entities)
        {
            var e = _dbset.AddRange(entities);
            SaveChanges();
            return e;
        }

        private void Delete(T entity)
        {
            // If entity is detached, fetch atached entity from dbset.
            var e = _context.Entry(entity).State == EntityState.Detached ? FindById(entity.Id) : entity;
            _dbset.Remove(e);
            SaveChanges();
        }

        public void Delete(int id, bool soft = true)
        {
            var entity = FindById(id);
            Delete(entity);
        }

        public void DeleteRange(IEnumerable<T> entities)
        {
            // Fetch attached entities which id is contained into entities to delete ids.
            _dbset.RemoveRange(List(null).Where(e => entities.Select(c => c.Id).Contains(e.Id)));
            SaveChanges();
        }

        public T FindById(int id)
        {
            return _dbset.FirstOrDefault(t => t.Id == id);
        }

        public void Update(T entity)
        {

            var oldEntity = FindById(entity.Id);
            if (oldEntity != null)
            {
                entity.CreatedDate = oldEntity.CreatedDate;
                entity.CreatedByName = oldEntity.CreatedByName;
                entity.CreatedById = oldEntity.CreatedById;
            }

            _dbset.AddOrUpdate(entity);
            SaveChanges();
        }

        public void UpdateEntity(T entity, T modifiedEntity)
        {
            _context.Entry(entity).CurrentValues.SetValues(modifiedEntity);
            _context.Entry(entity).State = EntityState.Modified;

            Update(entity);
        }

        private void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
