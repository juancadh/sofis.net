﻿using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.DBModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BID.PSF.DAL.Repositories
{
    public class CatalogueManagerRepository : ICatalogueManagerRepository//TODO Interface nueva
    {
        #region Properties
        private readonly DbContext _context;
        private readonly DbSet<Catalogue> _dbSetCatalogue;
        private readonly DbSet<DataSet> _dbSetDataSet;
        private readonly DbSet<CatalogueColumn> _dbSetCatalogueColumn;
        private readonly DbSet<UserDataSet> _dbSetUserDataset;

        public CatalogueManagerRepository(PSFContext ctx)
        {
            _context = ctx;
            _dbSetCatalogue = ctx.Set<Catalogue>();
            _dbSetDataSet = ctx.Set<DataSet>();
            _dbSetCatalogueColumn = ctx.Set<CatalogueColumn>();
            _dbSetUserDataset = ctx.Set<UserDataSet>();
        }
        #endregion

        #region Catalogue Methods

        public Catalogue AddCatalogue(Catalogue catalogue, string userId)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var c = _dbSetCatalogue.Add(catalogue);
                    SaveChanges();


                    var dt = _dbSetDataSet.Add(new DataSet()
                    {
                        CatalogueId = c.Id,
                        HistoricFrom = 1990,
                        HistoricTo = DateTime.Now.Year,
                        ProjectionFrom = DateTime.Now.Year + 1,
                        ProjectionTo = DateTime.Now.Year + 5,
                    });

                    _dbSetUserDataset.Add(new UserDataSet()
                    {
                        DataSetId = dt.Id,
                        UserId = userId,
                });

                    SaveChanges();
                    dbContextTransaction.Commit();

                    return c;
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    throw e;
                }

            }

        }

        public void DeleteCatalogue(Catalogue catalogue)
        {
            _dbSetCatalogue.Remove(catalogue);
            SaveChanges();
        }

        public void UpdateCatalogue(Catalogue catalogue)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var oldEntity = _dbSetCatalogue.FirstOrDefault(t => t.Id == catalogue.Id);
                    if (oldEntity != null)
                    {
                        catalogue.CreatedDate = oldEntity.CreatedDate;
                        catalogue.CreatedByName = oldEntity.CreatedByName;
                        catalogue.CreatedById = oldEntity.CreatedById;
                    }

                    _dbSetCatalogue.AddOrUpdate(catalogue);

                    SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    throw e;
                }

            }
        }

        public Catalogue CloneCatalogue(Catalogue catalogue, string userId, List<CatalogueColumn> catalogueColumns)
        {

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var c = _dbSetCatalogue.Add(catalogue);
                    SaveChanges();
                    //Diccionario con id de la columna a clonar e id de la nueva columna creada para mantener la asignación de parents
                    Dictionary<int, int> columnsIdDictionary = new Dictionary<int, int>();
                    List<CatalogueColumn> columnList = new List<CatalogueColumn>();
                    foreach (var item in catalogueColumns)
                    {
                        var id = item.Id;
                        var column = _dbSetCatalogueColumn.Add(new CatalogueColumn()
                        {
                            Category = item.Category,
                            ColumnName = item.ColumnName,
                            Index = item.Index,
                            ParentId = item.ParentId,
                            InternalName = item.InternalName,
                            CatalogueId = c.Id

                        });
                        SaveChanges();
                        columnsIdDictionary.Add(id, column.Id);
                    }


                    //Se actualizan los parents con los ids de las nuevas columnas creadas
                    foreach (var item in columnsIdDictionary)
                    {
                        IQueryable<CatalogueColumn> ret = _dbSetCatalogueColumn;
                        var columns = ret.Where(t => t.ParentId == item.Key && t.CatalogueId == c.Id);
                        foreach (var cl in columns)
                        {
                            cl.ParentId = item.Value;
                            _dbSetCatalogueColumn.AddOrUpdate(cl);
                        }

                    }
                    SaveChanges();
                    //Al clonar se crea un nuevo dataset y se asigna al usuario actual
                    var dataset = _dbSetDataSet.Add(new DataSet()
                    {
                        CatalogueId = c.Id,
                        HistoricFrom = 1990,
                        HistoricTo = DateTime.Now.Year,
                        ProjectionFrom = DateTime.Now.Year + 1,
                        ProjectionTo = DateTime.Now.Year + 5,
                    });

                    _dbSetUserDataset.Add(new UserDataSet()
                    {
                        DataSetId = dataset.Id,
                        UserId = userId
                    });

                    SaveChanges();
                    dbContextTransaction.Commit();
                    return c;
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    throw e;
                }
            }




        }
        #endregion

        #region Private Methods
        private void SaveChanges()
        {
            _context.SaveChanges();
        }

        #endregion
    }
}
