﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.DBModel;

namespace BID.PSF.DAL.Repositories
{
    public class OAuthRepository<T> : IOAuthRepository<T> where T : OAuthEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbset;

        public OAuthRepository(PSFContext ctx)
        {
            _context = ctx;
            _dbset = ctx.Set<T>();
        }

        public IEnumerable<T> List(Expression<Func<T, bool>> filter = null, bool? track = null)
        {
            IQueryable<T> ret = track == true ? _dbset : _dbset.AsNoTracking();

            return filter == null ? ret : ret.Where(filter);
        }

        public string Add(T entity)
        {
            var e = _dbset.Add(entity);
            SaveChanges();
            return e.Id;
        }

        public IEnumerable<T> AddRange(ICollection<T> entities)
        {
            var e = _dbset.AddRange(entities);
            SaveChanges();
            return e;
        }

        private void Delete(T entity)
        {
            // If entity is detached, fetch atached entity from dbset.
            var e = _context.Entry(entity).State == EntityState.Detached ? FindById(entity.Id, true) : entity;
            _dbset.Remove(e);
            SaveChanges();
        }

        public void Delete(string id, bool soft = true)
        {
            var entity = FindById(id, true);
            Delete(entity);
        }

        public void DeleteRange(IEnumerable<T> entities)
        {
            // Fetch attached entities which id is contained into entities to delete ids.
            _dbset.RemoveRange(List(null, true).Where(e => entities.Select(c => c.Id).Contains(e.Id)));
            SaveChanges();
        }

        public T FindById(string id, bool? track = null)
        {
            return track == true ? _dbset.FirstOrDefault(t => t.Id == id) : _dbset.AsNoTracking().FirstOrDefault(t => t.Id == id);
        }

        public void Update(T entity)
        {
            _dbset.AddOrUpdate(entity);
            SaveChanges();
        }

        public void UpdateEntity(T entity, T modifiedEntity)
        {
            _context.Entry(entity).CurrentValues.SetValues(modifiedEntity);
            _context.Entry(entity).State = EntityState.Modified;

            Update(entity);
        }

        private void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
