﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BID.PSF.Model.DBModel;

namespace BID.PSF.DAL.Repositories.Interfaces
{
    public interface ICollectionRepository<T>
    {
        IEnumerable<T> List(Expression<Func<T, bool>> filter = null);
        int Add(T entity);
        IEnumerable<T> AddRange(ICollection<T> entities);
        void Delete(int id, bool soft = true);
        void Update(T entity);
        void UpdateEntity(T entity, T modifiedEntity);
        T FindById(int id);
    }
}
