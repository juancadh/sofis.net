﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BID.PSF.Model.DBModel;

namespace BID.PSF.DAL.Repositories.Interfaces
{
    public interface IOAuthRepository<T> where T : OAuthEntity
    {
        IEnumerable<T> List(Expression<Func<T, bool>> filter = null, bool? track = null);
        string Add(T entity);
        IEnumerable<T> AddRange(ICollection<T> entities);
        void Delete(string id, bool soft = true);
        void Update(T entity);
        void UpdateEntity(T entity, T modifiedEntity);
        T FindById(string id, bool? track = null);
    }
}
