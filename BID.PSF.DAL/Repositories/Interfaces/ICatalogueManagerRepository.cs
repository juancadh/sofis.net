﻿using BID.PSF.Model.DBModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataSet = BID.PSF.Model.DBModel.DataSet;

namespace BID.PSF.DAL.Repositories.Interfaces
{
    public interface ICatalogueManagerRepository
    {
        Catalogue AddCatalogue(Catalogue catalogue, string userId);
        Catalogue CloneCatalogue(Catalogue catalogueToClone, string userId, List<CatalogueColumn> catalogueColumns);
        void DeleteCatalogue(Catalogue catalogueId);
        void UpdateCatalogue(Catalogue catalogue);
    }
}
