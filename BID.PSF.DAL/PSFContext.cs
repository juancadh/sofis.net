﻿using BID.PSF.Model.DBModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using BID.PSF.Core.Helpers;

namespace BID.PSF.DAL
{
    // ReSharper disable once InconsistentNaming
    public class PSFContext : IdentityDbContext<OAuthUser>
    {
        public DbSet<YearData> YearDatas { get; set; }
        public DbSet<OAuthClient> OAuthClients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Municipality> Municipalities { get; set; }
        public DbSet<DynamicColumn> DynamicColumns { get; set; }
        public DbSet<DynamicColumnValue> DynamicColumnValues { get; set; }
        public DbSet<DataSet> DataSets { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<UserDataSet> UserDataSets { get; set; }
        public DbSet<UserInterest> UserInterests { get; set; }
        public DbSet<UserProduct> UserProducts { get; set; }
        public DbSet<Catalogue> Catalogues { get; set; }
        public DbSet<CatalogueColumn> CatalogueColumns { get; set; }
        public DbSet<CatalogueColumnValue> CatalogueColumnValues { get; set; }
        public DbSet<Rule> Rules { get; set; }
        public DbSet<CurrencyUnit> CurrencyUnits { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<UserParameter> UserParameters { get; set; }
        public DbSet<UserRule> UserRules { get; set; }

        public PSFContext() : base("BID.PSF.DAL.BidPsfContext", false)
        {

        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync()
        {
            AddTimestamps();
            return await base.SaveChangesAsync();
        }

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            var userName = HttpContext.Current?.User?.Identity.GetUserName();
            var userId = HttpContext.Current?.User?.Identity.GetUserId();

            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).CreatedDate = DateTime.UtcNow;
                    ((BaseEntity)entity.Entity).CreatedByName = userName;
                    ((BaseEntity)entity.Entity).CreatedById = userId;
                }

                ((BaseEntity)entity.Entity).ModifiedDate = DateTime.UtcNow;
                ((BaseEntity)entity.Entity).ModifiedByName = userName;
                ((BaseEntity)entity.Entity).ModifiedById = userId;
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<DynamicColumnValue>()
                .HasRequired(m => m.YearData)
                .WithMany(t => t.DynamicColumnValue)
                .HasForeignKey(m => m.YearDataId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CatalogueColumnValue>()
                .HasRequired(m => m.YearData)
                .WithMany(t => t.CatalogueColumnValue)
                .HasForeignKey(m => m.YearDataId)
                .WillCascadeOnDelete(false);

        }
    }
}
