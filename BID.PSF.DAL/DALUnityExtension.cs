﻿using BID.PSF.Core;
using BID.PSF.Core.Helpers;
using BID.PSF.DAL.IdentityExtensions;
using BID.PSF.DAL.Repositories;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.DBModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;

namespace BID.PSF.DAL
{
    // ReSharper disable once InconsistentNaming
    public class DALUnityExtension : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.AddNewExtension<CoreUnityExtension>();

            Container.RegisterInstance(new PSFContext(), new PerResolveLifetimeManager());

            var accountInjectionConstructor = new InjectionConstructor(new ResolvedParameter<PSFContext>());
            Container.RegisterType<IUserStore<OAuthUser>, UserStore<OAuthUser>>(accountInjectionConstructor);
            Container.RegisterType<PSFUserManager>();

            Container.RegisterType<IOAuthRepository<RefreshToken>,OAuthRepository<RefreshToken>>();
            Container.RegisterType<IOAuthRepository<OAuthClient>,OAuthRepository<OAuthClient>>();

            Container.RegisterType<ICollectionRepository<DataSet>, CollectionRepository<DataSet>>();
            Container.RegisterType<ICollectionRepository<UserDataSet>, CollectionRepository<UserDataSet>>();
            Container.RegisterType<ICollectionRepository<Country>, CollectionRepository<Country>>();
            Container.RegisterType<ICollectionRepository<Municipality>, CollectionRepository<Municipality>>();
            Container.RegisterType<ICollectionRepository<Language>, CollectionRepository<Language>>();
            Container.RegisterType<ICollectionRepository<UserInterest>, CollectionRepository<UserInterest>>();
            Container.RegisterType<ICollectionRepository<UserProduct>, CollectionRepository<UserProduct>>();
            Container.RegisterType<ICollectionRepository<UserParameter>, CollectionRepository<UserParameter>>();
            Container.RegisterType<ICollectionRepository<UserRule>, CollectionRepository<UserRule>>();

            Container.RegisterType<ICollectionRepository<YearData>, CollectionRepository<YearData>>();

            Container.RegisterType<ICollectionRepository<Catalogue>, CollectionRepository<Catalogue>>();
            Container.RegisterType<ICatalogueManagerRepository, CatalogueManagerRepository >();
            Container.RegisterType<ICollectionRepository<CatalogueColumn>, CollectionRepository<CatalogueColumn>>();
            Container.RegisterType<ICollectionRepository<CatalogueColumnValue>, CollectionRepository<CatalogueColumnValue>>();
            Container.RegisterType<ICollectionRepository<DynamicColumn>, CollectionRepository<DynamicColumn>>();
            Container.RegisterType<ICollectionRepository<DynamicColumnValue>, CollectionRepository<DynamicColumnValue>>();
            Container.RegisterType<ICollectionRepository<Rule>, CollectionRepository<Rule>>();

            Container.RegisterType<ICollectionRepository<SubCategory>, CollectionRepository<SubCategory>>();
            Container.RegisterType<ICollectionRepository<CurrencyUnit>, CollectionRepository<CurrencyUnit>>();
        }
    }
}
