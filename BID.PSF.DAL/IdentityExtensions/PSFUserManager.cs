﻿using BID.PSF.Model.DBModel;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using Owin;

namespace BID.PSF.DAL.IdentityExtensions
{
    // ReSharper disable once InconsistentNaming
    public class PSFUserManager : UserManager<OAuthUser>
    {
        public PSFUserManager(IDataProtectionProvider protectionProvider, UserStore<OAuthUser> userStore) : base(userStore)
        {
            PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            UserTokenProvider = new DataProtectorTokenProvider<OAuthUser>(protectionProvider.Create("ASP.NET Identity"));

            UserValidator = new UserValidator<OAuthUser>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
        }

        public static void InitializeUserManager(PSFUserManager manager, IAppBuilder app)
        {
            manager.UserValidator = new UserValidator<OAuthUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            var dataProtectionProvider = app.GetDataProtectionProvider();

            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<OAuthUser>(
                    dataProtectionProvider.Create("ASP.NET Identity"))
                {
                    //TokenLifespan = TimeSpan.FromHours(3)
                };
            }
        }
    }
}
