﻿using BID.PSF.Core.Helpers;
using NUnit.Framework;

namespace BID.PSF.Tests
{
    [TestFixture]
    public class MathTests
    {
        [SetUp]
        public void Init()
        {
            
        }

        [Test]
        public void GetAverageTest()
        {
            var input1 = new decimal[] { };
            var input2 = new[] { 0m, 0m, 0m };
            var input3 = new[] { 10m, 10m, 10m, 10m, 10m, 10m, 10m, 10m };
            var input4 = new[] { 1m, 2m, 3m, 4m, 5m };
            var input5 = new[] { 1.30m, 1.6m, 1.9m, 1.30m, 1.6m, 1.9m, 1.30m, 1.6m, 1.9m, 1.30m, 1.6m, 1.9m, 1.30m, 1.6m, 1.9m };

            Assert.AreEqual(0, MathHelper.GetAverage(null));
            Assert.AreEqual(0, MathHelper.GetAverage(input1));
            Assert.AreEqual(0, MathHelper.GetAverage(input2));
            Assert.AreEqual(0, MathHelper.GetAverage(input3));
            Assert.AreEqual(0.52083m, decimal.Round(MathHelper.GetAverage(input4), 5));
            Assert.AreEqual(0.059156m, decimal.Round(MathHelper.GetAverage(input5), 6));
        }
    }
}
