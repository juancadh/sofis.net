﻿using System.Net;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class ApiResult
    {
        [JsonProperty(Order = 0, PropertyName = "data")]
        public object Data { get; set; }
        [JsonProperty(Order = 1, PropertyName = "result")]
        public string Result { get; set; }
        [JsonProperty(Order = 2, PropertyName = "statusCode")]
        public HttpStatusCode StatusCode { get; set; }
        [JsonProperty(Order = 3, PropertyName = "message")]
        public string Message { get; set; }
    }
}
