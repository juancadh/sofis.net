﻿using BID.PSF.Core.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace BID.PSF.Model.ApiModel
{
    public class CatalogueColumnValues
    {
        [JsonProperty(PropertyName = "category", NullValueHandling = NullValueHandling.Ignore)]
        public Enums.ColumnCategory Category { get; set; }

        [JsonProperty(PropertyName = "dataSetId", NullValueHandling = NullValueHandling.Ignore)]
        public int DataSetId { get; set; }

        [JsonProperty(PropertyName = "values", NullValueHandling = NullValueHandling.Ignore)]
        public List<CatalogueColumnValueDTO> Values { get; set; }
    }

    public class CatalogueColumnValueDTO
    {
        [JsonProperty(PropertyName = "columnId", NullValueHandling = NullValueHandling.Ignore)]
        public int ColumnId { get; set; }

        [JsonProperty(PropertyName = "value", NullValueHandling = NullValueHandling.Ignore)]
        public decimal Value { get; set; }

        [JsonProperty(PropertyName = "formula", NullValueHandling = NullValueHandling.Ignore)]
        public string Formula { get; set; }

        [JsonProperty(PropertyName = "year", NullValueHandling = NullValueHandling.Ignore)]
        public int Year { get; set; }

        [JsonProperty(PropertyName = "dynamic", NullValueHandling = NullValueHandling.Ignore)]
        public bool Dynamic { get; set; }

        [JsonProperty(PropertyName = "projected", NullValueHandling = NullValueHandling.Ignore)]
        public bool Projected { get; set; }
    }
}