﻿using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class HandsOnTableColumn
    {
        [JsonProperty(PropertyName = "data", NullValueHandling = NullValueHandling.Ignore)]
        public string Data { get; set; }

        [JsonProperty(PropertyName = "title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "render", NullValueHandling = NullValueHandling.Ignore)]
        public string Render { get; set; }

        [JsonProperty(PropertyName = "width", NullValueHandling = NullValueHandling.Ignore)]
        public int? Width { get; set; }

        [JsonProperty(PropertyName = "type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "format", NullValueHandling = NullValueHandling.Ignore)]
        public string Format { get; set; }

        [JsonProperty(PropertyName = "language", NullValueHandling = NullValueHandling.Ignore)]
        public string Language { get; set; }

        [JsonProperty(PropertyName = "readOnly", NullValueHandling = NullValueHandling.Ignore)]
        public bool ReadOnly { get; set; }

        [JsonProperty(PropertyName = "validator", NullValueHandling = NullValueHandling.Ignore)]
        public string Validator { get; set; }

        [JsonProperty(PropertyName = "allowInvalid", NullValueHandling = NullValueHandling.Ignore)]
        public bool AllowInvalid { get; set; }

        [JsonProperty(PropertyName = "manualColumnResize", NullValueHandling = NullValueHandling.Ignore)]
        public bool ManualColumnResize { get; set; }

        [JsonProperty(PropertyName = "className", NullValueHandling = NullValueHandling.Ignore)]
        public string ClassName { get; set; }

        [JsonProperty(PropertyName = "dynamic", NullValueHandling = NullValueHandling.Ignore)]
        public bool Dynamic { get; set; }

        [JsonProperty(PropertyName = "parentId", NullValueHandling = NullValueHandling.Ignore)]
        public int? ParentId { get; set; }

        [JsonProperty(PropertyName = "index", NullValueHandling = NullValueHandling.Ignore)]
        public int Index { get; set; }

        [JsonProperty(PropertyName = "level", NullValueHandling = NullValueHandling.Ignore)]
        public int Level { get; set; }

        [JsonProperty(PropertyName = "isDefaultColumn", NullValueHandling = NullValueHandling.Ignore)]
        public bool IsDefaultColumn { get; set; }

        [JsonProperty(PropertyName = "internalName", NullValueHandling = NullValueHandling.Ignore)]
        public string InternalName { get; set; }


        public HandsOnTableColumn(string data, string title, bool dynamic)
        {
            Type = "numeric";
            Data = data ?? title;
            Title = title;
            Dynamic = dynamic;
        }

        public HandsOnTableColumn(string data, string title, bool dynamic, int? parentId, bool isDefaultColumn, int index, int level, string internalName)
        {
            Type = "numeric";
            Data = data ?? title;
            Title = title;
            Dynamic = dynamic;
            ParentId = parentId;
            IsDefaultColumn = isDefaultColumn;
            Index = index;
            Level = level;
            InternalName = internalName;
        }

        // Used for Data Input
        public HandsOnTableColumn(string data, string title, bool standardDataColumn, bool dynamic) : this(data, title, dynamic)
        {
            Type = "numeric";
            Format = "0,0.00";
            Language = "en-US";
            Validator = "numeric";
            AllowInvalid = false;
            Width = 150;
            ManualColumnResize = true;
            Dynamic = dynamic;
        }

        // Used for Aproximations Output
        public HandsOnTableColumn(string data, string title, int width, bool readOnly, bool dynamic) : this(data, title, dynamic)
        {
            Type = "numeric";
            Format = "0,0.00";
            Language = "en-US";
            Validator = "numeric";
            AllowInvalid = false;
            Width = width;
            ReadOnly = readOnly;
            ManualColumnResize = true;
            Dynamic = dynamic;
        }

        // Used for Calculated Fields
        public HandsOnTableColumn(string data, string title, bool readOnly, string type, bool dynamic) : this(data, title, dynamic)
        {
            Type = type;
            Format = "0,0.00";
            AllowInvalid = false;
            Width = 150;
            ReadOnly = readOnly;
            ManualColumnResize = true;
            Dynamic = dynamic;
        }

        // Used for Data Input
        public HandsOnTableColumn(string data, string title, bool standardDataColumn, bool dynamic, int? parentId, bool isDefaultColumn, int index, int level) : this(data, title, dynamic)
        {
            Type = "numeric";
            Format = "0,0.00";
            Language = "en-US";
            Validator = "numeric";
            AllowInvalid = false;
            Width = 150;
            ManualColumnResize = true;
            Dynamic = dynamic;
            ParentId = parentId;
            IsDefaultColumn = isDefaultColumn;
            Index = index;
            Level = level;
        }

        public HandsOnTableColumn(string data, string title, bool standardDataColumn, bool dynamic, int? parentId, bool isDefaultColumn, int index, int level, string internalName) : this(data, title, dynamic)
        {
            Type = "numeric";
            Format = "0,0.00";
            Language = "en-US";
            Validator = "numeric";
            AllowInvalid = false;
            Width = 150;
            ManualColumnResize = true;
            Dynamic = dynamic;
            ParentId = parentId;
            IsDefaultColumn = isDefaultColumn;
            Index = index;
            Level = level;
            InternalName = internalName;
        }
    }
}
