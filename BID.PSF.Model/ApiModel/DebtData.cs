﻿using BID.PSF.Model.DBModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class DebtData
    {
        [JsonProperty(PropertyName = "year", NullValueHandling = NullValueHandling.Ignore)]
        public int Year { get; set; }

        [JsonProperty(PropertyName = "totalDebt", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TotalDebt { get; set; }

        [JsonProperty(PropertyName = "historicalDebtVariableRate", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HistoricalDebtVariableRate { get; set; }

        [JsonProperty(PropertyName = "historicalDebtFixedRate", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? HistoricalDebtFixedRate { get; set; }

        [JsonProperty(PropertyName = "totalAmortization", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TotalAmortization { get; set; }

        [JsonProperty(PropertyName = "amortizationHistoricalDebtVariableRate", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AmortizationHistoricalDebtVariableRate { get; set; }

        [JsonProperty(PropertyName = "amortizationHistoricalDebtFixedRate", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AmortizationHistoricalDebtFixedRate { get; set; }

        [JsonProperty(PropertyName = "totalPaymentForInterests", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TotalPaymentForInterests { get; set; }

        [JsonProperty(PropertyName = "interestRateHistoricalDebtVariableRate", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? InterestRateHistoricalDebtVariableRate { get; set; }

        [JsonProperty(PropertyName = "interestDebtHistoricalRateFixedRate", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? InterestDebtHistoricalRateFixedRate { get; set; }

        public DebtData()
        {
            
        }

        public DebtData(Debt debt)
        {
            Year = debt.YearData.Year;
            TotalDebt = debt.TotalDebt;
            HistoricalDebtVariableRate = debt.HistoricalDebtVariableRate;
            HistoricalDebtFixedRate = debt.HistoricalDebtFixedRate;
            TotalAmortization = debt.TotalAmortization;
            AmortizationHistoricalDebtVariableRate = debt.AmortizationHistoricalDebtVariableRate;
            AmortizationHistoricalDebtFixedRate = debt.AmortizationHistoricalDebtFixedRate;
            TotalPaymentForInterests = debt.TotalPaymentForInterests;
            InterestRateHistoricalDebtVariableRate = debt.InterestRateHistoricalDebtVariableRate;
            InterestDebtHistoricalRateFixedRate = debt.InterestDebtHistoricalRateFixedRate;
            InterestDebtHistoricalRateFixedRate = debt.InterestDebtHistoricalRateFixedRate;
        }
    }
}
