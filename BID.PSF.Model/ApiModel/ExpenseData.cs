﻿using Newtonsoft.Json;

// ReSharper disable InconsistentNaming
namespace BID.PSF.Model.ApiModel
{
    public class ExpenseData
    {
        [JsonProperty(PropertyName = "year", NullValueHandling = NullValueHandling.Ignore)]
        public int Year { get; set; }

        [JsonProperty(PropertyName = "totalExpenses", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TotalExpenses { get; set; }

        [JsonProperty(PropertyName = "personalServices", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PersonalServices { get; set; }

        [JsonProperty(PropertyName = "personalServicesCE", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PersonalServicesCE { get; set; }

        [JsonProperty(PropertyName = "generalServices", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? GeneralServices { get; set; }

        [JsonProperty(PropertyName = "materialsAndSuppliesCE", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? MaterialsAndSuppliesCE { get; set; }

        [JsonProperty(PropertyName = "generalServicesCE", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? GeneralServicesCE { get; set; }

        [JsonProperty(PropertyName = "investment", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Investment { get; set; }

        [JsonProperty(PropertyName = "realStateInvestment", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? RealStateInvestment { get; set; }

        [JsonProperty(PropertyName = "statePublicInvestment", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? StatePublicInvestment { get; set; }

        [JsonProperty(PropertyName = "contributionsAndTrustsInvestment", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ContributionsAndTrustsInvestment { get; set; }

        [JsonProperty(PropertyName = "transfers", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Transfers { get; set; }

        [JsonProperty(PropertyName = "subsidiesTransfers", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? SubsidiesTransfers { get; set; }

        [JsonProperty(PropertyName = "transfersAndReasignedSpending", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TransfersAndReasignedSpending { get; set; }

        [JsonProperty(PropertyName = "publicSectorTransfersAndAllocations", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PublicSectorTransfersAndAllocations { get; set; }

        [JsonProperty(PropertyName = "publicSectorRestTransfers", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PublicSectorRestTransfers { get; set; }

        [JsonProperty(PropertyName = "socialBenefits", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? SocialBenefits { get; set; }

        [JsonProperty(PropertyName = "socialHelps", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? SocialHelps { get; set; }

        [JsonProperty(PropertyName = "realStateManagement", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? RealStateManagement { get; set; }

        [JsonProperty(PropertyName = "others", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Others { get; set; }

        [JsonProperty(PropertyName = "trustTranfersAndOther", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TrustTranfersAndOther { get; set; }

        [JsonProperty(PropertyName = "municipalitiesAssignedResources", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? MunicipalitiesAssignedResources { get; set; }

        [JsonProperty(PropertyName = "investments", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Investments { get; set; }

        [JsonProperty(PropertyName = "shares", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Shares { get; set; }

        [JsonProperty(PropertyName = "conventions", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Conventions { get; set; }

        [JsonProperty(PropertyName = "incomeReturns", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? IncomeReturns { get; set; }

        [JsonProperty(PropertyName = "stockIssueTrusts", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? StockIssueTrusts { get; set; }

        [JsonProperty(PropertyName = "organizationAgreements", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? OrganizationAgreements { get; set; }

        [JsonProperty(PropertyName = "adefas", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ADEFAS { get; set; }
    }
}
