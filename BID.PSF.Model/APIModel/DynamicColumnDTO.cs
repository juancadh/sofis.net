﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class DynamicColumnDTO
    {
        [JsonProperty(PropertyName = "parentBaseColumnId", NullValueHandling = NullValueHandling.Ignore)]
        public int? ParentBaseColumnId { get; set; }

        [JsonProperty(PropertyName = "parentDynamicColumnId", NullValueHandling = NullValueHandling.Ignore)]
        public int? ParentDynamicColumnId { get; set; }

        [JsonProperty(PropertyName = "subcategoryId", NullValueHandling = NullValueHandling.Ignore)]
        public int SubCategoryId { get; set; }

        [JsonProperty(PropertyName = "currencyUnitId", NullValueHandling = NullValueHandling.Ignore)]
        public int CurrencyUnitId { get; set; }

        [JsonProperty(PropertyName = "dataSource", NullValueHandling = NullValueHandling.Ignore)]
        public string DataSource { get; set; }
    }

    public class DynamicColumnValueData
    {
        [JsonProperty(PropertyName = "columnId", NullValueHandling = NullValueHandling.Ignore)]
        public int ColumnId { get; set; }

        [JsonProperty(PropertyName = "value", NullValueHandling = NullValueHandling.Ignore)]
        public decimal Value { get; set; }

        [JsonProperty(PropertyName = "formula", NullValueHandling = NullValueHandling.Ignore)]
        public string Formula { get; set; }

        [JsonProperty(PropertyName = "year", NullValueHandling = NullValueHandling.Ignore)]
        public int Year { get; set; }
    }
}
