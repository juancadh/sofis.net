﻿using BID.PSF.Core.Enums;
using BID.PSF.Core.Validators;
using BID.PSF.Model.DBModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BID.PSF.Model.ApiModel
{
    public class CreateCatalogueDTO
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [Required]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        public bool IsSubcatalogue { get; set; }

        public string Icon { get; set; }

        public ICollection<CatalogueColumn> CatalogueColumns { get; set; }

    }
}
