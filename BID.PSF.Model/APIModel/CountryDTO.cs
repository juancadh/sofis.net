﻿using System.Collections.Generic;
using System.Linq;
using BID.PSF.Model.DBModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class CountryDTO
    {   
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonIgnore]
        [JsonProperty("municipalities")]
        public ICollection<MunicipalityDTO> Municipalities { get; set; }

        public CountryDTO()
        {
            
        }

        public CountryDTO(Country c)
        {
            Id = c.Id;
            Name = c.Name;
            Municipalities = c.Municipalities.Select(m => new MunicipalityDTO(m)).ToList();
        }
    }
}
