﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BID.PSF.Core.Enums;
using BID.PSF.Model.DBModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using static BID.PSF.Core.Enums.Enums;

namespace BID.PSF.Model.ApiModel
{
    public class UserDTO
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("country")]
        public CountryDTO Country { get; set; }
        [JsonProperty("language")]
        public LanguageDTO Language { get; set; }
        [JsonProperty("institution")]
        public string Institution { get; set; }
        [JsonProperty("institutionType")]
        public string InstitutionType { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("receiveUpdates")]
        public bool ReceiveUpdates { get; set; }
        [JsonProperty("interests")]
        public List<string> Interests { get; set; }
        [JsonProperty("products")]
        public List<string> Products { get; set; }
        public bool IsFirstLogin { get; set; }
        public List<Enums.Roles> Roles { get; set; }
        public int? CurrentCatalogueId { get; set; }
        public string FanChartConfig { get; set; }

        public UserDTO()
        {
            Products = new List<string>();
            Interests = new List<string>();
            Roles = new List<Roles>();
        }

        public UserDTO(OAuthUser u)
        {
            Id = u.Id;
            FirstName = u.FirstName;
            LastName = u.LastName;
            Email = u.Email;
            Country = u.Country != null ? new CountryDTO(u.Country) : null;
            Language = u.Language != null ? new LanguageDTO(u.Language) :  null;
            Institution = u.Institution;
            InstitutionType = u.InstitutionType.GetDescription();
            Title = u.Title.GetDescription();
            ReceiveUpdates = u.FiscalManagementNewsletter;
            Interests =u.UserInterests != null ? u.UserInterests.Select(i => i.Interest.GetDescription()).ToList() : new List<string>();
            Products = u.UserInterests != null ? u.UserProducts.Select(i => i.Product.GetDescription()).ToList() : new List<string>();
            IsFirstLogin = u.IsFirstLogin;
            Roles = u.Roles != null ? u.Roles.Select(x => (Enums.Roles)int.Parse(x.RoleId)).ToList() : new List<Roles>();
            CurrentCatalogueId = u.CurrentCatalogueId;
            FanChartConfig = u.FanChartConfig;
        }
    }
}
