﻿namespace BID.PSF.Model.ApiModel
{
    public class DebtDynamicsInputs
    {
        public int Tini { get; set; }
        public int Tproy { get; set; }

        /// <summary>
        /// Deuda en moneda local (Mex: Deuda Total, Otros: deuda a tasa de interes variable local + deuda a tasa de interes fija local)
        /// </summary>
        public decimal[] Dml { get; set; }
        /// <summary>
        /// Deuda en moneda extranjera (Mex: 0, Otros: deuda a tasa de interes variable extranjera + deuda a tasa de interes fila extranjera)
        /// </summary>
        public decimal[] Dme { get; set; }
        /// <summary>
        /// Interes en moneda local (Mex:  Deuda -> H, Otros: deudas -> Tasa de interes para deuda en moneda local )
        /// </summary>
        public decimal[] Rml { get; set; }
        /// <summary>
        /// Interes en moneda extranjera (Mex: 0, Otros: deudas -> Tasa de interes para deuda en moneda extranjera)
        /// </summary>
        public decimal[] Rme { get; set; }
        /// <summary>
        /// Tipo de cambio (Mex: 1, Otros: Tipo de Cambio)
        /// </summary>
        public decimal[] Tcambio { get; set; }
        /// <summary>
        /// Ingresos totales
        /// </summary>
        public decimal[] Ingt { get; set; }
        /// <summary>
        /// Egresos totales
        /// </summary>
        public decimal[] Teg { get; set; }
        /// <summary>
        /// Pago de Intereses (Mex: Gastos -> K +T, Otos: Gastos -> Intereses (2))
        /// </summary>
        public decimal[] Pint { get; set; }

        public bool Autocomplete { get; set; }
    }
}
