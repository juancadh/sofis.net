﻿using BID.PSF.Model.DBModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class SubCategoryDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        public SubCategoryDTO()
        {
            
        }

        public SubCategoryDTO(SubCategory s)
        {
            Id = s.Id;
            Name = s.Name;
        }
    }
}
