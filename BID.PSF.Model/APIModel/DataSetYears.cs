﻿namespace BID.PSF.Model.ApiModel
{
    public class DataSetYears
    {
        public int? HistoricFrom { get; set; }
        public int? HistoricTo { get; set; }
        public int? ProjectionFrom { get; set; }
        public int? ProjectionTo { get; set; }
    }
}
