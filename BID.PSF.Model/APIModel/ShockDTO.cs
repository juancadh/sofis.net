﻿namespace BID.PSF.Model.ApiModel
{
    public class ShockDTO
    {
        public string Shock { get; set; }

        public int InitialYear { get; set; }

        public int FinalYear { get; set; }

        public decimal StandardDeviation { get; set; }
    }
}
