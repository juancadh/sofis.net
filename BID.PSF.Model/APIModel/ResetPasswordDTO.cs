﻿using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class ResetPasswordDTO
    {
        [JsonProperty("oldPassword")]
        public string OldPassword { get; set; }

        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }
    }
}
