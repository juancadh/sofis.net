﻿namespace BID.PSF.Model.ApiModel
{
    public class ForgotPasswordDTO
    {
        public string Username { get; set; }
    }
}
