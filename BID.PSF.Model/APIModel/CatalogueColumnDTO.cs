﻿using BID.PSF.Core.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BID.PSF.Model.ApiModel
{
    public class CatalogueColumnDTO
    {

        public int Id { get; set; }

        public int Index { get; set; }

        public string ColumnName { get; set; }

        public string InternalName { get; set; }

        public Enums.ColumnCategory Category { get; set; }

        public bool IsDefaultColumn { get; set; }

        public int CatalogueId { get; set; }

        public int ParentId { get; set; }
    }
}
