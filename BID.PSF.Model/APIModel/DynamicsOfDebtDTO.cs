﻿using System.Collections.Generic;

namespace BID.PSF.Model.ApiModel
{
    public class DynamicsOfDebtDTO
    {
        public List<ShockDTO> Shocks { get; set; }
    }
}
