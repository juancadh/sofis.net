﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using BID.PSF.Core.Enums;
using BID.PSF.Model.DBModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class CatalogueDTO
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "selected")]
        public bool Selected { get; set; }

        public string Icon { get; set; }

        public Enums.CatalogueType Type { get; set; }

        public virtual ICollection<CatalogueColumn> CatalogueColumns { get; set; }

        public virtual ICollection<DataSet> DataSets { get; set; }

        public CatalogueDTO(Catalogue catalogue)
        {
            Id = catalogue.Id;
            Name = catalogue.Name;
            Icon = catalogue.Icon;
            Type = catalogue.Type;
        }
    }
}
