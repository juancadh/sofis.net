﻿using BID.PSF.Core.Enums;

namespace BID.PSF.Model.ApiModel
{
    public class StandardApproachInputs
    {
        public Enums.CatalogueType Catalog { get; set; }
        public int Periods { get; set; }
        public decimal Ting { get; set; }
        public decimal Teg { get; set; }
        public decimal Pint { get; set; }
        public decimal PublicDebtOverIncomes { get; set; }
    }

    public class StandardApproachMexicoInputs : StandardApproachInputs
    {
        public MexicoDebtInput Debt { get; set; }
        public MexicoIncomeExpenseInput IncomeExpense { get; set; }
        public bool Autocomplete { get; set; }
    }

    public class StandardApproachOtherInputs : StandardApproachInputs
    {
        public OtherDebtInput Debt { get; set; }
        public OtherIncomeExpenseInput IncomeExpense { get; set; }
        public bool Autocomplete { get;  set; }
    }

    public class MexicoDebtInput
    {
        /// <summary>
        /// Tasa de interés promedio de la deuda total
        /// </summary>
        public decimal [] LocalDebtInterestRate { get; set; }
    }

    public class MexicoIncomeExpenseInput
    {
        /// <summary>
        /// Ingresos Totales
        /// </summary>
        public decimal[] TotalIncome { get; set; }
        /// <summary>
        /// Gastos Totales
        /// </summary>
        public decimal[] TotalExpenses { get; set; }
        /// <summary>
        /// Ingresos de Libre Destino
        /// </summary>
        public decimal[] AvailableIncome { get; set; }

        public decimal[] PublicDebtK { get; set; }
        public decimal[] PublicDebtT { get; set; }
    }

    public class OtherDebtInput
    {
        /// <summary>
        /// Deuda a tasa de interés Variable en moneda local
        /// </summary>
        public decimal [] LocalVariableRateDebt { get; set; }
        /// <summary>
        /// Deuda a tasa de interés Fija en moneda local
        /// </summary>
        public decimal[] LocalFixedRateDebt { get; set; }
        /// <summary>
        /// Deuda a tasa de interés Variable en moneda extranjera
        /// </summary>
        public decimal[] ForeignVariableRateDebt { get; set; }
        /// <summary>
        /// Deuda a tasa de interés Fija en moneda extranjera
        /// </summary>
        public decimal[] ForeignFixedRateDebt { get; set; }
        /// <summary>
        /// Tasa de interés para deuda en moneda extranjera
        /// </summary>
        public decimal[] ForeignInterestRate { get; set; }
        /// <summary>
        /// Tasa de interés para deuda en moneda local
        /// </summary>
        public decimal[] LocalInterestRate { get; set; }

    }

    public class OtherIncomeExpenseInput
    {
        /// <summary>
        /// Ingreso Total
        /// </summary>
        public decimal[] TotalIncome { get; set; }
        /// <summary>
        /// Ingreso de Libre Disposición
        /// </summary>
        public decimal[] AvailableIncome { get; set; }
        /// <summary>
        /// Gasto Total
        /// </summary>
        public decimal[] TotalExpenses { get; set; }
        /// <summary>
        /// Intereses (G)
        /// </summary>
        public decimal[] InterestsG { get; set; }
        /// <summary>
        /// Intereses (P)
        /// </summary>
        public decimal[] InterestsP { get; set; }

        public decimal[] PublicDebtK { get; set; }
        public decimal[] PublicDebtT { get; set; }

    }
}
