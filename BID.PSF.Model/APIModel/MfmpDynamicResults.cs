﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BID.PSF.Model.ApiModel
{
    public class MfmpDynamicResults
    {
        public decimal[] rates { get; set; }
        public int years_to_project { get; set; }
        public decimal[] serie { get; set; }
    }
}
