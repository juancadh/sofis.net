﻿namespace BID.PSF.Model.ApiModel
{
    public class FanChartInitialConditions
    {
        public decimal[] FFit { get; set; }
        public decimal[] Rtml { get; set; }
        public decimal[] Rtme { get; set; }
        public decimal[] Et { get; set; }
        public decimal[] Gildt { get; set; }
        public decimal[] Gidet { get; set; }
        public decimal[] Ggpt { get; set; }

        public decimal DebtInitialValue { get; set; }
        public decimal Gp0 { get; set; }
        public decimal ILD0 { get; set; }
        public decimal IDE0 { get; set; }
        public bool Autocomplete {get; set;}
        public int HistoricColumns { get; set; }
    }
}
