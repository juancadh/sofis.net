﻿using BID.PSF.Model.DBModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class MunicipalityDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("countryId")]
        public int CountryId { get; set; }

        public MunicipalityDTO(Municipality m)
        {
            Id = m.Id;
            Name = m.Name;
            CountryId = m.CountryId;
        }
    }
}
