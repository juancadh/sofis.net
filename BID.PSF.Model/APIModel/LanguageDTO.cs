﻿using BID.PSF.Model.DBModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class LanguageDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("longName")]
        public string LongName { get; set; }
        [JsonProperty("shortName")]
        public string ShortName { get; set; }
        [JsonProperty("encodedName")]
        public string EncodedName { get; set; }

        public LanguageDTO()
        {
            
        }

        public LanguageDTO(Language l)
        {
            Id = l.Id;
            LongName = l.LongName;
            ShortName = l.ShortName;
            EncodedName = l.EncodedName;
        }
    }
}
