﻿using BID.PSF.Model.DBModel;

namespace BID.PSF.Model.ApiModel
{
    public class RuleDTO
    {
        public string Indicator { get; set; }

        public RuleDTO()
        {
            
        }

        public RuleDTO(Rule r)
        {
            Indicator = r.Indicator;
        }
    }
}
