﻿using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class StandardApproachDTO
    {
        /// <summary>
        /// Razón Deuda / Ingreso Disponible
        /// </summary>
        [JsonProperty("debtOverIncome")]
        public decimal DebtOverIncome { get; set; }

        /// <summary>
        /// Tasa de Interés Real
        /// </summary>
        [JsonProperty("realInterestRate")]
        public decimal RealInterestRate { get; set; }

        /// <summary>
        /// Tasa de Crecimiento
        /// </summary>
        [JsonProperty("growthRate")]
        public decimal GrowthRate { get; set; }

        /// <summary>
        /// Meta Deuda / Ingreso Disponible
        /// </summary>
        [JsonProperty("goal")]
        public decimal Goal { get; set; }

        /// <summary>
        /// Períodos para alcanzar objetivo
        /// </summary>
        [JsonProperty("periods")]
        public int Periods { get; set; }

        [JsonProperty("incomeDeviation")]
        public decimal IncomeDeviation { get; set; }

        [JsonProperty("rateDeviation")]
        public decimal RateDeviation { get; set; }

        [JsonProperty("estimatedSurplus")]
        public decimal EstimatedSurplus { get; set; }
    }
}