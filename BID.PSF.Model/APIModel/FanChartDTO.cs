﻿namespace BID.PSF.Model.ApiModel
{
    public class FanChartDTO
    {
        public double[,] VarCovMatrix { get; set; }
    }
}
