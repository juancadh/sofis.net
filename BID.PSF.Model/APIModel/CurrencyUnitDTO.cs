﻿using BID.PSF.Model.DBModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.ApiModel
{
    public class CurrencyUnitDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("shortName")]
        public string ShortName { get; set; }

        public CurrencyUnitDTO()
        {

        }

        public CurrencyUnitDTO(CurrencyUnit c)
        {
            Id = c.Id;
            Name = c.Name;
            ShortName = c.ShortName;
        }
    }
}
