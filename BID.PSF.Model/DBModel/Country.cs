﻿using System.Collections.Generic;
using BID.PSF.Model.ApiModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.DBModel
{
    public class Country : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Municipality> Municipalities { get; set; }
    }
}
