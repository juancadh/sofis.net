﻿using System.Collections.Generic;
using BID.PSF.Model.ApiModel;

namespace BID.PSF.Model.DBModel
{
    public class DynamicColumn : BaseEntity
    {
        public int Index { get; set; }

        /// <summary>
        /// Fuente de Información
        /// </summary>
        public string DataSource { get; set; }

        public int DataSetId { get; set; }

        public int SubCategoryId { get; set; }

        public int CurrencyUnitId { get; set; }

        // Uno de los padres será nulo
        public int? ParentBaseColumnId { get; set; }

        public int? ParentDynamicColumnId { get; set; }

        public virtual DataSet DataSet { get; set; }

        public virtual SubCategory SubCategory { get; set; }

        public virtual CurrencyUnit CurrencyUnit { get; set; }

        public virtual CatalogueColumn ParentBaseColumn { get; set; }

        public virtual DynamicColumn ParentDynamicColumn { get; set; }

        public virtual ICollection<DynamicColumn> DynamicChildren { get; set; }

        public virtual ICollection<DynamicColumnValue> DynamicColumnValues { get; set; }

        public DynamicColumn()
        {

        }

        public DynamicColumn(DynamicColumnDTO d)
        {
            DataSource = d.DataSource;
            SubCategoryId = d.SubCategoryId;
            CurrencyUnitId = d.CurrencyUnitId;
            ParentBaseColumnId = d.ParentBaseColumnId;
            ParentDynamicColumnId = d.ParentDynamicColumnId;
        }
    }
}
