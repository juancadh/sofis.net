﻿namespace BID.PSF.Model.DBModel
{
    public class SubCategory : BaseEntity
    {
        public string Name { get; set; }
        public string InternalName { get; set; }
    }
}
