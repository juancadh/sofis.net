﻿using System.Collections.Generic;

namespace BID.PSF.Model.DBModel
{
    public class Language : BaseEntity
    {
        public string LongName { get; set; }
        public string ShortName { get; set; }
        public string EncodedName { get; set; }

        public virtual ICollection<OAuthUser> Users { get; set; }
    }
}
