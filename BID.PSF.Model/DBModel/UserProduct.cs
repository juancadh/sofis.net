﻿using BID.PSF.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BID.PSF.Model.DBModel
{
    public class UserProduct : BaseEntity
    {
        public string UserId { get; set; }
        public OAuthUser User { get; set; }
        public Enums.UserProducts Product { get; set; }
    }
}
