﻿using System.Collections.Generic;
using BID.PSF.Core.Enums;

namespace BID.PSF.Model.DBModel
{
    public class CatalogueColumn : BaseEntity
    {
        public int Index { get; set; }

        public string ColumnName { get; set; }

        public string InternalName { get; set; }

        public Enums.ColumnCategory Category { get; set; }

        public int CatalogueId { get; set; }
        
        public bool IsDefaultColumn { get; set; }

        public virtual Catalogue Catalogue { get; set; }


        /// <summary>
        /// If available, this column value is used to calculate parent value
        /// </summary>
        public int? ParentId { get; set; }

        public virtual CatalogueColumn Parent { get; set; }

        public virtual ICollection<CatalogueColumnValue> ColumnValues { get; set; }

        /// <summary>
        /// If available, this column value is calculated from children values
        /// </summary>
        public virtual ICollection<CatalogueColumn> Children { get; set; }

        public virtual ICollection<DynamicColumn> DynamicChildren { get; set; }
    }
}
