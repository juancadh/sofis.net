﻿using System.Collections.Generic;
using BID.PSF.Model.ApiModel;
using Newtonsoft.Json;

namespace BID.PSF.Model.DBModel
{
    public class Municipality : BaseEntity
    {  
        public string Name { get; set; }

        public int CountryId { get; set; }

        public virtual Country Country { get; set; }

        public virtual ICollection<OAuthUser> Users { get; set; }
    }
}
