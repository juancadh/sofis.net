﻿using System.Collections.Generic;

namespace BID.PSF.Model.DBModel
{
    public class YearData : BaseEntity
    {
        public int DataSetId { get; set; }
        public int Year { get; set; }

        public virtual DataSet DataSet { get; set; }

        public virtual ICollection<CatalogueColumnValue> CatalogueColumnValue { get; set; }
        public virtual ICollection<DynamicColumnValue> DynamicColumnValue { get; set; }

    }
}
