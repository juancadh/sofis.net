﻿using System.Collections.Generic;
using BID.PSF.Core.Enums;
using BID.PSF.Model.ApiModel;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BID.PSF.Model.DBModel
{
    public class OAuthUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Institution { get; set; }
        public Enums.InstitutionType InstitutionType { get; set; }
        public Enums.UserTitle Title { get; set; }
        public bool FiscalManagementNewsletter { get; set; }
        public string FanChartConfig { get; set; }

        public int? CountryId { get; set; }
        public virtual Country Country { get; set; }

        public int? MunicipalityId { get; set; }
        public virtual Municipality Municipality { get; set; }

        public int? LanguageId { get; set; }
        public virtual Language Language{ get; set; }

        public bool IsFirstLogin { get; set; }

        public int? CurrentCatalogueId { get; set; }
        public virtual Catalogue CurrentCatalogue { get; set; }

        public virtual ICollection<UserDataSet> UserDataSets { get; set; }
        public virtual ICollection<UserInterest> UserInterests { get; set; }    
        public virtual ICollection<UserProduct> UserProducts { get; set; }
    }
}
