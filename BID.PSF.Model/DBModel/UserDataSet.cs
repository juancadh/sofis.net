﻿namespace BID.PSF.Model.DBModel
{
    public class UserDataSet : BaseEntity
    {
        public string UserId { get; set; }
        public int DataSetId { get; set; }

        public DataSet DataSet { get; set; }
        public OAuthUser User { get; set; }
    }
}
