﻿using System.ComponentModel.DataAnnotations;

namespace BID.PSF.Model.DBModel
{
    public class OAuthClient : OAuthEntity
    {
        public enum ApplicationTypes { JavaScript = 0, NativeConfidential = 1 }

        public string Secret { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        public ApplicationTypes ApplicationType { get; set; }
        public bool Active { get; set; }
        public int RefreshTokenLifeTime { get; set; }
        [MaxLength(100)]
        public string AllowedOrigin { get; set; }
    }
}
