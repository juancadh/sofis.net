﻿using System.ComponentModel.DataAnnotations;

namespace BID.PSF.Model.DBModel
{
    public class DynamicColumnValue : BaseEntity
    {
        [Range(0, 999999999999.99)]
        public decimal Value { get; set; }

        public int YearDataId { get; set; }

        public int DynamicColumnId { get; set; }

        public YearData YearData { get; set; }

        public virtual DynamicColumn DynamicColumn { get; set; }
    }
}
