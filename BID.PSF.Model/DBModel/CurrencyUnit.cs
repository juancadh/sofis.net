﻿namespace BID.PSF.Model.DBModel
{
    public class CurrencyUnit : BaseEntity
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
