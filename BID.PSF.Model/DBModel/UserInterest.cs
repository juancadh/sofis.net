﻿using BID.PSF.Core.Enums;

namespace BID.PSF.Model.DBModel
{
    public class UserInterest : BaseEntity
    {
        public string UserId { get; set; }
        public OAuthUser User { get; set; }
        public Enums.UserInterests Interest { get; set; }
    }
}
