﻿using System;
using System.Collections.Generic;

namespace BID.PSF.Model.DBModel
{
    public class DataSet : BaseEntity
    {
        public int CatalogueId { get; set; }

        public int? HistoricFrom { get; set; }
        public int? HistoricTo { get; set; }
        public int? ProjectionFrom { get; set; }
        public int? ProjectionTo { get; set; }

        public virtual ICollection<UserDataSet> UserDataSets { get; set; }

        public virtual ICollection<YearData> YearDatas { get; set; }

        public virtual ICollection<DynamicColumn> DynamicColumns { get; set; }

        public virtual Catalogue Catalogue { get; set; }
    }
}
