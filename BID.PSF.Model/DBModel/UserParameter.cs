﻿namespace BID.PSF.Model.DBModel
{
    public class UserParameter : BaseEntity
    {
        public string Email { get; set; }

        public int CatalogueColumnId { get; set; }

        public decimal Parameter { get; set; }

        public int? Year { get; set; }

        public virtual CatalogueColumn CatalogueColumn { get; set; }
    }
}
