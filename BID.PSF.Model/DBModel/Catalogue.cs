﻿using BID.PSF.Core.Enums;
using System.Collections.Generic;

namespace BID.PSF.Model.DBModel
{
    public class Catalogue : BaseEntity
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsSubcatalogue { get; set; }
        public virtual ICollection<CatalogueColumn> CatalogueColumns { get; set; }
        public virtual ICollection<DataSet> DataSets { get; set; }
        public string Icon { get; set; }
        public Enums.CatalogueType Type { get; set; }
    }
}