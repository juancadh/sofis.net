﻿using System.ComponentModel.DataAnnotations;

namespace BID.PSF.Model.DBModel
{
    public class CatalogueColumnValue : BaseEntity
    {
        [Range(0, 999999999999999999.99)]
        public decimal Value { get; set; }

        public int YearDataId { get; set; }

        public int CatalogueColumnId { get; set; }

        public bool Projected { get; set; }

        public virtual YearData YearData { get; set; }

        public virtual CatalogueColumn CatalogueColumn { get; set; }
    }
}
