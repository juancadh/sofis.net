﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BID.PSF.Model.DBModel
{
    public class UserRule : BaseEntity
    {
        public string Email { get; set; }

        public int RuleId { get; set; }

        public decimal Limits { get; set; }

        public virtual Rule Rule { get; set; }
    }
}
