﻿using System.ComponentModel.DataAnnotations;

namespace BID.PSF.Model.DBModel
{
    // ReSharper disable once InconsistentNaming
    public class OAuthEntity
    {
        [Key]
        public string Id { get; set; }
    }
}
