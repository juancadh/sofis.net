﻿using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Enums;
using BID.PSF.Core.Helpers.Interfaces;
using BID.PSF.Core.Resources;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.ApiModel;
using BID.PSF.Model.DBModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using static BID.PSF.Core.Enums.Enums;

namespace BID.PSF.BLL.Logic
{
    public class CatalogueLogic : ICatalogueLogic
    {
        private readonly ICollectionRepository<Catalogue> _cataloguesRepository;
        private readonly ICollectionRepository<CatalogueColumn> _catalogueColumnsRepository;
        private readonly ICollectionRepository<CatalogueColumnValue> _catalogueColumnValuesRepository;
        private readonly ICollectionRepository<DynamicColumn> _dynamicColumnsRepository;
        private readonly ICollectionRepository<DynamicColumnValue> _dynamicColumnValuesRepository;
        private readonly ICollectionRepository<DataSet> _dataSetsRepository;
        private readonly ICollectionRepository<SubCategory> _subCategoriesRepository;
        private readonly ICollectionRepository<CurrencyUnit> _currencyUnitsRepository;
        private readonly ICatalogueManagerRepository _catalogueManagerRepository;
        private readonly ICollectionRepository<Language> _languagesRepository;
        private readonly IUserStore<OAuthUser> _userRepository;

        private readonly IDataSetsLogic _dataSetsLogic;
        private readonly IYearDataLogic _yearDataLogic;
        private readonly ILogger _logger;
        private readonly IOAuthLogic _oAuthLogic;

        public CatalogueLogic(ICollectionRepository<Catalogue> cataloguesRepository, ICollectionRepository<CatalogueColumn> catalogueColumnsRepository, 
                              ICollectionRepository<CatalogueColumnValue> catalogueColumnValuesRepository, ICollectionRepository<DynamicColumn> dynamicColumnsRepository,
                              ICollectionRepository<DataSet> dataSetsRepository, IYearDataLogic yearDataLogic, IDataSetsLogic dataSetsLogic, ILogger logger,
                              ICollectionRepository<SubCategory> subCategoriesRepository, ICollectionRepository<CurrencyUnit> currencyUnitsRepository,
                              ICollectionRepository<Language> languagesRepository, ICollectionRepository<DynamicColumnValue> dynamicColumnValuesRepository,
                              ICatalogueManagerRepository catalogueManagerRepository, IOAuthLogic oAuthLogic, IUserStore<OAuthUser> userRepository)
        {
            _cataloguesRepository = cataloguesRepository;
            _catalogueColumnsRepository = catalogueColumnsRepository;
            _catalogueColumnValuesRepository = catalogueColumnValuesRepository;
            _dynamicColumnsRepository = dynamicColumnsRepository;
            _dataSetsRepository = dataSetsRepository;
            _yearDataLogic = yearDataLogic;
            _dataSetsLogic = dataSetsLogic;
            _logger = logger;
            _subCategoriesRepository = subCategoriesRepository;
            _currencyUnitsRepository = currencyUnitsRepository;
            _dynamicColumnValuesRepository = dynamicColumnValuesRepository;
            _catalogueManagerRepository = catalogueManagerRepository;
            _oAuthLogic = oAuthLogic;
            _languagesRepository = languagesRepository;
            _userRepository = userRepository;

        }

        public List<HandsOnTableColumn> GetTableStructure(string userId, int catalogueId, int category, int? dataSetId = null)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId, dataSetId);
            var dynamicColumns = _dynamicColumnsRepository.List(d => d.DataSetId == dataSet.Id).ToList();
            var columns = _catalogueColumnsRepository.List(c => c.CatalogueId == dataSet.CatalogueId && c.Category == (Enums.ColumnCategory)category).OrderBy(c => c.Index).ToList();

            var initialColumns = new List<HandsOnTableColumn>
            {
                new HandsOnTableColumn("year", Strings.ColName_Data_Year, false)
                {
                    Type = "numeric",
                    ReadOnly = true,
                    Width = 150,
                }
            };

            initialColumns.AddRange(
                columns.Where(item => item.ParentId == null).Select(item => new HandsOnTableColumn(item.Id.ToString(), item.ColumnName, true, false, item.ParentId, item.IsDefaultColumn, item.Index, 0, item.InternalName) { Type = "numeric", Format = "0,0.00", ReadOnly = item.Children.Any() })
            );

            var finalColumns = new List<HandsOnTableColumn>();
            foreach (var initialColumn in initialColumns)
            {

                finalColumns.Add(initialColumn);

                // Columnas de catálogo sin padre
                var column = columns.SingleOrDefault(c => c.Id.ToString() == initialColumn.Data);

                // Columnas de catálogo con padre
                if (columns.Any(c => c.Parent != null && c.Parent.Id.ToString() == initialColumn.Data))
                {
                    var firstLevel = columns.Where(c => c.Parent != null && c.Parent.Id.ToString() == initialColumn.Data);
                    foreach(var f in firstLevel)
                    {
                        var secondLevel = columns.Where(c => c.Parent != null && c.Parent.Id == f.Id);
                        if (secondLevel.Count()!= 0)
                        {
                            finalColumns.Add(new HandsOnTableColumn(f.Id.ToString(), f.ColumnName, false, f.ParentId, f.IsDefaultColumn, f.Index, 1, f.InternalName) { Type = "numeric", Format = "0,0.00", ReadOnly = true });
                            foreach (var s in secondLevel)
                            {
                                var thirdLevel = columns.Where(c => c.Parent != null && c.Parent.Id == s.Id);
                                if(thirdLevel.Count() != 0)
                                {
                                    finalColumns.Add(new HandsOnTableColumn(s.Id.ToString(), s.ColumnName, false, s.ParentId, s.IsDefaultColumn, s.Index, 2, s.InternalName) { Type = "numeric", Format = "0,0.00", ReadOnly = true });
                                    finalColumns.AddRange(thirdLevel.Select(child => new HandsOnTableColumn(child.Id.ToString(), child.ColumnName, false, child.ParentId, child.IsDefaultColumn, child.Index, 3, child.InternalName) { Type = "numeric", Format = "0,0.00" }));
                                }
                                else
                                {
                                    finalColumns.Add(new HandsOnTableColumn(s.Id.ToString(), s.ColumnName, false, s.ParentId, s.IsDefaultColumn, s.Index, 2, s.InternalName) { Type = "numeric", Format = "0,0.00", ReadOnly = false });
                                }
                            }
                        }
                        else
                        {
                            finalColumns.Add(new HandsOnTableColumn(f.Id.ToString(), f.ColumnName, false, f.ParentId, f.IsDefaultColumn, f.Index, 1, f.InternalName) { Type = "numeric", Format = "0,0.00", ReadOnly = false });
                        }
                    }
                    
                }

                // Columnas dinámicas base
                if (dynamicColumns.Any(d => d.ParentBaseColumnId != null && d.ParentBaseColumn.Id.ToString() == initialColumn.Data))
                {
                    var children = dynamicColumns.Where(d => d.ParentBaseColumnId != null && d.ParentBaseColumn.Id.ToString() == initialColumn.Data).ToList();
                    finalColumns.AddRange(children.Select(child => new HandsOnTableColumn(child.Id.ToString(), child.SubCategory.Name, true) { Type = "numeric", Format = "0,0.00" }));

                    // Columnas dinámicas hijas
                    foreach (var dynamicColumn in children)
                    {
                        var dynamicChildren = dynamicColumns.Where(d => d.ParentDynamicColumnId != null && d.ParentDynamicColumnId == dynamicColumn.Id).ToList();
                        finalColumns.AddRange(dynamicChildren.Select(child => new HandsOnTableColumn(child.Id.ToString(), child.SubCategory.Name, true)));
                    }
                }
            }

            return finalColumns;
        }

        public ApiResult GetCatalogues(string userId)
        {
            var user = _oAuthLogic.FindUserByEmail(userId);

            var datasetList = _dataSetsRepository.List(ds => ds.UserDataSets.Where(ud => ud.UserId ==user.Id).ToList().Count() > 0).ToList();
            var catalogues =  _cataloguesRepository.List().Where(c=> datasetList.Find(d => d.CatalogueId == c.Id)!= null).Select(c => new CatalogueDTO(c) { Selected = user.CurrentCatalogueId != null ? c.Id == user.CurrentCatalogueId : c.Id == datasetList.FirstOrDefault().CatalogueId}).ToList();

            return new ApiResult
            {
                Data = catalogues,
                StatusCode = catalogues.Count > 0 ? HttpStatusCode.OK : HttpStatusCode.NoContent
            };
        }

        public async Task<ApiResult> AssignCatalogueToUser(int catalogueId, string userId)
        {
            DataSet dataset = _dataSetsLogic.GetOrCreateUserDataSet(userId, catalogueId);

            OAuthUser user = _oAuthLogic.FindUserByEmail(userId);

            user.CurrentCatalogueId = catalogueId;
            await _userRepository.UpdateAsync(user);

            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK
            };
        }

        public ApiResult AddColumnValues(CatalogueColumnValues values)
        {
            var result = new ApiResult { StatusCode = HttpStatusCode.OK };
            var dataSet = _dataSetsRepository.FindById(values.DataSetId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError, Message = string.Format(ErrorDescriptions.DatSet_InvalidId, values.DataSetId) };

            // Group by Year in order to reduce db access
            foreach (var year in values.Values.GroupBy(v => v.Year))
            {
                var yearData = _yearDataLogic.GetOrCreateYearData(year.Key, values.DataSetId);
                if (yearData == null)
                    continue;

                foreach (var value in year)
                {
                    if (!value.Dynamic)
                    {
                        CatalogueColumn catalogueColumn = FindCatalogueColumnById(value.ColumnId);
                        if (catalogueColumn == null)
                            continue;

                        var prevValue = _catalogueColumnValuesRepository
                            .List(v => v.YearDataId == yearData.Id && v.CatalogueColumnId == catalogueColumn.Id)
                            .FirstOrDefault();
                        if (prevValue != null)
                        {
                            // Prevent 0 value to be saved
                            if (value.Value == 0)
                                _catalogueColumnValuesRepository.Delete(prevValue.Id);
                            else
                            {
                                prevValue.Value = value.Value;
                                _catalogueColumnValuesRepository.Update(prevValue);
                            }
                        }
                        else
                        {
                            if (value.Value == 0)
                                continue;

                            var columnValue = new CatalogueColumnValue
                            {
                                YearDataId = yearData.Id,
                                CatalogueColumnId = catalogueColumn.Id,
                                Value = value.Value,
                                Projected = value.Projected
                            };
                            _catalogueColumnValuesRepository.Add(columnValue);
                        }
                    }
                    else
                    {
                        DynamicColumn dynamicColumn = FindDynamicColumnById(value.ColumnId);
                        if (dynamicColumn == null)
                            continue;

                        var prevValue = _dynamicColumnValuesRepository
                            .List(v => v.YearDataId == yearData.Id && v.DynamicColumnId == dynamicColumn.Id)
                            .FirstOrDefault();
                        if (prevValue != null)
                        {
                            // Prevent 0 value to be saved
                            if (value.Value == 0)
                                _dynamicColumnValuesRepository.Delete(prevValue.Id);
                            else
                            {
                                prevValue.Value = value.Value;
                                _dynamicColumnValuesRepository.Update(prevValue);
                            }
                        }
                        else
                        {
                            if (value.Value == 0)
                                continue;

                            var columnValue = new DynamicColumnValue
                            {
                                YearDataId = yearData.Id,
                                DynamicColumnId = dynamicColumn.Id,
                                Value = value.Value,
                                //Projected = value.Projected
                            };
                            _dynamicColumnValuesRepository.Add(columnValue);
                        }
                    }
                }
            }


            return result;
        }

        public ApiResult GetSubCategories()
        {
            var subcategories = _subCategoriesRepository.List().Select(s => new SubCategoryDTO(s)).ToList();

            return new ApiResult { Data = subcategories, StatusCode = HttpStatusCode.OK };
        }

        public ApiResult GetCurrencyUnits()
        {
            var currencyUnits = _currencyUnitsRepository.List().Select(c => new CurrencyUnitDTO(c)).ToList();

            return new ApiResult { Data = currencyUnits, StatusCode = HttpStatusCode.OK };
        }

        public ApiResult GetValues(string userId, int catalogueId, Enums.ColumnCategory category, int? dataSetId = null)
        {
            var values = new CatalogueColumnValues
            {
                Category = category,
                Values = new List<CatalogueColumnValueDTO>()
            };

            DataSet dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId, dataSetId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError, Result = ErrorDescriptions.DataSet_NotFound };

            values.DataSetId = dataSet.Id;

            List<CatalogueColumn> columns = _catalogueColumnsRepository.List(c => c.CatalogueId == dataSet.CatalogueId && c.Category == category).OrderBy(c => c.Index).ToList();

            foreach (var column in columns)
            {
                if (column.Children == null || !column.Children.Any())
                {
                    values.Values.AddRange(column.ColumnValues.Where(v => v.YearData.DataSetId == dataSet.Id).Select(value => new CatalogueColumnValueDTO
                    {
                        ColumnId = column.Id,
                        Value = value.Value,
                        Year = value.YearData.Year,
                    }));
                }
                else
                {
                    var year = DateTime.Now.Year;
                    for (int i = year - 30; i <= year + 5; i++)
                    {
                        var cvalue = column.ColumnValues.FirstOrDefault(v => v.YearData.DataSetId == dataSet.Id && v.YearData.Year == i);

                        if (cvalue == null)
                            values.Values.Add(new CatalogueColumnValueDTO
                            {
                                ColumnId = column.Id,
                                Value = GetCatalogueValueFromChildren(column, dataSet.Id, i),
                                Formula = "=" + string.Join(",", column.Children.Select(c => c.Id)),
                                Year = i,
                            });
                        else
                            values.Values.Add(new CatalogueColumnValueDTO
                            {
                                ColumnId = column.Id,
                                Value = cvalue.Value,
                                Formula = "=" + string.Join(",", column.Children.Select(c => c.Id)),
                                Year = i,
                            });
                    }
                }
            }

            List<DynamicColumn> dynamicColumns = _dynamicColumnsRepository.List(c => c.ParentBaseColumn != null && c.ParentBaseColumn.CatalogueId == dataSet.CatalogueId && c.ParentBaseColumn.Category == category).OrderBy(c => c.Index).ToList();

            foreach (var column in dynamicColumns)
            {
                if (column.DynamicChildren == null || !column.DynamicChildren.Any())
                {
                    values.Values.AddRange(column.DynamicColumnValues.Where(v => v.YearData.DataSetId == dataSet.Id).Select(value => new CatalogueColumnValueDTO
                    {
                        ColumnId = column.Id,
                        Value = value.Value,
                        Year = value.YearData.Year,
                    }));
                }
                else
                {
                    var year = DateTime.Now.Year;
                    for (int i = year - 30; i <= year; i++)
                    {
                        var cvalue = column.DynamicColumnValues.FirstOrDefault(v => v.YearData.DataSetId == dataSet.Id && v.YearData.Year == i);

                        if (cvalue == null)
                            values.Values.Add(new CatalogueColumnValueDTO
                            {
                                ColumnId = column.Id,
                                Value = GetDynamicValueFromChildren(column, dataSet.Id, i),
                                Formula = "=" + string.Join(",", column.DynamicChildren.Select(c => c.Id)),
                                Year = i,
                            });
                        else
                            values.Values.Add(new CatalogueColumnValueDTO
                            {
                                ColumnId = column.Id,
                                Value = cvalue.Value,
                                Formula = "=" + string.Join(",", column.DynamicChildren.Select(c => c.Id)),
                                Year = i,
                            });
                    }
                }
            }

            return new ApiResult { Data = values, StatusCode = HttpStatusCode.OK };
        }

        private decimal GetCatalogueValueFromChildren(CatalogueColumn c, int datasetId, int year)
        {
            decimal value = 0;
            if (c.Children == null || c.Children.Count == 0)
                return 0;

            foreach (var child in c.Children)
            {
                if (child.Children != null && child.Children.Count > 0)
                    value += GetCatalogueValueFromChildren(child, datasetId, year);
                else
                    value += child.ColumnValues.FirstOrDefault(v => v.YearData.Year == year && v.YearData.DataSetId == datasetId)?.Value ?? 0;
            }

            return value;
        }

        private decimal GetDynamicValueFromChildren(DynamicColumn c, int datasetId, int year)
        {
            decimal value = 0;
            if (c.DynamicChildren == null || c.DynamicChildren.Count == 0)
                return 0;

            foreach (var child in c.DynamicChildren)
            {
                if (child.DynamicChildren != null && child.DynamicChildren.Count > 0)
                    value += GetDynamicValueFromChildren(child, datasetId, year);
                else
                    value += child.DynamicColumnValues.FirstOrDefault(v => v.YearData.Year == year && v.YearData.DataSetId == datasetId)?.Value ?? 0;
            }

            return value;
        }

        private CatalogueColumn FindCatalogueColumnById(int columnId)
        {
            return _catalogueColumnsRepository.FindById(columnId);
        }

        private DynamicColumn FindDynamicColumnById(int columnId)
        {
            return _dynamicColumnsRepository.FindById(columnId);
        }

        public ApiResult CreateDynamicColumn(int datasetId, DynamicColumnDTO dynamicColumn)
        {
            var result = new ApiResult { StatusCode = HttpStatusCode.OK };
            var dataSet = _dataSetsRepository.FindById(datasetId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError, Message = string.Format(ErrorDescriptions.DatSet_InvalidId, datasetId) };

            var column = new DynamicColumn(dynamicColumn)
            {
                Index = 1,
                DataSetId = datasetId
            };

            _dynamicColumnsRepository.Add(column);

            return result;
        }

        public ApiResult DeleteDynamicColumn(string userId, int catalogueId, int dynamicColumnId)
        {
            var result = new ApiResult { StatusCode = HttpStatusCode.OK };
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError, Message = string.Format(ErrorDescriptions.DataSet_NotFound, catalogueId) };

            _dynamicColumnsRepository.Delete(dynamicColumnId);

            return result;
        }

        public ApiResult GetDataSetYears(string userId, int catalogueId)
        {
            var dataset = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataset == null)
                return new ApiResult { StatusCode = HttpStatusCode.NoContent };

            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = new DataSetYears
                {
                    HistoricFrom = dataset.HistoricFrom,
                    HistoricTo = dataset.HistoricTo,
                    ProjectionFrom = dataset.ProjectionFrom,
                    ProjectionTo = dataset.ProjectionTo
                }
            };
        }

        public ApiResult UpdateDataSetYears(string userId, int catalogueId, DataSetYears years)
        {
            var dataset = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataset == null)
                return new ApiResult { StatusCode = HttpStatusCode.NoContent };

            dataset.HistoricFrom = years.HistoricFrom;
            dataset.HistoricTo = years.HistoricTo;
            dataset.ProjectionFrom = years.ProjectionFrom;
            dataset.ProjectionTo = years.ProjectionTo;

            _dataSetsRepository.Update(dataset);

            return new ApiResult { StatusCode = HttpStatusCode.OK };
        }

        public ApiResult CreateCataloge(string userId, CreateCatalogueDTO catalogue)
        {
            var user = _oAuthLogic.FindUserByEmail(userId);
            if (user.LanguageId != null)
            {
                var language = _languagesRepository.FindById(user.LanguageId.Value);

                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(language.EncodedName);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }

            var newCatalogue = new Catalogue()
            {
                Name = catalogue.Name,
                Icon = catalogue.Icon,
                CatalogueColumns = CreateCatalogueColumnsDefault(),
                IsActive = true
            };

            newCatalogue = _catalogueManagerRepository.AddCatalogue(newCatalogue, _oAuthLogic.FindUserByEmail(userId).Id);
            newCatalogue.CatalogueColumns = null;
            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = newCatalogue.Id

            };
        }

        public ApiResult UpdateCatalogue(CreateCatalogueDTO catalogue)
        {
            var updateCatalogue = new Catalogue()
            {
                Id = catalogue.Id,
                Name = catalogue.Name,
                Icon = catalogue.Icon

            };

            _catalogueManagerRepository.UpdateCatalogue(updateCatalogue);

            return new ApiResult { StatusCode = HttpStatusCode.OK };
        }

        public ApiResult DeleteCatalogue(int catalogueId)
        {
            var catalogue = _cataloguesRepository.FindById(catalogueId);
            catalogue.IsActive = false;
            _catalogueManagerRepository.UpdateCatalogue(catalogue);

            return new ApiResult { StatusCode = HttpStatusCode.OK };

        }

        public ApiResult CloneCatalogue(string userId, int catalogueId, string name)
        {

            Catalogue catalogueToClone = new Catalogue()
            {
                Name = name,
                IsSubcatalogue = GetCurrentUserByCatalogue(userId).Roles.Contains(Enums.Roles.Administrator) ?
                                 _cataloguesRepository.FindById(catalogueId).IsSubcatalogue
                                 : true,
                IsActive = true
            };


            List<CatalogueColumn> columnList = _catalogueColumnsRepository.List(c => c.CatalogueId == catalogueId).ToList();
            var catalogue = _catalogueManagerRepository.CloneCatalogue(catalogueToClone, _oAuthLogic.FindUserByEmail(userId).Id, columnList);
            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = catalogue.Id
            };
        }

        public ApiResult CreateCatalogueColumn(CatalogueColumnDTO column)
        {

            var columns = _catalogueColumnsRepository.List(c => c.CatalogueId == column.CatalogueId && c.Category == column.Category).ToList();
            foreach (var c in columns)
            {
                if (c.Index >= column.Index)
                {
                    c.Index = c.Index + 1;
                    _catalogueColumnsRepository.Update(c);
                }

            }

            _catalogueColumnsRepository.Add(new CatalogueColumn()
            {
                CatalogueId = column.CatalogueId,
                Category = column.Category,
                Index = column.Index,
                ColumnName = column.ColumnName,
                InternalName = column.InternalName,
                ParentId = column.ParentId,
                IsDefaultColumn = column.IsDefaultColumn
            });


            return new ApiResult { StatusCode = HttpStatusCode.OK };
        }

        public ApiResult UpdateCatalogueColumn(CatalogueColumnDTO column)
        {
            var col = _catalogueColumnsRepository.FindById(column.Id);

            if (column.IsDefaultColumn)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError, Message = Strings.CatalogueManager_ErrorMessage_UpdateDefaultColumn };

            col.ColumnName = column.ColumnName;

            _catalogueColumnsRepository.Update(col);
            var columns = _catalogueColumnsRepository.List(c => c.CatalogueId == column.CatalogueId);

            return new ApiResult { StatusCode = HttpStatusCode.OK };
        }

        public ApiResult DeleteCatalogueColumn(int columnId)
        {
            var column = _catalogueColumnsRepository.FindById(columnId);
            if(column.IsDefaultColumn)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError, Message = Strings.CatalogueManager_ErrorMessage_DeleteDefaultColumn };

            var columnList = _catalogueColumnsRepository.List(c => c.CatalogueId == column.CatalogueId && c.Category == column.Category).ToList();

            if(columnList.Find(d => d.ParentId == column.Id) != null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError, Message = Strings.CatalogueManager_ErrorMessage_DeleteParentColumn };
              

            foreach (var c in columnList)
            {
                if (c.Index >= column.Index)
                {
                    c.Index = c.Index - 1;
                    _catalogueColumnsRepository.Update(c);
                }

            }
            _catalogueColumnsRepository.Delete(columnId);

            return new ApiResult { StatusCode = HttpStatusCode.OK };
        }

        public UserDTO GetCurrentUserByCatalogue(string userId)
        {
            var user = new UserDTO();

            try
            {
                var oAuthUser = _oAuthLogic.FindUserByEmail(userId);
                user = new UserDTO(oAuthUser);
            }
            catch (Exception e)
            {
                _logger.LogException(e);
            }

            return user;
        }

        public bool UserIsInDataset(string userId, int catalogueId)
        {
            var dataset = _dataSetsRepository.List(c => c.CatalogueId == catalogueId).FirstOrDefault();

            if (dataset == null)
            {
                return false;
            }

            return dataset.UserDataSets.Contains(new UserDataSet() { UserId = userId });
        }

        public ApiResult SetHistoricYear(string userId, int catalogueId)
        {
            var dataSet = _dataSetsRepository.List(x => x.CatalogueId == catalogueId && x.CreatedById == userId).FirstOrDefault();
            var lastYearWithData = dataSet.HistoricTo;
            var columns = _catalogueColumnsRepository.List().Where(x => x.CatalogueId == catalogueId && x.Parent == null && x.Children.Any());
            bool changeHistoricYear = false;
            foreach (var c in columns)
            {
                if (c.ColumnValues.Any() &&
                    c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Any() &&
                    c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Max(x => x.YearData.Year) < lastYearWithData)
                {
                    lastYearWithData = c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Max(x => x.YearData.Year);
                }
            }

            if (dataSet.HistoricTo > lastYearWithData)
            {
                dataSet.HistoricTo = lastYearWithData;
                dataSet.ProjectionFrom = lastYearWithData + 1;
                _dataSetsRepository.Update(dataSet);
                changeHistoricYear = true;
            }

            return new ApiResult { StatusCode = HttpStatusCode.OK, Data = changeHistoricYear };
        }

        private List<CatalogueColumn> CreateCatalogueColumnsDefault()
        {
            return new List<CatalogueColumn>() {
                // Macroeconómicos
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Macroeconomy_ConsumerIndexPrice, InternalName = "ConsumerIndexPrice", Category = Enums.ColumnCategory.Macroeconomy, IsDefaultColumn = true},
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Macroeconomy_LocalChangeRate, InternalName = "LocalChangeRate",Category = Enums.ColumnCategory.Macroeconomy, IsDefaultColumn = true},
                //Ingresos
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Income_TotalIncome, InternalName = "TotalIncome", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                {

                    new CatalogueColumn { Index = 2, ColumnName = Strings.ColName_Income_AvailableIncome, InternalName = "AvailableIncome", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn {Index = 3, ColumnName = Strings.ColName_Income_AvailableTax, InternalName = "AvailableTax",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true},
                        new CatalogueColumn {Index = 4, ColumnName = Strings.ColName_Income_SocialSecurityContributions, InternalName = "", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 5,ColumnName = Strings.ColName_Income_ImprovementContributions, InternalName = "ImprovementContributions",Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 6,ColumnName = Strings.ColName_Income_Rights, InternalName = "Rights", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 7,ColumnName = Strings.ColName_Income_Products, InternalName = "Products",Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 8,ColumnName = Strings.ColName_Income_Advantage, InternalName = "Exploits",Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                         new CatalogueColumn {Index = 9,ColumnName = Strings.ColName_Income_SalesGoodsServices, InternalName = "GoodsServices", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 10,ColumnName = Strings.ColName_Income_Participations, InternalName = "Participations", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 11,ColumnName = Strings.ColName_Income_Incentives, InternalName = "Incentives",Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn { Index = 12, ColumnName = Strings.ColName_Income_AvailableTransfer, InternalName = "AvailableTransfer",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true},
                        new CatalogueColumn {Index = 13,ColumnName = Strings.ColName_Income_Agreements, InternalName = "LDAgreements", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 14,ColumnName = Strings.ColName_Income_AvailableOther, InternalName = "AvailableOther",Category = Enums.ColumnCategory.Income, IsDefaultColumn = true}
                    }},

                    new CatalogueColumn { Index = 15, ColumnName = Strings.ColName_Income_EarmarkedRevenues, InternalName = "NotAvailableIncome", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn {Index = 16,ColumnName = Strings.ColName_Income_FederalTransfers,InternalName = Enums.IncomeColumns.FederalTransfers.ToString(), Category = Enums.ColumnCategory.Income, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                        {
                            new CatalogueColumn {Index = 17,ColumnName = Strings.ColName_Income_Contribution, InternalName = "Contributions",Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                            new CatalogueColumn {Index = 18,ColumnName = Strings.ColName_Income_Agreements, InternalName = "Agreements",Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                            new CatalogueColumn {Index = 19,ColumnName = Strings.ColName_Income_DifferentContributions, InternalName = "DifferentContributions", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                            new CatalogueColumn {Index = 20,ColumnName = Strings.ColName_Income_Subsidies, InternalName = "Subsidies",Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                            new CatalogueColumn {Index = 21,ColumnName = Strings.ColName_Income_OtherTransfers, InternalName = "OtherTransfers", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true}
                        }
                    },
                    new CatalogueColumn {Index = 22,ColumnName = Strings.ColName_Income_DerivatedIncomes, InternalName = "DerivatedIncomes", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                    }},
                }},
                //Gastos
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Expense_TotalExpense, InternalName = "TotalExpense", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true, Children = new List<CatalogueColumn>()
                {
                    new CatalogueColumn {Index = 2,ColumnName = Strings.ColName_Expense_UntaggedExpense, InternalName = "",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn {Index = 3,ColumnName = Strings.ColName_Expense_PersonalServices, InternalName = "PSNLabeled", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 4,ColumnName = Strings.ColName_Expense_MaterialsAndSupplies, InternalName = "MSNLabeled", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 5,ColumnName = Strings.ColName_Expense_GeneralServices, InternalName = "GSNLabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 6,ColumnName = Strings.ColName_Expense_TASO, InternalName = "TASONLabeled", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 7,ColumnName = Strings.ColName_Expense_RealState, InternalName = "BINLabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 8,ColumnName = Strings.ColName_Expense_PublicInvestment, InternalName = "",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 9,ColumnName = Strings.ColName_Expense_FinancialInvestments, InternalName = "INVNolaveled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 10,ColumnName = Strings.ColName_Expense_ParticipationsContributions, InternalName = "PANLabeled", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 11,ColumnName = Strings.ColName_Expense_PublicDebt, InternalName = "PDNLabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true}
                    }},
                    new CatalogueColumn {Index = 12,ColumnName = Strings.ColName_Expense_TaggedExpense, InternalName = "",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn {Index = 13,ColumnName = Strings.ColName_Expense_PersonalServices, InternalName = "PSLabeled", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 14,ColumnName = Strings.ColName_Expense_MaterialsAndSupplies, InternalName = "MSLabeled", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 15,ColumnName = Strings.ColName_Expense_GeneralServices, InternalName = "GSLabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 16,ColumnName = Strings.ColName_Expense_TASO, InternalName = "TASOLabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 17,ColumnName = Strings.ColName_Expense_RealState, InternalName = "BILabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 18,ColumnName = Strings.ColName_Expense_PublicInvestment, InternalName = "",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 19,ColumnName = Strings.ColName_Expense_FinancialInvestments, InternalName = "INVolaveled", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 20,ColumnName = Strings.ColName_Expense_ParticipationsContributions, InternalName = "PALabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 21,ColumnName = Strings.ColName_Expense_PublicDebt, InternalName = "PDLabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 22,ColumnName = Strings.ColName_Expense_AvailablePublicInvestment, InternalName = "",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},

                    }}

                }},
                // Deudas
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Debt_TotalDebt, InternalName = "TotalDebt", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                {
                    new CatalogueColumn { Index = 2, ColumnName = Strings.ColName_Debt_HistoricVariableRateDomesticCurrency, InternalName = "HistoricVariableRateDomesticCurrency", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},
                    new CatalogueColumn { Index = 3, ColumnName = Strings.ColName_Debt_HistoricVariableRateForeignCurrency, InternalName = "HistoricVariableRateForeignCurrency",Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},
                    new CatalogueColumn { Index = 4, ColumnName = Strings.ColName_Debt_HistoricFixedRateDomesticCurrency, InternalName = "HistoricFixedRateDomesticCurrency",Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},
                    new CatalogueColumn { Index = 5, ColumnName = Strings.ColName_Debt_HistoricFixedRateForeignCurrency, InternalName = "HistoricFixedRateForeignCurrency", Category = Enums.ColumnCategory.Debt,IsDefaultColumn = true}
                } },
                new CatalogueColumn { Index = 6, ColumnName = Strings.ColName_Debt_LocalRate, InternalName = "LocalRate", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},
                new CatalogueColumn { Index = 7, ColumnName = Strings.ColName_Debt_ForeignRate, InternalName = "ForeignRate", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},

                new CatalogueColumn { Index = 8, ColumnName = Strings.ColName_Debt_Service, InternalName = "DebtService", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                {
                    new CatalogueColumn { Index = 9, ColumnName = Strings.ColName_Debt_TotalAmortization, InternalName = "TotalAmortizations", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn { Index = 10, ColumnName = Strings.ColName_Debt_LocalCurrencyAmortizationFixeRate, InternalName = "", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},
                        new CatalogueColumn { Index = 11, ColumnName = Strings.ColName_Debt_LocalCurrencyAmortizationVariableRate,InternalName = "", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},
                        new CatalogueColumn { Index = 12, ColumnName = Strings.ColName_Debt_ForeignCurrencyAmortizationFixeRate,InternalName = "", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},
                        new CatalogueColumn { Index = 13, ColumnName = Strings.ColName_Debt_ForeignCurrencyAmortizationVariableRate,InternalName = "", Category = Enums.ColumnCategory.Debt, IsDefaultColumn = true},
                    } },
                } }

            };
        }
    }
}
