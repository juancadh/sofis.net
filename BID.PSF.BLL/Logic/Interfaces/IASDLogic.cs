﻿using BID.PSF.Model.ApiModel;
using System.Threading.Tasks;

namespace BID.PSF.BLL.Logic.Interfaces
{
    // ReSharper disable once InconsistentNaming
    public interface IASDLogic
    {

        Task<ApiResult> StandardApproach(string userId, int catalogueId);
        Task<ApiResult> SensitivityAnalysis(string userId, int catalogueId, StandardApproachDTO standardData);
        ApiResult DynamicsOfDebt(string userId, int catalogueId, DynamicsOfDebtDTO dynamicsData);
        ApiResult GetDynamicYears(string userId, int catalogueId);
        ApiResult FanChart(string userId, int catalogueId, FanChartDTO fanChartData);
        ApiResult GetProbabilisticAssumptions(string userId, int catalogueId);
        ApiResult GetFanChartYears(string userId, int catalogueId);

        ApiResult GetStandardAproximationData(string userId, int catalogueId);
        ApiResult GetInitialConditions(string userId, int catalogueId);
        ApiResult GetDebtDynamicsData(string userId, int catalogueId);
        ApiResult SetFanChartConfig(string userId, string matrix);
    }
}
