﻿using System.Net;
using System.Security.Principal;
using System.Threading.Tasks;
using BID.PSF.Model.ApiModel;
using BID.PSF.Model.DBModel;
using Microsoft.AspNet.Identity;
using Okta.Core.Models;

namespace BID.PSF.BLL.Logic.Interfaces
{
    public interface IOAuthLogic
    {
        IdentityResult RegisterUser(OAuthUser user);
        IdentityResult UpdateUser(UserDTO userData);
        HttpStatusCode ResetPassword(ResetPasswordDTO resetPassword);
        HttpStatusCode ForgotPassword(ForgotPasswordDTO forgotPassword);
        Task<OAuthUser> FindUser(string userName, string password);
        OAuthClient FindClient(string clientId);
        OAuthUser FindUserByEmail(string email);
        bool AddRefreshToken(RefreshToken token);
        RefreshToken FindRefreshToken(string refreshTokenId);
        bool RemoveRefreshToken(string refreshTokenId);
    }
}
