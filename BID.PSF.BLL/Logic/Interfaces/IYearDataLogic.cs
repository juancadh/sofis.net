﻿using BID.PSF.Model.DBModel;

namespace BID.PSF.BLL.Logic.Interfaces
{
    public interface IYearDataLogic
    {
        YearData GetOrCreateYearData(int year, int dataSetId);
    }
}
