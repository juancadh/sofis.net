﻿using BID.PSF.Model.DBModel;
using System.Collections.Generic;

namespace BID.PSF.BLL.Logic.Interfaces
{
    public interface IDataSetsLogic
    {
        DataSet GetOrCreateUserDataSet(string userId, int catalogueId, int? dataSetId = null);
        DataSet GetUserDataSet(string userId, int catalogueId, int? dataSetId = null);
        DataSet CreateDataset(DataSet dataset);
    }
}
