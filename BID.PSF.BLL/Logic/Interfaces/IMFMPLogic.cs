﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BID.PSF.Model.ApiModel;
using BID.PSF.Model.DBModel;

namespace BID.PSF.BLL.Logic.Interfaces
{
    // ReSharper disable once InconsistentNaming
    public interface IMFMPLogic
    {
        ApiResult InertialApproximation(string userId, int catalogueId, int yearsToSimulate, bool fiveYears = false);    
        ApiResult GetFixedApproximationStructure(string userId, int catalogueId);
        ApiResult GetVariableApproximationStructure(string userId, int catalogueId);
        ApiResult GetRulesStructure(string userId, int catalogueId);
        ApiResult GetRulesCharts(string userId, int catalogueId);
        ApiResult SaveLimits(string userId, int catalogueId, UserRule userRule);
        ApiResult GetFirstProjectionYear(string userId, int catalogueId);
        ApiResult GetProjectionYear(string userId, int catalogueId);

    }
}
