﻿using System.Threading.Tasks;
using BID.PSF.Model.ApiModel;

namespace BID.PSF.BLL.Logic.Interfaces
{
    public interface IProfileLogic
    {
        Task<ApiResult> GetCurrentUser(string userId, string culture);
        ApiResult GetCountryById(int countryId);
        Task<ApiResult> UpdateUserAsync(UserDTO user);
        ApiResult GetLists();
        Task<ApiResult> SetUserLanguageAsync(string userId, string langCode);
    }
}
