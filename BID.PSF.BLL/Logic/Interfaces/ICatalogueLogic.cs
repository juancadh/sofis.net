﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BID.PSF.Core.Enums;
using BID.PSF.Model.ApiModel;
using BID.PSF.Model.DBModel;

namespace BID.PSF.BLL.Logic.Interfaces
{
    public interface ICatalogueLogic
    {
        List<HandsOnTableColumn> GetTableStructure(string userId, int catalogueId, int category, int? dataSetId = null);
        ApiResult GetCatalogues(string userId);
        Task<ApiResult> AssignCatalogueToUser(int catalogueId, string userId);
        ApiResult GetValues(string userId, int catalogueId, Enums.ColumnCategory category, int? dataSetId = null);
        ApiResult AddColumnValues(CatalogueColumnValues values);
        ApiResult GetSubCategories();
        ApiResult GetCurrencyUnits();
        ApiResult CreateDynamicColumn(int datasetId, DynamicColumnDTO dynamicColumn);
        ApiResult DeleteDynamicColumn(string userId, int catalogueId, int dynamicColumnId);

        ApiResult GetDataSetYears(string userId, int catalogueId);
        ApiResult UpdateDataSetYears(string userId, int catalogueId, DataSetYears years);

        ApiResult CreateCataloge(string userId, CreateCatalogueDTO catalogue);
        ApiResult UpdateCatalogue(CreateCatalogueDTO catalogue);
        ApiResult DeleteCatalogue(int catalogueId);
        ApiResult CloneCatalogue(string userId, int catalogueId, string name);

        ApiResult CreateCatalogueColumn(CatalogueColumnDTO column);
        ApiResult UpdateCatalogueColumn(CatalogueColumnDTO column);
        ApiResult DeleteCatalogueColumn(int columnId);
        UserDTO GetCurrentUserByCatalogue(string userId);
        ApiResult SetHistoricYear(string userId, int catalogueId);
        bool UserIsInDataset(string userId, int catalogueId);
    }
}
