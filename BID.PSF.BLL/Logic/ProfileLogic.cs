﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Enums;
using BID.PSF.Core.Helpers.Interfaces;
using BID.PSF.Core.Resources;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.ApiModel;
using BID.PSF.Model.DBModel;
using Microsoft.AspNet.Identity;

namespace BID.PSF.BLL.Logic
{
    public class ProfileLogic : IProfileLogic
    {
        private readonly IUserStore<OAuthUser> _usersRepository;
        private readonly ICollectionRepository<Country> _countriesRepository;
        private readonly ICollectionRepository<Municipality> _municipalitiesRepository;
        private readonly ICollectionRepository<Language> _languagesRepository;
        private readonly ICollectionRepository<UserInterest> _userInterestsRepository;
        private readonly ICollectionRepository<UserProduct> _userProductsRepository;
        private readonly ICollectionRepository<DataSet> _datasetRepository;
        private readonly ICollectionRepository<UserDataSet> _userDatasetRepository;
        private readonly ILogger _logger;
        private readonly IOAuthLogic _oAuthLogic;

        public ProfileLogic(IUserStore<OAuthUser> usersRepository, ICollectionRepository<Country> countriesRepository, ICollectionRepository<Municipality> municipalitiesRepository, ICollectionRepository<Language> languagesRepository, ILogger logger, ICollectionRepository<UserInterest> userInterestsRepository, ICollectionRepository<UserProduct> userProductsRepository, ICollectionRepository<DataSet> datasetRepository, ICollectionRepository<UserDataSet> userDatasetRepository, IOAuthLogic oAuthLogic)
        {
            _usersRepository = usersRepository;
            _countriesRepository = countriesRepository;
            _municipalitiesRepository = municipalitiesRepository;
            _languagesRepository = languagesRepository;
            _logger = logger;
            _userInterestsRepository = userInterestsRepository;
            _userProductsRepository = userProductsRepository;
            _datasetRepository = datasetRepository;
            _userDatasetRepository = userDatasetRepository;
            _oAuthLogic = oAuthLogic;
        }

        public async Task<ApiResult> GetCurrentUser(string userId, string culture)
        {
            var result = new ApiResult();

            try
            {
                var oAuthUser = _oAuthLogic.FindUserByEmail(userId);
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(culture);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

                if (oAuthUser == null)
                {
                    oAuthUser = new OAuthUser()
                    {
                        UserName = userId,
                        Email = userId,
                        LanguageId = (int)Enum.Parse(typeof(Enums.Languagues), culture)
                };
                    _oAuthLogic.RegisterUser(oAuthUser);
                }

                if (oAuthUser.CurrentCatalogueId == null)
                {
                    var userDataset = _userDatasetRepository.List(d => d.UserId == oAuthUser.Id).FirstOrDefault();
                    oAuthUser.CurrentCatalogueId = _datasetRepository.FindById(userDataset.DataSetId).CatalogueId;
                    await _usersRepository.UpdateAsync(oAuthUser);
                }

                UserDTO user = new UserDTO(oAuthUser);

                result.Data = user;
                result.StatusCode = HttpStatusCode.OK;

            }
            catch (Exception e)
            {
                _logger.LogException(e);
                result.StatusCode = HttpStatusCode.InternalServerError;
            }

            return result;
        }

        public async Task<ApiResult> UpdateUserAsync(UserDTO userData)
        {
            var result = new ApiResult();

            try
            {
                var user = await _usersRepository.FindByNameAsync(userData.Id);

                if (user != null)
                {

                    user.FirstName = string.IsNullOrWhiteSpace(userData.FirstName) ? user.FirstName : userData.FirstName;
                    user.LastName = string.IsNullOrWhiteSpace(userData.LastName) ? user.LastName : userData.LastName;
                    user.CountryId = userData.Country != null ? userData.Country.Id : user.CountryId.Value;
                    user.LanguageId = userData.Language != null ? userData.Language.Id : user.LanguageId.Value;
                    user.Institution = string.IsNullOrWhiteSpace(userData.Institution) ? user.Institution : userData.Institution;
                    user.InstitutionType = userData.InstitutionType != null ? EnumExtensions.GetValueFromDescription<Enums.InstitutionType>(userData.InstitutionType) : user.InstitutionType;

                    UpdateInterests(userData);
                    UpdateProducts(userData);

                    await _usersRepository.UpdateAsync(user);
                    result.StatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception e)
            {
                _logger.LogException(e);
                result.StatusCode = HttpStatusCode.InternalServerError;
            }

            return result;
        }

        public ApiResult GetLists()
        {
            var result = new ApiResult();

            try
            {
                var countries = GetCountries();
                var languages = GetLanguages();
                var institutionTypes = GetInstitutionTypes();
                var titles = GetTitles();
                var interests = GetInterests();
                var products = GetProducts();

                dynamic data = new ExpandoObject();
                data.countries = countries;
                data.languages = languages;
                data.institutionTypes = institutionTypes;
                data.titles = titles;
                data.interests = interests;
                data.products = products;

                result.Data = data;
                result.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                _logger.LogException(e);
                result.StatusCode = HttpStatusCode.InternalServerError;
            }

            return result;
        }

        private IEnumerable<CountryDTO> GetCountries()
        {
            return _countriesRepository.List().Select(c => new CountryDTO(c));
        }

        private IEnumerable<LanguageDTO> GetLanguages()
        {
            var langList = _languagesRepository.List().Select(l => new LanguageDTO(l));

            foreach (var l in langList)
            {
                switch (l.EncodedName)
                {
                    case "en":
                        l.LongName = Strings.Language_En;
                        break;
                    case "es":
                        l.LongName = Strings.Language_Es;
                        break;
                    case "pt":
                        l.LongName = Strings.Language_Pt;
                        break;
                    case "fr":
                        l.LongName = Strings.Language_Fr;
                        break;
                }
            }

            return langList;
        }

        private IEnumerable<string> GetInstitutionTypes()
        {
            return Enum.GetValues(typeof(Enums.InstitutionType)).Cast<Enums.InstitutionType>().Select(c => c.GetDescription());
        }

        private IEnumerable<string> GetTitles()
        {
            return Enum.GetValues(typeof(Enums.UserTitle)).Cast<Enums.UserTitle>().Select(t => t.GetDescription());
        }

        private IEnumerable<string> GetInterests()
        {
            return Enum.GetValues(typeof(Enums.UserInterests)).Cast<Enums.UserInterests>().Select(i => i.GetDescription());
        }

        private IEnumerable<string> GetProducts()
        {
            return Enum.GetValues(typeof(Enums.UserProducts)).Cast<Enums.UserProducts>().Select(i => i.GetDescription());
        }

        private void UpdateInterests(UserDTO user)
        {
            var currentInterests = _userInterestsRepository.List(i => i.UserId == user.Id).Select(i => i.Interest.GetDescription()).ToList();
            var interestsToDelete = _userInterestsRepository.List(i => i.UserId == user.Id).Where(i => !user.Interests.Contains(i.Interest.GetDescription())).ToList();
            var interestsToAdd = user.Interests.Where(i => !currentInterests.Contains(i)).Select(item => new UserInterest
            {
                Interest = EnumExtensions.GetValueFromDescription<Enums.UserInterests>(item),
                UserId = user.Id
            })
            .ToList();

            foreach (var item in interestsToDelete)
            {
                _userInterestsRepository.Delete(item.Id);
            }

            foreach (var item in interestsToAdd)
            {
                item.UserId = user.Id;
                _userInterestsRepository.Add(item);
            }
        }

        private void UpdateProducts(UserDTO user)
        {
            var currentProducts = _userProductsRepository.List(i => i.UserId == user.Id).Select(i => i.Product.GetDescription()).ToList();
            var productsToDelete = _userProductsRepository.List(i => i.UserId == user.Id).Where(i => !user.Products.Contains(i.Product.GetDescription())).ToList();
            var productsToAdd = user.Products.Where(i => !currentProducts.Contains(i)).Select(item => new UserProduct
            {
                Product = EnumExtensions.GetValueFromDescription<Enums.UserProducts>(item),
                UserId = user.Id
            })
            .ToList();

            foreach (var item in productsToDelete)
            {
                _userProductsRepository.Delete(item.Id);
            }

            foreach (var item in productsToAdd)
            {
                item.UserId = user.Id;
                _userProductsRepository.Add(item);
            }
        }

        public async Task<ApiResult> SetUserLanguageAsync(string userId, string langCode)
        {
            var result = new ApiResult();
            var user = await _usersRepository.FindByNameAsync(userId);
            var lang = _languagesRepository.List(x => x.EncodedName == langCode).FirstOrDefault();
            user.Language = lang;

            await _usersRepository.UpdateAsync(user);
            lang.Users = null;
            result.Data = null;
            result.StatusCode = HttpStatusCode.OK;

            return result;
        }

        #region OTHERS
        public ApiResult GetCountryById(int countryId)
        {
            var result = new ApiResult();

            try
            {
                var country = _countriesRepository.FindById(countryId);

                if (country == null)
                {
                    result.StatusCode = HttpStatusCode.NotFound;
                    return result;
                }

                result.Data = new CountryDTO(country);
                result.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                _logger.LogException(e);
                result.StatusCode = HttpStatusCode.InternalServerError;
            }

            return result;
        }

        public ApiResult AddCountry(CountryDTO countryData)
        {
            var result = new ApiResult();

            //try
            //{
            //    Country country = new Country(countryData);
            //    _countriesRepository.Add(country);
            //    result.Data = country;
            //    result.StatusCode = HttpStatusCode.OK;
            //}
            //catch (Exception e)
            //{
            //    _logger.LogException(e);
            //    result.StatusCode = HttpStatusCode.InternalServerError;
            //}

            return result;
        }

        public ApiResult DeleteCountry(int id)
        {
            try
            {
                _countriesRepository.Delete(id);
                return new ApiResult { StatusCode = HttpStatusCode.NoContent };
            }
            catch (Exception e)
            {
                _logger.LogException(e);
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };
            }
        }

        public ApiResult AddMunicipality(MunicipalityDTO municipalityData, int countryId)
        {
            var result = new ApiResult();

            try
            {
                //Municipality municipality = new Municipality(municipalityData);
                //Country country = _countriesRepository.FindById(countryId);
                //country.Municipalities.Add(municipality);

                //_countriesRepository.Update(country);
                //result.Data = municipality;
                //result.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception e)
            {
                _logger.LogException(e);
                result.StatusCode = HttpStatusCode.InternalServerError;
            }

            return result;
        }

        public ApiResult DeleteMunicipality(int municipalityId)
        {
            try
            {
                var municipality = _municipalitiesRepository.FindById(municipalityId);

                if (municipality == null)
                {
                    return new ApiResult { StatusCode = HttpStatusCode.NotFound };
                }

                _municipalitiesRepository.Delete(municipalityId);
                return new ApiResult { StatusCode = HttpStatusCode.NoContent };
            }
            catch (Exception e)
            {
                _logger.LogException(e);
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };
            }
        }
        #endregion
    }
}
