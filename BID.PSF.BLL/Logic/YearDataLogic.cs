﻿using System.Linq;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.DBModel;

namespace BID.PSF.BLL.Logic
{
    public class YearDataLogic : IYearDataLogic
    {
        private readonly ICollectionRepository<YearData> _yearDatasRepository;

        public YearDataLogic(ICollectionRepository<YearData> yearDatasRepository)
        {
            _yearDatasRepository = yearDatasRepository;
        }

        public YearData GetOrCreateYearData(int year, int dataSetId)
        {
            var yearData = _yearDatasRepository.List(y => y.DataSetId == dataSetId && y.Year == year).FirstOrDefault();
            if (yearData == null)
            {
                yearData = new YearData
                {
                    DataSetId = dataSetId,
                    Year = year,
                };

                _yearDatasRepository.Add(yearData);
            }
            return yearData;
        }
    }
}
