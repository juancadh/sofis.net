﻿using System;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.DBModel;
using System.Linq;
using System.Collections.Generic;

namespace BID.PSF.BLL.Logic
{
    public class DataSetsLogic : IDataSetsLogic
    {
        private readonly ICollectionRepository<DataSet> _dataSetsRepository;
        private readonly ICollectionRepository<UserDataSet> _userDataSetsRepository;
        public DataSetsLogic(ICollectionRepository<DataSet> dataSetsRepository, ICollectionRepository<UserDataSet> userDataSetsRepository)
        {
            _dataSetsRepository = dataSetsRepository;
            _userDataSetsRepository = userDataSetsRepository;
        }

        public DataSet GetOrCreateUserDataSet(string userId, int catalogueId, int? dataSetId = null)
        {
            var dataset = dataSetId.HasValue
                ? _dataSetsRepository.FindById(dataSetId.Value)
                : _dataSetsRepository.List(d => d.CreatedById == userId && d.CatalogueId == catalogueId).FirstOrDefault();

            if (dataset == null)
            {
                if (dataSetId.HasValue)
                    return null;

                dataset = new DataSet
                {
                    CatalogueId = catalogueId,
                    HistoricFrom = 1990,
                    HistoricTo = DateTime.Now.Year,
                    ProjectionFrom = DateTime.Now.Year + 1,
                    ProjectionTo = DateTime.Now.Year + 5
                };

                _dataSetsRepository.Add(dataset);

                var userDataSet = new UserDataSet
                {
                    DataSetId = dataset.Id,
                    UserId = userId
                };

                _userDataSetsRepository.Add(userDataSet);
            }

            return dataset;
        }

        public DataSet CreateUserDataSet(string userId, int catalogueId)
        {


            var dataset = new DataSet
            {
                CatalogueId = catalogueId,
                HistoricFrom = 1990,
                HistoricTo = DateTime.Now.Year,
                ProjectionFrom = DateTime.Now.Year + 1,
                ProjectionTo = DateTime.Now.Year + 5
            };

            _dataSetsRepository.Add(dataset);

            var userDataSet = new UserDataSet
            {
                DataSetId = dataset.Id,
                UserId = userId
            };

            _userDataSetsRepository.Add(userDataSet);


            return dataset;
        }

        public DataSet GetUserDataSet(string userId, int catalogueId, int? dataSetId = null)
        {
            return dataSetId.HasValue
                ? _dataSetsRepository.FindById(dataSetId.Value)
                : _dataSetsRepository.List(d => d.CreatedById == userId && d.CatalogueId == catalogueId).FirstOrDefault();
        }

        public DataSet CreateDataset(DataSet dataset)
        {
            dataset.Id = _dataSetsRepository.Add(dataset);
            return dataset;
        }
    }
}
