﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Enums;
using BID.PSF.Core.Helpers;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.ApiModel;
using BID.PSF.Model.DBModel;
using MathNet.Numerics.Distributions;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace BID.PSF.BLL.Logic
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable SuggestVarOrType_BuiltInTypes
    // ReSharper disable ConvertToConstant.Local
    public class ASDLogic : IASDLogic
    {
        private readonly ICollectionRepository<CatalogueColumn> _catalogueColumnsRepository;
        private readonly ICollectionRepository<CatalogueColumnValue> _catalogueColumnValuesRepository;
        private readonly ICollectionRepository<DataSet> _datasetRepository;
        private readonly IOAuthLogic _oAuthLogic;
        private readonly IDataSetsLogic _dataSetsLogic;

        public ASDLogic(ICollectionRepository<CatalogueColumnValue> catalogueColumnValuesRepository, ICollectionRepository<DataSet> datasetsRepository,
            ICollectionRepository<CatalogueColumn> catalogueColumnsRepository, IDataSetsLogic dataSetsLogic, IOAuthLogic oAuthLogic, ICollectionRepository<DataSet> datasetRepository)
        {
            _catalogueColumnValuesRepository = catalogueColumnValuesRepository;
            _catalogueColumnsRepository = catalogueColumnsRepository;
            _dataSetsLogic = dataSetsLogic;
            _oAuthLogic = oAuthLogic;
            _datasetRepository = datasetRepository;
        }

        #region Enfoque Estándar

        public ApiResult GetStandardAproximationData(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            SetHistoricYear(dataSet);
            int minYear = dataSet.HistoricFrom.Value;
            int maxYear = dataSet.HistoricTo.Value;

            // Mexico
            switch (dataSet.Catalogue.Type)
            {
                case Enums.CatalogueType.Mexico:
                    return GetMexicoInitialData(dataSet, minYear, maxYear);

                default:
                    return GetEstandarInitialData(dataSet, minYear, maxYear);

            }
        }

        private ApiResult GetMexicoInitialData(DataSet dataSet, int minYear, int maxYear)
        {
            var inputs = new StandardApproachMexicoInputs
            {
                Catalog = Enums.CatalogueType.Mexico,
                Debt = new MexicoDebtInput(),
                IncomeExpense = new MexicoIncomeExpenseInput()
            };

            var catalogColumnNames = new[]
            {
                Enums.DebtColumns.LocalRate.ToString(),
                Enums.IncomeColumns.TotalIncome.ToString(),
                Enums.ExpenseColumns.TotalExpense.ToString(),
                Enums.IncomeColumns.AvailableIncome.ToString(),
                Enums.DebtColumns.TotalDebt.ToString()
            };

            bool autocomplete = false;
            var datas = GetCatalogInitialData(dataSet, catalogColumnNames, minYear, maxYear, out IList<string> errorInternalNames);

            if (datas.Count == 0)
            {
                return new ApiResult
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Result = "NotData"
                };
            }
            if (errorInternalNames.ToList().Count() > 0)
            {
                return new ApiResult
                {
                    Message = GetMissingDataError(dataSet.CatalogueId, errorInternalNames.ToArray()),
                    StatusCode = HttpStatusCode.InternalServerError,
                    Result = "MissingData"
                };
            }

            inputs.Debt.LocalDebtInterestRate = GetColumnValueFiltered(datas[Enums.DebtColumns.LocalRate.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.TotalIncome = GetColumnValueFiltered(datas[Enums.IncomeColumns.TotalIncome.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.TotalExpenses = GetColumnValueFiltered(datas[Enums.ExpenseColumns.TotalExpense.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.AvailableIncome = GetColumnValueFiltered(datas[Enums.IncomeColumns.AvailableIncome.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.PublicDebtT = GetColumnValueFiltered(datas[Enums.DebtColumns.TotalDebt.ToString()], minYear, maxYear, autocomplete);

            inputs.Ting = inputs.IncomeExpense.TotalIncome.Any() ? inputs.IncomeExpense.TotalIncome.Last() : 0;
            inputs.Teg = inputs.IncomeExpense.TotalExpenses.Any() ? inputs.IncomeExpense.TotalExpenses.Last() : 0;
            inputs.PublicDebtOverIncomes = inputs.IncomeExpense.PublicDebtT.LastOrDefault() != 0
                                           ? inputs.IncomeExpense.AvailableIncome.LastOrDefault() != 0
                                             ? (inputs.IncomeExpense.PublicDebtT.LastOrDefault() / inputs.IncomeExpense.AvailableIncome.LastOrDefault()) * 100
                                             : 1
                                           : 1;

            inputs.Periods = maxYear - minYear + 1;
            inputs.Autocomplete = autocomplete;

            return new ApiResult { StatusCode = HttpStatusCode.OK, Data = inputs };
        }

        private ApiResult GetEstandarInitialData(DataSet dataSet, int minYear, int maxYear)
        {
            var inputs = new StandardApproachOtherInputs
            {
                Catalog = Enums.CatalogueType.Estandar,
                Debt = new OtherDebtInput(),
                IncomeExpense = new OtherIncomeExpenseInput()
            };

            var catalogColumnNames = new[]
            {
                Enums.DebtColumns.HistoricFixedRateForeignCurrency.ToString(),
                Enums.DebtColumns.ForeignRate.ToString(),
                Enums.DebtColumns.HistoricVariableRateForeignCurrency.ToString(),
                Enums.DebtColumns.HistoricFixedRateDomesticCurrency.ToString(),
                Enums.DebtColumns.LocalRate.ToString(),
                Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString(),
                Enums.IncomeColumns.AvailableIncome.ToString(),
                Enums.ExpenseColumns.TotalExpense.ToString(),
                Enums.DebtColumns.TotalDebt.ToString(),
                Enums.IncomeColumns.TotalIncome.ToString()
            };

            var optionalColumnNames = new[]
            {
                Enums.ExpenseColumns.Interests.ToString(),
                Enums.ExpenseColumns.FDEInterests.ToString()
            };

            var otherMinYear = minYear;
            var otherMaxYear = maxYear;
            bool autocomplete = false;
            IList<string> otherError;

            var datas = GetCatalogInitialData(dataSet, catalogColumnNames, minYear, maxYear, out IList<string> errorInternalNames);

            var optionalDatas = GetCatalogInitialData(dataSet, optionalColumnNames, otherMinYear, otherMaxYear, out otherError); //TODO VALIDAR SI NO HAY QUE RELLENAR

            var int1 = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.Interests.ToString()], minYear, maxYear);
            var int2 = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.FDEInterests.ToString()], minYear, maxYear);

            if (datas.Count == 0)
            {
                return new ApiResult
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Result = "NotData"
                };
            }
            if (errorInternalNames.ToList().Count() > 0)
            {
                return new ApiResult
                {
                    Message = GetMissingDataError(dataSet.CatalogueId, errorInternalNames.ToArray()),
                    StatusCode = HttpStatusCode.InternalServerError,
                    Result = "MissingData"
                };
            }

            inputs.Debt.ForeignFixedRateDebt = GetColumnValueFiltered(datas[Enums.DebtColumns.HistoricFixedRateForeignCurrency.ToString()], minYear, maxYear, autocomplete);

            inputs.Debt.ForeignInterestRate = GetColumnValueFiltered(datas[Enums.DebtColumns.ForeignRate.ToString()], minYear, maxYear, autocomplete);

            inputs.Debt.ForeignVariableRateDebt = GetColumnValueFiltered(datas[Enums.DebtColumns.HistoricVariableRateForeignCurrency.ToString()], minYear, maxYear, autocomplete);

            inputs.Debt.LocalFixedRateDebt = GetColumnValueFiltered(datas[Enums.DebtColumns.HistoricFixedRateDomesticCurrency.ToString()], minYear, maxYear, autocomplete);

            inputs.Debt.LocalInterestRate = GetColumnValueFiltered(datas[Enums.DebtColumns.LocalRate.ToString()], minYear, maxYear, autocomplete);

            inputs.Debt.LocalVariableRateDebt = GetColumnValueFiltered(datas[Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.AvailableIncome = GetColumnValueFiltered(datas[Enums.IncomeColumns.AvailableIncome.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.TotalExpenses = GetColumnValueFiltered(datas[Enums.ExpenseColumns.TotalExpense.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.TotalIncome = GetColumnValueFiltered(datas[Enums.IncomeColumns.TotalIncome.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.PublicDebtT = GetColumnValueFiltered(datas[Enums.DebtColumns.TotalDebt.ToString()], minYear, maxYear, autocomplete);

            inputs.IncomeExpense.InterestsG = int1;

            inputs.IncomeExpense.InterestsP = int2;

            inputs.Autocomplete = autocomplete;

            inputs.Ting = inputs.IncomeExpense.TotalIncome != null && inputs.IncomeExpense.TotalIncome.Any() ? inputs.IncomeExpense.TotalIncome.Last() : 0; //TODO Ver porqué viene en null
            inputs.Teg = inputs.IncomeExpense.TotalExpenses != null && inputs.IncomeExpense.TotalExpenses.Any() ? inputs.IncomeExpense.TotalExpenses.Last() : 0;
            inputs.Pint = GetColumnValue(dataSet, maxYear, Enums.ExpenseColumns.Interests.ToString()) +
                          GetColumnValue(dataSet, maxYear, Enums.ExpenseColumns.FDEInterests.ToString());
            inputs.PublicDebtOverIncomes = inputs.IncomeExpense.PublicDebtT.LastOrDefault() != 0
                                           ? inputs.IncomeExpense.AvailableIncome.LastOrDefault() != 0
                                             ? (inputs.IncomeExpense.PublicDebtT.LastOrDefault() / inputs.IncomeExpense.AvailableIncome.LastOrDefault()) * 100
                                             : 1
                                           : 1;

            inputs.Periods = maxYear - minYear;

            return new ApiResult { StatusCode = HttpStatusCode.OK, Data = inputs };
        }

        private IDictionary<string, Tuple<int, decimal>[]> GetCatalogInitialData(DataSet dataSet, string[] columnNames, int minYear, int maxYear, out IList<string> errorInternalNames)
        {
            var results = new Dictionary<string, Tuple<int, decimal>[]>();

            foreach (var cName in columnNames)
            {
                results.Add(cName, GetColumnValues(dataSet.CatalogueId, cName, dataSet.Id, minYear, maxYear));
            }

            errorInternalNames = new List<string>();

            if (results.Values.Min(v => v.Length) == 0)
            {
                errorInternalNames = results.Where(v => v.Value.Length == 0).Select(v => v.Key).ToList();
            }

            return results;
        }

        private string GetMissingDataError(int catalogueId, params string[] internalNames)
        {
            var displayNames = (from internalName in internalNames select _catalogueColumnsRepository.List(c => c.InternalName == internalName && c.CatalogueId == catalogueId).FirstOrDefault() into col where col != null select col.ColumnName).ToList();

            return string.Join(", ", displayNames);
        }

        private decimal[] GetColumnValueFiltered(Tuple<int, decimal>[] values, int minYear, int maxYear, bool autocomplete)
        {
            var array = new List<decimal>();

            for (var year = minYear; year <= maxYear; year++)
            {
                array.Add(values.Where(t => t.Item1 == year).Select(t => t.Item2).FirstOrDefault());
            }

            return array.ToArray();
        }

        private decimal[] GetFixedNonRequiredColumnValues(Tuple<int, decimal>[] values, int minYear, int maxYear)
        {
            var results = values.Where(t => t.Item1 >= minYear && t.Item1 <= maxYear).ToList();


            for (var year = minYear; year <= maxYear; year++)
            {
                if (results.All(y => y.Item1 != year))
                    results.Add(new Tuple<int, decimal>(year, 0));
            }

            return results.OrderBy(t => t.Item1).Select(t => t.Item2).ToArray();
        }

        /// <summary>
        /// </summary>
        /// <param name="catalogueId"></param>
        /// <param name="columnName"></param>
        /// <param name="dataSetId"></param>
        /// <param name="minYear"></param>
        /// <param name="maxYear"></param>
        /// <returns></returns>
        private Tuple<int, decimal>[] GetColumnValues(int catalogueId, string columnName, int dataSetId, int minYear, int maxYear)
        {
            var columnId = GetColumnId(catalogueId, columnName);

            var values = _catalogueColumnValuesRepository
                .List(v => v.CatalogueColumnId == columnId
                      && v.YearData.DataSetId == dataSetId
                      && v.YearData.Year >= minYear
                      && v.YearData.Year <= maxYear).OrderBy(v => v.YearData.Year);

            return values.Select(v => new Tuple<int, decimal>(v.YearData.Year, v.Value)).ToArray();
        }

        public async Task<ApiResult> StandardApproach(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            SetHistoricYear(dataSet);
            var result = new ApiResult();
            dynamic data = new ExpandoObject();

            // Cálculos previos
            var calculatedGrowthRate = CalculateGrowthRate(dataSet);
            var calculatedRealInterestRate = CalculateRealInterestRate(dataSet);
            var calculatedDebtOverIncome = CalculateDebtOverIncome(dataSet);
            var estimatedSurplus = CalculateEstimatedSurplus(dataSet);

            // Cálculo del Superávit
            var requiredSurplus = CalculateSurplus(calculatedGrowthRate, calculatedRealInterestRate, calculatedDebtOverIncome);

            // Cálculo del Ajuste Requerido 
            var adjustment = CalculateAdjustment(requiredSurplus, estimatedSurplus);

            // Cálculo de la Tasa de Inflación para el último año 
            var IPCColumnId = GetColumnId(dataSet.CatalogueId, Enums.MacroeconomicsColumns.ConsumerIndexPrice.ToString());
            var ipcValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == IPCColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToList();
            decimal inflationRate = 0;

            if (ipcValues.Count > 0)
            {
                var lastYear = ipcValues.Max(v => v.YearData.Year);
                inflationRate = CalculateInflationRate(dataSet, lastYear);
            }

            data.estimatedSurplus = estimatedSurplus;
            data.requiredSurplus = requiredSurplus;
            data.adjustment = adjustment;
            data.debtOverIncome = calculatedDebtOverIncome;
            data.realInterestRate = calculatedRealInterestRate;
            data.inflationRate = inflationRate;
            data.growthRate = calculatedGrowthRate;

            result.Data = data;
            result.StatusCode = HttpStatusCode.OK;

            return result;
        }

        public async Task<ApiResult> SensitivityAnalysis(string userId, int catalogueId, StandardApproachDTO standardData)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);
            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var result = new ApiResult();
            dynamic data = new ExpandoObject();

            var variation = CalculateStandardDeviation(dataSet);
            var growthValues = GetGrowthRateDeviationValues(dataSet, variation, standardData.IncomeDeviation, standardData.GrowthRate);
            var interestValues = GetRealInterestRateDeviationValues(dataSet, variation, standardData.RateDeviation, standardData.RealInterestRate);
            var surplusMatrix = new decimal[7, 7];
            var adjustmentMatrix = new decimal[7, 7];

            // Llenado de la matriz de superávit
            for (var i = 0; i < 7; i++)
            {
                for (var j = 0; j < 7; j++)
                {
                    surplusMatrix[i, j] = CalculateSurplus(growthValues[j], interestValues[i], standardData.DebtOverIncome);
                }
            }

            // Llenado de la matriz de ajustes
            for (var i = 0; i < 7; i++)
            {
                for (var j = 0; j < 7; j++)
                {
                    adjustmentMatrix[i, j] = CalculateAdjustment(surplusMatrix[i, j], standardData.EstimatedSurplus);
                }
            }

            data.growthValues = growthValues;
            data.interestValues = interestValues;
            data.surplusMatrix = surplusMatrix;
            data.adjustmentMatrix = adjustmentMatrix;

            // for testing purposes
            data.variation = variation;

            result.Data = data;
            result.StatusCode = HttpStatusCode.OK;

            return result;
        }

        #region Superávit y Ajuste

        private decimal CalculateSurplus(decimal growthRate, decimal realInterestRate, decimal debtOverIncome)
        {
            var surplus = (realInterestRate - growthRate) / (100 + growthRate) * debtOverIncome;
            return Math.Round(surplus, 2);
        }

        private decimal CalculateEstimatedSurplus(DataSet dataSet)
        {
            decimal estimatedSurplus = 0;

            var values = _catalogueColumnValuesRepository.List().ToList();

            if (values.Any())
            {
                var years = values.Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && !v.Projected);
                var year = years.Max(v => v.YearData.Year);

                // Superávit Estimado = Balance Primario / Ingreso Disponible del último año
                var balance = CalculateBalance(dataSet, year);
                var income = GetAvailableIncome(dataSet, year);

                if (income != 0)
                {
                    estimatedSurplus = Math.Round(balance / income * 100, 2);
                }
            }

            return estimatedSurplus;
        }

        private decimal CalculateAdjustment(decimal requiredSurplus, decimal estimatedSurplus)
        {
            return requiredSurplus - estimatedSurplus;
        }

        private decimal CalculateBalance(DataSet dataSet, int year)
        {

            //Balance primario en t = ingresoTotal(t) - gastoTotal(t) + intereses(t)
            //t = año; intereses = Incentivos derivados de la colaboracion fiscal - intereses derivados de financiamientos

            var totalIncome = GetTotalIncome(dataSet, year);
            var totalExpense = GetTotalExpense(dataSet, year);
            var totalInterests = GetTotalinterests(dataSet, year);

            return totalIncome - totalExpense + totalInterests;
        }

        private decimal CalculateBalanceWithDeviation(DataSet dataSet, int year, decimal deviation)
        {
            var totalIncome = GetTotalIncomeWithDeviation(dataSet, year, deviation);
            var totalExpense = GetTotalExpenseWithDeviation(dataSet, year);

            return totalIncome - totalExpense;
        }

        private decimal GetTotalIncomeWithDeviation(DataSet dataSet, int year, decimal deviation)
        {
            // (Transferencias Disponibles + Ingresos Tributarios + Ingresos no Tributarios) + Transferencias no Disponibles

            var availableTransferColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableTransfer.ToString());
            var availableTaxColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableTax.ToString());
            var availableContributionColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableContribution.ToString());
            var notAvailableTransferColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.NotAvailableTransfer.ToString());

            var availableTransferValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableTransferColumnId);
            var availableTaxValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableTaxColumnId);
            var availableContributionValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableContributionColumnId);
            var notAvailableTransferValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == notAvailableTransferColumnId);

            decimal availableTransfer = 0;
            decimal availableTax = 0;
            decimal availableContribution = 0;
            decimal notAvailableTransfer = 0;

            if (availableTransferValues != null)
            {
                var value = availableTransferValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    if (!value.Projected)
                    {
                        availableTransfer = value.Value;
                    }
                    else
                    {
                        var values = availableTransferValues.Where(v => !v.Projected).Select(t => t.Value).ToArray();
                        var standardDeviation = MathHelper.GetStandardDeviation(values);
                        availableTransfer = availableTransferValues.FirstOrDefault(v => v.YearData.Year == year).Value + deviation * standardDeviation;
                    }
                }
            }

            if (availableTaxValues != null)
            {
                var value = availableTaxValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    if (!value.Projected)
                    {
                        availableTax = value.Value;
                    }
                    else
                    {
                        var values = availableTaxValues.Where(v => !v.Projected).Select(t => t.Value).ToArray();
                        var standardDeviation = MathHelper.GetStandardDeviation(values);
                        availableTax = availableTaxValues.FirstOrDefault(v => v.YearData.Year == year).Value + deviation * standardDeviation;
                    }
                }
            }

            if (availableContributionValues != null)
            {
                var value = availableContributionValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    if (!value.Projected)
                    {
                        availableContribution = value.Value;
                    }
                    else
                    {
                        var values = availableContributionValues.Where(v => !v.Projected).Select(t => t.Value).ToArray();
                        var standardDeviation = MathHelper.GetStandardDeviation(values);
                        availableContribution = availableContributionValues.FirstOrDefault(v => v.YearData.Year == year).Value + deviation * standardDeviation;
                    }
                }
            }

            if (notAvailableTransferValues != null)
            {
                var value = notAvailableTransferValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    if (!value.Projected)
                    {
                        notAvailableTransfer = value.Value;
                    }
                    else
                    {
                        var values = notAvailableTransferValues.Where(v => !v.Projected).Select(t => t.Value).ToArray();
                        var standardDeviation = MathHelper.GetStandardDeviation(values);
                        notAvailableTransfer = notAvailableTransferValues.FirstOrDefault(v => v.YearData.Year == year).Value + deviation * standardDeviation;
                    }
                }
            }

            return availableTransfer + availableTax + availableContribution + notAvailableTransfer;
        }

        private decimal GetTotalExpenseWithDeviation(DataSet dataSet, int year)
        {
            // Nomina + Transferencias + Otros + Inversion

            var rosterColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.Roster.ToString());
            var transferColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.Transfer.ToString());
            var otherColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.Other.ToString());
            var investmentColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.Investment.ToString());

            var rosterValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == rosterColumnId);
            var transferValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == transferColumnId);
            var otherValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == otherColumnId);
            var investmentValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == investmentColumnId);

            decimal roster = 0;
            decimal transfer = 0;
            decimal other = 0;
            decimal investment = 0;

            if (rosterValues != null)
            {
                var value = rosterValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    roster = value.Value;
                }
            }

            if (transferValues != null)
            {
                var value = transferValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    transfer = value.Value;
                }
            }

            if (otherValues != null)
            {
                var value = otherValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    other = value.Value;
                }
            }

            if (investmentValues != null)
            {
                var value = investmentValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    investment = value.Value;
                }
            }

            return roster + transfer + other + investment;
        }

        private decimal GetAvailableIncomeWithDeviation(DataSet dataSet, int year, decimal deviation)
        {
            // Transferencias Disponibles + Ingresos Tributarios + Ingresos no Tributarios

            var availableTransferColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableTransfer.ToString());
            var availableTaxColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableTax.ToString());
            var availableContributionColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableContribution.ToString());

            var availableTransferValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableTransferColumnId);
            var availableTaxValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableTaxColumnId);
            var availableContributionValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableContributionColumnId);

            decimal availableTransfer = 0;
            decimal availableTax = 0;
            decimal availableContribution = 0;

            if (availableTransferValues != null)
            {
                var value = availableTransferValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    if (!value.Projected)
                    {
                        availableTransfer = value.Value;
                    }
                    else
                    {
                        var values = availableTransferValues.Where(v => !v.Projected).Select(t => t.Value).ToArray();
                        var standardDeviation = MathHelper.GetStandardDeviation(values);
                        availableTransfer = availableTransferValues.FirstOrDefault(v => v.YearData.Year == year).Value + deviation * standardDeviation;
                    }
                }
            }

            if (availableTaxValues != null)
            {
                var value = availableTaxValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    if (!value.Projected)
                    {
                        availableTax = value.Value;
                    }
                    else
                    {
                        var values = availableTaxValues.Where(v => !v.Projected).Select(t => t.Value).ToArray();
                        var standardDeviation = MathHelper.GetStandardDeviation(values);
                        availableTax = availableTaxValues.FirstOrDefault(v => v.YearData.Year == year).Value + deviation * standardDeviation;
                    }
                }
            }

            if (availableContributionValues != null)
            {
                var value = availableContributionValues.FirstOrDefault(v => v.YearData.Year == year);

                if (value != null)
                {
                    if (!value.Projected)
                    {
                        availableContribution = value.Value;
                    }
                    else
                    {
                        var values = availableContributionValues.Where(v => !v.Projected).Select(t => t.Value).ToArray();
                        var standardDeviation = MathHelper.GetStandardDeviation(values);
                        availableContribution = availableContributionValues.FirstOrDefault(v => v.YearData.Year == year).Value + deviation * standardDeviation;
                    }
                }
            }

            return availableTransfer + availableTax + availableContribution;
        }

        private decimal GetTotalIncome(DataSet dataSet, int year)
        {
            decimal income = 0;

            var totalIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.TotalIncome.ToString());
            var incomeValue = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == totalIncomeColumnId).FirstOrDefault(v => v.YearData.Year == year);

            if (incomeValue != null)
            {
                income = incomeValue.Value;
            }

            return income;
        }
        private decimal GetTotalinterests(DataSet dataset, int year)
        {
            decimal interests = 0;

            if (dataset.CatalogueId == 1)
            {
                //Mexico
                var GEDPColumnId = GetColumnId(dataset.CatalogueId, Enums.ExpenseColumns.PDLabeled.ToString());
                var GEDP = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == GEDPColumnId).FirstOrDefault(v => v.YearData.Year == year);
                var GNEDPColumnId = GetColumnId(dataset.CatalogueId, Enums.ExpenseColumns.PDNLabeled.ToString());
                var GNEDP = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == GNEDPColumnId).FirstOrDefault(v => v.YearData.Year == year);
                if (GEDP != null && GNEDP != null)
                {
                    interests = GEDP.Value + GNEDP.Value;
                }
            }
            else
            {
                //CATALOGO STANDARD
                //Columnas G (Intereses-GastoPredeterminado) y P(Intereses - Gasto de libre determinacion) del catalogo de otros
                var GPIColumnId = GetColumnId(dataset.CatalogueId, Enums.ExpenseColumns.Interests.ToString());
                var ISC = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == GPIColumnId).FirstOrDefault(v => v.YearData.Year == year);
                return ISC != null ? ISC.Value : 0;
            }

            return interests;
        }

        private decimal GetTotalExpense(DataSet dataSet, int year)
        {
            decimal expense = 0;

            var totalExpenseColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.TotalExpense.ToString());
            var expenseValue = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == totalExpenseColumnId).FirstOrDefault(v => v.YearData.Year == year);

            if (expenseValue != null)
            {
                //var lastYear = expenseValues.Max(v => v.YearData.Year);
                //var values = expenseValues.Where(v => v.YearData.Year == lastYear).Select(v => v.Value);

                expense = expenseValue.Value;

            }

            return expense;
        }

        private decimal GetAvailableIncome(DataSet dataSet, int year)
        {
            decimal availableIncome = 0;

            var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
            var incomeValue = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).FirstOrDefault(v => v.YearData.Year == year);

            if (incomeValue != null)
                availableIncome = incomeValue.Value;

            return availableIncome;
        }

        #endregion

        #region Tasa de Crecimiento

        private decimal CalculateGrowthRate(DataSet dataSet)
        {
            var values = GetGrowthRateValues(dataSet);
            decimal rate = 0;

            if (values != null)
            {
                rate = Math.Round(MathHelper.GetImplicitAverage(values), 2);
            }

            return rate;
        }

        private decimal[] GetGrowthRateValues(DataSet dataSet)
        {
            var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());

            var incomes = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToArray();
            decimal[] values = null;
            values = new decimal[incomes.Length - 1];

            if (incomes.Length > 1)
            {
                values = new decimal[incomes.Length - 1];

                for (var i = 1; i < incomes.Length; i++)
                {
                    var actualIPC = GetConsumerPriceIndex(dataSet, incomes[i].YearData.Year);
                    var previousIPC = GetConsumerPriceIndex(dataSet, incomes[i - 1].YearData.Year);

                    if (actualIPC != 0)
                    {
                        values[i - 1] = incomes[i].Value / incomes[i - 1].Value * previousIPC / actualIPC;
                    }
                    else
                    {
                        values[i - 1] = 0;
                    }
                }
            }

            return values;
        }

        private decimal GetConsumerPriceIndex(DataSet dataSet, int year)
        {
            decimal ipc = 0;

            var ipcColumnId = GetColumnId(dataSet.CatalogueId, Enums.MacroeconomicsColumns.ConsumerIndexPrice.ToString());
            var macroeconomicValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == ipcColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToList();

            if (macroeconomicValues.Count > 0)
            {
                var values = macroeconomicValues.Where(v => v.YearData.Year == year).Select(v => v.Value);
                ipc = values.FirstOrDefault();
            }

            return ipc;
        }

        #endregion

        #region Tasa de Interés Real

        private decimal CalculateRealInterestRate(DataSet dataSet)
        {
            // Tasa de Interés Real = [Alfa * Tasa de Interés Local] + [(1 - Alfa) * Tasa de Interés Extranjera]
            var year = 0;
            var values = _catalogueColumnValuesRepository.List().Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && !v.Projected).ToList();
            if (values.Any())
                year = values.Max(v => v.YearData.Year);

            var alpha = CalculateAlpha(dataSet, year);
            var tasaInteresLocal = CalculateInterestRate(dataSet, true);
            decimal tasaInteresExtrangera = 0;

            if (dataSet.CatalogueId >= 2)
                tasaInteresExtrangera = CalculateInterestRate(dataSet, false);

            return Math.Round(alpha * tasaInteresLocal + (1 - alpha) * tasaInteresExtrangera, 2);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="local">REQUIRED: true: Calculate local false: Calculate foreign</param>
        /// <returns></returns>
        private decimal CalculateInterestRate(DataSet dataSet, bool local)
        {
            // Tasa Interés Local = Promedio(deuda histórica local)
            decimal[] historicalDebtValues = null;
            if (local)
            {
                historicalDebtValues = GetHistoricDebtValues(dataSet, Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString(), Enums.DebtColumns.HistoricFixedRateDomesticCurrency.ToString());
            }
            else
            {
                historicalDebtValues = GetHistoricDebtValues(dataSet, Enums.DebtColumns.HistoricVariableRateForeignCurrency.ToString(), Enums.DebtColumns.HistoricFixedRateForeignCurrency.ToString());
            }

            decimal rate = 0;
            if (historicalDebtValues != null)
            {
                rate = Math.Round(MathHelper.GetAverage2(historicalDebtValues), 2);
            }
            return rate;

        }

        private decimal[] GetHistoricDebtValues(DataSet dataSet, string HistoricalVariableRateCurrency, string HistoricalfixedRateCurrency)
        {
            // Para cada año ingresado t
            // v[i] = (w[i] + y[i]) / (e[i] + g[i])
            // e = Deuda Histórica Tasa Variable ML en t
            // g = Deuda Histórica Tasa Fija en t
            // w = g en t - g en t-1
            // y = e en t - e en t-1

            var hvrDCColId = GetColumnId(dataSet.CatalogueId, HistoricalVariableRateCurrency);
            var hfrDCColId = GetColumnId(dataSet.CatalogueId, HistoricalfixedRateCurrency);

            var tasaVariable = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == hvrDCColId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).OrderBy(v => v.YearData.Year).Select(v => v.Value).ToList();
            var tasaFija = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == hfrDCColId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).OrderBy(v => v.YearData.Year).Select(v => v.Value).ToList();

            var cantidadAños = Math.Max(tasaVariable.Count, tasaFija.Count);

            tasaVariable.Insert(0, 0);
            tasaFija.Insert(0, 0);
            var values = new decimal[cantidadAños];

            for (var i = 1; i <= cantidadAños; i++)
            {
                if (i >= tasaVariable.Count)
                {
                    tasaVariable.Add(0);
                }

                if (i >= tasaFija.Count)
                {
                    tasaFija.Add(0);
                }

                var e = tasaVariable[i];
                var g = tasaFija[i];
                var w = tasaFija[i] - tasaFija[i - 1];
                var y = tasaVariable[i] - tasaVariable[i - 1];

                try
                {
                    values[i - 1] = (w + y) / (e + g);
                }
                catch (Exception)
                {
                    //in case of a math error
                }
            }

            return values;
        }

        private decimal[] GetInflationRateValues(DataSet dataSet)
        {
            decimal[] values = null;

            var IPCColumnId = GetColumnId(dataSet.CatalogueId, Enums.MacroeconomicsColumns.ConsumerIndexPrice.ToString());
            var macroeconomicValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == IPCColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToArray();

            if (macroeconomicValues.Length > 1)
            {
                values = new decimal[macroeconomicValues.Length - 1];

                for (var i = 1; i < macroeconomicValues.Length; i++)
                {
                    var actualIPC = macroeconomicValues[i].Value;
                    var previousIPC = macroeconomicValues[i - 1].Value;

                    // Se evita la divicion por 0, en ese caso se deja solo el valor de actualIPC
                    values[i - 1] = actualIPC / previousIPC > 1 ? previousIPC - 1 : 1;
                }
            }

            return values;
        }

        private decimal[] GetForeignInflationRateValues(DataSet dataSet)
        {
            var IPCColumnId = GetColumnId(dataSet.CatalogueId, Enums.MacroeconomicsColumns.ConsumerIndexPrice.ToString());

            var macroeconomicValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == IPCColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToArray();

            var values = new decimal[macroeconomicValues.Length - 1];

            for (var i = 1; i < macroeconomicValues.Length; i++)
            {
                var actualIPC = macroeconomicValues[i].Value;
                var previousIPC = macroeconomicValues[i - 1].Value;

                // TODO: Check for DivideByZeroException
                values[i - 1] = actualIPC / previousIPC - 1;
            }

            return values;
        }

        private decimal CalculateAlpha(DataSet dataSet, int year)
        {
            // Alfa = (E + G + I + K) / ((E + G + I + K) + (F + H + J + L) * D)
            // Alfa = (Deuda histórica con tasa variable ML + deuda histórica con tasa fija ML + deuda nueva con tasa variable ML + deuda nueva con tasa fija ML) / 
            //        ((Deuda histórica con tasa variable ML + deuda histórica con tasa fija ML + deuda nueva con tasa variable ML + deuda nueva con tasa fija ML) +
            //        (Deuda histórica con tasa variable ME + deuda histórica con tasa fija ME + deuda nueva con tasa variable ME + deuda nueva con tasa fija ME) * Tasa cambio local)

            var debtValues = _catalogueColumnValuesRepository.List(v => v.YearData.Year == year).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToList();
            var macroValues = _catalogueColumnValuesRepository.List(v => v.YearData.Year == year).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToList();

            var hvrDCColId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString());
            var hfrDCColId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricFixedRateDomesticCurrency.ToString());

            var hvrDC = debtValues.SingleOrDefault(v => v.CatalogueColumnId == hvrDCColId) != null ? debtValues.Single(v => v.CatalogueColumnId == hvrDCColId).Value : 0;
            var hfrDC = debtValues.SingleOrDefault(v => v.CatalogueColumnId == hfrDCColId) != null ? debtValues.Single(v => v.CatalogueColumnId == hfrDCColId).Value : 0;

            decimal alpha = 0;
            try
            {
                // CATÁLOGO ESTÁNDAR
                if (dataSet.CatalogueId >= 2)
                {
                    var hvrFCColId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricVariableRateForeignCurrency.ToString());
                    var hfrFCColId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricFixedRateForeignCurrency.ToString());
                    var localChangeRateColId = GetColumnId(dataSet.CatalogueId, Enums.MacroeconomicsColumns.LocalChangeRate.ToString());

                    var hvrFC = debtValues.SingleOrDefault(v => v.CatalogueColumnId == hvrFCColId) != null ? debtValues.Single(v => v.CatalogueColumnId == hvrFCColId).Value : 0;
                    var hfrFC = debtValues.SingleOrDefault(v => v.CatalogueColumnId == hfrFCColId) != null ? debtValues.Single(v => v.CatalogueColumnId == hfrFCColId).Value : 0;
                    var localChangeRate = macroValues.SingleOrDefault(v => v.CatalogueColumnId == localChangeRateColId) != null ? macroValues.Single(v => v.CatalogueColumnId == localChangeRateColId).Value : 0;

                    var den = hvrDC + hfrDC + (hvrFC + hfrFC) * localChangeRate;
                    if (den != 0)
                        alpha = (hvrDC + hfrDC) / den;
                }
                // CATÁLOGO MÉXICO
                else
                {
                    var den = hvrDC + hfrDC;
                    if (den != 0)
                        alpha = (hvrDC + hfrDC) / den;
                }
            }
            catch (Exception e)
            {
                //in case or a math error
                //Console.WriteLine(e);
            }

            return alpha;
        }

        private decimal CalculateInflationRate(DataSet dataSet, int year)
        {
            // Para el año t => (Índice de Precios al Consumidor en t / Índice de Precios al Consumidor en t-1) - 1
            var IPCColumnId = GetColumnId(dataSet.CatalogueId, Enums.MacroeconomicsColumns.ConsumerIndexPrice.ToString());
            var macroeconomicValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == IPCColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToList();
            var actualIPC = macroeconomicValues.Where(v => v.YearData.Year == year).Select(v => v.Value).FirstOrDefault();
            var previousIPC = macroeconomicValues.Where(v => v.YearData.Year == year - 1).Select(v => v.Value).FirstOrDefault();

            decimal rate = 0;
            if (previousIPC != 0)
            {
                rate = Math.Round((actualIPC / previousIPC - 1) * 100, 2);
            }

            return rate;
        }

        public decimal CalculateCurrencyDepreciation(DataSet dataset, int year)
        {
            //Depreciacion de la moneda = (Tipo de cambio en t / tipo de cambio en t-1) - 1
            decimal currencydep = 0;
            var CdColId = GetColumnId(dataset.CatalogueId, Enums.MacroeconomicsColumns.LocalChangeRate.ToString());
            var macroValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == CdColId).Where(v => dataset.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToList();
            var actualLC = macroValues.Where(v => v.YearData.Year == year).Select(v => v.Value).FirstOrDefault();
            var prevLC = macroValues.Where(v => v.YearData.Year == year - 1).Select(v => v.Value).FirstOrDefault();

            if (macroValues.Any())
            {
                if (prevLC != 0)
                {
                    currencydep = Math.Round((actualLC / prevLC) - 1);
                }
            }

            return currencydep;
        }


        #endregion

        #region Deuda sobre Ingreso

        private decimal CalculateDebtOverIncome(DataSet dataSet)
        {
            decimal debtOverIncome = 0;

            var year = 0;
            var values = _catalogueColumnValuesRepository.List().Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && !v.Projected).ToList();
            if (values.Any())
                year = values.Max(v => v.YearData.Year);

            IDictionary<int, decimal> yearDebts = new Dictionary<int, decimal>();
            var debt = GetDebt(dataSet, year, null, ref yearDebts);
            var income = GetAvailableIncome(dataSet, year);

            if (income != 0)
            {
                debtOverIncome = Math.Round(debt / income * 100, 2);
            }

            return debtOverIncome;
        }

        private decimal GetOtrosDebt(DataSet dataSet, int year, int? lastYear, ref IDictionary<int, decimal> yearDebts)
        {
            /*test*/
            //Dt = (at * ( 1 + rtml) + (1 - at) * (1 + rtme) * ( 1 + Dte )) * Dpt - Ft
            /*
             at -> porcentaje d ela deuda en moneda local con respecto a la deuda total
             rtml -> tasa de interes nominal
             rtme -> tasa de interes nominal extrangera
             dte -> depreciacion de la moneda
             ft -> balance primario
             Dtp -> deuda previa 
             */
            var actualYearData = _catalogueColumnValuesRepository.List().Where(v => v.YearData.Year == year).ToList();
            var prevYearData = _catalogueColumnValuesRepository.List().Where(v => v.YearData.Year == year - 1).ToList();
            var totalDebtColId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.TotalDebt.ToString());
            var totalDebt = actualYearData.FirstOrDefault(v => v.CatalogueColumnId == totalDebtColId).Value;

            var localDebtColId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString());
            var localDebt = actualYearData.FirstOrDefault(v => v.CatalogueColumnId == localDebtColId).Value;

            var dtColId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.TotalDebt.ToString());
            var dt = actualYearData.FirstOrDefault(v => v.CatalogueColumnId == dtColId).Value;

            if (year <= lastYear || lastYear == null)
            {
                if (yearDebts.Keys.Contains(year))
                    yearDebts[year] = dt;
                else
                    yearDebts.Add(year, dt);

                return dt;
            }

            decimal at = localDebt / totalDebt * 100;
            decimal rtml = CalculateInterestRate(dataSet, true); // local
            decimal rtme = CalculateInterestRate(dataSet, false); // foreign
            decimal dte = CalculateCurrencyDepreciation(dataSet, year);
            decimal ft = CalculateBalance(dataSet, year);
            decimal dtp = prevYearData.FirstOrDefault(v => v.CatalogueColumnId == dtColId).Value;

            var newDt = Math.Round(((at * (1 + rtml) + (1 - at) * (1 + rtme) * (1 + dte)) * dtp - ft), 2);

            if (yearDebts.Keys.Contains(year))
                yearDebts[year] = newDt;
            else
                yearDebts.Add(year, newDt);

            return newDt;


        }

        private decimal GetMexDebt(DataSet dataSet, int year, List<CatalogueColumnValue> debtValues, int? lastYear, ref IDictionary<int, decimal> yearDebts)
        {
            var debtprevyearValues = _catalogueColumnValuesRepository.List().Where(v => v.YearData.Year == year - 1).ToList();

            //Catalogo Mexico: DeudaEnAño(T) = DeudaEnAño(T-1) + IngresoTotal(t) - GastoTotal(t)
            var dtColId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.TotalDebt.ToString());

            if (year <= lastYear || lastYear == null)
            {
                var dt = debtValues.FirstOrDefault(v => v.CatalogueColumnId == dtColId)?.Value;
                if (!dt.HasValue)
                    return 0;

                if (yearDebts.Keys.Contains(year))
                    yearDebts[year] = dt.Value;
                else
                    yearDebts.Add(year, dt.Value);

                return dt.Value;
            }

            var dtp = debtprevyearValues.FirstOrDefault(v => v.CatalogueColumnId == dtColId) != null ? yearDebts[year - 1] : 0;
            var it = GetTotalIncome(dataSet, year);
            var gt = GetTotalExpense(dataSet, year);

            var newDt = Math.Round(dtp + it - gt);

            if (yearDebts.Keys.Contains(year))
                yearDebts[year] = newDt;
            else
                yearDebts.Add(year, newDt);

            return newDt;
        }

        /// <summary>
        ///  Calculos de deuda para ambos catalogos
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="year"></param>
        /// <param name="lastYear"></param>
        /// <param name="yearDebts">Diccionario con los calculos para cada año, evita que se repitan los calculos</param>
        /// <returns></returns>
        private decimal GetDebt(DataSet dataSet, int year, int? lastYear, ref IDictionary<int, decimal> yearDebts)
        {
            var debtValues = _catalogueColumnValuesRepository.List().Where(v => v.YearData.Year == year).ToList();
            if (!debtValues.Any())
                return 0;
            return dataSet.CatalogueId >= 2 ? GetOtrosDebt(dataSet, year, lastYear, ref yearDebts) : GetMexDebt(dataSet, year, debtValues, lastYear, ref yearDebts);

        }

        #endregion

        #region Análisis de Sensibilidad

        private decimal CalculateStandardDeviation(DataSet dataSet)
        {
            var values = GetGrowthRateValues(dataSet);
            return MathHelper.GetStandardDeviation(values);
        }

        private decimal[] GetGrowthRateDeviationValues(DataSet dataSet, decimal variation, decimal standardDeviation, decimal? calculatedGrowthRate = null)
        {
            decimal growthRate;
            if (calculatedGrowthRate == null)
                growthRate = CalculateGrowthRate(dataSet);
            else
                growthRate = calculatedGrowthRate.Value;

            var values = new decimal[7];
            values[3] = growthRate;

            for (var i = 3; i > 0; i--)
            {
                values[i - 1] = Math.Round(values[i] - variation * standardDeviation * 100, 2);
            }

            for (var i = 3; i < values.Length - 1; i++)
            {
                values[i + 1] = Math.Round(values[i] + variation * standardDeviation * 100, 2);
            }

            return values;
        }

        private decimal[] GetRealInterestRateDeviationValues(DataSet dataSet, decimal variation, decimal standardDeviation, decimal? calculatedRealInterestRate = null)
        {
            decimal realInterestRate;
            if (calculatedRealInterestRate == null)
                realInterestRate = CalculateRealInterestRate(dataSet);
            else
                realInterestRate = calculatedRealInterestRate.Value;

            var values = new decimal[7];
            values[3] = realInterestRate;

            for (var i = 3; i > 0; i--)
            {
                values[i - 1] = Math.Round(values[i] + variation * standardDeviation * 100, 2);
            }

            for (var i = 3; i < values.Length - 1; i++)
            {
                values[i + 1] = Math.Round(values[i] - variation * standardDeviation * 100, 2);
            }

            return values;
        }

        #endregion

        #endregion

        #region Dinámica de la Deuda

        public ApiResult GetDynamicYears(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var result = new ApiResult();
            dynamic data = new ExpandoObject();

            int lastYear;
            var values = _catalogueColumnValuesRepository.List().Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && !v.Projected);
            if (values.Any())
                lastYear = values.Max(v => v.YearData.Year);
            else
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var maxYear = lastYear + 5;
            var minYear = lastYear - 2;

            var years = new List<int>();
            for (var i = minYear; i <= maxYear; i++)
            {
                years.Add(i);
            }

            data.Years = years;

            result.Data = data;
            result.StatusCode = HttpStatusCode.OK;

            return result;
        }

        public ApiResult DynamicsOfDebt(string userId, int catalogueId, DynamicsOfDebtDTO dynamicsData)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var result = new ApiResult();
            dynamic data = new ExpandoObject();

            var balanceTable = new Dictionary<string, SortedDictionary<int, decimal>>();
            var debtTable = new Dictionary<string, SortedDictionary<int, decimal>>();

            var lastYear = _catalogueColumnValuesRepository.List().Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && !v.Projected).Max(v => v.YearData.Year);
            var maxYear = lastYear + 5;

            var dictTasa = new Dictionary<string, decimal>();
            var dictIngreso = new Dictionary<string, decimal>();
            var dictExtranjera = new Dictionary<string, decimal>();
            var dictLocal = new Dictionary<string, decimal>();

            //  Escenario Base: Desviación Estándar = 0
            // Balance
            var scenarioTable = new SortedDictionary<int, decimal>();
            IDictionary<int, decimal> yearDebt = new Dictionary<int, decimal>();

            for (var year = lastYear - 2; year <= maxYear; year++)
            {
                var primaryBalance = CalculateBalance(dataSet, year);
                var availableIncome = GetAvailableIncome(dataSet, year);

                decimal scenario = 0;

                if (availableIncome != 0)
                {
                    scenario = Math.Round(primaryBalance / availableIncome * 100, 1);
                }

                scenarioTable.Add(year, scenario);
            }

            balanceTable.Add("Escenario Base", scenarioTable);

            // Deuda
            scenarioTable = new SortedDictionary<int, decimal>();
            yearDebt = new Dictionary<int, decimal>();
            for (var year = lastYear - 2; year <= maxYear; year++)
            {
                var debt = GetDebt(dataSet, year, lastYear, ref yearDebt);
                var availableIncome = GetAvailableIncome(dataSet, year);

                decimal scenario = 0;

                if (availableIncome != 0)
                {
                    scenario = Math.Round(debt / availableIncome * 100, 1);
                }

                scenarioTable.Add(year, scenario);
            }

            debtTable.Add("Escenario Base", scenarioTable);

            if (dynamicsData != null)
            {
                // Tipo de Cambio
                var shockTasa = dynamicsData.Shocks.SingleOrDefault(s => s.Shock == "Tipo de Cambio");
                if (shockTasa != null)
                {
                    // Balance
                    var changeRateTable = new SortedDictionary<int, decimal>();
                    for (var year = lastYear - 2; year <= maxYear; year++)
                    {
                        var primaryBalance = CalculateBalance(dataSet, year);
                        var availableIncome = GetAvailableIncome(dataSet, year);

                        decimal scenario = 0;

                        if (availableIncome != 0)
                        {
                            scenario = Math.Round(primaryBalance / availableIncome * 100, 1);
                        }

                        changeRateTable.Add(year, scenario);
                    }

                    balanceTable.Add("Tipo de Cambio", changeRateTable);

                    // Deuda
                    changeRateTable = new SortedDictionary<int, decimal>();
                    yearDebt = new Dictionary<int, decimal>();
                    for (var year = lastYear - 2; year <= maxYear; year++)
                    {
                        var debt = GetDebt(dataSet, year, lastYear, ref yearDebt);
                        var availableIncome = GetAvailableIncome(dataSet, year);

                        decimal scenario = 0;

                        if (availableIncome != 0)
                        {
                            scenario = Math.Round(debt / availableIncome * 100, 1);
                        }

                        changeRateTable.Add(year, scenario);
                    }

                    debtTable.Add("Tipo de Cambio", changeRateTable);

                    // Dev Std y Valores
                    var desv = shockTasa?.StandardDeviation ?? 0;

                    decimal devStdTasa = 0;
                    decimal valorTasa = 0;

                    // CATÁLOGO ESTÁNDAR
                    if (dataSet.CatalogueId >= 2)
                    {
                        devStdTasa = CalculateDevStd(dataSet, Enums.MacroeconomicsColumns.LocalChangeRate.ToString());
                        valorTasa = CalculateValues(dataSet, Enums.MacroeconomicsColumns.LocalChangeRate.ToString(), devStdTasa, desv);
                    }

                    dictTasa.Add("devStd", devStdTasa);
                    dictTasa.Add("valor", valorTasa);
                }

                // Ingreso Total
                var shockIngreso = dynamicsData.Shocks.SingleOrDefault(s => s.Shock == "Ingreso Total");
                if (shockIngreso != null)
                {
                    // Balance
                    var totalIncomeTable = new SortedDictionary<int, decimal>();
                    for (var year = lastYear - 2; year <= maxYear; year++)
                    {
                        var shockDeviation = dynamicsData.Shocks.SingleOrDefault(s => s.Shock == "Ingreso Total");
                        decimal deviation = 0;
                        if (shockDeviation != null)
                        {
                            deviation = shockDeviation.StandardDeviation;
                        }

                        var primaryBalance = CalculateBalanceWithDeviation(dataSet, year, deviation);
                        var availableIncome = GetAvailableIncomeWithDeviation(dataSet, year, deviation);

                        decimal scenario = 0;

                        if (availableIncome != 0)
                        {
                            scenario = Math.Round(primaryBalance / availableIncome * 100, 1);
                        }

                        totalIncomeTable.Add(year, scenario);
                    }

                    balanceTable.Add("Ingreso Total", totalIncomeTable);

                    // Deuda
                    totalIncomeTable = new SortedDictionary<int, decimal>();
                    yearDebt = new Dictionary<int, decimal>();
                    for (var year = lastYear - 2; year <= maxYear; year++)
                    {
                        var shockDeviation = dynamicsData.Shocks.SingleOrDefault(s => s.Shock == "Ingreso Total");
                        decimal deviation = 0;
                        if (shockDeviation != null)
                        {
                            deviation = shockDeviation.StandardDeviation;
                        }

                        var debt = GetDebt(dataSet, year, lastYear, ref yearDebt);
                        var availableIncome = GetAvailableIncomeWithDeviation(dataSet, year, deviation);

                        decimal scenario = 0;

                        if (availableIncome != 0)
                        {
                            scenario = Math.Round(debt / availableIncome * 100, 1);
                        }

                        totalIncomeTable.Add(year, scenario);
                    }

                    debtTable.Add("Ingreso Total", totalIncomeTable);

                    // Dev Std y Valores
                    var desv = shockIngreso != null ? shockIngreso.StandardDeviation : 0;
                    var devStdIngreso = CalculateDevStd(dataSet, Enums.IncomeColumns.TotalIncome.ToString());
                    var valorIngreso = CalculateValues(dataSet, Enums.IncomeColumns.TotalIncome.ToString(), devStdIngreso, desv);

                    dictIngreso.Add("devStd", devStdIngreso);
                    dictIngreso.Add("valor", valorIngreso);
                }

                // Tasa de interés de deuda en moneda extranjera
                var shockExtranjera = dynamicsData.Shocks.SingleOrDefault(s => s.Shock == "Tasa de Interes de Deuda en Moneda Extranjera");
                if (shockExtranjera != null)
                {
                    // Balance
                    var foreignInterestTable = new SortedDictionary<int, decimal>();
                    for (var year = lastYear - 2; year <= maxYear; year++)
                    {
                        var primaryBalance = CalculateBalance(dataSet, year);
                        var availableIncome = GetAvailableIncome(dataSet, year);

                        decimal scenario = 0;

                        if (availableIncome != 0)
                        {
                            scenario = Math.Round(primaryBalance / availableIncome * 100, 1);
                        }

                        foreignInterestTable.Add(year, scenario);
                    }

                    balanceTable.Add("Tasa de Interés de deuda en moneda extranjera", foreignInterestTable);

                    // Deuda
                    foreignInterestTable = new SortedDictionary<int, decimal>();
                    yearDebt = new Dictionary<int, decimal>();
                    for (var year = lastYear - 2; year <= maxYear; year++)
                    {
                        var debt = GetDebt(dataSet, year, lastYear, ref yearDebt);
                        var availableIncome = GetAvailableIncome(dataSet, year);

                        decimal scenario = 0;

                        if (availableIncome != 0)
                        {
                            scenario = Math.Round(debt / availableIncome * 100, 1);
                        }

                        foreignInterestTable.Add(year, scenario);
                    }

                    debtTable.Add("Tasa de Interés de deuda en moneda extranjera", foreignInterestTable);

                    // Dev Std y Valores
                    var desv = shockExtranjera?.StandardDeviation ?? 0;

                    decimal devStdExtranjera = 0;
                    decimal valorExtranjera = 0;

                    // CATÁLOGO ESTÁNDAR
                    if (dataSet.CatalogueId >= 2)
                    {
                        devStdExtranjera = CalculateDevStd(dataSet, Enums.DebtColumns.ForeignRate.ToString());
                        valorExtranjera = CalculateValues(dataSet, Enums.DebtColumns.ForeignRate.ToString(), devStdExtranjera, desv);
                    }

                    dictExtranjera.Add("devStd", devStdExtranjera);
                    dictExtranjera.Add("valor", valorExtranjera);
                }

                // Tasa de interés de deuda en moneda local
                var shockLocal = dynamicsData.Shocks.SingleOrDefault(s => s.Shock == "Tasa de Interes de Deuda en Moneda Local");
                if (shockLocal != null)
                {
                    // Balance
                    var localInterestTable = new SortedDictionary<int, decimal>();
                    for (var year = lastYear - 2; year <= maxYear; year++)
                    {
                        var primaryBalance = CalculateBalance(dataSet, year);
                        var availableIncome = GetAvailableIncome(dataSet, year);

                        decimal scenario = 0;

                        if (availableIncome != 0)
                        {
                            scenario = Math.Round(primaryBalance / availableIncome * 100, 1);
                        }

                        localInterestTable.Add(year, scenario);
                    }

                    balanceTable.Add("Tasa de Interés de deuda en moneda local", localInterestTable);

                    // Deuda
                    localInterestTable = new SortedDictionary<int, decimal>();
                    yearDebt = new Dictionary<int, decimal>();
                    for (var year = lastYear - 2; year <= maxYear; year++)
                    {
                        var debt = GetDebt(dataSet, year, lastYear, ref yearDebt);
                        var availableIncome = GetAvailableIncome(dataSet, year);

                        decimal scenario = 0;

                        if (availableIncome != 0)
                        {
                            scenario = Math.Round(debt / availableIncome * 100, 1);
                        }

                        localInterestTable.Add(year, scenario);
                    }

                    debtTable.Add("Tasa de Interés de deuda en moneda local", localInterestTable);

                    var desv = shockLocal != null ? shockLocal.StandardDeviation : 0;
                    var devStdLocal = CalculateDevStd(dataSet, Enums.DebtColumns.LocalRate.ToString());
                    var valorLocal = CalculateValues(dataSet, Enums.DebtColumns.LocalRate.ToString(), devStdLocal, desv);

                    dictLocal.Add("devStd", devStdLocal);
                    dictLocal.Add("valor", valorLocal);
                }
            }

            // Todos los Shocks
            // Balance
            var allShocksTable = new SortedDictionary<int, decimal>();
            for (var year = lastYear - 2; year <= maxYear; year++)
            {
                var primaryBalance = CalculateBalance(dataSet, year);
                var availableIncome = GetAvailableIncome(dataSet, year);

                decimal scenario = 0;

                if (availableIncome != 0)
                {
                    scenario = Math.Round(primaryBalance / availableIncome * 100, 1);
                }

                allShocksTable.Add(year, scenario);
            }

            balanceTable.Add("Todos los Shocks", allShocksTable);

            // Deuda
            allShocksTable = new SortedDictionary<int, decimal>();
            yearDebt = new Dictionary<int, decimal>();
            for (var year = lastYear - 2; year <= maxYear; year++)
            {
                var debt = GetDebt(dataSet, year, lastYear, ref yearDebt);
                var availableIncome = GetAvailableIncome(dataSet, year);

                decimal scenario = 0;

                if (availableIncome != 0)
                {
                    scenario = Math.Round(debt / availableIncome * 100, 1);
                }

                allShocksTable.Add(year, scenario);
            }

            debtTable.Add("Todos los Shocks", allShocksTable);

            data.Balance = balanceTable;
            data.Debt = debtTable;

            data.DatosTasa = dictTasa;
            data.DatosIngreso = dictIngreso;
            data.DatosExtranjera = dictExtranjera;
            data.DatosLocal = dictLocal;

            result.Data = data;
            result.StatusCode = HttpStatusCode.OK;

            return result;
        }

        // Cálculo del DEV STD para Dinámica de la Deuda
        private decimal CalculateDevStd(DataSet dataSet, string columnName)
        {
            decimal devStd = 0;

            // DEV STD = Desviación Estándar de column para años históricos
            var columnId = GetColumnId(dataSet.CatalogueId, columnName);
            var columnValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == columnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToList();

            if (columnValues.Any())
            {
                var minYear = columnValues.Min(v => v.YearData.Year);
                var maxYear = columnValues.Where(v => !v.Projected).Max(v => v.YearData.Year);

                var values = columnValues.Where(v => v.YearData.Year >= minYear && v.YearData.Year <= maxYear).Select(v => v.Value).ToArray();

                var standarDeviation = MathHelper.GetStandardDeviation(values);
                devStd = Math.Round(standarDeviation, 2);
            }

            return devStd;
        }

        // Cálculo de Valores para Dinámica de la Deuda
        private decimal CalculateValues(DataSet dataSet, string columnName, decimal devStd, decimal deviation)
        {
            decimal values = 0;

            // Values = promedio de column para años históricos + DEV STD * desv. std
            var columnId = GetColumnId(dataSet.CatalogueId, columnName);
            var columnValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == columnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId));

            if (columnValues.Any())
            {
                var minYear = columnValues.Min(v => v.YearData.Year);
                var maxYear = columnValues.Where(v => !v.Projected).Max(v => v.YearData.Year);

                var vals = columnValues.Where(v => v.YearData.Year >= minYear && v.YearData.Year <= maxYear).Select(v => v.Value).ToArray();

                var avg = MathHelper.GetAverage2(vals);
                values = Math.Round(avg + devStd * deviation, 2);
            }

            return values;
        }

        #endregion

        #region Fan Chart

        public ApiResult FanChart(string userId, int catalogueId, FanChartDTO fanChartData)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var vectorErrores = ObtenerVectorErrores(fanChartData.VarCovMatrix);

            var vectorSeries = ObtenerVectorSeries(vectorErrores);

            var result = new ApiResult();
            dynamic data = new ExpandoObject();
            data.Series = vectorSeries;
            result.Data = data;
            result.StatusCode = HttpStatusCode.OK;

            return result;
        }

        private static double[,] ObtenerVectorErrores(double[,] varCov)
        {
            var errores = new double[1000, 6];

            var erroresNormales = ObtenerErroresNormales();
            var erroresCorrelacionados = ObtenerErroresCorrelacionados(erroresNormales, varCov);

            for (var kk = 0; kk < 1000; kk++)
            {
                for (var ll = 0; ll < 6; ll++)
                {
                    if (ll == 0)
                    {
                        errores[kk, ll] = erroresCorrelacionados[kk, 2];
                    }
                    else
                    {
                        if (ll == 1)
                        {
                            errores[kk, ll] = erroresCorrelacionados[kk, 0];
                        }
                        else
                        {
                            if (ll == 2)
                            {
                                errores[kk, ll] = erroresCorrelacionados[kk, 1];
                            }
                            else
                            {
                                errores[kk, ll] = erroresCorrelacionados[kk, ll];
                            }
                        }
                    }
                }
            }

            return errores;
        }

        private static double[,] ObtenerVectorSeries(double[,] vectorDeudaEndogena)
        {
            /*
               Para los años historicos, los valores de la serie se calculan con la formula de la deuda

               deuda_local = (E + G + I + K) / (P + Q + R)
               deuda_extra = (F + H + J + L) * D / (P + Q + R)

               total = deuda_local + deuda_extra 
           */

            var añosHistoricos = 13;
            var añosSimular = 5;
            var simulaciones = 18;

            var vectorSeries = new double[simulaciones, añosHistoricos + añosSimular];

            // TODO
            var datosHistoricos = ObtenerDeudaHistorica();

            // Datos históricos.
            for (var i = 0; i < simulaciones; i++)
            {
                for (var j = 0; j < añosHistoricos; j++)
                {
                    vectorSeries[0, i] = datosHistoricos[i];
                }
            }

            // Simulaciones. Usar vector deuda endogena
            for (var i = 0; i < simulaciones; i++)
            {
                for (var j = 0; j < añosSimular; j++)
                {
                    vectorSeries[i, añosHistoricos + j] = vectorDeudaEndogena[50 * i, j];
                }
            }

            return vectorSeries;
        }

        private static double[] ObtenerDeudaHistorica()
        {
            var deuda = new double[19];
            double n = 0;
            for (var i = 0; i < deuda.Length; i++)
            {
                deuda[i] = n;
                n += 0.4;
            }

            return deuda;
        }

        private static double[,] ObtenerErroresNormales()
        {
            var valoresAleatorios = new double[1000, 6];
            var erroresNormales = new double[1000, 6];

            var random = new Random();
            var normal = new Normal(random);

            for (var i = 0; i < 1000; i++)
            {
                for (var j = 0; j < 6; j++)
                {
                    valoresAleatorios[i, j] = random.NextDouble(); // returns a double between 0 and 1.
                    erroresNormales[i, j] = normal.InverseCumulativeDistribution(valoresAleatorios[i, j]);
                }
            }

            return valoresAleatorios;
        }

        private static double[,] ObtenerErroresCorrelacionados(double[,] erroresNormales, double[,] varCov)
        {
            var erroresCorrelacionados = new double[1000, 6];

            for (var k = 0; k < 1000; k++)
            {
                for (var l = 0; l < 6; l++)
                {
                    double suma = 0;
                    for (var z = 0; z < 6; z++)
                    {
                        var producto = erroresNormales[k, z] * varCov[z, l];
                        suma = suma + producto;
                    }
                    erroresCorrelacionados[k, l] = suma;
                }
            }

            return erroresCorrelacionados;
        }

        #endregion

        public ApiResult GetProbabilisticAssumptions(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var result = new ApiResult();
            dynamic data = new ExpandoObject();

            var crecimientoIngresos = CrecimientoIngresos(dataSet);
            var tasaLocal = TasaInteresLocal(dataSet);
            var tasaCambio = new Tuple<decimal, decimal>(0, 0);
            if (dataSet.CatalogueId >= 2)
            {
                tasaCambio = TasaCambio(dataSet);
            }
            var superavit = Superavit(dataSet);
            var inflacion = Inflacion(dataSet);
            var tasaExtranjera = new Tuple<decimal, decimal>(0, 0);
            if (dataSet.CatalogueId >= 2)
            {
                tasaExtranjera = TasaInteresExtranjera(dataSet);
            }

            data.CrecimientoIngresoStd = crecimientoIngresos.Item1;
            data.CrecimientoIngresoSerie = crecimientoIngresos.Item2;

            data.TasaLocalStd = tasaLocal.Item1;
            data.TasaLocalSerie = tasaLocal.Item2;

            data.TasaCambioStd = tasaCambio.Item1;
            data.TasaCambioSerie = tasaCambio.Item2;

            data.SuperavitStd = superavit.Item1;
            data.SuperavitSerie = superavit.Item2;

            data.InflacionStd = inflacion.Item1;
            data.InflacionSerie = inflacion.Item2;

            data.TasaExtranjeraStd = tasaExtranjera.Item1;
            data.TasaExtranjeraSerie = tasaExtranjera.Item2;

            result.Data = data;
            result.StatusCode = HttpStatusCode.OK;

            return result;
        }

        private Tuple<decimal, decimal> CrecimientoIngresos(DataSet dataSet)
        {
            decimal stdDev = 0;
            decimal ultimo = 0;

            var ingresoDisponible = ObtenerIngresoDisponible(dataSet);

            if (ingresoDisponible.Length > 0)
            {
                var values = new decimal[ingresoDisponible.Length - 1];

                for (var i = 1; i < ingresoDisponible.Length - 2; i++)
                {
                    if (ingresoDisponible[i - 1] != 0)
                    {
                        values[i - 1] = (ingresoDisponible[i] - ingresoDisponible[i - 1]) / ingresoDisponible[i - 1];
                    }
                    else
                    {
                        values[i - 1] = 0;
                    }
                }

                stdDev = Math.Round(MathHelper.GetStandardDeviation(values), 6);
                if (values.Any())
                {
                    ultimo = Math.Round(values.Last() / 100, 6);
                }
            }

            return new Tuple<decimal, decimal>(stdDev, ultimo);
        }

        private Tuple<decimal, decimal> TasaInteresLocal(DataSet dataSet)
        {
            decimal stdDev = 0;
            decimal ultimo = 0;

            var columnId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.LocalRate.ToString());
            var values = _catalogueColumnValuesRepository
                .List(v => v.CatalogueColumnId == columnId)
                .Where(v => dataSet.YearDatas.Select(y => y.Id)
                .Contains(v.YearDataId))
                .Select(v => v.Value)
                .ToArray();

            if (values.Any())
            {
                stdDev = Math.Round(MathHelper.GetStandardDeviation(values) / 100, 6);
                ultimo = Math.Round(values.Last() / 100, 6);
            }

            return new Tuple<decimal, decimal>(stdDev, ultimo);
        }

        private Tuple<decimal, decimal> TasaCambio(DataSet dataSet)
        {
            decimal stdDev = 0;
            decimal ultimo = 0;

            var tasaCambio = ObtenerTasaCambio(dataSet);

            if (tasaCambio != null && tasaCambio.Length > 0)
            {
                var values = new decimal[tasaCambio.Length - 1];

                for (var i = 1; i < tasaCambio.Length - 2; i++)
                {
                    if (tasaCambio[i - 1] != 0)
                    {
                        values[i - 1] = (tasaCambio[i] - tasaCambio[i - 1]) / tasaCambio[i - 1];
                    }
                    else
                    {
                        values[i - 1] = 0;
                    }
                }

                stdDev = Math.Round(MathHelper.GetStandardDeviation(values) / 100, 6);
                ultimo = Math.Round(values.Last() / 100, 6);
            }

            return new Tuple<decimal, decimal>(stdDev, ultimo);
        }

        private Tuple<decimal, decimal> Superavit(DataSet dataSet)
        {
            decimal stdDev = 0;
            decimal ultimo = 0;

            var columnId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.LocalRate.ToString());
            var values = _catalogueColumnValuesRepository
                .List(v => v.CatalogueColumnId == columnId)
                .Where(v => dataSet.YearDatas.Select(y => y.Id)
                .Contains(v.YearDataId))
                .Select(v => v.Value)
                .ToArray();

            if (values.Any())
            {
                stdDev = Math.Round(MathHelper.GetStandardDeviation(values), 6);
                ultimo = Math.Round(values.Last() / 100, 6);
            }

            return new Tuple<decimal, decimal>(stdDev, ultimo);
        }

        private Tuple<decimal, decimal> Inflacion(DataSet dataSet)
        {
            decimal stdDev = 0;
            decimal ultimo = 0;

            var inflacion = ObtenerInflacion(dataSet);

            if (inflacion != null && inflacion.Length > 1)
            {
                var values = new decimal[inflacion.Length - 1];

                for (var i = 1; i < inflacion.Length - 2; i++)
                {
                    if (inflacion[i - 1] != 0)
                    {
                        values[i - 1] = (inflacion[i] - inflacion[i - 1]) / inflacion[i - 1];
                    }
                    else
                    {
                        values[i - 1] = 0;
                    }
                }

                stdDev = Math.Round(MathHelper.GetStandardDeviation(values), 6);
                ultimo = Math.Round(values.Last() / 100, 6);
            }

            return new Tuple<decimal, decimal>(stdDev, ultimo);
        }

        private Tuple<decimal, decimal> TasaInteresExtranjera(DataSet dataSet)
        {
            decimal stdDev = 0;
            decimal ultimo = 0;

            var columnId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.ForeignRate.ToString());
            var values = _catalogueColumnValuesRepository
                .List(v => v.CatalogueColumnId == columnId)
                .Where(v => dataSet.YearDatas.Select(y => y.Id)
                .Contains(v.YearDataId))
                .Select(v => v.Value)
                .ToArray();

            if (values.Any())
            {
                stdDev = Math.Round(MathHelper.GetStandardDeviation(values) / 100, 6);
                ultimo = Math.Round(values.Last() / 100, 6);
            }

            return new Tuple<decimal, decimal>(stdDev, ultimo);
        }

        private decimal[] ObtenerIngresoDisponible(DataSet dataSet)
        {
            var years = _catalogueColumnValuesRepository
               .List()
               .Where(v => dataSet.YearDatas.Select(y => y.Id)
               .Contains(v.YearDataId) && !v.Projected);

            var minYear = 0;
            var maxYear = 0;

            if (years.Any())
            {
                minYear = years.Min(v => v.YearData.Year);
                maxYear = years.Max(v => v.YearData.Year);
            }

            var diffYears = maxYear - minYear;

            var arrIngresoDisponible = new decimal[diffYears];

            var i = 0;
            for (var year = minYear; year < maxYear; year++)
            {
                arrIngresoDisponible[i] = GetAvailableIncome(dataSet, year);
                i++;
            }

            return arrIngresoDisponible;
        }

        private decimal[] ObtenerTasaCambio(DataSet dataSet)
        {
            var years = _catalogueColumnValuesRepository
               .List()
               .Where(v => dataSet.YearDatas.Select(y => y.Id)
               .Contains(v.YearDataId) && !v.Projected);

            var minYear = 0;
            var maxYear = 0;

            if (years.Any())
            {
                minYear = years.Min(v => v.YearData.Year);
                maxYear = years.Max(v => v.YearData.Year);
            }

            var diffYears = maxYear - minYear;

            var arrTasaCambio = new decimal[diffYears];

            var i = 0;
            for (var year = minYear; year < maxYear; year++)
            {
                arrTasaCambio[i] = GetChangeRate(dataSet, year);
                i++;
            }

            return arrTasaCambio;
        }

        private decimal[] ObtenerInflacion(DataSet dataSet)
        {
            return GetInflationRateValues(dataSet);
        }

        private decimal GetChangeRate(DataSet dataSet, int year)
        {
            decimal changeRate = 0;

            var changeRateColumnId = GetColumnId(dataSet.CatalogueId, Enums.MacroeconomicsColumns.LocalChangeRate.ToString());
            var changeRateValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == changeRateColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId)).ToArray();

            if (changeRateValues.Any())
            {
                var changeRateValue = changeRateValues.SingleOrDefault(v => v.YearData.Year == year);

                if (changeRateValue != null)
                {
                    changeRate = changeRateValue.Value;
                }
            }

            return changeRate;
        }

        private int GetColumnId(int catalogueId, string internalColumnName)//TODO return 0 si no existe
        {
            var column = _catalogueColumnsRepository
                .List(c => c.CatalogueId == catalogueId && c.InternalName == internalColumnName).FirstOrDefault();

            return column?.Id ?? 0;
        }

        public ApiResult GetFanChartYears(string userId, int catalogueId)
        {
            var numberOfYears = int.Parse(WebConfigurationManager.AppSettings["numberOfYears"]);
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            //se setean 5 años de históricos y 5 de proyecciones ingresadas por el usuario
            int minYear = dataSet.ProjectionFrom.Value;
            int maxYear = dataSet.ProjectionFrom.Value + (numberOfYears - 1);

            dynamic data = new ExpandoObject();
            var years = new List<int>();
            for (var i = minYear; i <= maxYear; i++)
            {
                years.Add(i);
            }

            data.FutureYears = years;

            return new ApiResult { StatusCode = HttpStatusCode.OK, Data = data };
        }

        public ApiResult GetInitialConditions(string userId, int catalogueId)
        {
            var numberOfYears = int.Parse(WebConfigurationManager.AppSettings["numberOfYears"]);
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            //se setean 5 años de históricos y 5 de proyecciones ingresadas por el usuario
            int minYear = dataSet.HistoricTo.Value - (numberOfYears - 1);
            int maxYear = dataSet.ProjectionFrom.Value + (numberOfYears - 1);

            FanChartInitialConditions ret;

            IList<string> errorInternalNames;

            if (dataSet.Catalogue.Type == Enums.CatalogueType.Mexico)
            {
                ret = GetMexicoInitialConditions(dataSet, ref minYear, ref maxYear, out errorInternalNames);
            }
            else
            {
                ret = GetOtherInitialConditions(dataSet, minYear, maxYear, out errorInternalNames);
            }

            ret.FFit = GetHistoricFanChartValues(dataSet, dataSet.HistoricTo.Value - (numberOfYears - 1), dataSet.HistoricTo.Value);
            ret.HistoricColumns = numberOfYears;

            if (errorInternalNames.ToList().Count() > 0)
            {
                return new ApiResult
                {
                    Message = GetMissingDataError(dataSet.CatalogueId, errorInternalNames.ToArray()),
                    StatusCode = HttpStatusCode.InternalServerError,
                    Result = "MissingData"
                };
            }

            return new ApiResult
            {
                Data = ret,
                StatusCode = HttpStatusCode.OK
            };
        }


        private bool MissingData(DataSet dataSet, List<string> internalNameList)
        {
            List<int> columnIdList = new List<int>();
            foreach (var internalName in internalNameList)
            {
                var id = GetColumnId(dataSet.CatalogueId, internalName);
                if (id != 0)
                {
                    columnIdList.Add(id);
                }

            }

            foreach (var id in columnIdList)
            {
                if (_catalogueColumnValuesRepository.List(c => c.CatalogueColumnId == id)
                        .Where(v => dataSet.YearDatas.Select(y => y.Id)
                        .Contains(v.YearDataId))
                        .ToDictionary(item => item.YearData.Year, item => item.Value).Count() == 0)
                {
                    return true;
                }
            }

            return false;
        }

        private FanChartInitialConditions GetMexicoInitialConditions(DataSet dataSet, ref int minYear, ref int maxYear, out IList<string> errorInternalNames)
        {
            var numberOfYears = int.Parse(WebConfigurationManager.AppSettings["numberOfYears"]);

            var columnNames = new[]
            {
                Enums.DebtColumns.LocalRate.ToString(),
                Enums.IncomeColumns.AvailableIncome.ToString(),
                Enums.IncomeColumns.FederalTransfers.ToString(),
                Enums.ExpenseColumns.TotalExpense.ToString(),
                Enums.DebtColumns.TotalDebt.ToString()

            };

            var optionalColumnNames = new[]
            {
                Enums.ExpenseColumns.PDNLabeled.ToString(),
                Enums.ExpenseColumns.PDLabeled.ToString()
            };

            bool autocomplete = false;

            var datas = GetCatalogInitialData(dataSet, columnNames, minYear, maxYear, out errorInternalNames);

            var optionalDatas = GetCatalogInitialData(dataSet, optionalColumnNames, minYear, maxYear, out errorInternalNames);
            var list = errorInternalNames.Where(c => c == Enums.ExpenseColumns.PDNLabeled.ToString() || c == Enums.ExpenseColumns.PDLabeled.ToString());


            if (list.Count() == 1)
            {
                errorInternalNames.Remove(list.FirstOrDefault());
            }

            var pintK = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.PDNLabeled.ToString()], minYear, maxYear);
            var pintT = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.PDLabeled.ToString()], minYear, maxYear);
            var ggpt = GetColumnValueFiltered(datas[Enums.ExpenseColumns.TotalExpense.ToString()], minYear, maxYear, autocomplete);

            var ret = new FanChartInitialConditions
            {
                Rtml = GetColumnValueFiltered(datas[Enums.DebtColumns.LocalRate.ToString()], minYear, maxYear, autocomplete),
                Gildt = GetColumnValueFiltered(datas[Enums.IncomeColumns.AvailableIncome.ToString()], minYear, maxYear, autocomplete),
                Gidet = GetColumnValueFiltered(datas[Enums.IncomeColumns.FederalTransfers.ToString()], minYear, maxYear, autocomplete),
                Ggpt = ggpt.Select((g, i) => g - pintT[i] - pintK[i]).ToArray(),
            };

            var dummyRtme = new List<decimal>();
            var dummyTCambio = new List<decimal>();

            for (int i = minYear; i <= maxYear; i++)
            {
                dummyRtme.Add(0);
                dummyTCambio.Add(1);
                autocomplete = true;
            }

            ret.Rtme = dummyRtme.ToArray();
            ret.Et = dummyTCambio.ToArray();
            ret.Gp0 = ret.Ggpt.Any() ? ret.Ggpt.ElementAt(numberOfYears - 1) : 1;
            ret.IDE0 = ret.Gidet.Any() ? ret.Gidet.ElementAt(numberOfYears - 1) : 0;
            ret.ILD0 = ret.Gildt.Any() ? ret.Gildt.ElementAt(numberOfYears - 1) : 0;
            ret.DebtInitialValue = GetColumnValue(dataSet, maxYear, Enums.DebtColumns.TotalDebt.ToString());
            ret.Autocomplete = autocomplete;

            return ret;
        }

        private FanChartInitialConditions GetOtherInitialConditions(DataSet dataSet, int minYear, int maxYear, out IList<string> errorInternalNames)
        {
            var numberOfYears = int.Parse(WebConfigurationManager.AppSettings["numberOfYears"]);

            var columnNames = new[]
            {
                Enums.DebtColumns.LocalRate.ToString(),
                Enums.DebtColumns.ForeignRate.ToString(),
                Enums.MacroeconomicsColumns.LocalChangeRate.ToString(),
                Enums.IncomeColumns.AvailableIncome.ToString(),
                Enums.IncomeColumns.NotAvailableIncome.ToString(),
                Enums.ExpenseColumns.TotalExpense.ToString()
            };

            var optionalColumnNames = new[]
            {
                Enums.ExpenseColumns.Interests.ToString(),
                Enums.ExpenseColumns.FDEInterests.ToString()
            };

            var otherMinYear = minYear;
            var otherMaxYear = maxYear;
            bool autocomplete = false;

            var datas = GetCatalogInitialData(dataSet, columnNames, minYear, maxYear, out errorInternalNames);

            var optionalDatas = GetCatalogInitialData(dataSet, optionalColumnNames, otherMinYear, otherMaxYear, out errorInternalNames);

            var interests = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.Interests.ToString()], minYear, maxYear);
            var fdeiInterests = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.FDEInterests.ToString()], minYear, maxYear);
            var ggpt = GetColumnValueFiltered(datas[Enums.ExpenseColumns.TotalExpense.ToString()], minYear, maxYear, autocomplete);

            var ret = new FanChartInitialConditions
            {
                Rtml = GetColumnValueFiltered(datas[Enums.DebtColumns.LocalRate.ToString()], minYear, maxYear, autocomplete),
                Rtme = GetColumnValueFiltered(datas[Enums.DebtColumns.ForeignRate.ToString()], minYear, maxYear, autocomplete),
                Et = GetColumnValueFiltered(datas[Enums.MacroeconomicsColumns.LocalChangeRate.ToString()], minYear, maxYear, autocomplete),
                Gildt = GetColumnValueFiltered(datas[Enums.IncomeColumns.AvailableIncome.ToString()], minYear, maxYear, autocomplete),
                Gidet = GetColumnValueFiltered(datas[Enums.IncomeColumns.NotAvailableIncome.ToString()], minYear, maxYear, autocomplete),
                Ggpt = ggpt.Select((g, i) => g - interests[i] - fdeiInterests[i]).ToArray(),
            };

            ret.Gp0 = ret.Ggpt.Any() ? ret.Ggpt.ElementAt(numberOfYears - 1) : 1;
            ret.IDE0 = ret.Gidet.Any() ? ret.Gidet.ElementAt(numberOfYears - 1) : 0;
            ret.ILD0 = ret.Gildt.Any() ? ret.Gildt.ElementAt(numberOfYears - 1) : 0;
            ret.DebtInitialValue = GetColumnValue(dataSet, maxYear, Enums.DebtColumns.TotalDebt.ToString());
            ret.Autocomplete = autocomplete;

            return ret;
        }

        public ApiResult GetDebtDynamicsData(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            int minYear = (dataSet.HistoricTo ?? DateTime.Now.Year) - 2;
            int maxYear = (dataSet.ProjectionFrom ?? DateTime.Now.Year) + 4;


            DebtDynamicsInputs ret;

            IList<string> errorInternalNames;

            if (dataSet.Catalogue.Type == Enums.CatalogueType.Mexico)
            {
                ret = GetMexicoDebtDynamicsData(dataSet, minYear, maxYear, out errorInternalNames);
            }
            else
            {
                ret = GetOtherDebtDynamicsData(dataSet, minYear, maxYear, out errorInternalNames);
            }


            if (errorInternalNames.ToList().Count() > 0)
            {
                return new ApiResult
                {
                    Message = GetMissingDataError(dataSet.CatalogueId, errorInternalNames.ToArray()),
                    StatusCode = HttpStatusCode.InternalServerError,
                    Result = "MissingData"
                };
            }

            return new ApiResult
            {
                Data = ret,
                StatusCode = HttpStatusCode.OK
            };
        }

        public ApiResult SetFanChartConfig(string userId, string matrix)
        {
            var user = _oAuthLogic.FindUserByEmail(userId);
            user.FanChartConfig = matrix;
            _oAuthLogic.UpdateUser(new UserDTO(user));
            return new ApiResult
            {
                Data = matrix,
                StatusCode = HttpStatusCode.OK
            };
        }

        private DebtDynamicsInputs GetMexicoDebtDynamicsData(DataSet dataSet, int minYear, int maxYear, out IList<string> errorInternalNames)
        {
            var columnNames = new[]
            {
                Enums.DebtColumns.TotalDebt.ToString(),
                Enums.DebtColumns.LocalRate.ToString(),
                Enums.IncomeColumns.TotalIncome.ToString(),
                Enums.ExpenseColumns.TotalExpense.ToString(),
            };

            var optionalColumnNames = new[]
            {
                Enums.ExpenseColumns.PDNLabeled.ToString(),
                Enums.ExpenseColumns.PDLabeled.ToString()
            };

            bool autocomplete = false;

            var datas = GetCatalogInitialData(dataSet, columnNames, minYear, maxYear, out errorInternalNames);

            var optionalDatas = GetCatalogInitialData(dataSet, optionalColumnNames, minYear, maxYear, out errorInternalNames);

            var list = errorInternalNames.Where(c => c == Enums.ExpenseColumns.PDNLabeled.ToString() || c == Enums.ExpenseColumns.PDLabeled.ToString());
            if (list.Count() == 1)
            {
                errorInternalNames.Remove(list.FirstOrDefault());
            }

            var pintK = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.PDNLabeled.ToString()], minYear, maxYear);
            var pintT = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.PDLabeled.ToString()], minYear, maxYear);

            var ret = new DebtDynamicsInputs
            {
                Dml = GetColumnValueFiltered(datas[Enums.DebtColumns.TotalDebt.ToString()], minYear, maxYear, autocomplete),
                Rml = GetColumnValueFiltered(datas[Enums.DebtColumns.LocalRate.ToString()], minYear, maxYear, autocomplete),
                Ingt = GetColumnValueFiltered(datas[Enums.IncomeColumns.TotalIncome.ToString()], minYear, maxYear, autocomplete),
                Teg = GetColumnValueFiltered(datas[Enums.ExpenseColumns.TotalExpense.ToString()], minYear, maxYear, autocomplete),
                Pint = pintK.Select((t, i) => t + pintT[i]).ToArray(),
                Tini = minYear,
                Tproy = maxYear,
            };

            var dummyDme = new List<decimal>();
            var dummyTCambio = new List<decimal>();

            for (int i = minYear; i <= maxYear; i++)
            {
                dummyDme.Add(0);
                dummyTCambio.Add(1);
                autocomplete = true;
            }

            ret.Dme = dummyDme.ToArray();
            ret.Rme = ret.Dme;
            ret.Tcambio = dummyTCambio.ToArray();
            ret.Autocomplete = autocomplete;

            return ret;
        }

        private DebtDynamicsInputs GetOtherDebtDynamicsData(DataSet dataSet, int minYear, int maxYear, out IList<string> errorInternalNames)
        {
            var columnNames = new[]
            {
                Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString(),
                Enums.DebtColumns.HistoricFixedRateDomesticCurrency.ToString(),
                Enums.DebtColumns.HistoricVariableRateForeignCurrency.ToString(),
                Enums.DebtColumns.HistoricFixedRateForeignCurrency.ToString(),
                Enums.DebtColumns.LocalRate.ToString(),
                Enums.DebtColumns.ForeignRate.ToString(),
                Enums.MacroeconomicsColumns.LocalChangeRate.ToString(),
                Enums.IncomeColumns.TotalIncome.ToString(),
                Enums.ExpenseColumns.TotalExpense.ToString(),
            };

            var optionalColumnNames = new[]
            {
                Enums.ExpenseColumns.Interests.ToString(),
                Enums.ExpenseColumns.FDEInterests.ToString()
            };

            IList<string> otherErrors;
            bool autocomplete = false;

            var datas = GetCatalogInitialData(dataSet, columnNames, minYear, maxYear, out errorInternalNames);

            var optionalDatas = GetCatalogInitialData(dataSet, optionalColumnNames, minYear, maxYear, out otherErrors);

            var int1 = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.Interests.ToString()], minYear, maxYear);
            var int2 = GetFixedNonRequiredColumnValues(optionalDatas[Enums.ExpenseColumns.FDEInterests.ToString()], minYear, maxYear);

            var dmlV = GetColumnValueFiltered(datas[Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString()], minYear, maxYear, autocomplete);
            var dmlF = GetColumnValueFiltered(datas[Enums.DebtColumns.HistoricFixedRateDomesticCurrency.ToString()], minYear, maxYear, autocomplete);
            var dmeV = GetColumnValueFiltered(datas[Enums.DebtColumns.HistoricVariableRateForeignCurrency.ToString()], minYear, maxYear, autocomplete);
            var dmeL = GetColumnValueFiltered(datas[Enums.DebtColumns.HistoricFixedRateForeignCurrency.ToString()], minYear, maxYear, autocomplete);

            return new DebtDynamicsInputs
            {
                Dml = dmlV.Select((t, i) => t + dmlF[i]).ToArray(),
                Dme = dmeV.Select((t, i) => t + dmeL[i]).ToArray(),
                Rml = GetColumnValueFiltered(datas[Enums.DebtColumns.LocalRate.ToString()], minYear, maxYear, autocomplete),
                Rme = GetColumnValueFiltered(datas[Enums.DebtColumns.ForeignRate.ToString()], minYear, maxYear, autocomplete),
                Tcambio = GetColumnValueFiltered(datas[Enums.MacroeconomicsColumns.LocalChangeRate.ToString()], minYear, maxYear, autocomplete),
                Ingt = GetColumnValueFiltered(datas[Enums.IncomeColumns.TotalIncome.ToString()], minYear, maxYear, autocomplete),
                Teg = GetColumnValueFiltered(datas[Enums.ExpenseColumns.TotalExpense.ToString()], minYear, maxYear, autocomplete),
                Pint = int1.Select((t, i) => t + int2[i]).ToArray(),
                Tini = minYear,
                Tproy = maxYear,
                Autocomplete = autocomplete
            };
        }

        private decimal GetColumnValue(DataSet dataSet, int year, string columnName)
        {
            decimal cv = 0;

            var columnId = GetColumnId(dataSet.CatalogueId, columnName);
            var columnValue = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == columnId).FirstOrDefault(v => v.YearData.Year == year);

            if (columnValue != null)
                cv = columnValue.Value;

            return cv;
        }

        private decimal[] GetHistoricFanChartValues(DataSet dataset, int minYear, int maxYear)
        {
            var array = new List<decimal>();
            var id = GetColumnId(dataset.CatalogueId, Enums.DebtColumns.TotalDebt.ToString());

            var totalDebt = _catalogueColumnValuesRepository.List(t => t.CatalogueColumnId == id
                                                                  && t.YearData.DataSetId == dataset.Id
                                                                  && t.YearData.Year >= minYear && t.YearData.Year <= maxYear)
                                                            .ToDictionary(i => i.YearData.Year, i => i.Value);

            id = GetColumnId(dataset.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
            var availableIncome = _catalogueColumnValuesRepository.List(t => t.CatalogueColumnId == id
                                                                  && t.YearData.DataSetId == dataset.Id
                                                                  && t.YearData.Year >= minYear && t.YearData.Year <= maxYear)
                                                            .ToDictionary(i => i.YearData.Year, i => i.Value);
            decimal val = 0;
            for (var year = minYear; year <= maxYear; year++)
            {
                array.Add(totalDebt.TryGetValue(year, out val)
                         ? availableIncome.TryGetValue(year, out val)
                            ? totalDebt[year] / availableIncome[year]
                            : 0
                         : 0);
            }

            return array.ToArray();
        }

        private void SetHistoricYear(DataSet dataSet)
        {
            var lastYearWithData = dataSet.HistoricTo;
            var columns = _catalogueColumnsRepository.List().Where(x => x.CatalogueId == dataSet.CatalogueId && x.Parent == null && x.Children.Any());
            foreach (var c in columns)
            {
                if (c.ColumnValues.Any() &&
                    c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Any() &&
                    c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Max(x => x.YearData.Year) < lastYearWithData)
                {
                    lastYearWithData = c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Max(x => x.YearData.Year);
                }
            }

            if (dataSet.HistoricTo > lastYearWithData)
            {
                dataSet.HistoricTo = lastYearWithData;
                dataSet.ProjectionFrom = lastYearWithData + 1;
                _datasetRepository.Update(dataSet);
            }
        }
    }
}