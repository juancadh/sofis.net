﻿using System;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.DAL.IdentityExtensions;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.ApiModel;
using BID.PSF.Model.DBModel;
using Microsoft.AspNet.Identity;
using Okta.Core.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.Security.Principal;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net;
using BID.PSF.Core.Resources;
using System.Collections.Generic;
using BID.PSF.Core.Enums;
using System.Threading;

namespace BID.PSF.BLL.Logic
{
    public class OAuthLogic : IOAuthLogic
    {
        private readonly PSFUserManager _userManager;
        private readonly IOAuthRepository<OAuthClient> _oAuthClientsRepository;
        private readonly IOAuthRepository<RefreshToken> _refreshTokensRepository;
        private readonly IDataSetsLogic _dataSetsLogic;

        public OAuthLogic(PSFUserManager userManager, IOAuthRepository<OAuthClient> oAuthClientsRepository, IOAuthRepository<RefreshToken> refreshTokensRepository, IDataSetsLogic dataSetsLogic)
        {
            _userManager = userManager;
            _oAuthClientsRepository = oAuthClientsRepository;
            _refreshTokensRepository = refreshTokensRepository;
            _dataSetsLogic = dataSetsLogic;
        }

        public IdentityResult RegisterUser(OAuthUser user)
        {
            var result = _userManager.Create(user);
            CreateUserEnvironment(user);

            return result;
        }

        public IdentityResult UpdateUser(UserDTO userData)
        {
            OAuthUser user = FindUserByEmail(userData.Email);

            if (user != null)
            {
                //user.MunicipalityId = userData.MunicipalityId;
            }

            return _userManager.Update(user);
        }

        public async Task<OAuthUser> FindUser(string userName, string password)
        {
            return await _userManager.FindAsync(userName, password);
        }

        public OAuthClient FindClient(string clientId)
        {
            return _oAuthClientsRepository.FindById(clientId);
        }

        public OAuthUser FindUserByEmail(string email)
        {
            return _userManager.FindByEmail(email);
        }

        public bool AddRefreshToken(RefreshToken token)
        {
            var existingToken = _refreshTokensRepository
                .List(r => r.Subject == token.Subject && r.ClientId == token.ClientId).FirstOrDefault();

            if (existingToken != null)
                RemoveRefreshToken(existingToken.Id);

            _refreshTokensRepository.Add(token);

            return token.Id != null;
        }

        public RefreshToken FindRefreshToken(string refreshTokenId)
        {
            return _refreshTokensRepository.FindById(refreshTokenId);
        }

        public bool RemoveRefreshToken(string refreshTokenId)
        {
            var refreshToken = _refreshTokensRepository.FindById(refreshTokenId);

            if (refreshToken == null) return false;

            _refreshTokensRepository.Delete(refreshTokenId);

            return true;
        }

        public HttpStatusCode ResetPassword(ResetPasswordDTO resetPassword)
        {
            var oktaUserId = GetCurrentOktaUser();

            var apiKey = ConfigurationManager.AppSettings["okta:ApiToken"];
            var apiUri = new Uri(ConfigurationManager.AppSettings["okta:ApiUrl"]);
            var usersApiUri = ConfigurationManager.AppSettings["okta:UsersApiUrl"] + "/" + oktaUserId;
            var changePasswordApiUri = ConfigurationManager.AppSettings["okta:ChangePasswordApiUrl"];
            var baseUri = new Uri(apiUri, relativeUri: usersApiUri);
            var uri = new Uri(baseUri + changePasswordApiUri);

            var statusCode = HttpStatusCode.OK;

            using (var client = new HttpClient())
            {
                client.BaseAddress = uri;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "SSWS " + apiKey);

                var content = JsonConvert.SerializeObject(resetPassword);
                var buffer = System.Text.Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                try
                {
                    var response = client.PostAsync(uri, byteContent).Result;
                    statusCode = response.StatusCode;
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex.Message);
                    //this only should happed if there is network connection
                }
            }

            return statusCode;
        }

        public HttpStatusCode ForgotPassword(ForgotPasswordDTO forgotPassword)
        {
            var oktaUserId = GetOktaUserByEmail(forgotPassword.Username);

            var apiKey = ConfigurationManager.AppSettings["okta:ApiToken"];
            var apiUri = new Uri(ConfigurationManager.AppSettings["okta:ApiUrl"]);
            var usersApiUri = ConfigurationManager.AppSettings["okta:UsersApiUrl"] + "/" + oktaUserId;
            var forgotPasswordApiUri = ConfigurationManager.AppSettings["okta:ForgotPasswordApiUrl"];
            var baseUri = new Uri(apiUri, relativeUri: usersApiUri);
            var uri = new Uri(baseUri + forgotPasswordApiUri);

            var statusCode = HttpStatusCode.OK;

            using (var client = new HttpClient())
            {
                client.BaseAddress = uri;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "SSWS " + apiKey);

                try
                {
                    var response = client.PostAsync(uri, null).Result;
                    statusCode = response.StatusCode;
                }
                catch (Exception ex)
                {
                     //Console.WriteLine(ex.Message);
                    //this only should happed if there is network connection
                }
            }

            return statusCode;
        }

        private string GetOktaUserByEmail(string username)
        {
            string userId = string.Empty;
            var apiKey = ConfigurationManager.AppSettings["okta:ApiToken"];
            var apiUri = new Uri(ConfigurationManager.AppSettings["okta:ApiUrl"]);
            var usersApiUri = ConfigurationManager.AppSettings["okta:UsersApiUrl"] + "?q=" + username;
            var uri = new Uri(apiUri, relativeUri: usersApiUri);

            using (var client = new HttpClient())
            {
                client.BaseAddress = uri;
                client.DefaultRequestHeaders.Add("Authorization", "SSWS " + apiKey);

                try
                {
                    var response = client.GetStringAsync(uri).Result;
                    userId = JsonConvert.DeserializeObject<dynamic>(response)[0]["id"];
                }
                catch (Exception ex)
                {
                     //Console.WriteLine(ex.Message);
                    //this only should happed if there is network connection
                }
            }

            return userId;
        }

        public string GetCurrentOktaUser()
        {
            string userId = string.Empty;
            var apiKey = ConfigurationManager.AppSettings["okta:ApiToken"];
            var apiUri = new Uri(ConfigurationManager.AppSettings["okta:ApiUrl"]);
            var usersApiUri = ConfigurationManager.AppSettings["okta:UsersApiUrl"];
            var currentUserApiUrl = ConfigurationManager.AppSettings["okta:GetCurrentUserApiUrl"];
            var baseUri = new Uri(apiUri, relativeUri: usersApiUri);
            var uri = new Uri(baseUri + currentUserApiUrl);

            using (var client = new HttpClient())
            {
                client.BaseAddress = uri;
                client.DefaultRequestHeaders.Add("Authorization", "SSWS " + apiKey);

                try
                {
                    var response = client.GetStringAsync(uri).Result;
                    userId = JsonConvert.DeserializeObject<dynamic>(response)["id"];
                }
                catch (Exception ex)
                {
                     //Console.WriteLine(ex.Message);
                    //this only should happed if there is network connection
                }
            }

            return userId;
        }

        private void CreateUserEnvironment(OAuthUser user)
        {

            //Mexico
            var dataset = new DataSet()
            {
                Catalogue = CreateMexicanCatalogue(user),
                CreatedById = user.Id,
                CreatedByName = user.Email,
                CreatedDate = DateTime.Now,
                ModifiedById = user.Id,
                ModifiedByName = user.Email,
                ModifiedDate = DateTime.Now,
                HistoricFrom = 2000,
                HistoricTo = DateTime.Now.Year,
                ProjectionFrom = DateTime.Now.Year + 1,
                ProjectionTo = DateTime.Now.Year + 5,
                UserDataSets = new List<UserDataSet>()

            };
            dataset.UserDataSets.Add(new UserDataSet()
            {
                User = user,
                DataSet = dataset

            });
            _dataSetsLogic.CreateDataset(dataset);

            //Estandar
            var datasetE = new DataSet()
            {
                Catalogue = CreateEstandarCatalogue(user),
                CreatedById = user.Id,
                CreatedByName = user.Email,
                CreatedDate = DateTime.Now,
                ModifiedById = user.Id,
                ModifiedByName = user.Email,
                ModifiedDate = DateTime.Now,
                HistoricFrom = 2000,
                HistoricTo = DateTime.Now.Year,
                ProjectionFrom = DateTime.Now.Year + 1,
                ProjectionTo = DateTime.Now.Year + 5,
                UserDataSets = new List<UserDataSet>()

            };
            datasetE.UserDataSets.Add(new UserDataSet()
            {
                User = user
            });

            _dataSetsLogic.CreateDataset(datasetE);
        }

        private Catalogue CreateMexicanCatalogue(OAuthUser user)
        {

            return new Catalogue()
            {
                IsActive = true,
                IsSubcatalogue = false,
                Name = Strings.Catalogue_Mexico_Name,
                Type = Enums.CatalogueType.Mexico,
                CreatedDate = DateTime.Now,
                CreatedById = user.Id,
                CreatedByName = user.Email,
                ModifiedById = user.Id,
                ModifiedByName = user.Email,
                ModifiedDate = DateTime.Now,
                CatalogueColumns = new List<CatalogueColumn>()
                {
                    // Macroeconómicos
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Macroeconomy_ConsumerIndexPrice, InternalName = "ConsumerIndexPrice", Category = Enums.ColumnCategory.Macroeconomy},

                 // Ingresos
                new CatalogueColumn {Index = 1,ColumnName = Strings.ColName_Income_TotalIncome, InternalName = Enums.IncomeColumns.TotalIncome.ToString(),Category = Enums.ColumnCategory.Income, Children = new List<CatalogueColumn>
                {
                    new CatalogueColumn {Index = 2,ColumnName = Strings.ColName_Income_AvailableIncome,InternalName = "AvailableIncome", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn { Index = 3, ColumnName = Strings.ColName_Income_AvailableTax, InternalName = "AvailableTax",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true},
                        new CatalogueColumn { Index = 4, ColumnName = Strings.ColName_Income_SocialSecurityContributions, InternalName = "AvailableContribution",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true},
                        new CatalogueColumn {Index = 6,ColumnName = Strings.ColName_Income_ImprovementContributions, InternalName = "ImprovementContributions",Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 7,ColumnName = Strings.ColName_Income_Rights, InternalName = "Rights", Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 8,ColumnName = Strings.ColName_Income_Products, InternalName = "Products",Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 9,ColumnName = Strings.ColName_Income_Exploits, InternalName = "Exploits",Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 10,ColumnName = Strings.ColName_Income_SalesGoodsServices, InternalName = "GoodsServices", Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 11,ColumnName = Strings.ColName_Income_Participations, InternalName = "Participations", Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 12,ColumnName = Strings.ColName_Income_Incentives, InternalName = "Incentives",Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn { Index = 13, ColumnName = Strings.ColName_Income_Transfers, InternalName = "AvailableTransfer",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true},
                        new CatalogueColumn {Index = 14,ColumnName = Strings.ColName_Income_LDAgreements, InternalName = "LDAgreements", Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 15,ColumnName = Strings.ColName_Expense_AvailableOther, InternalName = "AvailableOther",Category = Enums.ColumnCategory.Income}
                    }},
                    new CatalogueColumn {Index = 16,ColumnName = Strings.ColName_Income_FederalTransfers,InternalName = Enums.IncomeColumns.FederalTransfers.ToString(), Category = Enums.ColumnCategory.Income, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn {Index = 17,ColumnName = Strings.ColName_Income_Contribution, InternalName = "Contributions",Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 18,ColumnName = Strings.ColName_Income_Agreements, InternalName = "Agreements",Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 19,ColumnName = Strings.ColName_Income_DifferentContributions, InternalName = "DifferentContributions", Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 20,ColumnName = Strings.ColName_Income_Subsidies, InternalName = "Subsidies",Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn {Index = 21,ColumnName = Strings.ColName_Income_OtherTransfers, InternalName = "OtherTransfers", Category = Enums.ColumnCategory.Income}
                    }},
                }},

                // Gastos
                new CatalogueColumn {Index = 1,ColumnName = Strings.ColName_Expense_TotalExpense, InternalName = "TotalExpense", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true, Children = new List<CatalogueColumn>
                {
                    new CatalogueColumn {Index = 2,ColumnName = Strings.ColName_Expense_UntaggedExpense, InternalName = "UntaggedExpense",Category = Enums.ColumnCategory.Expense, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn {Index = 7,ColumnName = Strings.ColName_Expense_PersonalServices, InternalName = "PSNLabeled", Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 8,ColumnName = Strings.ColName_Expense_MaterialsAndSupplies, InternalName = "MSNLabeled", Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 9,ColumnName = Strings.ColName_Expense_GeneralServices, InternalName = "GSNLabeled",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 10,ColumnName = Strings.ColName_Expense_TASO, InternalName = "TASONLabeled", Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 11,ColumnName = Strings.ColName_Expense_PublicInvestment, InternalName = "",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 12,ColumnName = Strings.ColName_Expense_RealState, InternalName = "BINLabeled",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 13,ColumnName = Strings.ColName_Expense_FinancialInvestments, InternalName = "INVNolaveled",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 14,ColumnName = Strings.ColName_Expense_ParticipationsContributions, InternalName = "PANLabeled", Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},
                        new CatalogueColumn {Index = 15,ColumnName = Strings.ColName_Expense_PublicDebt, InternalName = "PDNLabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true}
                    }},
                    new CatalogueColumn {Index = 16,ColumnName = Strings.ColName_Expense_TaggedExpense, InternalName = "TaggedExpense",Category = Enums.ColumnCategory.Expense, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn {Index = 17,ColumnName = Strings.ColName_Expense_PersonalServices, InternalName = "PSLabeled", Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 18,ColumnName = Strings.ColName_Expense_MaterialsAndSupplies, InternalName = "MSLabeled", Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 19,ColumnName = Strings.ColName_Expense_GeneralServices, InternalName = "GSLabeled",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 20,ColumnName = Strings.ColName_Expense_TASO, InternalName = "TASOLabeled",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 21,ColumnName = Strings.ColName_Expense_RealState, InternalName = "BILabeled",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 22,ColumnName = Strings.ColName_Expense_PublicInvestment, InternalName = "",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 23,ColumnName = Strings.ColName_Expense_FinancialInvestments, InternalName = "INVolaveled", Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 24,ColumnName = Strings.ColName_Expense_ParticipationsContributions, InternalName = "PALabeled",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn {Index = 25,ColumnName = Strings.ColName_Expense_PublicDebt, InternalName = "PDLabeled",Category = Enums.ColumnCategory.Expense, IsDefaultColumn = true},

                    }}
                }},

                // Deudas
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Debt_TotalDebt, InternalName = "TotalDebt", Category = Enums.ColumnCategory.Debt, Children = new List<CatalogueColumn>
                {
                    new CatalogueColumn { Index = 2, ColumnName = Strings.ColName_Debt_HistoricFixedRateDomesticCurrency, InternalName = "HistoricFixedRateDomesticCurrency",Category = Enums.ColumnCategory.Debt },
                    new CatalogueColumn { Index = 3, ColumnName = Strings.ColName_Debt_HistoricVariableRateDomesticCurrency,InternalName = "HistoricVariableRateDomesticCurrency", Category = Enums.ColumnCategory.Debt },
                } },

                 new CatalogueColumn { Index = 5, ColumnName = Strings.ColName_Debt_DebtService, InternalName = "DebtService", Category = Enums.ColumnCategory.Debt, Children = new List<CatalogueColumn>
                {
                        new CatalogueColumn { Index = 7, ColumnName = Strings.ColName_Debt_AmortizationDebtFixedRate, InternalName = "", Category = Enums.ColumnCategory.Debt},
                        new CatalogueColumn { Index = 8, ColumnName = Strings.ColName_Debt_AmortizationDebtVariableRate,InternalName = "", Category = Enums.ColumnCategory.Debt},
                } },
                 new CatalogueColumn { Index = 9, ColumnName = Strings.ColName_Debt_LocalRate, InternalName = "LocalRate", Category = Enums.ColumnCategory.Debt}
                }
            };
        }

        private Catalogue CreateEstandarCatalogue(OAuthUser user)
        {
            return new Catalogue()
            {
                IsActive = true,
                IsSubcatalogue = false,
                Name = Strings.Catalogue_Estandar_Name,
                Type = Enums.CatalogueType.Estandar,
                CreatedDate = DateTime.Now,
                CreatedById = user.Id,
                CreatedByName = user.Email,
                ModifiedById = user.Id,
                ModifiedByName = user.Email,
                ModifiedDate = DateTime.Now,
                CatalogueColumns = new List<CatalogueColumn>()
            {
                // Macroeconómicos
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Macroeconomy_ConsumerIndexPrice, InternalName = "ConsumerIndexPrice", Category = Enums.ColumnCategory.Macroeconomy },
                new CatalogueColumn { Index = 2, ColumnName = Strings.ColName_Macroeconomy_LocalChangeRate, InternalName = "LocalChangeRate",Category = Enums.ColumnCategory.Macroeconomy },

                // Ingresos
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Income_TotalIncome, InternalName = "TotalIncome",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true, Children = new List<CatalogueColumn>
                {
                    new CatalogueColumn { Index = 2, ColumnName = Strings.ColName_Income_AvailableIncome, InternalName = "AvailableIncome", Category = Enums.ColumnCategory.Income , IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn { Index = 3, ColumnName = Strings.ColName_Income_AvailableTax, InternalName = "AvailableTax",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true},
                        new CatalogueColumn { Index = 4, ColumnName = Strings.ColName_Income_AvailableContribution, InternalName = "AvailableContribution",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true},
                        new CatalogueColumn { Index = 5, ColumnName = Strings.ColName_Income_AvailableTransfer, InternalName = "AvailableTransfer",Category = Enums.ColumnCategory.Income , IsDefaultColumn = true},
                        new CatalogueColumn { Index = 6, ColumnName = Strings.ColName_Income_AvailableOther, InternalName = "AvailableOther", Category = Enums.ColumnCategory.Income}
                    }},
                    new CatalogueColumn { Index = 7, ColumnName = Strings.ColName_Income_NotAvailableIncome, InternalName = "NotAvailableIncome", Category = Enums.ColumnCategory.Income , IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn { Index = 8, ColumnName = Strings.ColName_Income_AvailableTax, InternalName = "NDImpuestos", Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn { Index = 9, ColumnName = Strings.ColName_Income_AvailableContribution, InternalName = "NDContribuciones",Category = Enums.ColumnCategory.Income},
                        new CatalogueColumn { Index = 10, ColumnName = Strings.ColName_Income_Transfers, InternalName = "NotAvailableTransfer", Category = Enums.ColumnCategory.Income, IsDefaultColumn = true},
                        new CatalogueColumn { Index = 11, ColumnName = Strings.ColName_Income_Others, InternalName = "NDOtros", Category = Enums.ColumnCategory.Income}
                    }}
                }},
                
                // Gastos
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Expense_TotalExpense, InternalName = "TotalExpense", Category = Enums.ColumnCategory.Expense , IsDefaultColumn = true, Children = new List<CatalogueColumn>
                {
                    new CatalogueColumn { Index = 2, ColumnName = Strings.ColName_Expense_EXPDefaultExpense, InternalName = "EXPDefaultExpense", Category = Enums.ColumnCategory.Expense, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn { Index = 3, ColumnName = Strings.ColName_Expense_Payroll ,InternalName = "EXPPayroll", Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn { Index = 4, ColumnName = Strings.ColName_Expense_GoodsAndServices, InternalName = "GoodsAndServices",Category = Enums.ColumnCategory.Expense , IsDefaultColumn = true},
                        new CatalogueColumn { Index = 5, ColumnName = Strings.ColName_Expense_Investment, InternalName = "EXPinvestment ",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn { Index = 6, ColumnName = Strings.ColName_Expense_Interests, InternalName = "Interests", Category = Enums.ColumnCategory.Expense , IsDefaultColumn = true},
                        new CatalogueColumn { Index = 7, ColumnName = Strings.ColName_Expense_Subsidies, InternalName = "FDESub",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn { Index = 8, ColumnName = Strings.ColName_Expense_Transfers, InternalName = "EXPTransfers",Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn { Index = 9, ColumnName = Strings.ColName_Expense_SocialBenefits, InternalName = "EXPSocialbeneficial", Category = Enums.ColumnCategory.Expense},
                        new CatalogueColumn { Index = 10, ColumnName = Strings.ColName_Expense_Others, InternalName = "EXPOther", Category = Enums.ColumnCategory.Expense},
                    }},
                    new CatalogueColumn { Index = 11, ColumnName = Strings.ColName_Expense_FreeDeterminationExpense,InternalName = "FreeDeterminationExpense", Category = Enums.ColumnCategory.Expense , IsDefaultColumn = true, Children = new List<CatalogueColumn>
                    {
                        new CatalogueColumn { Index = 12, ColumnName = Strings.ColName_Expense_Payroll, InternalName = "FDEPayroll", Category = Enums.ColumnCategory.Expense },
                        new CatalogueColumn { Index = 13, ColumnName = Strings.ColName_Expense_GoodsAndServices, InternalName = "FDEGoodsAndServices",Category = Enums.ColumnCategory.Expense },
                        new CatalogueColumn { Index = 14, ColumnName = Strings.ColName_Expense_Investment, InternalName = "FDEinvestment ",Category = Enums.ColumnCategory.Expense },
                        new CatalogueColumn { Index = 15, ColumnName = Strings.ColName_Expense_Interests, InternalName = "FDEInterests", Category = Enums.ColumnCategory.Expense },
                        new CatalogueColumn { Index = 16, ColumnName = Strings.ColName_Expense_Subsidies, InternalName = "FDESub",Category = Enums.ColumnCategory.Expense },
                        new CatalogueColumn { Index = 17, ColumnName = Strings.ColName_Expense_Transfers, InternalName = "FDETransfers",Category = Enums.ColumnCategory.Expense },
                        new CatalogueColumn { Index = 18, ColumnName = Strings.ColName_Expense_SocialBenefits, InternalName = "FDESocialbeneficial", Category = Enums.ColumnCategory.Expense },
                        new CatalogueColumn { Index = 19, ColumnName = Strings.ColName_Income_Others, InternalName = "FDEOther", Category = Enums.ColumnCategory.Expense},
                    }}

                }},
                
                // Deudas
                new CatalogueColumn { Index = 1, ColumnName = Strings.ColName_Debt_TotalDebt, InternalName = "TotalDebt", Category = Enums.ColumnCategory.Debt , Children = new List<CatalogueColumn>
                {
                    new CatalogueColumn { Index = 2, ColumnName = Strings.ColName_Debt_HistoricVariableRateDomesticCurrency, InternalName = "HistoricVariableRateDomesticCurrency", Category = Enums.ColumnCategory.Debt },
                    new CatalogueColumn { Index = 3, ColumnName = Strings.ColName_Debt_HistoricVariableRateForeignCurrency, InternalName = "HistoricVariableRateForeignCurrency",Category = Enums.ColumnCategory.Debt},
                    new CatalogueColumn { Index = 4, ColumnName = Strings.ColName_Debt_HistoricFixedRateDomesticCurrency, InternalName = "HistoricFixedRateDomesticCurrency",Category = Enums.ColumnCategory.Debt},
                    new CatalogueColumn { Index = 5, ColumnName = Strings.ColName_Debt_HistoricFixedRateForeignCurrency, InternalName = "HistoricFixedRateForeignCurrency", Category = Enums.ColumnCategory.Debt}
                } },
                new CatalogueColumn { Index = 6, ColumnName = Strings.ColName_Debt_LocalRate, InternalName = "LocalRate", Category = Enums.ColumnCategory.Debt},
                new CatalogueColumn { Index = 7, ColumnName = Strings.ColName_Debt_ForeignRate, InternalName = "ForeignRate", Category = Enums.ColumnCategory.Debt},

                new CatalogueColumn { Index = 8, ColumnName = Strings.ColName_Debt_Service, InternalName = "DebtService", Category = Enums.ColumnCategory.Debt ,  Children = new List<CatalogueColumn>
                {
                        new CatalogueColumn { Index = 10, ColumnName = Strings.ColName_Debt_LocalCurrencyAmortizationFixeRate, InternalName = "", Category = Enums.ColumnCategory.Debt},
                        new CatalogueColumn { Index = 11, ColumnName = Strings.ColName_Debt_LocalCurrencyAmortizationVariableRate,InternalName = "", Category = Enums.ColumnCategory.Debt},
                        new CatalogueColumn { Index = 12, ColumnName = Strings.ColName_Debt_ForeignCurrencyAmortizationFixeRate,InternalName = "", Category = Enums.ColumnCategory.Debt},
                        new CatalogueColumn { Index = 13, ColumnName = Strings.ColName_Debt_ForeignCurrencyAmortizationVariableRate,InternalName = "", Category = Enums.ColumnCategory.Debt},
                } }
            }
            };
        }
    }
}
