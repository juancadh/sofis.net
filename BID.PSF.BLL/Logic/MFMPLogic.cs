﻿using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.Core.Enums;
using BID.PSF.Core.Helpers;
using BID.PSF.Core.Resources;
using BID.PSF.DAL.Repositories.Interfaces;
using BID.PSF.Model.ApiModel;
using BID.PSF.Model.DBModel;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web.Configuration;

namespace BID.PSF.BLL.Logic
{
    // ReSharper disable once InconsistentNaming
    public class MFMPLogic : IMFMPLogic
    {
        private readonly ICollectionRepository<CatalogueColumn> _catalogueColumnsRepository;
        private readonly ICollectionRepository<DataSet> _datasetRepository;
        private readonly ICollectionRepository<CatalogueColumnValue> _catalogueColumnValuesRepository;
        private readonly ICollectionRepository<Rule> _rulesRepository;
        private readonly ICollectionRepository<UserParameter> _userParameterRepository;
        private readonly ICollectionRepository<UserRule> _userRuleRepository;
        private readonly IUnityContainer _container;

        private readonly IDataSetsLogic _dataSetsLogic;

        private readonly IOAuthLogic _authLogic;

        private readonly List<string> _internalNames = new List<string>()
        {
            Enums.IncomeColumns.TotalIncome.ToString(),
            Enums.IncomeColumns.AvailableIncome.ToString(),
            Enums.IncomeColumns.NotAvailableIncome.ToString(),
            Enums.ExpenseColumns.TotalExpense.ToString(),
            Enums.ExpenseColumns.FreeDestinationIncome.ToString(),
            Enums.ExpenseColumns.EXPDefaultExpense.ToString(),
            Enums.MacroeconomicsColumns.ConsumerIndexPrice.ToString(),
            Enums.MacroeconomicsColumns.LocalChangeRate.ToString(),
            Enums.DebtColumns.LocalRate.ToString()
        };

        public MFMPLogic(IUnityContainer container, ICollectionRepository<CatalogueColumnValue> catalogueColumnValuesRepository, ICollectionRepository<Rule> rulesRepository, ICollectionRepository<CatalogueColumn> catalogueColumnsRepository, ICollectionRepository<UserParameter> userParameterRepository, IDataSetsLogic dataSetsLogic, ICollectionRepository<UserRule> userRuleRepository, IOAuthLogic authLogic, ICollectionRepository<DataSet> datasetRepository)
        {
            _container = container;
            _catalogueColumnValuesRepository = catalogueColumnValuesRepository;
            _rulesRepository = rulesRepository;
            _catalogueColumnsRepository = catalogueColumnsRepository;
            _userParameterRepository = userParameterRepository;
            _dataSetsLogic = dataSetsLogic;
            _userRuleRepository = userRuleRepository;
            _authLogic = authLogic;
            _datasetRepository = datasetRepository;
        }

        public ApiResult InertialApproximation(string userId, int catalogueId, int yearsToSimulate, bool fiveYears = false)
        {
            
            var user = _authLogic.FindUserByEmail(userId);

            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(user.Language.EncodedName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            SetHistoricYear(dataSet);

            if (_catalogueColumnValuesRepository.List(c => c.CatalogueColumn.CatalogueId == dataSet.CatalogueId)
                        .Where(v => dataSet.YearDatas.Select(y => y.Id)
                        .Contains(v.YearDataId))
                        .Count() == 0)
                return new ApiResult
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Result = "NotData"
                };
            if (MissingData(dataSet, _internalNames))
                return new ApiResult
                {
                    Message = GetMissingDataError(dataSet.CatalogueId, _internalNames.ToArray()),
                    StatusCode = HttpStatusCode.InternalServerError,
                    Result = "MissingData"
                };

            var tableStructure = new List<HandsOnTableColumn>();

            // Key is column Id in both dictionaries
            var calculatedData = new SortedDictionary<int, SortedDictionary<int, decimal>>();
            var tableData = new List<dynamic>();
            //Para poder darle el orden solicitado en en pantalla, se guardan los datos de la deuda en una tabla aparte y se incorporan al fin de la tabla principal
            var debtTable = new List<dynamic>();
            var yearsToSend = fiveYears ? 5 : dataSet.HistoricTo.Value - dataSet.HistoricFrom.Value;

            DatosIngresos(tableData, calculatedData, dataSet, yearsToSend);
            DatosGastos(tableData, calculatedData, dataSet, yearsToSend);
            DatosBalancePrimario(dataSet.CatalogueId, tableData, calculatedData);
            DatosDeudas(debtTable, calculatedData, dataSet, yearsToSend);
            DatosBalanceFiscal(dataSet.CatalogueId, tableData, calculatedData);

            tableData.AddRange(debtTable);

            tableStructure.Add(new HandsOnTableColumn("Account", "Cuenta/Año", false) { Width = null, ReadOnly = true, ClassName = "text-left" });

            var maxYear = dataSet.HistoricTo + yearsToSimulate;
            var minYear = dataSet.HistoricTo - 2;

            if (maxYear > 0)
            {
                for (var i = minYear; i <= maxYear; i++)
                {
                    tableStructure.Add(new HandsOnTableColumn(i.ToString(), i.ToString(), 150, true, false) { Type = "numeric", Format = "0.0,00" });
                }
            }
            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = new { Structure = tableStructure, Data = tableData, BasicColumns = yearsToSend, CatalogueType = dataSet.Catalogue.Type}
            };
        }

        public ApiResult GetFirstProjectionYear(string userId, int catalogueId)
        {
            var yearsDataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId)?.ProjectionFrom.Value;
            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = yearsDataSet
            };
        }


        private SortedDictionary<int, decimal> ChildSum(CatalogueColumn column, DataSet dataset,
            int yearsToShow, ref SortedDictionary<int, SortedDictionary<int, decimal>> tableData, Dictionary<int, decimal> fixedVariation = null, Dictionary<int, Dictionary<int, decimal>> yearVariation = null)
        {
            if (tableData.ContainsKey(column.Id))
                return tableData[column.Id];

            var res = new SortedDictionary<int, decimal>();
            if (column.Children != null && column.Children.Any())
            {
                var results = new List<SortedDictionary<int, decimal>>();
                foreach (var c in column.Children)
                {
                    if (fixedVariation != null)
                        results.Add(ChildSum(c, dataset, yearsToShow,
                            ref tableData, fixedVariation));
                    else if (yearVariation != null)
                        results.Add(ChildSum(c, dataset, yearsToShow,
                            ref tableData, null, yearVariation));
                    else
                        results.Add(ChildSum(c, dataset, yearsToShow,
                            ref tableData));
                }

                foreach (var r in results)
                {
                    foreach (var k in r.Keys)
                        if (res.ContainsKey(k))
                            res[k] += r[k];
                        else
                            res.Add(k, r[k]);
                }
            }
            else
            {
                res = ColumnInertialApproximation(dataset, column.Id, yearsToShow);
            }

            tableData.Add(column.Id, res);
            return res;
        }

        private SortedDictionary<int, decimal> ColumnInertialApproximation(DataSet dataset, int columnId, int yearsToShow)
        {
            // List of Tuple<year, value> used to simulate data
            var valuesArray = new SortedDictionary<int, decimal>();
            var catalogue = _container.Resolve<ICollectionRepository<CatalogueColumnValue>>();
            var maxYear = dataset.HistoricTo.Value;
            var minYear = maxYear - (yearsToShow - 1);
            var values = catalogue.List(v => v.CatalogueColumnId == columnId && v.YearData.DataSetId == dataset.Id).GroupBy(y => y.YearDataId).Select(g => g.First()).OrderBy(v => v.YearData.Year).ToList();

            for (var i = minYear; i <= maxYear; i++)
            {
                if (values.FirstOrDefault(v => v.YearData.Year == i) != null)
                {
                    valuesArray.Add(i, values.FirstOrDefault(v => v.YearData.Year == i).Value);
                }
                else
                {
                    valuesArray.Add(i, 0);
                }
            }

            return valuesArray;
        }

        public ApiResult GetFixedApproximationStructure(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var tableData = new List<dynamic>();
            var catalogueColumns = GetCatalogueColumns(dataSet);

            foreach (var col in catalogueColumns)
            {
                var userParameter = _userParameterRepository.List(u => u.Email == userId && u.CatalogueColumnId == col.Id).FirstOrDefault();

                dynamic column = new ExpandoObject();
                column.Account = col.ColumnName;
                column.ColumnId = col.Id;
                column.Category = col.Category;
                column.InternalName = col.InternalName != null && Enum.IsDefined(typeof(Enums.MacroeconomicsColumns), col.InternalName) ? Enum.Parse(typeof(Enums.MacroeconomicsColumns), col.InternalName) : null;
                column.Parameter = userParameter?.Parameter ?? 0;
                column.Level = GetLevel(col, catalogueColumns.ToList());

                column.Autocalculated = col.Children != null && col.Children.Any();

                tableData.Add(column);
            }

            var tableStructure = new List<HandsOnTableColumn>
            {
                new HandsOnTableColumn("Account", "Cuenta", false) {Width = null, ReadOnly = true, ClassName = "text-left"},
                new HandsOnTableColumn("Parametro", "Paramétrico Fijo",false) {Width = null}
            };

            dynamic data = new ExpandoObject();
            data.Catalogue = dataSet.CatalogueId;
            data.Data = tableData;
            data.CatalogueType = dataSet.Catalogue.Type;

            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = new { Structure = tableStructure, Data = data, CatalogueType = dataSet.Catalogue.Type}
            };
        }

        private IEnumerable<CatalogueColumn> GetCatalogueColumns(DataSet dataSet)
        {
            return _catalogueColumnsRepository.List(x => x.CatalogueId == dataSet.CatalogueId).OrderBy(c => c.Category).ThenBy(c => c.Index).ToList();
        }

        public ApiResult GetVariableApproximationStructure(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            SetHistoricYear(dataSet);

            var tableData = new List<dynamic>();
            var catalogueColumns = GetCatalogueColumns(dataSet);
            catalogueColumns = catalogueColumns.OrderBy(c => c.Category);
            foreach (var col in catalogueColumns)
            {
                var userParameter = _userParameterRepository.List(u => u.Email == userId && u.CatalogueColumnId == col.Id && u.Year != null);

                dynamic column = new ExpandoObject();
                column.Account = col.ColumnName;
                column.ColumnId = col.Id;
                column.Parameters = new List<decimal>();
                column.Level = GetLevel(col, catalogueColumns.ToList());
                column.Category = col.Category;

                foreach (var item in userParameter)
                {
                    column.Parameters.Add(item.Parameter);
                }

                column.Autocalculated = col.Children != null && col.Children.Any();

                tableData.Add(column);
            }

            var tableStructure = new List<HandsOnTableColumn>
            {
                new HandsOnTableColumn("Account", "Cuenta", false) {Width = null, ReadOnly = true, ClassName = "text-left"},
            };

            var values = _catalogueColumnValuesRepository.List()
                        .Where(v => dataSet.YearDatas
                        .Select(y => y.Id).Contains(v.YearDataId) && v.YearData.DataSetId == dataSet.Id && v.YearData.Year <= dataSet.HistoricTo)
                        .ToArray();

            if (values.Any())
            {
                var maxYear = values.Where(v => v.Value != 0).Max(v => v.YearData.Year);

                for (var i = maxYear + 1; i <= maxYear + 5; i++)
                {
                    tableStructure.Add(new HandsOnTableColumn(i.ToString(), i.ToString(), 150, false, false));
                }
            }

            dynamic data = new ExpandoObject();
            data.Catalogue = dataSet.CatalogueId;
            data.Data = tableData;
            data.CatalogueType = dataSet.Catalogue.Type;

            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = new { Structure = tableStructure, Data = data }
            };
        }

        public ApiResult GetRulesStructure(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var tableData = new List<dynamic>();

            try
            {
                var columns = _rulesRepository.List().ToList();

                foreach (var col in columns)
                {
                    var userRule = _userRuleRepository.List(u => u.Email == userId && u.RuleId == col.Id).FirstOrDefault();

                    dynamic column = new ExpandoObject();
                    column.Rule = col.Id;

                    if (userRule != null)
                    {
                        column.Limit = userRule.Limits;
                    }
                    else
                    {
                        column.Limit = 0.00F;
                    }

                    tableData.Add(column);
                }
            }
            catch (Exception ex)
            {
                return new ApiResult
                {
                    StatusCode = HttpStatusCode.Conflict,
                    Data = ex.Message
                };
            }

            var tableStructure = new List<HandsOnTableColumn>
            {
                new HandsOnTableColumn("Indicator", "Indicador", false) {Width = null, ReadOnly = true, ClassName = "text-left"},
                new HandsOnTableColumn("Limits", "Límites", false) {Width = null, ReadOnly = true, ClassName = "text-left"},
            };

            dynamic data = new ExpandoObject();
            data.Catalogue = dataSet.CatalogueId;
            data.Data = tableData;

            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = new { Structure = tableStructure, Data = data }
            };
        }

        public ApiResult GetRulesCharts(string userId, int catalogueId)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            var result = new ApiResult();
            dynamic data = new ExpandoObject();

            var rules = new List<Dictionary<int, decimal>>();

            var ruleOne = GetRuleOneDictionary(dataSet);
            var ruleTwo = GetRuleTwoDictionary(dataSet);
            var ruleThree = GetRuleThreeDictionary(dataSet);
            var ruleFour = GetRuleFourDictionary(dataSet);
            var ruleFive = GetRuleFiveDictionary(dataSet);
            var ruleSix = GetRuleSixDictionary(dataSet);
            var ruleSeven = GetRuleSevenDictionary(dataSet);
            var ruleEight = GetRuleEightDictionary(dataSet);
            var ruleNine = GetRuleNineDictionary(dataSet);

            rules.Add(ruleOne);
            rules.Add(ruleTwo);
            rules.Add(ruleThree);
            rules.Add(ruleFour);
            rules.Add(ruleFive);
            rules.Add(ruleSix);
            rules.Add(ruleSeven);
            rules.Add(ruleEight);
            rules.Add(ruleNine);

            data.Data = rules;

            result.Data = data;
            result.StatusCode = HttpStatusCode.OK;
            return result;
        }

        public ApiResult SaveLimits(string userId, int catalogueId, UserRule userRule)
        {
            var dataSet = _dataSetsLogic.GetUserDataSet(userId, catalogueId);

            if (dataSet == null)
                return new ApiResult { StatusCode = HttpStatusCode.InternalServerError };

            userRule.Email = userId;
            var data = _userRuleRepository.List(r => r.Email == userId && r.RuleId == userRule.RuleId).FirstOrDefault();

            if (data == null)
            {
                _userRuleRepository.Add(userRule);
            }
            else
            {
                data.Limits = userRule.Limits;
                _userRuleRepository.Update(data);
            }


            var result = new ApiResult { StatusCode = HttpStatusCode.OK };
            return result;
        }

        private bool MissingData(DataSet dataSet, List<string> internalNameList)
        {
            List<int> columnIdList = new List<int>();
            foreach (var internalName in internalNameList)
            {
                var id = GetColumnId(dataSet.CatalogueId, internalName);
                if (id != 0)
                {
                    columnIdList.Add(id);
                }

            }

            foreach (var id in columnIdList)
            {
                if (_catalogueColumnValuesRepository.List(c => c.CatalogueColumnId == id)
                        .Where(v => dataSet.YearDatas.Select(y => y.Id)
                        .Contains(v.YearDataId))
                        .ToDictionary(item => item.YearData.Year, item => item.Value).Count() == 0)
                {
                    return true;
                }
            }

            return false;
        }

        private string GetMissingDataError(int catalogueId, params string[] internalNames)
        {
            var displayNames = (from internalName in internalNames select _catalogueColumnsRepository.List(c => c.InternalName == internalName && c.CatalogueId == catalogueId).FirstOrDefault() into col where col != null select col.ColumnName).ToList();

            return string.Join(", ", displayNames);
        }

        #region Reglas

        private Dictionary<int, decimal> GetRuleOneDictionary(DataSet dataSet)
        {
            // Regla 1 - Gastos Operacionales / Ingreso Disponible
            // Gastos Operacionales =  (ESTANDAR)Nómina + Uso de Bienes y Servicios | (MX) Servicios personales etiquetados y no etiquetados

            var dictionary = new Dictionary<int, decimal>();

            // Test
            var rule = _rulesRepository.List(r => r.Id == 1).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule != null)
            {
                var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);
                var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
                var taggetPersonalServiceId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.PSLabeled.ToString());
                var untaggetPersonalServiceId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.PSNLabeled.ToString());
                var taggetMaterialsAndSuppliesId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.MSLabeled.ToString());
                var untaggetMaterialsAndSuppliesId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.MSNLabeled.ToString());
                var taggetGeneralServiceId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.GSLabeled.ToString());
                var untaggetGeneralServiceId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.GSNLabeled.ToString());

                var totalIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totalIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var totaltaggetPersonalServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == taggetPersonalServiceId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaltaggetPersonalServiceValues, yearsToProyect, dataSet.CatalogueId);

                var totaluntaggetPersonalServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == untaggetPersonalServiceId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaluntaggetPersonalServiceValues, yearsToProyect, dataSet.CatalogueId);

                var totaltaggetMaterialsAndSuppliesValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == taggetMaterialsAndSuppliesId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaltaggetMaterialsAndSuppliesValues, yearsToProyect, dataSet.CatalogueId);

                var totaluntaggetMaterialsAndSuppliesValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == untaggetMaterialsAndSuppliesId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaluntaggetMaterialsAndSuppliesValues, yearsToProyect, dataSet.CatalogueId);

                var totaltaggetGeneralServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == taggetGeneralServiceId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaltaggetGeneralServiceValues, yearsToProyect, dataSet.CatalogueId);

                var totaluntaggetGeneralServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == untaggetGeneralServiceId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaluntaggetGeneralServiceValues, yearsToProyect, dataSet.CatalogueId);

                if (totalIncomeValues.Any())
                {
                    // Para todos los años ingresados, realizar el cálculo (máximo de años 10 para históricos)
                    var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
                    var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;



                    for (var i = minYear; i <= maxYear; i++)
                    {
                        decimal percent = 0;
                        decimal val = 0;
                        decimal totalExpenseValues = (totaltaggetPersonalServiceValues.TryGetValue(i, out val) ? totaltaggetPersonalServiceValues[i] : 0)
                                                + (totaluntaggetPersonalServiceValues.TryGetValue(i, out val) ? totaluntaggetPersonalServiceValues[i] : 0)
                                                + (totaltaggetGeneralServiceValues.TryGetValue(i, out val) ? totaltaggetGeneralServiceValues[i] : 0)
                                                + (totaltaggetMaterialsAndSuppliesValues.TryGetValue(i, out val) ? totaltaggetMaterialsAndSuppliesValues[i] : 0)
                                                + (totaluntaggetMaterialsAndSuppliesValues.TryGetValue(i, out val) ? totaluntaggetMaterialsAndSuppliesValues[i] : 0)
                                                + (totaluntaggetGeneralServiceValues.TryGetValue(i, out val) ? totaluntaggetGeneralServiceValues[i] : 0);

                        if (totalIncomeValues.TryGetValue(i, out val) && totalIncomeValues[i] != 0 && totalExpenseValues != 0)
                        {
                            percent = (totalExpenseValues / totalIncomeValues[i]) * 100;
                        }

                        dictionary.Add(i, percent);
                    }

                }

            }

            return dictionary;
        }

        private Dictionary<int, decimal> GetRuleTwoDictionary(DataSet dataSet)
        {
            // Regla 2 - Cambio en Gastos Operacionales - Cambio en Ingreso Disponible
            // (Gastos Operacionales en t / Gastos Operacionales en t-1) - (Ingreso Disponible en t / Ingreso Disponible en t-1)

            var dictionary = new Dictionary<int, decimal>();
            var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);

            // Test
            var rule = _rulesRepository.List(r => r.Id == 2).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule != null)
            {
                var incomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
                var taggetPersonalServiceId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.PSLabeled.ToString());
                var untaggetPersonalServiceId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.PSNLabeled.ToString());
                var taggetMaterialsAndSuppliesId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.MSLabeled.ToString());
                var untaggetMaterialsAndSuppliesId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.MSNLabeled.ToString());
                var taggetGeneralServiceId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.GSLabeled.ToString());
                var untaggetGeneralServiceId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.GSNLabeled.ToString());


                var totalIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == incomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totalIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var totaltaggetPersonalServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == taggetPersonalServiceId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaltaggetPersonalServiceValues, yearsToProyect, dataSet.CatalogueId);

                var totaluntaggetPersonalServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == untaggetPersonalServiceId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaluntaggetPersonalServiceValues, yearsToProyect, dataSet.CatalogueId);

                var totaltaggetMaterialsAndSuppliesValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == taggetMaterialsAndSuppliesId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaltaggetMaterialsAndSuppliesValues, yearsToProyect, dataSet.CatalogueId);

                var totaluntaggetMaterialsAndSuppliesValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == untaggetMaterialsAndSuppliesId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaluntaggetMaterialsAndSuppliesValues, yearsToProyect, dataSet.CatalogueId);

                var totaltaggetGeneralServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == taggetGeneralServiceId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaltaggetGeneralServiceValues, yearsToProyect, dataSet.CatalogueId);

                var totaluntaggetGeneralServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == untaggetGeneralServiceId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totaluntaggetGeneralServiceValues, yearsToProyect, dataSet.CatalogueId);


                if (totalIncomeValues.Any())
                {
                    // Para todos los años ingresados, realizar el cálculo (máximo de años 10 para históricos)
                    var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
                    var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;


                    decimal val;

                    for (var i = minYear; i <= maxYear; i++)
                    {
                        decimal percent = 0;

                        decimal totalExpenseValues = (totaltaggetPersonalServiceValues.TryGetValue(i, out val) ? totaltaggetPersonalServiceValues[i] : 0)
                                                   + (totaluntaggetPersonalServiceValues.TryGetValue(i, out val) ? totaluntaggetPersonalServiceValues[i] : 0)
                                                   + (totaluntaggetPersonalServiceValues.TryGetValue(i, out val) ? totaluntaggetPersonalServiceValues[i] : 0)
                                                   + (totaltaggetMaterialsAndSuppliesValues.TryGetValue(i, out val) ? totaltaggetMaterialsAndSuppliesValues[i] : 0)
                                                   + (totaluntaggetMaterialsAndSuppliesValues.TryGetValue(i, out val) ? totaluntaggetMaterialsAndSuppliesValues[i] : 0)
                                                   + (totaluntaggetGeneralServiceValues.TryGetValue(i, out val) ? totaluntaggetGeneralServiceValues[i] : 0);

                        decimal totalExpenseValuesLastYear = (totaltaggetPersonalServiceValues.TryGetValue(i - 1, out val) ? totaltaggetPersonalServiceValues[i - 1] : 0)
                                                    + (totaluntaggetPersonalServiceValues.TryGetValue(i - 1, out val) ? totaluntaggetPersonalServiceValues[i - 1] : 0)
                                                    + (totaluntaggetPersonalServiceValues.TryGetValue(i - 1, out val) ? totaluntaggetPersonalServiceValues[i - 1] : 0)
                                                    + (totaltaggetMaterialsAndSuppliesValues.TryGetValue(i - 1, out val) ? totaltaggetMaterialsAndSuppliesValues[i - 1] : 0)
                                                    + (totaluntaggetMaterialsAndSuppliesValues.TryGetValue(i - 1, out val) ? totaluntaggetMaterialsAndSuppliesValues[i - 1] : 0)
                                                    + (totaluntaggetGeneralServiceValues.TryGetValue(i - 1, out val) ? totaluntaggetGeneralServiceValues[i - 1] : 0);

                        if (totalIncomeValues.TryGetValue(i, out val)
                            && totalIncomeValues[i] != 0
                            && totalIncomeValues.TryGetValue(i - 1, out val)
                            && totalIncomeValues[i - 1] != 0
                            && totalExpenseValues != 0
                            && totalExpenseValuesLastYear != 0)
                        {
                            percent = (totalExpenseValues / totalExpenseValuesLastYear - totalIncomeValues[i] / totalIncomeValues[i - 1]) * 100;
                        }

                        dictionary.Add(i, percent);
                    }
                }

            }

            return dictionary;
        }

        private Dictionary<int, decimal> GetRuleThreeDictionary(DataSet dataSet)
        {
            // Regla 3 - Balance Primario / Ingreso Disponible
            // Balance Primario = Ingreso Total - Gasto Total + Intereses

            var dictionary = new Dictionary<int, decimal>();
            var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);

            // Test
            var rule = _rulesRepository.List(r => r.Id == 3).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule != null)
            {
                var totalIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.TotalIncome.ToString());
                var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
                var expenseColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.TotalExpense.ToString());

                //Intereses
                var pdnLabeledId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.PDNLabeled.ToString());
                var psLabeledId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.PSLabeled.ToString());
                var fdeInterestsId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.FDEInterests.ToString());
                var interestsId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.Interests.ToString());

                var totalIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == totalIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totalIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var availableIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(availableIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var totalExpenseValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == expenseColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totalExpenseValues, yearsToProyect, dataSet.CatalogueId);

                //Intereses
                var pdnLabeledValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == pdnLabeledId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(pdnLabeledValues, yearsToProyect, dataSet.CatalogueId);

                var psLabeledValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == psLabeledId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(psLabeledValues, yearsToProyect, dataSet.CatalogueId);

                var fdeInterestsValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == fdeInterestsId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(fdeInterestsValues, yearsToProyect, dataSet.CatalogueId);

                var interestsValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == interestsId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(interestsValues, yearsToProyect, dataSet.CatalogueId);

                if (availableIncomeValues.Any())
                {
                    // Para todos los años ingresados, realizar el cálculo (máximo de años 10 para históricos)
                    var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
                    var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;


                    decimal val;

                    for (var i = minYear; i <= maxYear; i++)
                    {
                        decimal percent = 0;

                        var totalinterest = (pdnLabeledValues.TryGetValue(i, out val) ? pdnLabeledValues[i] : 0)
                                          + (psLabeledValues.TryGetValue(i, out val) ? psLabeledValues[i] : 0)
                                          + (fdeInterestsValues.TryGetValue(i, out val) ? fdeInterestsValues[i] : 0)
                                          + (interestsValues.TryGetValue(i, out val) ? interestsValues[i] : 0);

                        var balance = (totalIncomeValues.TryGetValue(i, out val) ? totalIncomeValues[i] : 0)
                                    - (totalExpenseValues.TryGetValue(i, out val) ? totalExpenseValues[i] : 0)
                                    + totalinterest;

                        if (availableIncomeValues.TryGetValue(i, out val)
                            && availableIncomeValues[i] != 0
                            && balance != 0)
                        {
                            percent = (balance / availableIncomeValues[i]) * 100;
                        }

                        dictionary.Add(i, percent);
                    }

                }


            }

            return dictionary;
        }

        private Dictionary<int, decimal> GetRuleFourDictionary(DataSet dataSet)
        {
            // Regla 4 - Balance Fiscal / Ingreso Disponible
            // Balance Fiscal = Ingreso - Gasto total
            var dictionary = new Dictionary<int, decimal>();
            var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);

            // Test
            var rule = _rulesRepository.List(r => r.Id == 4).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule != null)
            {
                var totalIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.TotalIncome.ToString());
                var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
                var expenseColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.TotalExpense.ToString());

                var totalIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == totalIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totalIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var availableIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(availableIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var totalExpenseValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == expenseColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totalExpenseValues, yearsToProyect, dataSet.CatalogueId);

                if (availableIncomeValues.Any())
                {
                    // Para todos los años ingresados, realizar el cálculo (máximo de años 10 para históricos)
                    var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
                    var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;


                    decimal val;

                    for (var i = minYear; i <= maxYear; i++)
                    {
                        decimal percent = 0;
                        var value = (totalIncomeValues.TryGetValue(i, out val) ? totalIncomeValues[i] : 0)
                                  - (totalExpenseValues.TryGetValue(i, out val) ? totalExpenseValues[i] : 0);
                        if (availableIncomeValues.TryGetValue(i, out val) && availableIncomeValues[i] != 0 && value != 0)
                        {
                            percent = (value / availableIncomeValues[i]) * 100;
                        }

                        dictionary.Add(i, percent);
                    }
                }

                // TODO: Cálculo de valores futuros
            }

            return dictionary;
        }

        private Dictionary<int, decimal> GetRuleFiveDictionary(DataSet dataSet)
        {
            // Regla 5 - (Balance Fiscal - Inversión) / Ingreso Disponible
            // Balance Fiscal = Ingreso total - Gasto total
            // (MX) Inversión = Bienes Muebles, Inmuebles e Intangibles  + Inversión Pública + Inversiones Financieras y Otras Provisiones
            // (ST) Inversión = Columna inversión
            var dictionary = new Dictionary<int, decimal>();
            var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);

            // Test
            var rule = _rulesRepository.List(r => r.Id == 5).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule != null)
            {
                var totalIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.TotalIncome.ToString());
                var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
                var expenseColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.TotalExpense.ToString());

                var investment = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.Investment.ToString());
                var binLabeled = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.BINLabeled.ToString());
                var biLabeled = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.BILabeled.ToString());
                var invNoLaveled = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.INVNolaveled.ToString());
                var invLaveled = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.INVolaveled.ToString());
                var fdeInvestment = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.FDEinvestment.ToString());
                var expInvestment = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.EXPinvestment.ToString());
                var labeledPublicInvestment = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.LabeledPublicInvestment.ToString());
                var unlabeledPublicInvestment = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.UnlabeledPublicInvestment.ToString());


                var totalIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == totalIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totalIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var availableIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(availableIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var totalExpenseValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == expenseColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(totalExpenseValues, yearsToProyect, dataSet.CatalogueId);

                var investmentValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == investment).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(investmentValues, yearsToProyect, dataSet.CatalogueId);

                var binLabeledValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == binLabeled).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(binLabeledValues, yearsToProyect, dataSet.CatalogueId);

                var biLabeledValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == biLabeled).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(biLabeledValues, yearsToProyect, dataSet.CatalogueId);

                var invNoLaveledValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == invNoLaveled).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(invNoLaveledValues, yearsToProyect, dataSet.CatalogueId);

                var invLaveledValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == invLaveled).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(invLaveledValues, yearsToProyect, dataSet.CatalogueId);

                var fdeInvestmentValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == fdeInvestment).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(fdeInvestmentValues, yearsToProyect, dataSet.CatalogueId);

                var expInvestmentValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == expInvestment).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(expInvestmentValues, yearsToProyect, dataSet.CatalogueId);

                var labeledPublicInvestmentValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == labeledPublicInvestment).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(labeledPublicInvestmentValues, yearsToProyect, dataSet.CatalogueId);

                var unlabeledPublicInvestmentValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == unlabeledPublicInvestment).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(unlabeledPublicInvestmentValues, yearsToProyect, dataSet.CatalogueId);

                if (availableIncomeValues.Any())
                {

                    // Para todos los años ingresados, realizar el cálculo (máximo de años 10 para históricos)
                    var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
                    var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;

                    for (var i = minYear; i <= maxYear; i++)
                    {
                        decimal percent = 0;
                        decimal val;
                        var balance = (totalIncomeValues.TryGetValue(i, out val) ? totalIncomeValues[i] : 0)
                                      - (totalExpenseValues.TryGetValue(i, out val) ? totalExpenseValues[i] : 0);

                        var totalInvestment = (investmentValues.TryGetValue(i, out val) ? investmentValues[i] : 0)
                                              + (binLabeledValues.TryGetValue(i, out val) ? binLabeledValues[i] : 0)
                                              + (biLabeledValues.TryGetValue(i, out val) ? biLabeledValues[i] : 0)
                                              + (invNoLaveledValues.TryGetValue(i, out val) ? invNoLaveledValues[i] : 0)
                                              + (invLaveledValues.TryGetValue(i, out val) ? invLaveledValues[i] : 0)
                                              + (fdeInvestmentValues.TryGetValue(i, out val) ? fdeInvestmentValues[i] : 0)
                                              + (expInvestmentValues.TryGetValue(i, out val) ? expInvestmentValues[i] : 0)
                                              + (labeledPublicInvestmentValues.TryGetValue(i, out val) ? labeledPublicInvestmentValues[i] : 0)
                                              + (unlabeledPublicInvestmentValues.TryGetValue(i, out val) ? unlabeledPublicInvestmentValues[i] : 0);

                        var value = balance - totalInvestment;

                        if (value != 0 && availableIncomeValues.TryGetValue(i, out val) && availableIncomeValues[i] != 0)
                        {
                            percent = (value / availableIncomeValues[i]) * 100;
                        }

                        dictionary.Add(i, percent);
                    }
                }

                // TODO: Cálculo de valores futuros
            }

            return dictionary;
        }

        private Dictionary<int, decimal> GetRuleSixDictionary(DataSet dataSet)
        {
            // Regla 6 - Servicio de la Deuda / Ingreso Disponible

            var dictionary = new Dictionary<int, decimal>();
            var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);

            // Test
            var rule = _rulesRepository.List(r => r.Id == 6).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule != null)
            {
                var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
                var localCurrencyFixeRateId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.LocalCurrencyAmortizationFixeRate.ToString());
                var localCurrencyVariableRateId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.LocalCurrencyAmortizationVariableRate.ToString());
                var foreignCurrencyFixeRateId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.ForeignCurrencyAmortizationFixeRate.ToString());
                var foreignCurrencyVariableRateId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.ForeignCurrencyAmortizationVariableRate.ToString());
                var amortizationDebtFixedRateId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.AmortizationDebtFixedRate.ToString());
                var amortizationDebtVariableRateId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.AmortizationDebtVariableRate.ToString());

                var availableIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(availableIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var localCurrencyFixeRateValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == localCurrencyFixeRateId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(localCurrencyFixeRateValues, yearsToProyect, dataSet.CatalogueId);

                var localCurrencyVariableRateValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == localCurrencyVariableRateId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(localCurrencyVariableRateValues, yearsToProyect, dataSet.CatalogueId);

                var foreignCurrencyFixeRateValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == foreignCurrencyFixeRateId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(foreignCurrencyFixeRateValues, yearsToProyect, dataSet.CatalogueId);

                var foreignCurrencyVariableRateValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == foreignCurrencyVariableRateId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(foreignCurrencyVariableRateValues, yearsToProyect, dataSet.CatalogueId);

                var amortizationDebtFixedRateValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == amortizationDebtFixedRateId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(amortizationDebtFixedRateValues, yearsToProyect, dataSet.CatalogueId);

                var amortizationDebtVariableRateValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == amortizationDebtVariableRateId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(amortizationDebtVariableRateValues, yearsToProyect, dataSet.CatalogueId);

                if (availableIncomeValues.Any())
                {
                    var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
                    var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;

                    decimal percent = 0;
                    decimal val;

                    // Para todos los años ingresados, realizar el cálculo
                    for (var i = minYear; i <= maxYear; i++)
                    {

                        var totalDebt = (localCurrencyFixeRateValues.TryGetValue(i, out val) ? localCurrencyFixeRateValues[i] : 0)
                                       + (localCurrencyVariableRateValues.TryGetValue(i, out val) ? localCurrencyVariableRateValues[i] : 0)
                                       + (foreignCurrencyFixeRateValues.TryGetValue(i, out val) ? foreignCurrencyFixeRateValues[i] : 0)
                                       + (foreignCurrencyVariableRateValues.TryGetValue(i, out val) ? foreignCurrencyVariableRateValues[i] : 0)
                                       + (amortizationDebtFixedRateValues.TryGetValue(i, out val) ? amortizationDebtFixedRateValues[i] : 0)
                                       + (amortizationDebtVariableRateValues.TryGetValue(i, out val) ? amortizationDebtVariableRateValues[i] : 0);

                        if (totalDebt != 0 && availableIncomeValues.TryGetValue(i, out val) && availableIncomeValues[i] != 0)
                        {
                            percent = (totalDebt / availableIncomeValues[i]) * 100;
                        }
                        else
                        {
                            percent = 0;
                        }

                        dictionary.Add(i, percent);
                    }
                }

                // TODO: Cálculo de valores futuros
            }

            return dictionary;
        }

        private Dictionary<int, decimal> GetRuleSevenDictionary(DataSet dataSet)
        {
            // Regla 7 - Servicio de la Deuda / Balance Primario
            // Balance Primario = Ingreso Total - Gasto Total
            // Servicio de la Deuda = Pago Total por Intereses

            var dictionary = new Dictionary<int, decimal>();
            var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);

            // Test
            var rule = _rulesRepository.List(r => r.Id == 7).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule == null)
                return dictionary;

            var incomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.TotalIncome.ToString());
            var expenseColumnId = GetColumnId(dataSet.CatalogueId, Enums.ExpenseColumns.TotalExpense.ToString());
            var debtServiceColumnId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.DebtService.ToString());

            var incomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == incomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
            GetProyectedValues(incomeValues, yearsToProyect, dataSet.CatalogueId);

            var expenseValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == expenseColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
            GetProyectedValues(expenseValues, yearsToProyect, dataSet.CatalogueId);

            var debtServiceValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == debtServiceColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
            GetProyectedValues(debtServiceValues, yearsToProyect, dataSet.CatalogueId);

            if (incomeValues.Count <= 0 || expenseValues.Count <= 0 || debtServiceValues.Count <= 0)
                return dictionary;

            // Para todos los años ingresados, realizar el cálculo
            var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
            var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;

            decimal percent = 0;

            for (var i = minYear; i <= maxYear; i++)
            {
                if (!incomeValues.TryGetValue(i, out decimal val))
                {
                    incomeValues[i] = 0;
                }

                if (!expenseValues.TryGetValue(i, out val))
                {
                    expenseValues[i] = 0;
                }

                if (!debtServiceValues.TryGetValue(i, out val))
                {
                    debtServiceValues[i] = 0;
                }

                var balance = incomeValues[i] - expenseValues[i];

                if (balance != 0)
                {
                    percent = (debtServiceValues[i] / balance) * 100;
                }

                dictionary.Add(i, percent);
            }

            // TODO: Cálculo de valores futuros

            return dictionary;
        }

        private Dictionary<int, decimal> GetRuleEightDictionary(DataSet dataSet)
        {
            // Regla 8 - Variacion en Nivel de la Deuda / Ingreso Disponible
            // Variación en Nivel de la Deuda = Servicio de la Deuda en t - Servicio de la Deuda en t-1

            var dictionary = new Dictionary<int, decimal>();
            var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);

            // Test
            var rule = _rulesRepository.List(r => r.Id == 8).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule == null)
                return dictionary;

            var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
            var historicFixedRateDomesticCurrencyId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricFixedRateDomesticCurrency.ToString());
            var historicVariableRateDomesticCurrencyId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString());
            var historicVariableRateForeignCurrencyId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricVariableRateForeignCurrency.ToString());
            var historicFixedRateForeignCurrencyId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricFixedRateForeignCurrency.ToString());

            var availableIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
            GetProyectedValues(availableIncomeValues, yearsToProyect, dataSet.CatalogueId);

            var historicFixedRateDomesticCurrencyValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == historicFixedRateDomesticCurrencyId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
            GetProyectedValues(historicFixedRateDomesticCurrencyValues, yearsToProyect, dataSet.CatalogueId);

            var historicVariableRateDomesticCurrencyValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == historicVariableRateDomesticCurrencyId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
            GetProyectedValues(historicVariableRateDomesticCurrencyValues, yearsToProyect, dataSet.CatalogueId);

            var historicVariableRateForeignCurrencyValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == historicVariableRateForeignCurrencyId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
            GetProyectedValues(historicVariableRateForeignCurrencyValues, yearsToProyect, dataSet.CatalogueId);

            var historicFixedRateForeignCurrencyValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == historicFixedRateForeignCurrencyId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
            GetProyectedValues(historicFixedRateForeignCurrencyValues, yearsToProyect, dataSet.CatalogueId);

            // Para todos los años ingresados, realizar el cálculo
            var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
            var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;

            decimal percent = 0;
            decimal val;
            if (availableIncomeValues.Any())
            {
                for (var i = minYear; i <= maxYear; i++)
                {
                    var debtValues = (historicFixedRateDomesticCurrencyValues.TryGetValue(i, out val) ? historicFixedRateDomesticCurrencyValues[i] : 0)
                                                + (historicVariableRateDomesticCurrencyValues.TryGetValue(i, out val) ? historicVariableRateDomesticCurrencyValues[i] : 0)
                                                + (historicVariableRateDomesticCurrencyValues.TryGetValue(i, out val) ? historicVariableRateDomesticCurrencyValues[i] : 0)
                                                + (historicVariableRateForeignCurrencyValues.TryGetValue(i, out val) ? historicVariableRateForeignCurrencyValues[i] : 0)
                                                + (historicVariableRateForeignCurrencyValues.TryGetValue(i, out val) ? historicFixedRateForeignCurrencyValues[i] : 0);

                    var debtValuesLastYear = (historicFixedRateDomesticCurrencyValues.TryGetValue(i - 1, out val) ? historicFixedRateDomesticCurrencyValues[i - 1] : 0)
                                                + (historicVariableRateDomesticCurrencyValues.TryGetValue(i - 1, out val) ? historicVariableRateDomesticCurrencyValues[i - 1] : 0)
                                                + (historicVariableRateDomesticCurrencyValues.TryGetValue(i - 1, out val) ? historicVariableRateDomesticCurrencyValues[i - 1] : 0)
                                                + (historicVariableRateForeignCurrencyValues.TryGetValue(i - 1, out val) ? historicVariableRateForeignCurrencyValues[i - 1] : 0)
                                                + (historicVariableRateForeignCurrencyValues.TryGetValue(i - 1, out val) ? historicFixedRateForeignCurrencyValues[i - 1] : 0);


                    var variation = debtValues - debtValuesLastYear;
                    if (availableIncomeValues.TryGetValue(i, out val)
                        && availableIncomeValues[i] != 0
                        && variation != 0)
                    {

                        percent = (variation / availableIncomeValues[i]) * 100;
                    }
                    else
                    {
                        percent = 0;
                    }

                    dictionary.Add(i, percent);
                }
            }


            // TODO: Cálculo de valores futuros

            return dictionary;
        }

        private Dictionary<int, decimal> GetRuleNineDictionary(DataSet dataSet)
        {
            // Regla 9 - Endeudamiento / Ingreso Disponible
            // Endeudamiento = Deuda Total

            var dictionary = new Dictionary<int, decimal>();
            var yearsToProyect = int.Parse(ConfigurationManager.AppSettings["YearsToProyect"]);

            var rule = _rulesRepository.List(r => r.Id == 9).Select(r => new RuleDTO(r)).FirstOrDefault();
            if (rule != null)
            {
                var availableIncomeColumnId = GetColumnId(dataSet.CatalogueId, Enums.IncomeColumns.AvailableIncome.ToString());
                var historicFixedRateDomesticCurrencyId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricFixedRateDomesticCurrency.ToString());
                var historicVariableRateDomesticCurrencyId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricVariableRateDomesticCurrency.ToString());
                var historicVariableRateForeignCurrencyId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricVariableRateForeignCurrency.ToString());
                var historicFixedRateForeignCurrencyId = GetColumnId(dataSet.CatalogueId, Enums.DebtColumns.HistoricFixedRateForeignCurrency.ToString());

                var availableIncomeValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == availableIncomeColumnId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(availableIncomeValues, yearsToProyect, dataSet.CatalogueId);

                var historicFixedRateDomesticCurrencyValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == historicFixedRateDomesticCurrencyId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(historicFixedRateDomesticCurrencyValues, yearsToProyect, dataSet.CatalogueId);

                var historicVariableRateDomesticCurrencyValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == historicVariableRateDomesticCurrencyId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(historicVariableRateDomesticCurrencyValues, yearsToProyect, dataSet.CatalogueId);

                var historicVariableRateForeignCurrencyValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == historicVariableRateForeignCurrencyId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(historicVariableRateForeignCurrencyValues, yearsToProyect, dataSet.CatalogueId);

                var historicFixedRateForeignCurrencyValues = _catalogueColumnValuesRepository.List(v => v.CatalogueColumnId == historicFixedRateForeignCurrencyId).Where(v => dataSet.YearDatas.Select(y => y.Id).Contains(v.YearDataId) && v.YearData.Year <= dataSet.HistoricTo).OrderBy(v => v.YearDataId).ToDictionary(item => item.YearData.Year, item => item.Value);
                GetProyectedValues(historicFixedRateForeignCurrencyValues, yearsToProyect, dataSet.CatalogueId);

                if (availableIncomeValues.Any())
                {
                    // Para todos los años ingresados, realizar el cálculo
                    var maxYear = dataSet.HistoricTo.Value + yearsToProyect;
                    var minYear = maxYear - dataSet.HistoricFrom.Value <= 10 ? dataSet.HistoricFrom.Value : maxYear - 10;

                    decimal percent = 0;
                    decimal val;

                    for (var i = minYear; i <= maxYear; i++)
                    {
                        decimal totaldebt = (historicFixedRateDomesticCurrencyValues.TryGetValue(i, out val) ? historicFixedRateDomesticCurrencyValues[i] : 0)
                                                + (historicVariableRateDomesticCurrencyValues.TryGetValue(i, out val) ? historicVariableRateDomesticCurrencyValues[i] : 0)
                                                + (historicVariableRateDomesticCurrencyValues.TryGetValue(i, out val) ? historicVariableRateDomesticCurrencyValues[i] : 0)
                                                + (historicVariableRateForeignCurrencyValues.TryGetValue(i, out val) ? historicVariableRateForeignCurrencyValues[i] : 0)
                                                + (historicVariableRateForeignCurrencyValues.TryGetValue(i, out val) ? historicFixedRateForeignCurrencyValues[i] : 0);

                        if (availableIncomeValues.TryGetValue(i, out val)
                            && totaldebt != 0
                            && availableIncomeValues[i] != 0)
                        {
                            percent = (totaldebt / availableIncomeValues[i]) * 100;
                        }
                        else
                        {
                            percent = 0;
                        }

                        dictionary.Add(i, percent);
                    }
                }

                // TODO: Cálculo de valores futuros
            }

            return dictionary;
        }

        #endregion 

        private void DatosIngresos(ICollection<dynamic> tableData, SortedDictionary<int, SortedDictionary<int, decimal>> calculatedData, DataSet dataset, int yearsToShow)
        {
            var columns = _catalogueColumnsRepository.List(x => x.CatalogueId == dataset.CatalogueId)
                .Where(c => c.Category == Enums.ColumnCategory.Income)
                .OrderBy(c => c.Index).ToList();

            var tuples = new List<Tuple<int, int>>();

            foreach (var column in columns)
            {
                dynamic data = new ExpandoObject();
                data.Id = column.Id;
                data.Account = column.ColumnName;
                data.Category = column.Category;
                data.Table = true;
                data.Chart = true;
                data.BaseParent = false;
                data.InternalName = column.InternalName != null && Enum.IsDefined(typeof(Enums.IncomeColumns), column.InternalName) ? Enum.Parse(typeof(Enums.IncomeColumns), column.InternalName) : null;
                data.Level = GetLevel(column, columns);
                data.ParentId = column.ParentId;

                if (column.Parent == null)
                    data.BaseParent = true;

                data.Autocalculated = column.Children != null && column.Children.Any();

                var approximation = ChildSum(column, dataset, yearsToShow, ref calculatedData);

                if (approximation.Count == 0)
                    continue;


                tuples.Add(new Tuple<int, int>(approximation.First().Key, approximation.Last().Key));

                IDictionary<string, object> underlyingData = data;


                foreach (var yearData in approximation)
                {
                    underlyingData.Add(yearData.Key.ToString(), yearData.Value);
                }

                tableData.Add(data);

            }
        }

        private Tuple<int, int> DatosGastos(ICollection<dynamic> tableData, SortedDictionary<int, SortedDictionary<int, decimal>> calculatedData, DataSet dataset, int yearsToShow, bool simulateMissingYears = false, bool fiveYears = false, Dictionary<int, decimal> fixedVariation = null,
            Dictionary<int, Dictionary<int, decimal>> yearVariation = null)
        {
            var columns = _catalogueColumnsRepository.List(x => x.CatalogueId == dataset.CatalogueId)
                .Where(c => c.Category == Enums.ColumnCategory.Expense)
                .OrderBy(c => c.Index).ToList();



            var tuples = new List<Tuple<int, int>>();

            int? minYear = null;
            int maxYear = 0;

            foreach (var column in columns)
            {
                dynamic data = new ExpandoObject();
                data.Id = column.Id;
                data.Account = column.ColumnName;
                data.Category = column.Category;
                data.Table = true;
                data.Chart = true;
                data.BaseParent = false;
                data.InternalName = column.InternalName != null && Enum.IsDefined(typeof(Enums.ExpenseColumns), column.InternalName) ? Enum.Parse(typeof(Enums.ExpenseColumns), column.InternalName) : null;
                data.Level = GetLevel(column, columns);
                data.ParentId = column.ParentId;

                if (column.Parent == null)
                    data.BaseParent = true;

                data.Autocalculated = column.Children != null && column.Children.Any();

                var approximation = ChildSum(column, dataset, yearsToShow,
                    ref calculatedData, fixedVariation, yearVariation);

                if (approximation.Count == 0)
                    continue;

                if (!minYear.HasValue || approximation.First().Key < minYear.Value)
                    minYear = approximation.First().Key;

                tuples.Add(new Tuple<int, int>(approximation.First().Key, approximation.Last().Key));

                IDictionary<string, object> underlyingData = data;
                foreach (var yearData in approximation)
                    underlyingData.Add(yearData.Key.ToString(), yearData.Value);

                tableData.Add(data);
            }

            return new Tuple<int, int>(minYear.HasValue ? minYear.Value : 0, maxYear);
        }

        private Tuple<int, int> DatosDeudas(ICollection<dynamic> tableData, SortedDictionary<int, SortedDictionary<int, decimal>> calculatedData, DataSet dataset, int yearsToShow, bool simulateMissingYears = false, bool fiveYears = false, Dictionary<int, decimal> fixedVariation = null,
            Dictionary<int, Dictionary<int, decimal>> yearVariation = null)
        {
            var columns = _catalogueColumnsRepository.List(x => x.CatalogueId == dataset.CatalogueId)
                .Where(c => c.Category == Enums.ColumnCategory.Debt)
                .OrderBy(c => c.Index)
                .GroupBy(c => c.Category)
                .SelectMany(c => c).ToList();

            var tuples = new List<Tuple<int, int>>();

            foreach (var column in columns)
            {
                dynamic data = new ExpandoObject();
                data.Account = column.ColumnName;
                data.Category = column.Category;
                data.Table = true;
                data.Chart = true;
                data.InternalName = column.InternalName != null && Enum.IsDefined(typeof(Enums.DebtColumns), column.InternalName) ? Enum.Parse(typeof(Enums.DebtColumns), column.InternalName) : null;
                data.Level = GetLevel(column, columns);
                data.BaseParent = false;
                data.ParentId = column.ParentId;
                data.Id = column.Id;
                data.BaseParent = column.Parent == null;
                data.Autocalculated = column.Children != null && column.Children.Any() && column.Category != Enums.ColumnCategory.Debt ||
                                      column.InternalName == Enums.DebtColumns.TotalDebt.ToString();

            data.BaseParent = column.Parent == null;

            var approximation = ChildSum(column, dataset, yearsToShow,
                ref calculatedData, fixedVariation, yearVariation);

                if (approximation.Count == 0)
                    continue;

                tuples.Add(new Tuple<int, int>(approximation.First().Key, approximation.Last().Key));

                IDictionary<string, object> underlyingData = data;
                foreach (var yearData in approximation)
                    underlyingData.Add(yearData.Key.ToString(), yearData.Value);

                tableData.Add(data);
            }

            var minYear = 0;
            var maxYear = 0;

            if (tuples.Count <= 0)
                return new Tuple<int, int>(minYear, maxYear);

            minYear = tuples.Min(t => t.Item1);
            maxYear = tuples.Max(t => t.Item2);

            return new Tuple<int, int>(minYear, maxYear);
        }

        private void DatosBalancePrimario(int catalogueId, ICollection<dynamic> tableData, SortedDictionary<int, SortedDictionary<int, decimal>> calculatedData)
        {


            // Balance Primario = Ingreso Total - Gasto Total
            dynamic data = new ExpandoObject();
            data.Account = Strings.ColName_Debt_PrimaryBalance;
            data.Category = 5;
            data.Table = true;
            data.Chart = true;
            data.Autocalculated = true;
            data.Level = 0;
            data.ParentId = null;
            data.BaseParent = true;

            var columnaIngresoTotal = GetColumnId(catalogueId, Enums.IncomeColumns.TotalIncome.ToString());
            var columnaGastoTotal = GetColumnId(catalogueId, Enums.ExpenseColumns.TotalExpense.ToString());

            var aproximacionIngresos = calculatedData.Single(d => d.Key == columnaIngresoTotal).Value;
            var aproximacionGastos = calculatedData.Single(d => d.Key == columnaGastoTotal).Value;

            var approximation = new Dictionary<int, decimal>();

            if (aproximacionIngresos.Count > 0 && aproximacionGastos.Count > 0)
            {
                var min = Math.Min(aproximacionIngresos.Min(v => v.Key), aproximacionGastos.Min(v => v.Key));
                var max = Math.Max(aproximacionIngresos.Max(v => v.Key), aproximacionGastos.Max(v => v.Key));


                for (var i = min; i <= max; i++)
                {
                    decimal val;
                    if (!aproximacionIngresos.TryGetValue(i, out val))
                    {
                        aproximacionIngresos[i] = 0;
                    }

                    if (!aproximacionGastos.TryGetValue(i, out val))
                    {
                        aproximacionGastos[i] = 0;
                    }

                    approximation[i] = aproximacionIngresos[i] - aproximacionGastos[i];
                }

                IDictionary<string, object> underlyingData = data;
                foreach (var yearData in approximation)
                    underlyingData.Add(yearData.Key.ToString(), yearData.Value);

                tableData.Add(data);

            }
        }

        private void DatosBalanceFiscal(int catalogueId, ICollection<dynamic> tableData, SortedDictionary<int, SortedDictionary<int, decimal>> calculatedData)
        {
            // Balance Fiscal = Balance Primario - Servicio de la Deuda
            // Balance Primario = Ingreso Total - Gasto Total
            dynamic data = new ExpandoObject();
            data.Account = Strings.ColName_Debt_FiscalBalance;
            data.Category = 6;
            data.Table = true;
            data.Chart = true;
            data.Autocalculated = true;
            data.BaseParent = true;
            data.Level = 0;
            data.ParentId = null;
            data.Id = null;



            var columnaIngresoTotal = GetColumnId(catalogueId, Enums.IncomeColumns.TotalIncome.ToString());
            var columnaGastoTotal = GetColumnId(catalogueId, Enums.ExpenseColumns.TotalExpense.ToString());
            var columnaServicioDeuda = GetColumnId(catalogueId, Enums.DebtColumns.DebtService.ToString());

            var aproximacionIngresos = columnaIngresoTotal != 0 ? calculatedData.Single(d => d.Key == columnaIngresoTotal).Value : new SortedDictionary<int, decimal>();
            var aproximacionGastos = columnaGastoTotal != 0 ? calculatedData.Single(d => d.Key == columnaGastoTotal).Value : new SortedDictionary<int, decimal>();

            var aproximacionServicioDeuda = columnaServicioDeuda != 0 ? calculatedData.Single(d => d.Key == columnaServicioDeuda).Value : new SortedDictionary<int, decimal>();

            var approximation = new Dictionary<int, decimal>();

            if (aproximacionIngresos.Count > 0 && aproximacionGastos.Count > 0)
            {
                var min = Math.Min(aproximacionIngresos.Min(v => v.Key), aproximacionGastos.Min(v => v.Key));
                var max = Math.Max(aproximacionIngresos.Max(v => v.Key), aproximacionGastos.Max(v => v.Key));

                for (var i = min; i <= max; i++)
                {
                    decimal val;
                    if (!aproximacionIngresos.TryGetValue(i, out val))
                    {
                        aproximacionIngresos[i] = 0;
                    }

                    if (!aproximacionGastos.TryGetValue(i, out val))
                    {
                        aproximacionGastos[i] = 0;
                    }

                    if (!aproximacionServicioDeuda.TryGetValue(i, out val))
                    {
                        aproximacionServicioDeuda[i] = 0;
                    }

                    approximation[i] = aproximacionIngresos[i] - aproximacionGastos[i] - aproximacionServicioDeuda[i];
                }

                IDictionary<string, object> underlyingData = data;
                foreach (var yearData in approximation)
                    underlyingData.Add(yearData.Key.ToString(), yearData.Value);

                tableData.Add(data);
            }
        }

        private void SaveParameters(string userId, Dictionary<int, decimal> fixedVariation, Dictionary<int, Dictionary<int, decimal>> yearVariation)
        {
            // Agrego o actualizo parámetros   
            if (fixedVariation != null)
            {
                foreach (var item in fixedVariation)
                {
                    var user = _userParameterRepository.List(u => u.Email == userId && u.CatalogueColumnId == item.Key).FirstOrDefault();

                    if (user == null)
                    {
                        var userParameter = new UserParameter
                        {
                            Email = userId,
                            CatalogueColumnId = item.Key,
                            Parameter = item.Value
                        };

                        _userParameterRepository.Add(userParameter);
                    }
                    else
                    {
                        user.Parameter = item.Value;
                        _userParameterRepository.Update(user);
                    }
                }
            }
            else if (yearVariation != null)
            {
                foreach (var item in yearVariation)
                {
                    foreach (var subItem in item.Value)
                    {
                        var user = _userParameterRepository.List(u => u.Email == userId && u.CatalogueColumnId == item.Key && u.Year == subItem.Key).FirstOrDefault();

                        if (user == null)
                        {
                            var userParameter = new UserParameter
                            {
                                Email = userId,
                                CatalogueColumnId = item.Key,
                                Parameter = subItem.Value,
                                Year = subItem.Key
                            };

                            _userParameterRepository.Add(userParameter);
                        }
                        else
                        {
                            user.Parameter = subItem.Value;
                            _userParameterRepository.Update(user);
                        }

                    }
                }
            }
        }

        private int GetColumnId(int catalogueId, string internalColumnName)
        {
            var column = _catalogueColumnsRepository
                .List(c => c.CatalogueId == catalogueId && c.InternalName == internalColumnName).FirstOrDefault();

            return column?.Id ?? 0;
        }

        private int GetLevel(CatalogueColumn column, IList<CatalogueColumn> columns)
        {
            if (column.ParentId != null)
                return 1 + GetLevel(columns.Where(d => d.Id == column.ParentId).FirstOrDefault(), columns);
            else
                return 0;
        }

        public ApiResult GetProjectionYear(string userId, int catalogueId)
        {

            return new ApiResult
            {
                StatusCode = HttpStatusCode.OK,
                Data = _dataSetsLogic.GetUserDataSet(userId, catalogueId).ProjectionFrom
            };

        }

        private void GetProyectedValues(Dictionary<int, decimal> values, int yearsToProyect, int catalogueId)
        {
            if (values.Any())
            {
                decimal[] serie = new decimal[5];
                decimal val;
                var index = 0;
                int proyectedYear = values.Keys.Max() + 1;
                //se setea en 5 el array enviado porque es lo mínimo con lo que la api puede proyectar datos
                for (int i = values.Keys.Last() - 4; i <= values.Keys.Last(); i++)
                {
                    serie[index] = values.TryGetValue(i, out val) ? values[i] : 0;
                    index++;
                }

                UtilitiesApiConnect api = new UtilitiesApiConnect();
                var result = api.GetMfmpDynamicsResults(catalogueId, new MfmpDynamicResults
                {
                    rates = null,
                    serie = serie,
                    years_to_project = yearsToProyect
                });

                if (result != null && result.Count() >= yearsToProyect)
                {
                    for (var r = 0; r < yearsToProyect; r++)
                    {
                        values.Add(proyectedYear + r, result[r]);
                    }
                }

            }
        }

        private void SetHistoricYear(DataSet dataSet)
        {
            var lastYearWithData = dataSet.HistoricTo;
            var columns = _catalogueColumnsRepository.List().Where(x => x.CatalogueId == dataSet.CatalogueId && x.Parent == null && x.Children.Any());
            foreach (var c in columns)
            {
                if (c.ColumnValues.Any() &&
                    c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Any() &&
                    c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Max(x => x.YearData.Year) < lastYearWithData)
                {
                    lastYearWithData = c.ColumnValues.Where(x => x.YearData.DataSetId == dataSet.Id).Max(x => x.YearData.Year);
                }
            }

            if (dataSet.HistoricTo > lastYearWithData)
            {
                dataSet.HistoricTo = lastYearWithData;
                dataSet.ProjectionFrom = lastYearWithData + 1;
                _datasetRepository.Update(dataSet);
            }
        }
    }
}
