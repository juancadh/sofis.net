﻿using BID.PSF.BLL.Logic;
using BID.PSF.BLL.Logic.Interfaces;
using BID.PSF.DAL;
using Microsoft.Practices.Unity;

namespace BID.PSF.BLL
{
    // ReSharper disable once InconsistentNaming
    public class BLLUnityExtension : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.AddNewExtension<DALUnityExtension>();

            Container.RegisterType<IOAuthLogic, OAuthLogic>();

            Container.RegisterType<IYearDataLogic, YearDataLogic>();


            Container.RegisterType<IDataSetsLogic, DataSetsLogic>();
            Container.RegisterType<IProfileLogic, ProfileLogic>();
            Container.RegisterType<IASDLogic, ASDLogic>();
            Container.RegisterType<IMFMPLogic, MFMPLogic>();
            Container.RegisterType<ICatalogueLogic, CatalogueLogic>();

           //Container.RegisterType<IFanChartLogic, FanChartLogic>();
        }
    }
}
