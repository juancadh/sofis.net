﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using BID.PSF.Model.ApiModel;

namespace BID.PSF.BLL
{
    public class UtilitiesApiConnect
    {

        public decimal[] GetMfmpDynamicsResults(int catalogueId, MfmpDynamicResults data)
        {
            string uri = string.Format("{0}/{1}?code={2}&catalogueId={3}", ConfigurationManager.AppSettings["HostApi"], ConfigurationManager.AppSettings["MFMP"], ConfigurationManager.AppSettings["Token"], catalogueId);
            var response = ApiConnect(uri, data);

            return JsonConvert.DeserializeObject<decimal[]>(response);
        }

        private string ApiConnect(string uri, object data)
        {
            string result;

            HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
            request.Method = "Post";
            request.ContentType = "application/json";

            byte[] postData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));
            request.ContentLength = postData.Length;

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(postData, 0, postData.Length);
            }

            var httpResponse = request.GetResponse() as HttpWebResponse;
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd().ToString();
            }

            return result;
        }

    }
}
