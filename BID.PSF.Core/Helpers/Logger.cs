﻿using System;
using BID.PSF.Core.Helpers.Interfaces;
using Microsoft.ApplicationInsights;

namespace BID.PSF.Core.Helpers
{
    public class Logger : ILogger
    {
        public void LogException(Exception e, bool reThrow = false)
        {
            var cli = new TelemetryClient();
            cli.TrackException(e);

            if (reThrow)
                throw e;
        }

        public void LogMessage(string msg)
        {
            var cli = new TelemetryClient();
            cli.TrackTrace(msg);
        }
    }
}
