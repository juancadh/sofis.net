﻿using System;
using System.Configuration;

namespace BID.PSF.Core.Helpers
{
    public static class ConfigurationHelper
    {
        private static string GetParam(string paramKey)
        {
            if (ConfigurationManager.AppSettings[paramKey] == null)
                throw new Exception();

            return ConfigurationManager.AppSettings[paramKey];
        }
    }
}
