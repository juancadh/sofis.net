﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.Azure.Services.AppAuthentication;

namespace BID.PSF.Core.Helpers
{
    public static class ADAuthentication
    {
        const string SqlResource = "https://iadborg-np-sofis.database.azure.net";

        public static async Task<string> GetSqlTokenAsync(string cs)
        {
            string conn = "Server=tcp:iadborg-np-sofis.database.windows.net,1433;Initial Catalog=sofis-uat;Connection Timeout=30;RunAs=App";

            var provider = new AzureServiceTokenProvider(conn);
            return await provider.GetAccessTokenAsync(SqlResource);
        }

        public static SqlConnection GetSqlConnection()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            builder["Data Source"] = "tcp:iadborg-np-sofis.database.windows.net,1433";
            builder["Initial Catalog"] = "sofis-uat";
            builder["Connect Timeout"] = 30;

            string cnx = builder.ConnectionString;

            SqlConnection connection = new SqlConnection(cnx);

            connection.AccessToken = GetSqlTokenAsync(cnx).Result;
            connection.Open();

            return connection;
        }

        //public static async Task<string> UsingSP()
        //{
        //    string cs = "Server=tcp:iadborg-np-sofis.database.windows.net,1433;Initial Catalog=sofis-uat;Persist Security Info=False;User ID=local_admin@iadborg-np-sofis.database.windows.net;Password=Washington12345;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;App=EntityFramework";
        //    //string cs = ConfigurationManager.ConnectionStrings["ProductionUAT"].ConnectionString;
        //    var token = await GetSqlTokenAsync();
        //    var user = await GetSqlUserName(cs, token);

        //    return user;
        //}

        //private static async Task<string> GetSqlUserName(string connectionString, string token)
        //{
        //    using (var conn = new SqlConnection(connectionString))
        //    {
        //        conn.AccessToken = token;
        //        await conn.OpenAsync();
        //        string text = "SELECT SUSER_SNAME()";
        //        using (var cmd = new SqlCommand(text, conn))
        //        {
        //            var result = await cmd.ExecuteScalarAsync();
        //            return result as string;
        //        }
        //    }
        //}
    }
}
