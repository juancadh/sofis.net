﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BID.PSF.Core.Helpers
{
    public class MathHelper
    {
        public static decimal GetAverage(decimal[] values)
        {
            decimal average = 0;

            if (values == null || values.Length <= 1)
                return average;

            for (var i = 1; i < values.Length; i++)
            {
                if (values[i - 1] == 0)
                    continue;

                average += (values[i] - values[i - 1]) / values[i - 1];
            }

            return average / (values.Length - 1);
        }

        public static decimal GetAverage2(decimal[] values)
        {
            decimal average = 0;

            if (values == null || values.Length <= 1)
                return average;

            for (var i = 0; i < values.Length; i++)
            {
                if (values[i] == 0)
                    continue;

                average += values[i];
            }

            return average / values.Length;
        }

        public static decimal GetImplicitAverage(decimal[] values)
        {
            decimal average = 1;

            if (values == null || values.Length <= 1)
                return average;

            average = values.Where(t => t != 0).Aggregate(average, (current, t) => current * t);

            average = ((decimal)Math.Pow((double)average, 1 / (double)(values.Length + 1)) - 1) * 100;

            return average;
        }

        public static decimal GetStandardDeviation(decimal[] values)
        {
            decimal standardDeviation = 0;
            int count = values != null ? values.Length : 0;

            if (count > 1)
            {
                decimal avg = values.Average();
                decimal sum = values.Sum(d => (d - avg) * (d - avg));
                standardDeviation = (decimal)Math.Sqrt((double)sum / count);
            }
            return standardDeviation;
        }
    }
}
