﻿using System;

namespace BID.PSF.Core.Helpers.Interfaces
{
    public interface ILogger
    {
        void LogException(Exception e, bool reThrow = false);
        void LogMessage(string msg);
    }
}
