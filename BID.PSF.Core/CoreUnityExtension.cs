﻿using BID.PSF.Core.Helpers;
using BID.PSF.Core.Helpers.Interfaces;
using Microsoft.Practices.Unity;

namespace BID.PSF.Core
{
    public class CoreUnityExtension : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<ILogger, Logger>();
        }
    }
}
