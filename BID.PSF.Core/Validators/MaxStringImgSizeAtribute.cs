﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BID.PSF.Core.Validators
{
    public class MaxStringImgSizeAtribute : ValidationAttribute
    {
        private readonly int _maxFileSize;
        public MaxStringImgSizeAtribute(int maxFileSize)
        {
            _maxFileSize = maxFileSize;
        }

        public override bool IsValid(object value)
        {
            var file = value as string;
            if (file == null)
            {
                return false;
            }
            return ((decimal)Encoding.Unicode.GetByteCount(file) / 1048576) <= _maxFileSize;
        }

        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(_maxFileSize.ToString());
        }
    }
}
