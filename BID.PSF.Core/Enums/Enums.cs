﻿using System.ComponentModel;
using BID.PSF.Core.Resources;

// ReSharper disable InconsistentNaming
namespace BID.PSF.Core.Enums
{
    public class Enums
    {
        public enum InstitutionType
        {
            [LocalizedDescription("Enum_InstitutionType_ResearchCenter", typeof(EnumDescriptions))]
            ResearchCenter,
            [LocalizedDescription("Enum_InstitutionType_Government", typeof(EnumDescriptions))]
            Government,
            [LocalizedDescription("Enum_InstitutionType_Media", typeof(EnumDescriptions))]
            Media,
            [LocalizedDescription("Enum_InstitutionType_MultilateralOrganization", typeof(EnumDescriptions))]
            MultilateralOrganization,
            [LocalizedDescription("Enum_InstitutionType_CivilSociety", typeof(EnumDescriptions))]
            CivilSociety,
            [LocalizedDescription("Enum_InstitutionType_PrivateSector", typeof(EnumDescriptions))]
            PrivateSector,
            [LocalizedDescription("Enum_InstitutionType_Academy", typeof(EnumDescriptions))]
            Academy,
            [LocalizedDescription("Enum_InstitutionType_GeneralPublic", typeof(EnumDescriptions))]
            GeneralPublic
        }

        public enum UserTitle
        {
            [LocalizedDescription("Enum_UserTitle_CEO", typeof(EnumDescriptions))]
            CEO,
            [LocalizedDescription("Enum_UserTitle_CFO", typeof(EnumDescriptions))]
            CFO,
            [LocalizedDescription("Enum_UserTitle_CIO", typeof(EnumDescriptions))]
            CIO,
            [LocalizedDescription("Enum_UserTitle_COO", typeof(EnumDescriptions))]
            COO,
            [LocalizedDescription("Enum_UserTitle_Manager", typeof(EnumDescriptions))]
            Manager,
            [LocalizedDescription("Enum_UserTitle_Lawyer", typeof(EnumDescriptions))]
            Lawyer,
            [LocalizedDescription("Enum_UserTitle_Consultant", typeof(EnumDescriptions))]
            Consultant,
            [LocalizedDescription("Enum_UserTitle_Technical", typeof(EnumDescriptions))]
            Technical,
            [LocalizedDescription("Enum_UserTitle_Other", typeof(EnumDescriptions))]
            Other

        }


        public enum UserInterests
        {
            [LocalizedDescription("Enum_UserInterests_PublicSpending", typeof(EnumDescriptions))]
            PublicSpending,
            [LocalizedDescription("Enum_UserInterests_Taxes", typeof(EnumDescriptions))]
            Taxes,
            [LocalizedDescription("Enum_UserInterests_FiscalPolicy", typeof(EnumDescriptions))]
            FiscalPolicy,
            [LocalizedDescription("Enum_UserInterests_Decentralization", typeof(EnumDescriptions))]
            Decentralization,
            [LocalizedDescription("Enum_UserInterests_PublicProcurement", typeof(EnumDescriptions))]
            PublicProcurement,
            [LocalizedDescription("Enum_UserInterests_PublicPrivatePartnerships", typeof(EnumDescriptions))]
            PublicPrivatePartnerships
        }

        public enum UserProducts
        {
            [LocalizedDescription("Enum_UserProducts_Publications", typeof(EnumDescriptions))]
            Publications,
            [LocalizedDescription("Enum_UserProducts_Courses", typeof(EnumDescriptions))]
            Courses,
            [LocalizedDescription("Enum_UserProducts_DevelopmentNumbers", typeof(EnumDescriptions))]
            DevelopmentNumbers,
            [LocalizedDescription("Enum_UserProducts_DevelopmentCode", typeof(EnumDescriptions))]
            DevelopmentCode,
            [LocalizedDescription("Enum_UserProducts_Blog", typeof(EnumDescriptions))]
            Blog,
            [LocalizedDescription("Enum_UserProducts_Events", typeof(EnumDescriptions))]
            Events,
            [LocalizedDescription("Enum_UserProducts_PressReleases", typeof(EnumDescriptions))]
            PressReleases,
            [LocalizedDescription("Enum_UserProducts_Newsletter", typeof(EnumDescriptions))]
            Newsletter,
            [LocalizedDescription("Enum_UserProducts_AnnualReports", typeof(EnumDescriptions))]
            AnnualReports,
            [LocalizedDescription("Enum_UserProducts_Announcements", typeof(EnumDescriptions))]
            Announcements
        }

        public enum ColumnCategory
        {
            Macroeconomy, Income, Expense, Debt
        }

        public enum MacroeconomicsColumns
        {
            ConsumerIndexPrice, LocalChangeRate
        }

        public enum IncomeColumns
        {
            TotalIncome, AvailableIncome, NotAvailableIncome,AvailableTransfer,NotAvailableTransfer,AvailableTax,AvailableContribution, Incentives, DerivatedIncomes, FederalTransfers
        }

        public enum ExpenseColumns
        {
            TotalExpense,
            Investment,
            PublicInvestment,
            Roster,
            GoodsAndServices,
            Transfer,
            Other,
            PDLabeled,
            PDNLabeled,
            Interests,
            FDEInterests,
            FreeDestinationIncome,
            SpecificDestinationIncome,
            PSLabeled,
            MSLabeled,
            GSLabeled,
            SocialBenefits,
            EXPDefaultExpense,
            UntaggedExpense,
            TaggedExpense,
            FreeDeterminationExpense,
            TASONLabeled,
            PSNLabeled,
            MSNLabeled,
            GSNLabeled,
            BINLabeled,
            BILabeled,
            INVNolaveled,
            INVolaveled,
            FDEinvestment,
            EXPinvestment,
            UnlabeledPublicInvestment,
            LabeledPublicInvestment
        }

        public enum DebtColumns
        {
            TotalDebt,
            DebtService,
            TotalAmortizations,
            TotalInterests,
            HistoricVariableRateDomesticCurrency,
            HistoricFixedRateDomesticCurrency,
            NewVariableRateDomesticCurrency,
            NewFixedRateDomesticCurrency,
            HistoricVariableRateForeignCurrency,
            HistoricFixedRateForeignCurrency,
            NewVariableRateForeignCurrency,
            NewFixedRateForeignCurrency,
            ForeignRate,
            LocalRate,
            LocalCurrencyAmortizationFixeRate,
            LocalCurrencyAmortizationVariableRate,
            ForeignCurrencyAmortizationFixeRate,
            ForeignCurrencyAmortizationVariableRate,
            AmortizationDebtFixedRate,
            AmortizationDebtVariableRate
        }

        public enum BalanceColumns
        {
            PrimaryBalance,
            FiscalBalance
        }

        public enum AuthenticationType
        {
            Okta,
            Identity
        }

        public enum Roles
        {
            Administrator = 1,
            Member = 2,
        }

        public enum CatalogueType
        {
            Mexico = 1,
            Estandar,
            Other
        }

        public enum Languagues
        {
            en = 1,
            es,
            fr,
            pt

        }


    }
}
