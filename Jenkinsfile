#!groovy

// You should use a compatible version of pipeline utils according to you Job.
// Reach Enterprise Architecture team for more information.
def pipelineUtilsVersion = "1.5"

// Reach Enterprise Architecture team to get this information.
def projectKey = "${env.SOFIS_KEY}"
def projectName = "${env.SOFIS_NAME}"

// Reach Enterprise Architecture team to get this information.
def projectFEKey = "${env.SOFIS_FE_KEY}"
def projectFEName = "${env.SOFIS_FE_NAME}"

// Folder structure that separete the different Jobs.
// Change only if needed.
def wsDir = "${WORKSPACE_ROOT}${projectKey}\\${env.BRANCH_NAME}-${env.BUILD_NUMBER}"

// URLs of Microsoft Office 365 Webhooks to receive notifications.
// Multiple URLs are supported.
//
// Can be configured as a Teams Connector, please refer to the link below on how to configure it.
// http://bit.ly/MSTeamConfiguration
String[] microsoftTeamsWebhooks = ["", ""]

// Interval variables. Please do not remove them.
def err = null
def utils = null
currentBuild.result = "SUCCESS"

node('master') {
    ws (wsDir) {  
        try {
            // This step will checkout the most updated code in the current branch from Bitbucket.
            stage('Checking out code') {
                checkout scm
            }

            // ╔═╗┬  ┌─┐┌─┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┬┐  ┌─┐┬ ┬┌─┐┌┐┌┌─┐┌─┐  ┌┬┐┬ ┬┬┌─┐  ┌─┐┌┬┐┌─┐┌─┐
            // ╠═╝│  ├┤ ├─┤└─┐├┤    │││ ││││ │   │  ├─┤├─┤││││ ┬├┤    │ ├─┤│└─┐  └─┐ │ ├┤ ├─┘
            // ╩  ┴─┘└─┘┴ ┴└─┘└─┘  ─┴┘└─┘┘└┘ ┴   └─┘┴ ┴┴ ┴┘└┘└─┘└─┘   ┴ ┴ ┴┴└─┘  └─┘ ┴ └─┘┴  
            stage('Loading pipeline utils') {
                bat "${env.GET_LATEST_PIPELINE_UTILS} ${pipelineUtilsVersion}"
                utils = load "${env.PIPELINE_UTILS_FILE}"            
            }

            // ╔═╗┬  ┌─┐┌─┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┬┐  ┌─┐┬ ┬┌─┐┌┐┌┌─┐┌─┐  ┌┬┐┬ ┬┬┌─┐  ┌─┐┌┬┐┌─┐┌─┐
            // ╠═╝│  ├┤ ├─┤└─┐├┤    │││ ││││ │   │  ├─┤├─┤││││ ┬├┤    │ ├─┤│└─┐  └─┐ │ ├┤ ├─┘
            // ╩  ┴─┘└─┘┴ ┴└─┘└─┘  ─┴┘└─┘┘└┘ ┴   └─┘┴ ┴┴ ┴┘└┘└─┘└─┘   ┴ ┴ ┴┴└─┘  └─┘ ┴ └─┘┴  
            stage('Executing pre-actions') {
                utils.preActions(wsDir)
            }

            //In this step you should use nuget to download all dependencies of your Solution.
            //For the build process to work correctly, all the dependencies should be handled by Nuget.
            stage('Downloading dependencies') {
                bat 'nuget restore BID.PSF.sln'
            }

            // ╔═╗┬  ┌─┐┌─┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┬┐  ┌─┐┬ ┬┌─┐┌┐┌┌─┐┌─┐  ┌┬┐┬ ┬┬┌─┐  ┌─┐┌┬┐┌─┐┌─┐
            // ╠═╝│  ├┤ ├─┤└─┐├┤    │││ ││││ │   │  ├─┤├─┤││││ ┬├┤    │ ├─┤│└─┐  └─┐ │ ├┤ ├─┘
            // ╩  ┴─┘└─┘┴ ┴└─┘└─┘  ─┴┘└─┘┘└┘ ┴   └─┘┴ ┴┴ ┴┘└┘└─┘└─┘   ┴ ┴ ┴┴└─┘  └─┘ ┴ └─┘┴  
            stage('Preparing SonarQube to receive data') {
                utils.prepareSonarQube(projectKey, projectName)
            }

            //The building steps for your project should be listed in here. Depending on the application more then one steps are necessary.
            stage('Building and compiling') {
                bat 'MSBuild BID.PSF.sln'
            }

            // ╔═╗┬  ┌─┐┌─┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┬┐  ┌─┐┬ ┬┌─┐┌┐┌┌─┐┌─┐  ┌┬┐┬ ┬┬┌─┐  ┌─┐┌┬┐┌─┐┌─┐
            // ╠═╝│  ├┤ ├─┤└─┐├┤    │││ ││││ │   │  ├─┤├─┤││││ ┬├┤    │ ├─┤│└─┐  └─┐ │ ├┤ ├─┘
            // ╩  ┴─┘└─┘┴ ┴└─┘└─┘  ─┴┘└─┘┘└┘ ┴   └─┘┴ ┴┴ ┴┘└┘└─┘└─┘   ┴ ┴ ┴┴└─┘  └─┘ ┴ └─┘┴  
            stage('Sending results to SonarQube') {
                utils.sendResultsToSonarQube()
            }

            // ╔═╗┬  ┌─┐┌─┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┬┐  ┌─┐┬ ┬┌─┐┌┐┌┌─┐┌─┐  ┌┬┐┬ ┬┬┌─┐  ┌─┐┌┬┐┌─┐┌─┐
            // ╠═╝│  ├┤ ├─┤└─┐├┤    │││ ││││ │   │  ├─┤├─┤││││ ┬├┤    │ ├─┤│└─┐  └─┐ │ ├┤ ├─┘
            // ╩  ┴─┘└─┘┴ ┴└─┘└─┘  ─┴┘└─┘┘└┘ ┴   └─┘┴ ┴┴ ┴┘└┘└─┘└─┘   ┴ ┴ ┴┴└─┘  └─┘ ┴ └─┘┴  
            stage('Scanning Frontend project') {
                utils.scanJavaScriptAndTypeScript(projectFEKey, projectFEName, wsDir)
            }

            // ╔═╗┬  ┌─┐┌─┐┌─┐┌─┐  ┌┬┐┌─┐┌┐┌┌┬┐  ┌─┐┬ ┬┌─┐┌┐┌┌─┐┌─┐  ┌┬┐┬ ┬┬┌─┐  ┌─┐┌┬┐┌─┐┌─┐
            // ╠═╝│  ├┤ ├─┤└─┐├┤    │││ ││││ │   │  ├─┤├─┤││││ ┬├┤    │ ├─┤│└─┐  └─┐ │ ├┤ ├─┘
            // ╩  ┴─┘└─┘┴ ┴└─┘└─┘  ─┴┘└─┘┘└┘ ┴   └─┘┴ ┴┴ ┴┘└┘└─┘└─┘   ┴ ┴ ┴┴└─┘  └─┘ ┴ └─┘┴  
            stage('Executing post-actions') {
                utils.postActions(projectKey, projectName)
            }
        }
        catch (caughtError) {
            err = caughtError
            currentBuild.result = "FAILURE"
        }
        finally {
            stage('Cleanning workspace') {
                cleanWs()
                deleteDir()
                utils.notifyBuildStatus(microsoftTeamsWebhooks)
            }
            if (err) {
                throw err
            }
        }
    }
}